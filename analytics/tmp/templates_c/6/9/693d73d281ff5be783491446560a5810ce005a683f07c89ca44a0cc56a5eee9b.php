<?php

/* @VisitFrequency/getSparklines.twig */
class __TwigTemplate_693d73d281ff5be783491446560a5810ce005a683f07c89ca44a0cc56a5eee9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("@VisitFrequency/_sparklines.twig", "@VisitFrequency/getSparklines.twig", 1)->display($context);
    }

    public function getTemplateName()
    {
        return "@VisitFrequency/getSparklines.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
