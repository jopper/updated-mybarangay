<?php

class Adcampaigns extends 
\Phalcon\Mvc\Model 
{
	public $id;
	public $created;
	public $modified;
	public $member_id;
    public $business_id;
    public $content;

	public function initialize() 
    {
        $this->belongsTo('business_id', 'Business', 'id');
		$this->belongsTo('member_id', 'Members', 'id'); 
	}

	public function columnMap()
    {
        return array(
            'id' => 'id',
            'created' => 'created',
            'modified' => 'modified',
            'member_id' => 'member_id',
            'business_id' => 'business_id',
            'content' => 'content'
        );
    }
}  

