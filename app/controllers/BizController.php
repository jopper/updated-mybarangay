<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Google\Client;
use Google\Service\Plus;
use Google\Http\Request;
use Google\IO\Curl;
use \Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;

class BizController extends ControllerBase
{

  public function initialize()
  {
    parent::initialize();
  }

  public function delete_leaderboardAction($id = null) {
    //$this->view->disable();
    $photos = ImageAds::findFirst($id);
    $userSession = $this->session->get('userSession');
    echo $photos->file_path . $photos->file_name;
      if(!$photos || $userSession['id'] != $photos->member_id) {
        $this->session>set('delete_leaderboard_failed', 1);
        return $this->response->redirect('biz/manage_adu/'.$userSession['id']);
      } else {
        if(unlink($photos->file_path.$photos->file_name)) {
          $this->session->set('delete_leaderboard_success', 1);
          $photos->delete();
          //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo has been deleted.');
          return $this->response->redirect('biz/manage_adu/'.$userSession['id']);
        }
      }
  }

  public function signupAction($business_id = null)
    {
    $this->view->setTemplateAfter('oneheader_default');
    $userSession = $this->session->get('userSession');

    if ($business_id == null) {
      // do nothing
    } else {
      $this->view->setVar('business_id' , $business_id);
    }

    if ($userSession['type'] == 'Business') {
      return $this->response->redirect('biz/page/'. $userSession['id']);
    } else if ($userSession['type'] == 'Member') {
        $this->session->destroy();
          $date_of_expiry = time() - 60 ;
          setcookie( "mid", "anonymous", $date_of_expiry, "/");
          setcookie( "e", "anonymous", $date_of_expiry, "/");
          setcookie( "token", "anonymous", $date_of_expiry, "/");
          $this->view->setVar('userSession', '');
    }

      if ($this->request->isPost()) {
      $error = 0;
      // if($this->security->checkToken() == false){
      //  $error = 1;
      //  $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Invalid CSRF Token');
      //  return $this->response->redirect('signup');
      // }
      $firstName = $this->request->getPost('first_name');
      $lastName = $this->request->getPost('last_name');
      $email = $this->request->getPost('email'); 
      $password = $this->request->getPost('password');
      $confirmPassword = $this->request->getPost('confirm_password');
      $business_id = $this->request->getPost('biz_id');
      
      $oldInputs = array();
      $oldInputs['first_name'] = $this->request->getPost('first_name');
      $oldInputs['last_name'] = $this->request->getPost('last_name');
      $oldInputs['email'] = $this->request->getPost('email'); 
      $oldInputs['password'] = $this->request->getPost('password');
      $oldInputs['confirm_password'] = $this->request->getPost('confirm_password');

      if(empty($firstName) || empty($lastName)  || empty($email)  || empty($password)  || empty($confirmPassword)){ 
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>All fields required');
        return $this->response->redirect(); 
      }

      if($password != $confirmPassword){ 
        $errorMsg = "Confirm password does not match"; 
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);        
        return $this->response->redirect();
      }


      if(!empty($email) && Members::findFirstByEmail($email)){
        //$errorMsg = "Email address is already in use here or as a MyBarangay Member. Please use another email address.";
        $this->session->set("oldInputs", $oldInputs);
        $this->session->set("signup-biz", "Email address is already in use here or as a MyBarangay Member. Please use another email address.");        
        //$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);
        return $this->response->redirect('biz/signup');
      }

      $member = new Members();
      $member->created = date('Y-m-d H:i:s');
      $member->modified = date('Y-m-d H:i:s');
      $member->first_name = $firstName;
      $member->last_name = $lastName;
      $member->email = $email;
      $member->type = 'Business';
      $member->password = $this->security->hash($password);
      
      if($member->create()){
        $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
        $emailConfimation = new EmailConfirmations();
        $emailConfimation->created = date('Y-m-d H:i:s');
        $emailConfimation->modified = date('Y-m-d H:i:s');
        $emailConfimation->user_id = $member->id;
        $emailConfimation->email = $email;
        $emailConfimation->token = $activationToken;
        $emailConfimation->business_id = $business_id;
        $emailConfimation->claim_status = '0';
        $emailConfimation->confirmed = 'N';
        if($emailConfimation->save()){
          $this->getDI()->getMail()->send(
                        array($email => $firstName.' '.$lastName),
                        'Please confirm your email',
                        'confirmation',
                        array( 'confirmUrl' => 'biz/emailConfimation/'. $member->id .'/'. $email .'/'. $activationToken)
                    );
        }
        $this->session->set("signup-biz", "You've successfully created a MyBarangay for Business account but you need to verify it. Please check your email to confirm it.");
        //$this->flash->success('<button type="button" cl ass="close" data-dismiss="alert">×</button>You\'ve successfully created a MyBarangay account. We sent a confirmation email to '.$email.'.');
        $this->response->redirect('');
      } else {
        //print_r($user->getMessages());
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Registration failed. Please try again.');
        $this->response->redirect('biz/signup');
      }
      return $this->response->redirect('biz/signup/'.$business_id);
    } 

    }

    public function emailConfimationAction($userId = null, $email =null, $activationToken = null){
      $emaiConfirmed = EmailConfirmations::findFirst(array('columns'    => '*', 
                                 'conditions' => 'user_id = ?1 AND email=?2 AND token = ?3 AND confirmed = ?4', 
                                 'bind' => array(1 => $userId, 2 => $email, 3 => $activationToken, 4 => 'N')));
      if($emaiConfirmed) {
          $emaiConfirmed->confirmed = 'Y';
          $emaiConfirmed->update();
          $this->session->set("loginAsAMember", "Your email was successfully confirmed.");
          //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button><H4>You \'re email has been confirmed.</H4>You\'re now officially part of the <strong>MyBarangay</strong> community. Mabuhay!');
          return $this->response->redirect('biz/claimreq_success/'.$emaiConfirmed->user_id.'/'.$emaiConfirmed->business_id);
      } else {
        return $this->response->redirect('biz/signup');
      }
    }

    public function logoutMemberAction() {
      $this->view->disable();
      $this->session->destroy();
      $date_of_expiry = time() - 60 ;
      setcookie( "mid", "anonymous", $date_of_expiry, "/");
      setcookie( "e", "anonymous", $date_of_expiry, "/");
      setcookie( "token", "anonymous", $date_of_expiry, "/");
      $this->view->setVar('userSession', '');
      //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged out.');
      return $this->response->redirect('biz/login/');
    }




    /**
     * Login user
     * @return \Phalcon\Http\ResponseInterface
     */
    public function loginAction(){


        $this->view->setTemplateAfter('oneheader_default');
        $userSession = $this->session->get('userSession');
        $logs = '';
        if (isset($userSession['type'])) {
            if ($userSession['type'] == 'Member') {
                //$this->session->set("loginAsAMember", "Before you can login with your Business Owner account, you need to logout your member account.");
                $ms = 'before login with your Business Owner account, you need to logout your member account.';
            }
        }
        if (isset($userSession['type'])) {
            if(!empty($userSession['id']) or !$userSession['id'] == '' ){
                return $this->response->redirect('biz/pageu/'. $userSession['id']); 
            }
        }

        $this->view->setVar('logs', $ms);
        if ($this->request->isPost()) {

            // if (isset($userSession['type'])) {
            //     if ($userSession['type'] == 'Member') {
            //     // $this->session->set("loginAsAMember", "This email address is already used by another Member in MyBarangay, Please Register or Login as a Business Owner");
            //     // return $this->response->redirect('biz/login');
            //     echo '<br> This email address is already used by another Member in MyBarangay'; 
            //     }
            // }

            // $this->view->disable();
            $email = $this->request->getPost('email'); // $_POST
            $password = $this->request->getPost('password');
            if(empty($email) || empty($password)){ 
                $this->session->set("message", "all field are required");
                echo '<br> all field are required'; 
                return $this->response->redirect('biz/login/'); 
            }

            $member = Members::findFirstByEmail($email);

 
            if($member == true && $this->security->checkHash($password, $member->password)) {

            if (isset($userSession['type'])) {
                if ($userSession['type'] == 'Member') {
                   $this->session->set("message", "You are still login as a member you need to logout first before you login here.");
                   return $this->response->redirect('biz/login/'); 
                }
            }


                if ($member->type == 'Business') { 

                    $emaiConfirmed = EmailConfirmations::findFirst(array('columns'    => '*', 
                    'conditions' => 'user_id = ?1 AND email=?2 AND confirmed = ?3', 
                    'bind' => array(1 => $member->id, 2 => $email, 3 => 'Y')));

                    if(!$emaiConfirmed) {
                        // $this->session->set("loginAsAMember", "Your email is not yet confirmed. Please check and confirm your email.");
                        // $this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>You\'re email is not yet confirmed.');
                        // return $this->response->redirect('biz/login/');
                         //echo '<br> your email is not'; 
                    }

                    $userSession = get_object_vars($member);
                    $profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
                    $userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
                    $this->session->set('userSession', $userSession);

                    //member id
                    $cookie_name = "mid";
                    $cookie_value = $userSession['id'];
                    $date_of_expiry = time() + 60 * 60 * 24 * 90;
                    setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
                    //email 
                    $cookie_name = "e";
                    $cookie_value = $userSession['email'];
                    setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
                    //cookie token
                    $cookie_name = "token";
                    $cookie_token = substr(md5(uniqid(rand(), true)), 0, 20);
                    setcookie($cookie_name, $this->encrypt($cookie_token), $date_of_expiry, "/"); 
                    $member->modified = date('Y-m-d H:i:s');
                    $member->cookie_token = $this->security->hash($cookie_token);

                    if($member->update()){
                        // $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged in.');
                        $this->response->redirect('biz/page/'.$userSession['id']);
                      $this->view->disable();
                    }
                }else{
                    $this->session->set("message", "Only Business Account can be login here");
                   // echo '<br>Only Business Account can be login here'; 
                }



            }else{
                $this->session->set("message", "Wrong Username/Password");
               // echo '<br> Wrong Username/Password';
            }


            // $this->view->disable();
        }
    
    }
    
    function encrypt($pure_string) {
    $encryption_key = 'sMeynBiaprpainlgiyhP';
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return $encrypted_string;
  }

  public function logoutAction() {
      $this->view->disable();
      $this->session->destroy();
      $date_of_expiry = time() - 60 ;
      setcookie( "mid", "anonymous", $date_of_expiry, "/");
      setcookie( "e", "anonymous", $date_of_expiry, "/");
      setcookie( "token", "anonymous", $date_of_expiry, "/");
      $this->view->setVar('userSession', '');
      $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged out.');
      
      return $this->response->redirect('biz/login');
    }

    public function page1Action($id = null) {
  $this->view->setTemplateAfter('default1');
      $userSession = $this->session->get('userSession');
  if ($userSession['type'] == 'Member') {
    $this->response->redirect('member/page/'. $userSession['id']);
  } 
      $member = Members::findFirstById($id);
      $this->view->setVar('member', $member);

      $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('reviews', $reviews);

      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('claimrequest', $claimRequests);
    }

    public function pageAction($id = null) {

      $this->response->redirect('biz/pageu/'.$id);

  $this->view->setTemplateAfter('default1');
      $userSession = $this->session->get('userSession');
  if ($userSession['type'] == 'Member') {
    $this->response->redirect('member/page/'. $userSession['id']);
  } 
      $member = Members::findFirstById($id);
      $this->view->setVar('member', $member);

      $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('reviews', $reviews);

      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('claimrequest', $claimRequests);
    }




    /**
     * [pageuAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function pageuAction($id = null) {

        $this->view->setTemplateAfter('businessTemplate');
        $this->view->seschecker = $this->sessionChecker($id);

        $member = Members::findFirstById($id);

        if(!$member){
            $this->response->redirect('biz/login');
        }

        $claimRequests = ClaimRequests::findFirst(array('member_id = "'.$id.'"', 'order' => 'id DESC'));

        $this->view->setVars([
        'claimRequest'  => $claimRequests,
        'member'        => $member,
        'navact'        => 'business',
        'userid'        => $id
        ]);

    }






    public function business_searchAction() {
      $searchWords = '';
        $business = array();
        if(isset($_GET["page"])){
            $currentPage = (int) $_GET["page"];
        } else {
            $currentPage = 1;
        }
        if ($this->request->isPost()) {
            $businessName = $this->request->getPost('name');
            $businessAddress = $this->request->getPost('address');
            $businessCategoryId = $this->request->getPost('business_category_id');

            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'country LIKE :country:', 
                                             'bind' => array('country' => $businessAddress)));
          
            $countryId = '';
            if($country) {
                $countryId = $country->id;
            } 

            $businessCategoryLists = BusinessCategoryLists::find(array('columns'    => '*', 
                                             'conditions' => 'business_category_id = :business_category_id:',
                                             'bind' => array('business_category_id' => $businessCategoryId)));
            $conditions = '';
            if(!empty($businessCategoryLists)) {
                foreach ($businessCategoryLists as $key => $businessCategoryList) {
                    $conditions .= ' OR id = :'.$key.':';
                    $bind[$key] = $businessCategoryList->business_id;
                }//$searchWords .= ', '.$businessName;
            }

            if(!empty($businessName)) {
                $conditions .= ' OR name LIKE :name:';
                $bind['name'] = '%'.$businessName.'%';
                $searchWords .= ', '.$businessName;
            }

            if(!empty($businessAddress)) {
                $conditions .= ' OR street LIKE :street: OR city LIKE :city:';
                $bind['street'] = '%'.$businessAddress.'%';
                $bind['city'] = '%'.$businessAddress.'%';
                $searchWords .= ', '.$businessAddress;
            }

            if(!empty($countryId)) {
                $conditions .= ' OR country_id = :country_id:';
                $bind['country_id'] = $countryId;
                $searchWords .= ', '.$country->country;
            }

            $searchWords = substr($searchWords, 2); 
            $business = Business::find(array('columns'    => '*', 
                                     'conditions' => substr($conditions, 3), 
                                     'bind' => $bind
                                        ));

            $this->view->setVar('business', $business);
        } else {
            $business = Business::find(array('order'    => 'id DESC'));
        }

        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $business,
                "limit"=> 12,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        $this->view->setVar('business', $page);
        //$this->view->setVar('business', $business);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);
    }

    public function claimAction($business_id = null) {

      $userSession = $this->session->get("userSession");

      $type = $userSession['type'];
      $id   = $userSession['id'];
      $business = $business_id;
      if ($type != 'Business') {
        //$this->session->set("claimAsAUser", "Basic User account is already logged-in, Please Log it out and Register or Log-in as a Business Owner.");
        $this->session->set("proceed", $business_id);
        if ($type == null) {
            $g = '0';
        } else {
            $g = '1';
        }
        $this->session->set("guestoruser", $g);
        return $this->response->redirect($this->request->getHTTPReferer());
      } 
      //else {
        // return $this->response->redirect('biz/claimreq_success/'.$userSession['id'].'/'.$business_id);
      //}
    }

    public function respondAction($review_id = null, $business_id = null) {
      $userSession = $this->session->get("userSession");

      if ($this->request->isPost()) {
        $biz_id = $this->request->getPost("business_id");
        $respond = new ReviewResponds();
        $respond->created = date('Y-m-d H:i:s');
        $respond->modified = date('Y-m-d H:i:s');
        $respond->member_id = $userSession['id'];
        $respond->review_id = $this->request->getPost("review_id");
        $respond->business_id = $this->request->getPost("business_id");
        $respond->content = $this->request->getPost("content");

        if ($respond->create()) {
          return $this->response->redirect('business/view2/'.$biz_id);
        } else {
          $this->view->disable();
          print_r($respond->getMessages());
        }
      }

        $business = Business::findFirstById($business_id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $this->view->setVar('business', $business);
        $reviews = Reviews::findById($review_id);
        if (!$reviews) {
          $this->view->disable();
          echo "failed";
        }
        $this->view->setVar('reviews', $reviews);


    }
  
  public function claimreq_successAction($userId = null, $businessId = null) {
      //$this->view->setTemplateAfter('default1');
      $this->view->setTemplateAfter('oneheader_default');
      $business = Business::findFirst("id='".$businessId."'");
      $this->view->setVars([
        'userId' => $userId, 
        'businessId' => $businessId,
        'business' => $business
      ]);
  }    

  public function checkclaimAction($businessId = null) {
  if ($this->request->isPost()) {
    $referer = $this->request->getHTTPReferer();
    $firstNum = $this->request->getPost('biz_num');
    $alternateNum = $this->request->getPost('alt_num');
    $userId = $this->request->getPost('user_id');
    $businessId = $this->request->getPost("biz_id");
    if ($firstNum == '' && $alternateNum == '') {
    $this->session->set('failedtoconfirm', 1);
    return $this->response->redirect($referer);
    } else if ($firstNum == '' && $alternateNum != ''){
        $claimRequest = new ClaimRequests();
        $claimRequest->created = date('Y-m-d H:i:s');
        $claimRequest->modified = date('Y-m-d H:i:s');
        $claimRequest->member_id = $userId;
        $claimRequest->business_id = $businessId;
        $claimRequest->status = 'pending'; // default status
    $businessCredential = Business::findFirst("id='".$businessId."'");
    $businessAddress = $businessCredential->street .", ". $businessCredential->city.", ".$businessCredential->state.", ".$businessCredential->country_code_id;
    $userCredential = Members::findFirst("id=".$userId);
    if ($claimRequest->create()) {

    $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business Claim has been sent.');
    $email = 'info@mybarangay.com';
    $getBusinessName = Business::findFirst('id='.$businessId);
    $businessName = $getBusinessName->name;
    $this->getDI()->getMail()->send(
                        array($email => ''),
                        'Check on Business Establishment Claim',
                        'claim_req',
                        array( 'first_name' => $userCredential->first_name, 'last_name' => $userCredential->last_name, 'email'=> $userCredential->email, 'address' => $businessAddress, 'business_name' => $businessName, "telephone" =>$alternateNum)
                );
      //return $this->response->redirect('business/view/'. $business_id);
      $this->session->set("claimingsuccess", 1);
          return $this->response->redirect('business/view2/'.$businessId);
        } else {
          $this->view->disable();
          print_r($claimRequest->getMessages());
        }
    //$this->session->set("claimingsuccess", 1);
    return $this->response->redirect('business/view2/'.$businessId);
    } else if ($firstNum != '' && $alternateNum == ''){
    $claimRequest = new ClaimRequests();
        $claimRequest->created = date('Y-m-d H:i:s');
        $claimRequest->modified = date('Y-m-d H:i:s');
        $claimRequest->member_id = $userId;
        $claimRequest->business_id = $businessId;
        $claimRequest->status = 'pending'; // default status
    $businessCredential = Business::findFirst("id='".$businessId."'");
    $businessAddress = $businessCredential->street .", ". $businessCredential->city.", ".$businessCredential->state.", ".$businessCredential->country_code_id;
    $userCredential = Members::findFirst("id=".$userId);
        if ($claimRequest->create()) {
    $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business Claim has been sent.');
    $email = 'info@mybarangay.com';
    $getBusinessName = Business::findFirst('id='.$businessId);
    $businessName = $getBusinessName->name;
    $this->getDI()->getMail()->send(
                        array($email => ''),
                        'Check on Business Establishment Claim',
                        'claim_req',
                        array( 'first_name' => $userCredential->first_name, 'last_name' => $userCredential->last_name, 'email'=> $userCredential->email, 'address' => $businessAddress, 'business_name' => $businessName, "telephone" =>$firstNum)
                );
      //return $this->response->redirect('business/view/'. $business_id);
      $this->session->set("claimingsuccess", 1);
          return $this->response->redirect('business/view2/'.$businessId);
        } else {
          $this->view->disable();
          print_r($claimRequest->getMessages());
        }
    return $this->response->redirect('business/view2/'.$biz_id);
    
    } else {
    $claimRequest = new ClaimRequests();
        $claimRequest->created = date('Y-m-d H:i:s');
        $claimRequest->modified = date('Y-m-d H:i:s');
        $claimRequest->member_id = $userId;
        $claimRequest->business_id = $businessId;
        $claimRequest->status = 'pending'; // default status
    $businessCredential = Business::findFirst("id='".$businessId."'");
    $businessAddress = $businessCredential->street .", ". $businessCredential->city.", ".$businessCredential->state.", ".$businessCredential->country_code_id;
    $userCredential = Members::findFirst("id=".$userId);
        if ($claimRequest->create()) {
    $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business Claim has been sent.');
    $email = 'info@mybarangay.com';
    $getBusinessName = Business::findFirst('id='.$businessId);
    $businessName = $getBusinessName->name;
    $this->getDI()->getMail()->send(
                        array($email => ''),
                        'Check on Business Establishment Claim',
                        'claim_req',
                        array( 'first_name' => $userCredential->first_name, 'last_name' => $userCredential->last_name, 'email'=> $userCredential->email, 'address' => $businessAddress, 'business_name' => $businessName, "telephone" =>$alternateNum)
                );
      //return $this->response->redirect('business/view/'. $business_id);
      $this->session->set("claimingsuccess", 1);
          return $this->response->redirect('business/view2/'.$businessId);
        } else {
          $this->view->disable();
          print_r($claimRequest->getMessages());
        }
    //$this->session->set("claimingsuccess", 1);
    return $this->response->redirect('business/view2/'.$businessId);
    }
  }
  }
  
    public function update_businessAction($businessId = null)
    {
        $business = Business::findFirst($businessId);
        if(!$business) {
            return $this->response->redirect('biz/business_search');
        }
        if ($this->request->isPost()) {
            $countryId = $this->request->getPost('country_id');
            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'id LIKE :id:', 
                                             'bind' => array('id' => $countryId)));
            $countryName = '';
            if($country) {
                $countryName = $country->country;
            } 

            $address = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            $userSession = $this->session->get("userSession");
            $content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            $json = json_decode($content, true);
            $lat = $json['results'][0]['geometry']['location']['lat'];
            $lng = $json['results'][0]['geometry']['location']['lng'];
            $business->modified = date('Y-m-d H:i:s'); 
            $business->member_id =  $userSession['id'];
            $business->name = $this->request->getPost('name');
            $business->website = $this->request->getPost('website');
            $business->telephone = $this->request->getPost('telephone');
            $business->street = $this->request->getPost('street');
            $business->city = $this->request->getPost('city');
            $business->country_id = $this->request->getPost('country_id');
            $business->lat = $lat;
            $business->lng = $lng;
            $opened = '';
            if(!empty($this->request->getPost('opened'))) { $opened = 'Opened'; };
            if(empty($this->request->getPost('opened'))) { $opened = 'Opening Soon'; };
            $business->opened = $opened;
            if($business->update()){
                if(!empty($this->request->getPost('business_category_ids'))) {
                    $bCtegories = $this->request->getPost('business_category_ids');
                    $bCtegoryIds = explode(',', $bCtegories);
                    BusinessCategoryLists::find('business_id="'.$businessId.'"')->delete();
                    foreach ($bCtegoryIds as $key => $bCtegoryId) {
                        $businessCategoryLists = new BusinessCategoryLists();
                        $businessCategoryLists->created = date('Y-m-d H:i:s');
                        $businessCategoryLists->business_id = $businessId;
                        $businessCategoryLists->business_category_id = $bCtegoryId;
                        $businessCategoryLists->create();
                    }
                }

                $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Business has been updated');
               
                return $this->response->redirect('business/view2/'.$businessId);
            }
        }
        $this->view->setVar('business', $business);
        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $businessCategoryLists = BusinessCategoryLists::find('business_id="'.$businessId.'"');
        $this->view->setVar('businessCategoryLists', $businessCategoryLists);
    }

    public function add_photoAction($id = null) {
  $this->view->setTemplateAfter('default1');
        $userSession = $this->session->get('userSession');
        $id = $userSession['id'];
        $member = Members::findFirst($id);
        
        if(!$member) {
            $this->response->redirect('biz/page/'.$userSession['id']);
        }

        $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));

        $this->view->setVars([
            'photos' => $photos, 
            'member' => $member
        ]);

        // POST 
        if($this->request->isPost() && $this->request->hasFiles() == true){
            //ini_set('upload_max_filesize', '64M');
            set_time_limit(1200);
            $uploads = $this->request->getUploadedFiles();

            $isUploaded = false;
            #do a loop to handle each file individually
            foreach($uploads as $upload){
                #define a “unique” name and a path to where our file must go
                $fileName = $upload->getname();
                $fileInfo = new SplFileInfo($fileName);
                $fileExt = $fileInfo->getExtension();
                $fileExt = strtolower($fileExt);
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
                //$fileExt = $upload->getExtension();
                $fileImageExt = array('jpeg', 'jpg', 'png');
                //error_log("File Extension :".$fileExt, 0);
                $fileType = '';
                $filePath = '';
                $path = '';
                //$path = ''.$newFileName;
                if(in_array($fileExt, $fileImageExt)){
                    $path = 'img/member/'.$newFileName; // img/business_member ?
                    $filePath = 'img/member/'; // img/business_member ?
                    //$fileType = 'Image';
                }
                #move the file and simultaneously check if everything was ok
                ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
            }

            #if any file couldn't be moved, then throw an message
            if($isUploaded) {
                $memberPhotos = new MemberPhotos();
                $memberPhotos->created = date('Y-m-d H:i:s'); 
                $memberPhotos->modified = date('Y-m-d H:i:s'); 
                $memberPhotos->member_id =  $userSession['id'];
                $memberPhotos->type = '1';
                $memberPhotos->file_path =  $filePath;
                $memberPhotos->filename =  $newFileName;
                $memberPhotos->caption = $this->request->getPost('caption');
                if(count($photos) > 0) {
                    $memberPhotos->primary_pic = 'No';
                } else {
                    $memberPhotos->primary_pic = 'Yes';
                    $userSession['primary_pic'] = $filePath.$newFileName;
                    $this->session->set('userSession', $userSession);
                }
                
                if($memberPhotos->save()){
                    return $this->response->redirect('biz/add_photo/'.$id);
                } else {
                    // if member photos creation is failed. 
                    $this->view->disable();
                    print_r($memberPhotos->getMessages());
                }
            }
        }

    }

    public function delete_photoAction($id = null) {
        
    }
    

public function biz_claimsearchAction() {
  //$this->view->setTemplateAfter('default1');
    $this->view->setTemplateAfter('oneheader_default');
    if ($this->request->isPost()) {
      $name = $this->request->getPost('name');
      $location = $this->request->getPost('location');
      
      if ($name == '' && $location == '' ) {
        return $this->response->redirect('biz/biz_claimsearch/');
      } else if ($name != '' && $location == '') {
    return $this->response->redirect('biz/biz_claimresult/'.$name .'/xxx');
      } else if ($name == '' && $location != '') {
    return $this->response->redirect('biz/biz_claimresult/xxx/'. $location);
      }
    }
    }

    public function biz_claimresultAction($name = null, $location = null) {
  $this->view->setTemplateAfter('default1');
        $searchWords = '';
        $business = array();

        if (isset($_GET["page"])) {
          $currentPage = (int) $_GET["page"];
        } else {
          $currentPage = 1;
        }

        $conditions = '';
        if ($name == null && $location == null) {
          return $this->response->redirect('biz/biz_claimsearch/');
        } else {
          if ($name != 'xxx') {
            $conditions .= ' OR name LIKE :name:';
            $bind['name'] = '%'.$name.'%';
            $searchWords .= ', '.$name;
          }

          if ($location != 'xxx') {
             $conditions .= ' OR street LIKE :street: OR city LIKE :city:';
                $bind['street'] = '%'.$location.'%';
                $bind['city'] = '%'.$location.'%';
                $searchWords .= ', '.$location;
          }

          $searchWords = substr($searchWords, 2);
          $business = Business::find(array(
            'columns' => '*',
            'conditions' => substr($conditions, 3),
            'bind' => $bind
          ));

          //$this->view->setVar('business', $business);
        }

         $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $business,
                "limit"=> 4,
                "page" => $currentPage
            )
          );
        $page = $paginator->getPaginate();
  $this->view->setVar('business', $page);
    }

    public function set_primary_photoAction($id = null) {
        $this->view->disable();
        $photos = MemberPhotos::findFirst($id);
        $userSession = $this->session->get('userSession');
        if(!$photos || $userSession['id'] != $photos->member_id) {
            return $this->response->redirect('biz/add_photo/'.$userSession['id']);
        } else { 
            $currentPhotos = MemberPhotos::find('member_id = "'.$userSession['id'].'"');
            foreach ($currentPhotos as $key => $currentPhoto) {
                $currentPhoto->primary_pic = 'No';
                if (!$currentPhoto->update()) {
                    $this->view->disable();
                    echo "failed to update current photo.";
                }
               
            }

            $photos->modified = date('Y-m-d H:i:s');
            $photos->primary_pic = 'Yes';
            if($photos->update()) {
              $userSession['primary_pic'] = $photos->file_path.$photos->filename;
        $this->session->set('userSession', $userSession);
                $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo has been set as primary.');
                return $this->response->redirect('biz/add_photo/'.$userSession['id']);
            } else {
                $this->view->disable();
                echo "failed to modified data. ";
            }
        }
    }

    public function update_photo_captionAction($id = null) {

    }

    public function manage_adAction($id = null) {
      $this->view->setTemplateAfter('default1');
      $member  = Members::findFirst("id=".$id);
      $check_subscription = MemberSubscriptions::count("member_id='".$id."'");
      if ($check_subscription > 0) {
        $subscription = 1;
      } else {
        $subscription = 0;
      }
      $this->view->setVar('subscription', $subscription);
      $this->view->setVar('member', $member);
        $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $this->view->setVar('claimRequests', $claimRequests);     
      $imageAds = ImageAds::find("member_id=".$id);
      $this->view->setVar('imageAds', $imageAds);
  }

    public function add_adAction($id = null, $business_id = null) {
  if ($id == null || $business_id = null) {
    return $this->response->redirect('biz/manage_ad');
  } 
    } 
    public function homepage_banner2Action($id = null) {
  if ($id == null) {
    return $this->response->redirect('biz/login');
  }
  
  $member  = Members::findFirst("id=".$id);
  $this->view->setVar('member', $member);
    }
    public function homepage_bannerAction($id = null) {
  if ($id == null) {
    return $this->response->redirect('biz/login');
  }
  
  $member  = Members::findFirst("id=".$id);
  $this->view->setVar('member', $member);

    }

    public function subpage_bannerAction($id = null) {
  if ($id == null) {
    return $this->response->redirect('biz/login');
  }

  $member = Members::findFirst("id=".$id);
  $this->view->setVar('member', $member);
    }

    public function crop_subpagebannerAction() {
  if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new ImageAds();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $this->request->getPost('business');
                    $businessImages->file_path =  $filePath;
                    $businessImages->file_name =  $newFileName;
                    $businessImages->type = $this->request->getPost('type');
        $businessImages->content = $this->request->getPost('content');
                    if($businessImages->create()){
                        return $this->response->redirect('business/view2/'.$this->request->getPost('business'));
                    } else {
                        $this->view->disable();
                        print_r($businessImages->getMessages());
                    }
            } else {
    $this->view->disable();
    echo $file;
      }

        }
    
  return $this->response->redirect('business/view2/'. $this->request->getPost('business'));
    }
public function crop_homepagebanner2Action() {
if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/ad/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new ImageAds();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $this->request->getPost('business');
                    $businessImages->file_path =  $filePath;
                    $businessImages->file_name =  $newFileName;
                    $businessImages->type = $this->request->getPost('type');
                    $businessImages->side_text = $this->request->getPost('side_text');
                    $businessImages->bground_color = $this->request->getPost('bcolor');
                    $businessImages->status = '1';

                    if($businessImages->create()){
                        return $this->response->redirect('business/view2/'.$this->request->getPost('business'));
                    } else {
                        $this->view->disable();
                        print_r($businessImages->getMessages());
                    }
            } else {
    $this->view->disable();
    echo $file;
      }

        }
    
  return $this->response->redirect('business/view2/'. $this->request->getPost('business'));
    }

public function crop_homepagebannerAction() {
if ($this->request->isPost()) {
            $userSession = $this->session->get("userSession");
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/ad/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new ImageAds();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $this->request->getPost('business');
                    $businessImages->file_path =  $filePath;
                    $businessImages->file_name =  $newFileName;
                    $businessImages->type = $this->request->getPost('type');
                    $businessImages->status = '1';

                    if($businessImages->create()){
                        $this->session->set('upload_ad', 1);
                        return $this->response->redirect('biz/manage_ad/'.$userSession['id']);
                        //return $this->response->redirect('business/view2/'.$this->request->getPost('business'));
                    } else {
                        $this->view->disable();
                        print_r($businessImages->getMessages());
                    }
            } else {
    $this->view->disable();
    echo $file;
      }

        }
    
  return $this->response->redirect('business/view2/'. $this->request->getPost('business'));
    }

    // MOBILE BANNER AD
    public function mobile_bannerAction($id = null) {
      if ($id == null) {
      return $this->response->redirect('biz/login');
    }
    
    $member  = Members::findFirst("id=".$id);
    $this->view->setVar('member', $member);
    }

    // WEBSITE BANNER AD 
    public function website_bannerAction($id = null) {
      if ($id == null) {
      return $this->response->redirect('biz/login');
    }
    
    $member  = Members::findFirst("id=".$id);
    $this->view->setVar('member', $member);
    }



    public function adcampaignAction($id = null, $business_id = null) {
      $member  = Members::findFirst("id='".$id."'");
      $this->view->setVar('member', $member);
      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('claimRequests', $claimRequests); 

      if ($this->request->isPost()) {
        $businessId = $this->request->getPost('business');
        $business = Business::findFirst("id='".$businessId."'");
        $this->view->setVars([
          'business' => $business,
          'postContent'  => $this->request->getPost('content')
        ]);
      }
    }

    public function setcampaignadAction() {
        if($this->request->isAjax()) 
        {
            $memberId = $this->request->getQuery('member_id');
            $businessId = $this->request->getQuery('business_id');
            $content = $this->request->getQuery('content');

            $arrFlag = array();

            $check_biz_ad = Adcampaigns::count("business_id='".$businessId."'");
            if ($check_biz_ad > 0) {
              $updateCampaign = Adcampaigns::findFirst("business_id='".$businessId."'");
              $updateCampaign->modified = date('Y-m-d H:i:s');
              $updateCampaign->member_id = $memberId;
              $updateCampaign->content = $content;

              if ($updateCampaign->update()) {
                $result = "Updating Ad Campaign Success";
              } else {
                $result = "Updating Ad Campaign Failed";
              }
            } else {
              $newAdCampaign = new Adcampaigns();
              $newAdCampaign->created = date('Y-m-d H:i:s'); 
              $newAdCampaign->modified = date('Y-m-d H:i:s'); 
              $newAdCampaign->business_id = $businessId;
              $newAdCampaign->member_id = $memberId;
              $newAdCampaign->content = $content;

              if ($newAdCampaign->create()) {
                $result = 'Adding new Ad Campaign Success';
              } else {
                $result = 'Adding new Ad Campaign Failed';
              }
            }
            $arrFlag[] = ["count_result" => $countResult, "status_result" => $result];

            $payload     = $arrFlag; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;

        }
    }



    public function howtoclaimAction() {
      
    }


    /**
     * @param  [type]
     * @return [type]
     */
    public function manage_aduAction($id) {
        $this->view->setTemplateAfter('businessTemplate');
        $this->view->seschecker = $this->sessionChecker($id);

      // if($id == null or $id == ''){
      //    $userSession = $this->session->get('userSession');
      //    if($userSession){
      //       $id = $userSession['id'];
      //       return $this->response->redirect('biz/manage_adu/'.$id);
      //    }else{
      //       return $this->response->redirect('biz/login/');
      //    }
      // }

        $member  = Members::findFirst($id);
        if(!$member){
            return $this->response->redirect('biz/pageu');
        }

        $check_subscription = MemberSubscriptions::count("member_id='".$id."'");
        if ($check_subscription) {
            $subscription = 1;
        } else {
            $subscription = 0;
        }

        $claimRequests = ClaimRequests::findFirst(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $imageAds = ImageAds::find("member_id=".$id);
     

        $this->view->setVars([
            'imageAds'      => $imageAds,
            'userid'        => $id,
            'navact'        => 'ads',
            'subscription'  => $subscription,
            'member'        => $member,
            'claimRequests' => $claimRequests
        ]);

    }

    /**
     * @param  [type]
     * @return [type]
     */
    public function statisticAction($id = nul){
        $this->view->setTemplateAfter('businessTemplate');
        
        
        $last = $this->request->getQuery('last');
        if($last == ''){
            $last = 'last7';
        }else{
            if($last == 'last7'){
            }elseif($last == 'last30'){
            }elseif($last == 'last90'){
            }else{
                $last = 'last7';
            }
        }

        $this->view->seschecker = $this->sessionChecker($id);

        $check_subscription = MemberSubscriptions::count("member_id='".$id."'");
        if ($check_subscription > 0) {
            $subscription = 1;
        } else {
            $subscription = 0;
        }

        $claimRequests = ClaimRequests::findFirst(array('member_id = "'.$id.'"', 'order' => 'id DESC'));

        // if(!$claimRequests){
        //     return $this->response->redirect('biz/pageu');
        // }
        $this->view->claimRequests = $claimRequests;
        $this->view->setVars([
            'navact'        => 'statistic',
            'userid'        => $id,
            'busid'         => $claimRequests->business_id,
            'last'          => $last,
            'subscription'  => $subscription
        ]);

    }


    /**
     * [manage_ad_cropAction description]
     * @param  integer $type [description]
     * @return [type]        [description]
     */
    public function manage_ad_cropAction($type = 1) {
        $this->view->setTemplateAfter('businessTemplate');
         $userSession = $this->session->get('userSession');
         if($userSession){
            $id = $this->view->userSession['id'];
            $this->view->seschecker = $this->sessionChecker($id);
        }else{
            return $this->response->redirect('biz/logout');
        }



        $claimRequests = ClaimRequests::findFirst(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $this->view->setVar('claimRequests', $claimRequests);     

        $this->view->setVars([
            'userid'       => $id,
            'preview'   => $type,
            'navact'    => 'ads'
            ]);

    }

    public function downloadManageCropAction() {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
      
        if($this->request->isAjax()) {

                $message = 'null';
                
                $userSession = $this->session->get("userSession");


                $busids = $this->request->getPost('busids');
                $type = $this->request->getPost('type');
                $encoded = $this->request->getPost('imagelink');
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'ADL.png';
                $filePath = 'img/ad/leaderboard/';

                $businessImages = new ImageAds();
                $businessImages->created        = date('Y-m-d H:i:s'); 
                $businessImages->modified       = date('Y-m-d H:i:s'); 
                $businessImages->member_id      =  $userSession['id'];
                $businessImages->business_id    =  $busids;
                $businessImages->file_path      =  $filePath;
                $businessImages->file_name      =  $newFileName;
                $businessImages->type           = $type;
                $businessImages->status         = '1';

                if($businessImages->create()){

                    $file = $filePath . $newFileName;
                    if (file_put_contents($file, file_get_contents($encoded))) {
                        
                        $message = 'success';

                    }else{
                        $message = 'Problem uploading leaderboard';
                    }
                    
                } else {
                    $errno = '';
                    foreach ($businessImages->getMessages() as $err) {
                        echo $errno .= $err->getMessage(). '</br>';
                    }
                    echo $errno;
                    $this->view->disable();
                }

            $output = json_encode(array('type'=>'ok', 'message' => $message));
            die($output); //exit script outputting json data

        }

    }


    /**
     * [businessChecker description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function businessChecker($id = null){
        $claimRequests = ClaimRequests::find([
            'conditions'  =>  'member_id = :id:',
            'bind'        =>  array('id' => $id),
            'order'       =>  'id DESC'
        ]);
        if($claimRequests){
            return $claimRequests;
        }else{
            return false;
        }
    }

    /**
     * [sesChecker checker if the session id is not null or empty]
     * @return [type] [description]
     */
    private function sesChecker(){
        $bool = TRUE;
        if(empty($this->view->userSession)){
            if(!isset($this->view->userSession['id']) or !isset($this->view->userSession['type'])){
                $bool = FALSE;
            }
        }
        return $bool;
    }

    /**
     * [idChecker matching the session if true right user]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function idChecker($id = null){
        $bool = TRUE;

        if(!empty($this->view->userSession)){
            if(isset($this->view->userSession['id'])){
                if($this->view->userSession['id'] != $id){
                    $bool = FALSE;
                }
            }
        }
        return $bool;
    }



    public function update_businessValidate() {

        $validation = new Phalcon\Validation();

        $validation->add('name', new PresenceOf(array(
            'message' => 'Business Name field is required'   
        )));

        return $validation->validate($_POST);
    
    }



    public function update_reviewsAction($id = null, $busid = null){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($this->request->isPost()) {

                if(!$this->sesChecker() OR !$this->idChecker($id)){
                   return $this->response->redirect('biz/login');
                }

                $userSession = $this->session->get("userSession");

                $revoptCheck = ReviewOptions::findFirst([
                    'conditions'    =>  'business_id = :busid: AND member_id = :memid:',
                    'bind'          =>  array('busid' => $busid, 'memid' => $userSession['id'] )
                ]);

                if($revoptCheck){
                    $revopt = ReviewOptions::findFirstById($revoptCheck->id);
                }else{
                    $revopt = new ReviewOptions();
                }

                $revopt->business_id    = $busid;
                $revopt->member_id      = $userSession['id'];
                $revopt->created        = date('Y-m-d H:i:s');
                $revopt->modified       = date('Y-m-d H:i:s');
                $revopt->caters         = $this->request->getPost('reviews_caters');
                $revopt->wifi           = $this->request->getPost('reviews_wifi');
                $revopt->hastv          = $this->request->getPost('reviews_hastv');
                $revopt->pricerange     = $this->request->getPost('reviews_pricerange');
                $revopt->creditcard     = $this->request->getPost('reviews_creditcard');
                $revopt->parking        = $this->request->getPost('reviews_parking');
                $revopt->bike_parking   = $this->request->getPost('reviews_bike_parking');
                $revopt->attire         = $this->request->getPost('reviews_attire');
                $revopt->groups         = $this->request->getPost('reviews_groups');
                $revopt->kids           = $this->request->getPost('reviews_kids');
                $revopt->reservations   = $this->request->getPost('reviews_reservations');
                $revopt->delivery       = $this->request->getPost('reviews_delivery');
                $revopt->takeaway       = $this->request->getPost('reviews_takeaway');
                $revopt->outdoor        = $this->request->getPost('reviews_outdoor');
                $revopt->goodfor        = $this->request->getPost('reviews_goodfor');
                $revopt->noise          = $this->request->getPost('reviews_noise');
                $revopt->waiter         = $this->request->getPost('reviews_waiter');
                $revopt->alcohol        = $this->request->getPost('reviews_alcohol');
                $revopt->ambience       = $this->request->getPost('reviews_ambience');


                if($revoptCheck){
                    if($revopt->update()){
                        $this->session->set('successMess', 'Reviews successfully updated. ');
                    }else{
                        $err = '';
                        foreach ($revopt->getMessages() as $message) {
                            $err .= $message->getMessage() . " <br>";
                        }
                       $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
                    }
                    $this->response->redirect('biz/update_businessu/'.$id);
                }else{
                    if($revopt->create()){
                        $this->session->set('successMess', 'Reviews successfully created. ');
                    }else{
                        $err = '';
                        foreach ($revopt->getMessages() as $message) {
                            $err .= $message->getMessage() . " <br>";
                        }
                       $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
                    }
                    $this->response->redirect('biz/update_businessu/'.$id);
                }


        }else{
            $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again.');
            $this->response->redirect('biz/update_businessu/'.$id);
        }

    }



    /**
     * [update_businessuAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function update_businessuAction($id = null){

        $this->view->setTemplateAfter('businessTemplate');
        if(!$this->sesChecker() OR !$this->idChecker($id)){
           return $this->response->redirect('biz/login');
        }

        $claimRequests = ClaimRequests::findFirst([
            'conditions'  =>  'member_id = :id:',
            'bind'        =>  array('id' => $id),
            'order'       =>  'id DESC'
        ]);
        if(!$claimRequests){
            return $this->response->redirect('biz/login');
        }

        $business = Business::findFirst($claimRequests->business_id);
        if(!$business){
            return $this->response->redirect('biz/login');
        }
        

        $revopt = ReviewOptions::findFirst([
            'conditions'    =>  'business_id = :busid: AND member_id = :memid:',
            'bind'          =>  array('busid' => $claimRequests->business_id, 'memid' => $claimRequests->member_id )
        ]);

 
        if ($this->request->isPost()) {


            $messages = $this->update_businessValidate();
            if (count($messages)) {
                $errorMsg = '';
                foreach ($messages as $msg) {
                  $errorMsg .= $msg . "<br>";       
                }
                $this->session->set('errorMess', $errorMsg);
                return $this->response->redirect('biz/update_businessu/'.$claimRequests->member_id);
            } 


            $countryId = $this->request->getPost('country_id');
            $businessId = $this->request->getPost('businessId');

            $country = Countries::findFirst([
                'columns'    => '*', 
                'conditions' => 'id LIKE :id:', 
                'bind' => array('id' => $countryId)
            ]);

            $countryName = '';
            if($country) {
                $countryName = $country->country;
            } 

            $cityName = '';
            if ($this->request->getPost('city') != '') {
                $getCityName = Cities::findFirst("id='".$this->request->getPost('city')."'");
                $cityName = $getCityName->city_name;
            }
     
            $stateName = '';
            if ($this->request->getPost('state') != '') {
                $getStateName = States::findFirst("id='".$this->request->getPost('state')."'");
                $stateName = $getStateName->state_name;
            } 
     
            $address                    = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            $userSession                = $this->session->get("userSession");
            $content                    = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            $json                       = json_decode($content, true);
            $lat                        = $json['results'][0]['geometry']['location']['lat'];
            $lng                        = $json['results'][0]['geometry']['location']['lng'];


            $business->member_id        = $userSession['id'];
            $business->name             = $this->request->getPost('name');
            $business->website          = $this->request->getPost('website');
            $business->telephone        = $this->request->getPost('telephone');
            $business->street           = $this->request->getPost('street');
            $business->city             = $cityName;
            $business->state            = $stateName;
            $business->postal_code      = $this->request->getPost('postal_code');
            $business->country_code_id  = $this->request->getPost('country_code');
            $business->lat              = $lat;
            $business->lng              = $lng;
            $opened = '';

            if(!empty($this->request->getPost('opened'))) { $opened = 'Opened'; };
            if(empty($this->request->getPost('opened'))) { $opened = 'Opening Soon'; };
            $business->opened = $opened;
            if($business->update()){
                if(!empty($this->request->getPost('business_category_ids'))) {
                    $bCtegories = $this->request->getPost('business_category_ids');
                    $bCtegoryIds = explode(',', $bCtegories);
                    BusinessCategoryLists::find('business_id="'.$businessId.'"')->delete();
                    foreach ($bCtegoryIds as $key => $bCtegoryId) {
                        $businessCategoryLists = new BusinessCategoryLists();
                        $businessCategoryLists->created = date('Y-m-d H:i:s');
                        $businessCategoryLists->business_id = $businessId;
                        $businessCategoryLists->business_category_id = $bCtegoryId;
                        $businessCategoryLists->create();
                    }
                }
                $this->session->set('successMess', 'business successfully updated!');
                return $this->response->redirect('biz/update_businessu/'.$business->member_id);
            }else{
                $err = '';
                foreach ($business->getMessages() as $message) {
                    echo $message->getMessage() . " <br>";
                }
               $this->session->set('errorMess', 'Failed updating this record <br>'.$err);
           
            }
        }

        $countries = CountryCodes::find();
        $cities = Cities::find("country_code_id='".$business->country_code_id."'");
        $states = States::find("country_code_id='".$business->country_code_id."'");
        $businessCategoryLists = BusinessCategoryLists::find('business_id="'.$claimRequests->business_id.'"');


        $this->view->setVars([
            'businessCategoryLists' => $businessCategoryLists,
            'cities'                => $cities,
            'states'                => $states,
            'countries'             => $countries,
            'ids'                   => $id,
            'claimRequests'         => $claimRequests,
            'revopt'                => $revopt
        ]);


    }
    /**
     * @return [type]
     */
    public function getImagePreviewAction() {
        $this->view->disable();
        if($this->request->isAjax()){

            $imageId    = $this->request->getQuery('image_id');
            $image      = ImageAds::findFirst("id='".$imageId."'");
            if($image){
                $file       = $this->url->getBaseUri().$image->file_path.$image->file_name;
            }else{
                $file       = 'img/default_large.png';
            }
            $img        = '<img src="'.$file.'" style="width:100%">';
            $output = json_encode(array('type'=>'ok', 'message' => 'success', 'html' => $img));
            die($output); //exit script outputting json data

        }
    }


    public function setcampaignaduAction() {
        $this->view->disable();
        if($this->request->isAjax()){
            $memberId = $this->request->getQuery('member_id');
            $businessId = $this->request->getQuery('business_id');
            $content = $this->request->getQuery('content');
            $result = '';
            $check_biz_ad = Adcampaigns::count("business_id='".$businessId."'");
            if ($check_biz_ad > 0) {
                $updateCampaign = Adcampaigns::findFirst("business_id='".$businessId."'");
                $updateCampaign->modified = date('Y-m-d H:i:s');
                $updateCampaign->member_id = $memberId;
                $updateCampaign->content = $content;

                if ($updateCampaign->update()) {
                    $result = "Updating Ad Campaign Success";
                } else {
                    $result = "Updating Ad Campaign Failed";
                }
            } else {
                $newAdCampaign = new Adcampaigns();
                $newAdCampaign->created = date('Y-m-d H:i:s'); 
                $newAdCampaign->modified = date('Y-m-d H:i:s'); 
                $newAdCampaign->business_id = $businessId;
                $newAdCampaign->member_id = $memberId;
                $newAdCampaign->content = $content;

                if ($newAdCampaign->create()) {
                    $result = 'Adding new Ad Campaign Success';
                } else {
                    $result = 'Adding new Ad Campaign Failed';
                }
            }

            $output = json_encode(array('type'=>'ok', 'message' => $result));
            die($output); //exit script outputting json data
        }
    }



    public function adcampaignuAction($id = null, $business_id = null) {
        $this->view->setTemplateAfter('businessTemplate');
        $member  = Members::findFirst($id);
        $this->view->setVar('member', $member);

        $claimRequests = ClaimRequests::findFirst("member_id = ". $id);
        $this->view->setVar('claimRequests', $claimRequests); 

        if ($this->request->isPost()) {

            $businessId = $this->request->getPost('business');
            $business = Business::findFirst("id='".$businessId."'");

            $this->view->setVars([
                'business' => $business,
                'postContent'  => $this->request->getPost('content')
            ]);
        }
    }

    /**
     * [profileu description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function profileAction($id = null){

        $this->view->setTemplateAfter('businessTemplate');
        $this->view->seschecker = $this->sessionChecker($id);


        $member = Members::findFirstById($id);
        if(!$member) {
          $this->response->redirect('biz/profile/'.$userSession['id']);
        }


        $claimRequests = ClaimRequests::findFirst("member_id = ". $id);
 

        $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $oldEmail = $member->email;

        if ($this->request->isPost()) {
            $member = Members::findFirstById($this->request->getPost('id'));
            $member->modified           = date('Y-m-d H:i:s');
            $member->first_name         = $this->request->getPost('first_name');
            $member->last_name          = $this->request->getPost('last_name');
            $member->street             = $this->request->getPost('street');
            $member->city               = $this->request->getPost('city');
            $member->country_code_id    = $this->request->getPost('country_id');
            $member->email              = $this->request->getPost('email');
            $member->mobile             = $this->request->getPost('mobile');


            if(!empty($this->request->getPost('email'))){
                $cookie_name = "e";
                $cookie_value = $this->request->getPost('email');
                setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
            }
            if($member->update()){
                if($oldEmail != $this->request->getPost('email')){
                    $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
                    $emailConfimation = new EmailConfirmations();
                    $emailConfimation->created      = date('Y-m-d H:i:s');
                    $emailConfimation->modified     = date('Y-m-d H:i:s');
                    $emailConfimation->user_id      = $member->id;
                    $emailConfimation->email        = $this->request->getPost('email');
                    $emailConfimation->token        = $activationToken;
                    $emailConfimation->confirmed    = 'N';
                    if($emailConfimation->save()){
                        $this->getDI()->getMail()->send(
                        array($this->request->getPost('email') => $member->first_name.' '.$member->last_name),
                        'Please confirm your email',
                        'email_update_confirmation',
                        array( 'confirmUrl' => 'member/emailConfimation/'. $member->id .'/'. $this->request->getPost('email') .'/'. $activationToken)
                        );
                        $info = 'Please confirm your email'; 
                        $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$info);
                        return $this->response->redirect("biz/logout");
                    }
                }
                $info = 'You profile has been updated.'; 
                $this->flash->success('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$info);
                return $this->response->redirect("biz/profile/".$this->request->getPost('id'));
            }else{
                $info = 'Updating profile failed.'; 
                $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$info);
            }
        }


        $this->view->setVars([
            'photos'            => $photos,
            'member'            => $member,
            'claimRequests'     => $claimRequests,
            'userid'            => $id
        ]);

    }


    public function profileImageAction(){

        if($this->request->isAjax()) {
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

            // $userSession = $this->session->get('userSession');
            // $userid = $userSession['id'];
            $id     = $this->request->getPost('id');
            $img    = '';
            $profilepic = MemberPhotos::findFirst([
                'conditions' => 'member_id = :id: AND primary_pic = :type:',
                'bind' => array('id' => $id, 'type' => 'Yes'),
                'order' => 'id desc' 
            ]);
            if ($profilepic) {
                $profilePic = $this->url->getBaseUri() . $profilepic->file_path . $profilepic->filename;
                $img .= '<img src="'.$profilePic.'">';
            }else{
                $img .= Phalcon\Tag::image(array('img/default_large.png', 'alt' => 'Profile')); 
            }    
                        
            $output = json_encode(array('type'=>'ok', 'html' => $img));
            die($output); //exit script outputting json data
        }
        
    }


    public function downloadCropImageAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if($this->request->isAjax()) {
            $type       = 'failed';
            $message    = 'message : ';

            $userid         = $this->request->getPost('userid');
            $encoded        = $this->request->getPost('imagelink');
            $newFileName    = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'YES.png';
            $filePath       = 'img/member/';

            $ups = MemberPhotos::findFirst([
                'conditions'    => 'member_id = :id: AND primary_pic = :type:',
                'bind'          => array('id' => $userid, 'type' => 'Yes'),
                'order'         => 'id desc'
            ]);
            if($ups){
                $mpic = MemberPhotos::findFirst($ups->id);
                $filedel = $mpic->filename;
            }else{
                $mpic = new MemberPhotos();
                $filedel = '';
                $mpic->created      = date('Y-m-d H:i:s');
            }
            $mpic->modified     = date('Y-m-d H:i:s');
            $mpic->file_path    = $filePath;
            $mpic->filename     = $newFileName;
            $mpic->member_id    = $userid;
            $mpic->primary_pic  = 'Yes';
            $mpic->caption      = 'Profile Picture';
            if ($mpic->save()) {
                $type = 'ok';
                $file = $filePath . $newFileName;
                if (file_put_contents($file, file_get_contents($encoded))) {
                    if($filedel != ''){
                        $path = $filePath.$filedel;
                         if (file_exists($path)) {
                            unlink($path);
                          }
                    }
                    $message = 'Profile Picture successfully updated';
                }else{
                    $type = 'failed';
                    $message = 'Upload failed, please try again.';
                }
                
            }else{
                $message = 'Error!! Upload failed, please try again.';
            }

            $output = json_encode(array('type'=> $type, 'text' => $message));
            die($output); //exit script outputting json data
        }

    }



    /**
     * [update_emailAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function update_emailAction($id) { 

        if ($this->request->isPost()) {
            $userSession = $this->session->get('userSession');
            $new_email = $this->request->getPost('new_email');

            //authenticate the user
            $member = Members::findFirstById($id);
            if(!$member || $userSession['id'] != $member->id) {
                $myError = 'Redirect to your profile'; 
                $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
                $this->response->redirect('biz/profile/'.$id);
            }

            $member->modified = date('Y-m-d H:i:s');

            // get the current email
            $currentEmail = $member->email;

            // Warning : the same email 
            if($currentEmail == $new_email){
                $myError = 'can\'t change your email with the same email. '; 
                $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
                $this->response->redirect('biz/profile/'.$id);
            }

            // update email if changed
            if($currentEmail != $new_email){

                // Warning : already used email
                if(Members::findFirstByEmail($new_email)){
                    $myError = 'This email address is already used by a Member or Business Owner in MyBarangay. Please use another email address'; 
                    $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
                }else{
                    $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
                    $emailConfimation = new EmailConfirmations();
                    $emailConfimation->created = date('Y-m-d H:i:s');
                    $emailConfimation->modified = date('Y-m-d H:i:s');
                    $emailConfimation->user_id = $member->id;
                    $emailConfimation->email = $this->request->getPost('new_email');
                    $emailConfimation->token = $activationToken;
                    $emailConfimation->confirmed = 'N';
                    if($emailConfimation->save()){
                        $this->getDI()->getMail()->send(
                        array($this->request->getPost('new_email') => $member->first_name.' '.$member->last_name),
                        'Please confirm your email',
                        'email_update_confirmation',
                        array( 'confirmUrl' => 'biz/confirmEmail/'. $member->id .'/'. $this->request->getPost('new_email') .'/'. $activationToken)
                        );
                        $info = 'Email successfully updated, please confirm your email.'; 
                        $this->flash->success('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$info);
                        $this->response->redirect('biz/profile/'.$id);

                    }else{
                        // Error : post error message
                        $info = 'Error Occured : Please Contact Us'; 
                        $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$info);
                    }
                }
            }
            // redirect page
            $this->response->redirect('biz/profile/'.$id);
        }
        // redirect page
        $this->response->redirect('biz/profile/'.$id);
    }


    /**
     * [emailConfimationAction description]
     * @param  [type] $userId          [description]
     * @param  [type] $email           [description]
     * @param  [type] $activationToken [description]
     * @return [type]                  [description]
     */
    public function confirmEmailAction($userId = null, $email =null, $activationToken = null){
        $this->view->disable();
        $emaiConfirmed = EmailConfirmations::findFirst([
            'conditions' => 'user_id = ?1 AND email=?2 AND token = ?3 AND confirmed = ?4',
            'bind' => array(1 => $userId, 2 => $email, 3 => $activationToken, 4 => 'N')
        ]);

        if($emaiConfirmed) {
            $emaiConfirmed->confirmed = 'Y';
            if($emaiConfirmed->update()){

                $member = Members::findFirst($userId);

                $member->modified  = date('Y-m-d H:i:s');
                $member->email    = $email;
    
                if($member->update()){

                    $info = 'Email successfully confirmed, try and login now.'; 
                    $this->flash->success('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$info);
                    return $this->response->redirect('biz/profile/'.$member->email); 
                }else{
                    $errno = '';
                    foreach ($member->getMessages() as $error) {
                        $errno .= $error->getMessage().'<br>';
                    }
                    $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$errno);
                    return $this->response->redirect('biz/login'); 
                }
            }else{
                $errno = '';
                foreach ($emaiConfirmed->getMessages as $err) {
                    $errno .= $err->getMessage().'<br>';
                }
                $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$errno);
                return $this->response->redirect('biz/login'); 
            }

        } else {
            $info = 'Email confirmation failed, please try again.'; 
            $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$info);
            return $this->response->redirect('biz/login'); 
        }
    }


    /**
     * [change_passwordAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function change_passwordAction($id = null) { 
 
        $member = Members::findFirstById($id);
        $userSession = $this->session->get('userSession');
        if(!$member || $userSession['id'] != $member->id) {
            $myError = 'Redirect to your profile'; 
            $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
        }

        if ($this->request->isPost()) {

            $oldPassword = $this->request->getPost('current_password');
            $newPassword = $this->request->getPost('new_password');
            $confirmPassword = $this->request->getPost('confirm_password');

            if ($this->security->checkHash($oldPassword, $member->password)) {
                if ($newPassword == $confirmPassword) {
                    if (empty($newPassword) OR empty($confirmPassword)) {
                        $myError = 'Your new password is empty'; 
                        $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
                    }else {  
                        $member->modified = date('Y-m-d H:i:s');
                        $member->password = $this->security->hash($newPassword);
                        if ($member->update()) {
                            $this->getDI()->getMail()->send(
                                array($member->email => $member->first_name.' '.$member->last_name),
                                'Your password has been changed',
                                'change_password',
                                array('old' => $oldPassword, 'new' => $newPassword)
                            );
             
                            $this->flash->success('<a href="#" class="close" data-dismiss="alert">&times;</a> Password successfully changed!');
                            return $this->response->redirect("biz/profile/".$this->request->getPost('id'));

                        }else {
                            $alertErr = '';
                            foreach ($member->getMessages() as $err) {
                                $alertErr .= $err->getMessage().'<br>'; 
                            }
                            $myError = 'ERROR OCCURED! Problem changing your password, this error will be send to the administrator to check, sorry inconvience '; 
                            $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
                        }
                    }
                }else {
                    $myError = 'Password doesn\'t match.'; 
                    $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
                }
            }else {
                $myError = 'Your current password is not correct.';
                $this->flash->error('<a href="#" class="close" data-dismiss="alert">&times;</a>'.$myError);
            }

            return $this->response->redirect("biz/profile/".$id);
        }

    }




   public function business_galleryAction($id = null){
        $this->view->setTemplateAfter('businessTemplate');

        if(!$this->sesChecker() OR !$this->idChecker($id)){
           return $this->response->redirect('biz/login');
        }

        $claimRequests = ClaimRequests::findFirst([
            'conditions'  =>  'member_id = :id:',
            'bind'        =>  array('id' => $id),
            'order'       =>  'id DESC'
        ]);
        if(!$claimRequests){
            return $this->response->redirect('biz/login');
        }
        // one image
        $busImage = BusinessImages::findFirst([
            'conditions'    => 'business_id = :busid:',
            'bind'          =>  array('busid' => $claimRequests->business_id),
            'order'         =>  'id DESC'
        ]);

        // many images
        $BusinessImage = BusinessImages::find([
            'conditions'    => 'business_id = :busid:',
            'bind'          =>  array('busid' => $claimRequests->business_id),
            'order'         =>  'id DESC'
        ]);


        $currentPage = (isset($_GET["page"]))? (int) $_GET["page"] : 1 ;
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
                    "data" => $BusinessImage,
                    "limit"=> 10,
                    "page" => $currentPage
                )
            );
        $page = $paginator->getPaginate();


        $this->view->setVars([
            'busImage'           => $busImage,
            'businessImages'     => $page,
            'claimRequests'      =>   $claimRequests
        ]);



    }



    public function business_profile_photoAction($id = null, $imageId = null){

        $this->view->setTemplateAfter('businessTemplate');

        if(!$this->sesChecker() OR !$this->idChecker($id)){
           return $this->response->redirect('biz/login');
        }


        $getPhoto = BusinessImages::findFirst([
            'conditions'    =>  'id = :id:',
            'bind'          =>  array('id' => $imageId),
            'order'         =>  'id DESC'
        ]);

        if(!$getPhoto){
           return $this->response->redirect('biz/business_gallery/'.$id);
        }
        $busid = $getPhoto->business_id;
        $claimRequests = ClaimRequests::findFirst([
            'conditions'  =>  'member_id = :id: AND business_id = :busid:',
            'bind'        =>  array('id' => $id, 'busid' => $busid ),
            'order'       =>  'id DESC'
        ]);

        if(!$claimRequests){
           return $this->response->redirect('biz/business_gallery/'.$id);
        }


        $this->view->setVars([
            'getPhoto'      =>  $getPhoto,
            'claimRequests' =>  $claimRequests
        ]);
    }





    /**
     * AJAX
     * [setPrimaryPhotoAction description]
     */
    public function setPrimaryPhotoAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {


            $userSession = $this->session->get('userSession');

            $type       = 'failed';
            $message    = 'message : ';
            $busid          = $this->request->getPost('bux');
            $id             = $this->request->getPost('xxx');
            $encoded        = $this->request->getPost('imagelink');
            $userid         = $this->request->getPost('userid');
            $newFileName    = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'YES.png';
            $filePath       = 'img/business/';

            if(!$this->sesChecker() OR !$this->idChecker($userid) OR $encoded == '' OR $busid == '' OR $id == ''){
                $ot = json_encode(['type'=> $type, 'text' => 'Error occured, please try again.']);
                die($ot);
            }

            $getPhoto = BusinessImages::find([
                'conditions'    =>  'business_id = :busid: AND primary_pic = :prime:',
                'bind'          =>  array('busid' => $busid, 'prime' => 'Yes' )
            ]);
            if($getPhoto){
                foreach ($getPhoto as $photUpdate) {
                    $photUpdate->primary_pic = 'No';
                    if($photUpdate->save()){
                        $message = 'alright';
                    }else{
                        $errno = '';
                        foreach ($photUpdate->getMessages() as $error) {
                           $errno .= $error->getMessage(). '<br>';
                        }
                        $message = 'errno';
                    }
                }
            }

            $bpic       =   BusinessImages::findFirstById($id);
            $filedel    =   '';
            if($bpic){
                $filedel            = $bpic->filename;
                $bpic->filename     = $newFileName;
                $bpic->member_id    = $userSession['id'];
                $bpic->primary_pic  = 'Yes';
                if ($bpic->save()) {
                    $type = 'ok';
                    $file = $filePath . $newFileName;
                    if (file_put_contents($file, file_get_contents($encoded))) {
                        if($filedel != ''){
                            $path = $filePath.$filedel;
                             if (file_exists($path)) {
                                unlink($path);
                              }
                        }
                        $message = 'Business profile photo successfully updated';
                    }else{
                        $type = 'failed';
                        $message = 'Upload failed, please try again.';
                    }
                    
                }else{
                    $errno = '';
                    foreach ($bpic->getMessages() as $error) {
                       $errno .= $error->getMessage(). '<br>';
                    }
                    $message = 'Can\'t continue the process: Error occured!!! ';
                }
            }else{
                $message = 'Unknown image please, find another image.';
            }
           
            $href = 'https://mybarangay.com/business/view2/'.$busid;
            $output = json_encode(array('type'=> $type, 'text' => $message, 'redirect' => $href));
            die($output); //exit script outputting json data


        }
    }


    /**
     * AJAX
     * [getPrimaryPhotoAction description]
     * @return [type] [description]
     */
    public function getPrimaryPhotoAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {
            $id         = $this->request->getQuery('busImage');
            $message    = '';
            $name       = '';
            $response   = 'ok';
            $profilePic        = '';


            $getPhoto = BusinessImages::findFirst([
                'conditions'    =>  'id = :id:',
                'bind'          =>  array('id' => $id),
                'order'         =>  'id DESC'
            ]);

            if($getPhoto){
                $profilePic = $this->url->getBaseUri() . $getPhoto->file_path . $getPhoto->filename;
                $img = '<img src="'.$profilePic.'" id="prev-img">';
         
            }

            $output = json_encode(array('type' => $response, 'html' => $img));
            die($output); //exit script outputting json data

        }
    }


    /**
     * AJAX
     * [deleteGalleryPhotoAction description]
     * @return [type] [description]
     */
    public function deleteGalleryPhotoAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $id         = trim($this->request->getQuery('ids'), 'delete-');
            $message    = '';
            $name       = '';
            $response   = 'ok';
            if(!empty($id)){
                $photdel = BusinessImages::findFirst($id);
                if($photdel){
                     $path      =   $photdel->file_path;
                     $filename  =   $photdel->filename;
               
                    if(!empty($path) OR !empty($filename)) { // optional
                        $pathtodel = $path.$filename;
                         $message = $pathtodel;
                        if($photdel->delete()){
                            if (file_exists($pathtodel)) {
                                unlink($pathtodel);
                            }
                            $message .= 'Image successfully deleted!';
                        }else{
                            $message .= 'Failed deleting Image';
                            $response = 'failed';
                        }
                    }
                }
            }
            $output = json_encode(array('type' => $response, 'message' => $message));
            die($output); //exit script outputting json data
        }

    }

    /**
     * AJAX
     * [responseDeleteAction description]
     * @return [type] [description]
     */
    public function responseDeleteAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $id         = trim($this->request->getQuery('ids'), 'resdel-');
            $message    = '';
            $response   = 'ok';
            $html       = '';
         
            $reviewRespond = ReviewResponds::findFirstById($id);
            if($reviewRespond){
                if($reviewRespond->delete()){
                    $message = 'successfully deleted';
                }else{
                    $response = 'false';
                    $message = 'Oops! Something went wrong.. Please try again.';
                }
            }else{
                $response = 'false';
                $message = 'Sorry, we can\'t find the data you want to delete. Please try again.';
            }

     
            $output = json_encode(array('type' => $response, 'message' => $message, 'html' => $html));
            die($output); //exit script outputting json data
        }
    }



    /**
     * AJAX
     * [responseEditAction description]
     * @return [type] [description]
     */
    public function responseEditAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $id         = $this->request->getQuery('ids');
            $message    = '';
            $response   = 'ok';
            $html       = '';
         
            $reviewRespond = ReviewResponds::findFirstById($id);
            if($reviewRespond){
                $html = $reviewRespond->content;
            }else{
                $response = 'false';
                $message = 'Sorry, we can\'t find the data you want to Edit. Please try again.';
            }

     
            $output = json_encode(array('type' => $response, 'message' => $message, 'html' => $html));
            die($output); //exit script outputting json data
        }
    }

    /**
     * AJAX
     * [responseSaveAction description]
     * @return [type] [description]
     */
   public function responseSaveAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $id             = $this->request->getPost('ids');
            $new_content    = $this->request->getPost('content');
            $response       = 'ok';
            $html           = '';
         
            $reviewRespond = ReviewResponds::findFirstById($id);
            if($reviewRespond){

                $reviewRespond->content       = $new_content;
                $reviewRespond->modified      = date('Y-m-d H:i:s');
                if($reviewRespond->update()){
                    $message = 'successfully updated';
                    $html = $reviewRespond->content;
                }else{
                    $response = 'false';
                    $message = 'Oops! Something went wrong.. Please try again.';
                }
            }else{
                $response = 'false';
                $message = 'Sorry, we can\'t find the data you want to update. Please try again.';
            }

            $output = json_encode(array('type' => $response, 'message' => $message, 'html' => $html));
            die($output); //exit script outputting json data
        }
    }

    /**
     * AJAX
     * [responseSaveAction description]
     * @return [type] [description]
     */
   public function responseAddAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {


            $userSession = $this->session->get("userSession");
            $response       = 'ok';
            $html           = '';

            $respond = new ReviewResponds();
            if($respond){
                $respond->created       = date('Y-m-d H:i:s');
                $respond->modified      = date('Y-m-d H:i:s');
                $respond->member_id     = $userSession['id'];
                $respond->review_id     = $this->request->getPost("revid");
                $respond->business_id   = $this->request->getPost("memid");
                $respond->content       = $this->request->getPost("content");
                if($respond->create()){
                    $message = 'display';

                        $html .= '<div class="comment-item ">';
                  
                        $resPhoto =  MemberPhotos::findFirst([
                        'conditions' => 'member_id = :id: AND primary_pic = :type:',
                        'bind' => array('id' => $respond->member_id, 'type' => 'Yes'),
                        'order' => 'id desc'
                        ]);
                        if ($resPhoto) {
                            $profilePic = $this->url->getBaseUri() . $resPhoto->file_path . $resPhoto->filename;
                            $html .= '<img class="object-fit_cover" src="'.$profilePic.'" alt="profile">';
                        }else{
                            $html .= Phalcon\Tag::image(array('img/default_large.png', 'alt' => 'Profile')); 
                        } 
                        $business = Business::findFirstById($respond->business_id);

                        $html .= '<img src="'.$this->url->getBaseUri().'themes3/img/ajaxloader.png" class="ajaxload" style="width:20px;height:20px;display:none">';
                        $html .= '<p class="comment-head">';
                        $html .= '<span style="font-size:12px;font-weight:bold;color:#333;text-transform:capitalize">'.$business->name.'</span>';
                        $html .= '</p>';
                        $html .= '<div class="dropdown pull-right" >';
                        $html .= '<button class="btn btn-grave btn-sm dropdown-toggle" type="button" id="menu1" data-toggle="dropdown"><i class="fa fa-edit"></i>';
                        $html .= '<span class="caret"></span></button>';
                        $html .= '<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">';
                        $html .= '<li role="presentation"><a data-target="#myModal" data-toggle="modal"  role="menuitem" tabindex="-1" href="#"   class="responEdit" id="'.$respond->id.'"><i class="fa fa-edit"></i> Edit</a></li>';
                        $html .= '<li role="presentation"><a role="menuitem" tabindex="-1" href="#"  class="responseDelete" id="resdel-'.$respond->id.'"><i class="fa fa-close"></i> Delete</a></li>';
                        $html .= '</ul>';
                        $html .= '</div>';

                        $html .= '<p class="respond-content">'.$respond->content.'</p>';
                        $html .= '<small class="text-muted">@'.date('M d ', strtotime($respond->modified)).'</small>';
                        $html .= '</div>';





                }else{
                    $errno = '';
                    foreach ($respond->getMessages() as $err) {
                        $errno .= $err->getMessage().'</br>';
                    }
                    $response = 'false';
                    $message = 'Error please try again!'. $errno;
                }

            }else{
                $response = 'false';
                $message = 'Error occured! we can\'t find your respond, please try to refresh the page';
            }

            $output = json_encode(array('type' => $response, 'message' => $message, 'html' => $html));
            die($output); //exit script outputting json data
        }
    }


    public function subscriptAction(){
        $this->view->setTemplateAfter('businessTemplate');
    }




}// end of class
