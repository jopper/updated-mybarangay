<?php

class PaymentLogs extends \Phalcon\Mvc\Model
{

    public $id;
    public $created;
    public $modified;
    public $member_id;
    public $stripe_customer_id;
    public $member_email;
    public $entered_email;
    public $plan_type;
    public $timer;
    public $description;
    public $gst;
    public $amount;
    public $total_amount;

    public function initialize()
    {
         // $this->belongsTo("whatsup_topic_id", "WhatsupTopics", "id");
         // $this->belongsTo("member_id", "Members", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'created' => 'created',
            'modified' => 'modified',
            'member_id' => 'member_id',
            'stripe_customer_id' => 'stripe_customer_id',
            'member_email' => 'member_email',
            'entered_email' => 'entered_email',
            'plan_type' => 'plan_type',
            'timer' => 'timer',
            'description' => 'description',
            'gst' => 'gst',
            'amount' => 'amount',
            'total_amount' => 'total_amount'
        );
    }

}
