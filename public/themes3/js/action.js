$(function(){


	//***************** GALLERY PHOTOS  *****************

	// image list when click change the panel main box
    var totalImgCount = $('#totalImgCount').val();

    $('.img-list').on('click', function(){
        var src = $(this).attr('src');
        var member = $(this).data('member');
        var headername = $(this).data('headername');
        var caption = $(this).data('caption');
        var key = $(this).data('key');
        $('.img-current').attr('src', src);
        $('#memberName').text(member);
        $('#headerName').text(headername);
        $('#imgCaption').text(caption);
        $('#currentKey').text(parseInt(key,10)+1);
    });

	// goto to previous
    $('#previousImgBtn').on('click', function(){
        var key = $('.img-current').data('key');
        if(key > 0){
            var newKey = parseInt(key,10)-1;
            var img = $(".img-list[data-key='" + newKey + "']");
            var src = img.attr('src');
            var member = img.data('member');
            var headername = $(this).data('headername');
            var caption = img.data('caption');
            $('.img-current').attr('src', src);
            $('.img-current').data('key', newKey);
            $('#memberName').text(member);
            $('#headerName').text(headername);
            $('#imgCaption').text(caption);
            $('#currentKey').text(parseInt(newKey,10)+1);
        }
    });

	// goto to nexr
    $('#nextImgBtn').on('click', function(){
        var key = $('.img-current').data('key');
        if(key < totalImgCount){
            var newKey = parseInt(key,10)+1;
            var img = $(".img-list[data-key='" + newKey + "']");
            var src = img.attr('src');
            var member = img.data('member');
            var headername = $(this).data('headername');
            var caption = img.data('caption');
            $('.img-current').attr('src', src);
            $('.img-current').data('key', newKey);
            $('#memberName').text(member);
            $('#headerName').text(headername);
            $('#imgCaption').text(caption);
            $('#currentKey').text(parseInt(newKey,10)+1);
        }
    });



});
