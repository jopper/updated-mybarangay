<?php
	
namespace CustomerHelpers;

/**
 * Class Formatter
 * @package CustomHelpers
 * @author Loki Daniels <kafkaontheshore@outlook.ph>
 */

class Formatter 
{	

	/**
	 * datetimeToDate - Return a new instance of DateTime class or a date format 'Y-m-d' of datetime parameter
	 * @param string $datetime 
	 *
	 * @return DateTime
	 *
	 */
	public function datetimeToDate($datetime = null) {
		if ($datetime == null) {
			echo "whoops!. datetime parameter is missing.";
			die();
		}

		$date = new DateTime($datetime);
		return $date->format('Y-m-d');
	}
}
?> 