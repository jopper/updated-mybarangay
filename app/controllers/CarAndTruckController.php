<?php
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;



class CarAndTruckController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
    }
    
  public function getCitiesAction() {
        if ($this->request->isAjax()) {
            $arrCities = array();
             $stateCode = $this->request->getQuery('param1');
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }
            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }





    public function getStatesAction() {

        if ($this->request->isAjax()) {
            $arrStates = array();
            $countryCode = $this->request->getQuery('param');


            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }
          
            return $response;
        }
    }



    /**
     * [flagAction description]
     * @return [type] [description]
     */
    public function flagAction(){

        $this->view->disable();

        if($this->request->isAjax()) 
        {
            $review = $this->request->getQuery('review');
            $reviewer = $this->request->getQuery('reviewer');
            $flagger = $this->request->getQuery('flagger');
            $flag_type = $this->request->getQuery('flag_type');
            $referer = $this->request->getHTTPReferer (); 
            $controller = 'classifieds';
            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }
            $arrayName = array('review' => $review, 'reviewer' => $reviewer, 'flagger' => $flagger, 'result' => $resultInfo);
            echo json_encode(array('result' => 'OK', 'items' => $arrayName));
        }

    }


    public function searchesAction(){
       // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        if ($this->request->isPost()) {
            $locations = [];
            $getters = [];
            $queries = [];
            foreach ($_POST as $key => $value) {
               $temp = is_array($value) ? $value : trim($value) ;
               if(!empty($temp)){
                    list($key) = explode("-", $key);
                    if($key == 'name'){
                        array_push($locations, $value);
                    }
                    if(!in_array($key, $getters)){
                        $getters[$key] = $value;
                    }
               }
            }

            if(!empty($city)){
                $loc_qry = implode(",", $locations);
            }
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                    switch ($key) {
                        case 'name':
                            # code...
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                }
            }
            $this->view->disable();
        }
    }

    /**
     * [indexAction description]
     * @return [type] [description]
     */
    public function indexAction(){

       // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        // variables undefined

        $sql = '';
        $auto = '';
        $name = '';
        $body_type = '';
        $transmission = '';
        $fuel_type = '';
        $btn_search = '';
        $minPrice = '';
        $maxPrice = '';
        $city = '';
        $city_code = '';
      $sql = "SELECT * FROM Automotives ";
        if (isset($_GET['btn_search'])) {
            $btn_search = $this->request->getQuery('btn_search');
            $locations = [];
            $getters = [];
            $queries = [];
            $error = [];
            foreach ($_GET as $key => $value) {
               $temp = is_array($value) ? $value : trim($value) ;
               if(!empty($temp)){
                    list($key) = explode("-", $key);
                    if($key == 'name'){
                        array_push($locations, $value);
                    }
                    if(!in_array($key, $getters)){
                        $getters[$key] = $value;
                    }
               }
            }
            if(!empty($locations)){
                $loc_qry = implode(",", $locations);
            }
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                   ${$key} = $value;
                    switch ($key) {
                        case 'name':
                                  $name = $this->real_escape($name);
                               array_push($queries, "(name LIKE '%$name%' OR brand LIKE '%$name%' OR model LIKE '%$name%' )");
                            break;
                            case 'minPrice':
                            break;
                            case 'maxPrice':
                            break;
                            case 'btn_search':
                            if(is_numeric($minPrice) OR is_numeric($maxPrice)){
                                if(!empty($minPrice) AND !empty($maxPrice)){
                                    array_push($queries, "(price BETWEEN $minPrice AND $maxPrice )");
                                }else{
                                    if(!empty($minPrice)){
                                        array_push($queries, "(price < $minPrice )");
                                    }
                                    if(!empty($maxPrice)){
                                        array_push($queries, "(price > $maxPrice )");
                                    }
                                }
                            }
                            break;
                            case 'body_type':
                                array_push($queries, "(body_type = $body_type)");
                            break;
                            case 'transmission':
                                array_push($queries, "(transmission = $transmission)");
                            break;
                            case 'fuel_type':
                                array_push($queries, "(fuel_type_id = $fuel_type)");
                            break;
                            case 'city_code':
                                array_push($queries, "(city = $city_code)");
                            break;
                            case 'city':
                            break;

                        default:
                            # code...
                            break;
                    }
                }
            }

            if(!empty($queries)){
                $sql .= " WHERE ";
                $i = 1;
                foreach ($queries as $query) {
                    if($i < count($queries)){
                        $sql .= $query." AND ";
                    }else{
                        $sql .= $query;
                    }
                    $i++;
                }
            }

        }

    $auto = $this->modelsManager->executeQuery($sql);
        $this->view->setVar('autos', $auto);
        $this->view->setVars([
        'name' => $name, 
        'body_type' => $body_type, 
        'transmission' => $transmission, 
        'fuel_type' => $fuel_type, 
        'btn_search' => $btn_search,
        'minPrice' => $minPrice,
        'maxPrice' => $maxPrice,
        'city' => $city,
        'city_code' => $city_code
        ]);
    
       // $currentPage = 1;
       // $parameters = array();

       //  //print_r($parameters);
       //  $autos = Automotives::find($parameters);
       //  $paginator = new Paginator(
       //      [
       //          "data" => $autos,
       //          "limit"=> 12,
       //          "page" => $currentPage
       //      ]
       //  );
       //  //paginator
        // $page = $paginator->getPaginate();
        // $this->view->setVar('counter', $autos);

        // $this->view->setVars([
        //     'ename' => $name,
        //     'ecity' => $ecity,
        //     'ecity_code' => $city_code
        // ]);



    }

    public function newXAction(){


     $this->view->setTemplateAfter('default1');
       // $this->view->disable();
        if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
               // $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/business/';

            echo $file = $filePath . $newFileName;
       }

    
    }


    public function formValidate() {

  $validation = new Phalcon\Validation();
    $validation->add('name', new PresenceOf(array(
        'message' => 'name field is required'   
    )));
    // $validation->add('street', new PresenceOf(array(
    //     'message' => 'street field is required'   
    // )));
    $validation->add('details', new PresenceOf(array(
        'message' => 'details field is required'    
    )));
    $validation->add('price', new PresenceOf(array(
        'message' => 'price field is required'  
    )));
    $validation->add('brand', new PresenceOf(array(
        'message' => 'brand field is required'  
    )));
    $validation->add('model', new PresenceOf(array(
        'message' => 'model field is required'  
    )));
    $validation->add('year_model', new PresenceOf(array(
        'message' => 'year model field is required' 
    )));
    $validation->add('condition', new PresenceOf(array(
        'message' => 'condition field is required'  
    )));
    $validation->add('body_type', new PresenceOf(array(
        'message' => 'body type field is required'  
    )));
    $validation->add('transmission', new PresenceOf(array(
        'message' => 'transmission field is required'   
    )));
    // $validation->add('mileage', new PresenceOf(array(
    //     'message' => 'mileage field is required'    
    // )));
    $validation->add('fuel_type', new PresenceOf(array(
        'message' => 'fuel type field is required'  
    )));
        return $validation->validate($_POST);
    
    }



    public function newAction(){   

     $this->view->setTemplateAfter('default1');
   

      if ($this->request->isPost()) {

        $messages = $this->formValidate();
        if (count($messages)) {
              $errorMsg = '';
            foreach ($messages as $msg) {
              $errorMsg .= $msg . "<br>";       
            }
            $this->session->set('errorMessasge', $errorMsg);
            $this->response->redirect('car_and_truck/new');
        } 
    
            $success = '';
            $userSession = $this->session->get("userSession");
            $details = $this->request->getPost("details");
            if($details != '' OR !empty($details)){
                $detail = $this->request->getPost("details");
            }else{
                $detail = 'please insert content here...';
            }

            $auto = new Automotives();
            $auto->created = date('Y-m-d H:i:s');
            $auto->modified = date('Y-m-d H:i:s');
            $auto->member_id = $userSession['id'];
            $auto->street = $this->request->getPost("location");
            $auto->name = $this->request->getPost('name');
            $auto->details = $detail;
            $auto->price = $this->request->getPost("price");
            $auto->brand = $this->request->getPost("brand");
            $auto->model = $this->request->getPost("model");
            $auto->currency_type = $this->request->getPost("currency_type");
            $auto->year_model = $this->request->getPost("year_model");
            $auto->condition_id = $this->request->getPost("condition");
            $auto->body_type = $this->request->getPost("body_type");
            $auto->transmission = $this->request->getPost("transmission");
            $auto->mileage = $this->request->getPost("mileage");
            $auto->fuel_type_id =$this->request->getPost("fuel_type");
            $auto->contact_no = $this->request->getPost("contact_no");
            $auto->email = $this->request->getPost("email");
            $auto->other_name =$this->request->getPost("other_name");

            $auto->country_code_id = $this->request->getPost("country_id");
            $auto->state = $this->request->getPost("state");
            $auto->city =$this->request->getPost("city");

            if ($auto->create()) {

             $id = $auto->id;
                if($this->request->hasFiles() == true) {

                   

                    // $this->folderOrganizer('classfieds', $id);
                    $uploads = $this->request->getUploadedFiles();

                    // upload default to false
                    $isUploaded = false;

                    //allowed file extension
                    $fileImageExt = array('jpeg', 'jpg', 'png');

                    $filePath = 'img/auto/';

                    $fileType = '';

                    foreach ($uploads as $upload) {

                        $fileName = $upload->getname() ;

                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        // filename convert to md5
                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'-car.'.$fileExt;

                        $path = NULL;

                        //
                        if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/auto/'.$newFileName;
                        }

                       //move the file and simultaneously check if everything was ok
                       ($path != NULL OR !empty($path)) ? $isUploaded = true : $isUploaded = false;

                        if ($isUploaded) {
                            $thingPhotos = new AutomotivePhotos();
                            $thingPhotos->created = date('Y-m-d H:i:s');
                            $thingPhotos->modified = date('Y-m-d H:i:s');
                            $thingPhotos->member_id = $userSession['id'];
                            $thingPhotos->automotive_id = $id;
                            $thingPhotos->file_path = $filePath;
                            $thingPhotos->filename = $newFileName;
                           
                            if ($thingPhotos->create()) {
                                $upload->moveTo($path);
                            } else {
                                $this->session->set('errorMessasge', 'failed add image');
                                //print_r($thingPhotos->getMessages());
                            }
                        }

                    }

            
                }

                $this->session->set('successMessasge', 'Information successfully added');
                return $this->response->redirect('car_and_truck/view2/'.$auto->id);
           // echo json_encode(array('status' => 'success', 'message' => 'nice', 'id' => $auto->id));
           }else{

                foreach ($auto->getMessages() as  $error) {
                    $err .= $error->getMessage() .'</br>';
                }
                 $this->session->set('errorMessasge', $err);

           }

        }

      $this->view->setVars([
        'carConditions' => AutomotiveConditions::find(),
        'carFuels' => AutomotiveFuels::find()
      ]);


    }//end of function New


    /**
     * [delete_photoAction description]
     * @param  [type] $id     [description]
     * @param  [type] $itemid [description]
     * @return [type]         [description]
     */
    public function delete_photoAction($id = null, $itemid = null){

        $photo = AutomotivePhotos::findFirst($id);
        $itemid = $photo->automotive_id;
        $trashImg = 'img/auto/'.$photo->filename;
        unlink($trashImg);
        
        if ($photo->delete()) {
            $this->session->set('successMessasge', 'Photo Succesfuly Deleted');

        }else{
            $this->session->set('successMessasge', 'Error can\'t delete');
        }
        $this->response->redirect('car_and_truck/update/'.$itemid);

    }



    public function viewAction($id = null)
    {
      $auto = Automotives::findFirst($id);

      if (!$auto) {
        return $this->response->redirect('car_and_truck/index');
      }

      $this->view->setVar('auto', $auto);
    }
    

    /**
     * [view2Action description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function view2Action($id = null) {

     $this->view->setTemplateAfter('default1');
      $auto = Automotives::findFirst($id);

      if (!$auto) {
        return $this->response->redirect('car_and_truck/index');
      }

      $this->view->setVar('auto', $auto);
    }

    /**
     * [updateAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function updateAction($id = null){   

         $this->view->setTemplateAfter('default1');

        if ($this->request->isPost()) {
            


            $this->session->get("userSession");
            $messages = $this->formValidate();
            if (count($messages)) {
                    $errorMsg = '';
                foreach ($messages as $msg) {
                  $errorMsg .= $msg . "<br>";       
                }
            $this->session->set('errorMessasge', $errorMsg);
               // return $this->response->redirect('car_and_truck/new/'); 
            } 


            $userSession = $this->session->get("userSession");
            $id = $this->request->getPost('id');
            $auto = Automotives::findFirst($id);
            $auto->name = $this->request->getPost("name");
            $auto->details = $this->request->getPost("details");
            $auto->price = str_replace(',', '', $this->request->getPost('price'));
            $auto->brand = $this->request->getPost("brand");
            $auto->model = $this->request->getPost("model");
            $auto->year_model = $this->request->getPost("year_model");
            $auto->condition_id = $this->request->getPost("condition");
            $auto->body_type = $this->request->getPost("body_type");
            $auto->transmission = $this->request->getPost("transmission");
            $auto->mileage = $this->request->getPost("mileage");
            $auto->fuel_type_id = $this->request->getPost("fuel_type");
            $auto->currency_type = $this->request->getPost("currency_type");
            $auto->contact_no = $this->request->getPost("contact_no");
            $auto->email = $this->request->getPost("email");
            $auto->other_name =$this->request->getPost("other_name");


            $auto->country_code_id = $this->request->getPost("country_id");
            $auto->state = $this->request->getPost("state");
            $auto->city =$this->request->getPost("city");


                if($auto->update()){
         
                   $this->session->set('successMessasge', 'Successful Update');
                }else{

                    foreach ($auto->getMessages() as $error) {
                       $errorMsg .= $error->getMessage() .'</br>';
                    }
                    $this->session->set('errorMessasge', $errorMsg);
                }  
       



            }
        //$this->view->disable();
      $auto = Automotives::findFirst($id);
      $this->view->setVar('auto', $auto);

      $autoPhotos = AutomotivePhotos::find("automotive_id = $id");
      $this->view->setVar('autoPhotos', $autoPhotos);
    }


    public function update_photoAction($id = null){
      

         $this->view->disable();
        if ($this->request->isPost()) {


                if($this->request->hasFiles() == true) {


                    $userSession = $this->session->get("userSession");
                    // $this->folderOrganizer('classfieds', $id);
                    $uploads = $this->request->getUploadedFiles();

                    // upload default to false
                    $isUploaded = false;

                    //allowed file extension
                    $fileImageExt = array('jpeg', 'jpg', 'png');

                    $filePath = 'img/auto/';

                    $fileType = '';

                    foreach ($uploads as $upload) {

                        $fileName = $upload->getname() ;

                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        // filename convert to md5
                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'-car.'.$fileExt;

                        $path = NULL;

                        //
                        if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/auto/'.$newFileName;
                        }

                       //move the file and simultaneously check if everything was ok
                       ($path != NULL OR !empty($path)) ? $isUploaded = true : $isUploaded = false;

                        if ($isUploaded) {
                            $thingPhotos = new AutomotivePhotos();
                            $thingPhotos->created = date('Y-m-d H:i:s');
                            $thingPhotos->modified = date('Y-m-d H:i:s');
                            $thingPhotos->member_id = $userSession['id'];
                            $thingPhotos->automotive_id = $id;
                            $thingPhotos->file_path = $filePath;
                            $thingPhotos->filename = $newFileName;
                           
                            if ($thingPhotos->create()) {
                                $upload->moveTo($path);

                            } else {
                                $this->session->set('errorMessasge', 'Uploading Error');
                                //print_r($thingPhotos->getMessages());
                            }
                        }

                    }

                  
                }

            $this->response->redirect('car_and_truck/update/'.$id);
        }else{
            $this->response->redirect('car_and_truck/update/'.$id);
        }

    }



    public function listAction($link = null){


        $userSession = $this->session->get("userSession");
        $member_id = $userSession['id'];
        if($link == null){
            $this->view->setVar('idClass', '1');
        }else{
            $this->view->setVar('idClass', $link);
        }

        $this->view->act_auto = '';
        $this->view->act_real = '';
        $this->view->act_job = '';
        $this->view->act_thing = '';

        switch ($link) {
            case 1:
                $this->view->autos = Automotives::find("member_id = '$member_id' ");
                $this->view->act_auto = 'active';
                break;
            case 2:
                $this->view->jobs = Jobs::find("member_id = '$member_id' ");
                $this->view->act_job = 'active';
                break;
            case 3:
                $this->view->property = Realties::find("member_id = '$member_id' ");
                $this->view->act_real = 'active';
                break;

            case 4:
                $this->view->things = Things::find("member_id = '$member_id' ");
                $this->view->act_thing = 'active';
                break;
            default:
                $this->view->autos = Automotives::find("member_id = '$member_id' ");
                $this->view->act_auto = 'active';
                break;
        }


    }


    public function delete_listAction($id = null, $link = null){


        // $photo = AutomotivePhotos::findFirst($id);
       
        // $trashImg = 'img/auto/'.$photo->filename;
        // unlink($trashImg);
        
        // if ($photo->delete()) {
        //     $this->session->set('successMessasge', 'Photo Succesfuly Deleted');
        // }else{
        //     $this->session->set('successMessasge', 'Error can\'t delete');
        // }
        // $this->response->redirect('car_and_truck/update/'.$itemid);


        if($id != null Or $link != null){

            switch ($link) {

              case 1:
                    $query = Automotives::find($id);

                    // $idphoto = $query->AutomotivePhotos->id;
                    $name = $query->name;
                    if ($query->delete()) {

                        // $photo = AutomotivePhotos::findFirst($idphoto);
                        // $trashImg = 'img/auto/'.$photo->filename;
                        // unlink($trashImg);

                        // if ($photo->delete()) {
                        //     // $this->session->set('successMessasge', $name.'Photo Succesfuly Deleted');
                        // }else{
                        //     //$this->session->set('errorMessasge', 'error');
                        // }
                        $this->session->set('successMessasge', $name.'succesfully deleted');
                    }else{
                        $this->session->set('errorMessasge', 'A problem has been occurred while deleting your data');
                    }
                    $this->response->redirect('car_and_truck/list/1');
                    break;
                case 2:
                    $query = Jobs::find($id);

                    // $idphoto = $query->JobLogos->id;

                    if ($query->delete()) {

                        // $photo = JobLogos::findFirst($idphoto);
                        // $trashImg = 'img/jobs/'.$photo->filename;
                        // unlink($trashImg);

                        // if ($photo->delete()) {
                        //     // $this->session->set('successMessasge', $name.'Photo Succesfuly Deleted');
                        // }else{
                        //    // $this->session->set('errorMessasge', 'error');
                        // }

                        $this->session->set('successMessasge', 'succesfully deleted');
                    }else{
                        $this->session->set('errorMessasge', 'A problem has been occurred while deleting your data');
                    }
                    $this->response->redirect('car_and_truck/list/2');
                    break;
                case 3:
                    $query = Realties::find($id);

                    // $idphoto = $query->RealtyPhotos->id;

                    if ($query->delete()) {

                        // $photo = RealtyPhotos::findFirst($idphoto);

                        // $trashImg = 'img/realties/'.$photo->filename;
                        // unlink($trashImg);

                        // if ($photo->delete()) {
                        //     // $this->session->set('successMessasge', $name.'Photo Succesfuly Deleted');
                        // }else{
                        //     //$this->session->set('errorMessasge', 'error');
                        // }

                        $this->session->set('successMessasge', 'succesfully deleted');
                    }else{
                        $this->session->set('errorMessasge', 'A problem has been occurred while deleting your data');
                    }
                    $this->response->redirect('car_and_truck/list/3');
                    break;

                case 4:
                    $query = Things::find($id);
                    
                    // $idphoto = $query->ThingPhotos->id;

                    if ($query->delete()) {
 
                        // $photo = ThingPhotos::findFirst($idphoto);

                        // $trashImg = 'img/thing/'.$photo->filename;
                        // unlink($trashImg);

                        // if ($photo->delete()) {
                        //     // $this->session->set('successMessasge', $name.'Photo Succesfuly Deleted');
                        // }else{
                        //     //$this->session->set('errorMessasge', 'error');
                        // }


                        $this->session->set('successMessasge', 'succesfully deleted');
                    }else{
                        $this->session->set('errorMessasge', 'A problem has been occurred while deleting your data');
                    }
                    $this->response->redirect('car_and_truck/list/4');
                    break;
                default:
                $this->session->set('errorMessasge', 'Error please try again');
                $this->response->redirect('car_and_truck/list/1');
                break;
            }


        }

    }




}

