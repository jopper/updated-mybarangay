<?php

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Mvc\Model\Criteria;



class EventController extends ControllerBase 
{


    public function initialize(){
        parent::initialize();
    }
    public function sampleAction(){

         // This is an Ajax response so it doesn't generate any kind of view
         $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
            
 $cities = Cities::find(['limit' => '10']);
foreach ($cities as $city) {
 $array[] =  $city->city_name;
}
echo json_encode($array);

            


    }

  public function getCitiesAction() {
        if ($this->request->isAjax()) {
            $arrCities = array();
             $stateCode = $this->request->getQuery('param1');
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }
            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }





    public function getStatesAction() {


        if ($this->request->isAjax()) {
            $arrStates = array();
            $countryCode = $this->request->getQuery('param');


            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

          

            return $response;
        }
    }


    /**
     * [flagAction description]
     * @return [type] [description]
     */
    public function flagAction(){

        $this->view->disable();

        if($this->request->isAjax()) 
        {
            $review = $this->request->getQuery('review');
            $reviewer = $this->request->getQuery('reviewer');
            $flagger = $this->request->getQuery('flagger');
            $flag_type = $this->request->getQuery('flag_type');
            $referer = $this->request->getHTTPReferer (); 
            $controller = 'classifieds';
            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }
            $arrayName = array('review' => $review, 'reviewer' => $reviewer, 'flagger' => $flagger, 'result' => $resultInfo);
            echo json_encode(array('result' => 'OK', 'items' => $arrayName));
        }

    }



    public function formValidate() {
        $validation = new Phalcon\Validation();
        $validation->add('name', new PresenceOf(array(
            'message' => 'name field is required'   
        )));
        // $validation->add('website', new PresenceOf(array(
        //     'message' => 'website field is required'    
        // )));
        $validation->add('telephone', new PresenceOf(array(
            'message' => 'telephone field is required'  
        )));
        $validation->add('event_info', new PresenceOf(array(
            'message' => 'event information field is required'  
        )));
        $validation->add('street', new PresenceOf(array(
            'message' => 'street field is required' 
        )));
        // $validation->add('city', new PresenceOf(array(
        //     'message' => 'city field is required'   
        // )));
        // $validation->add('country', new PresenceOf(array(
        //     'message' => 'city field is required'   
        // )));
        $validation->add('category', new PresenceOf(array(
            'message' => 'category field is required'   
        )));
        return $validation->validate($_POST);
    
    }


    /**
     * [loadMoreEvent description]
     * @return [type] [description]
     */
    public function load_ajaxAction(){

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        //if ajax request run
        if ($this->request->isAjax()) {


            $id = $this->request->getQuery('id');
        // Events display order by descending 
            $events = Events::find([
            'conditions' => 'id < :id:',
            'bind' => array('id' => $id),
            'order' => 'id desc',
            'limit' => 6
            ]);

            if($events){
                $i = 1;

                $cc = count($events);

                foreach ($events as $event): 

              ?>
         
            <!-- 1st item events-->
                 
            <div class="item-list make-grid">
                            <div class="col-sm-2 no-padding photobox">
                                <div class="add-image custom-thumbnail" style=""> <!-- <span class="photo-count"><i class="fa fa-camera"></i> 2 </span> --> 
                                <?php                    
                                $eventPhoto = 'img/nophoto.jpg'; 
                                $rPhotoCount = count($event->EventPhotos);
                                if ($rPhotoCount > 0) {
                                $eventPhoto = $event->eventphotos[0]->file_path.$event->eventphotos[0]->filename;
                                }
                                echo Phalcon\Tag::image([$eventPhoto, 'class' => 'thumbnail no-margin', 'style' => 'border:none;padding:0px']);
                                ?>
                                </div>
                            </div>

                            <div class="col-sm-7 add-desc-box" >
                                <div class="add-details">
                                   <h4 class="add-title" style="font-weight:bold;white-space: nowrap; width: 100%; overflow: hidden;text-overflow: ellipsis; z-index:10; " data-toggle="tooltip" title="<?php echo $event->name;?> " > 
                                        <?php echo Phalcon\Tag::linkTo(array('event/view2/'.$event->id, $event->name, 'class' => ''));  ?>
                                    </h4>

                    <blockquote >
                        <span class="info-row">
                        <?php
                            $category = EventsCategories::findFirst($event->event_category_id);
                            echo '<i class="fa fa-bookmark-o"></i> '.$category->name;
                            echo '<div class="clearfix"></div>';
                            echo '<i class="fa fa-phone"></i> '.$event->telephone;
                        ?>
                        
                        </span> 
                    </blockquote>


                                </div>
                            </div>

            <div class="col-sm-3 text-right  price-box" style="padding-bottom:10px;">
                <!-- start of media -->

                
                <div class="media" style="border-bottom:1px solid #ddd;border-top:1px solid #ddd;margin:5px 0px;padding-top:5px">
  

                    <div class="media-body">
                        <div class="media-heading">
                        <small class="info-row">
                            <span class="fa fa-map-marker"> </span>
                                <?php 
                                    $ecity = '';
                                    $cities = Cities::findFirst($event->city);
                                    if($cities){
                                        $ecity .= $cities->city_name;
                                        $ecity .= ', '. $cities->state_code_id;
                                        $ecity .= ', '.$cities->CountryCodes->country_name;
                                        echo $ecity;
                                    }else{
                                        echo 'no address';
                                    }
                                ?>
                        </small>
                        </div>
                    </div>
                </div><!-- end of media -->

                
                <a class="btn btn-primary  btn-sm make-favorite" href="<?php echo $this->url->getBaseUri() . 'event/view2/'.$event->id; ?>"> 
                    <i class="fa fa-star"></i> <span></span> View
                </a>
            </div><!-- end price-box -->




                        </div>

                  <!-- 1st item items events-->
                <?php endforeach; ?>

        <?php  if($cc > 6){ ?>
            <div class="row">
            <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
                        <div class="show_more_main" id="show_more_main<?php echo $event->id; ?>">
                            <span id="<?php echo $event->id; ?>" class="btn btn-primary show_more btn-block" title="Load more posts">Show more</span>
                            <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
                        </div>
            </div>
            </div>
            </div>
        <?php  } ?>
 
        <?php
            }
        }

        
    }


    /**
     * 
     * [indexAction description]
     * @return [type]
     */
    public function indexAction() {

        // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        // set breadcrum for page identification
        $this->view->setVar('breadcmp', 'Events');

        // EventsCategories and Countries  database select all
        // $categories = EventsCategories::find();
        // $countries  = Countries::find();

        // //set vars
        // $this->view->setVars([
        //     'categories' => $categories,
        //     'countries' => $countries
        // ]);

       $event_type = '';
        $name = '';
        $ecity = '';
        $city_code = '';
        $cc = '';
        $trig = '';
        // Form Events (if Find Event click search [post == true] run this program)
        if ($this->request->isPost()) {


            $keyword = $this->request->getPost("name");
            $city_code = $this->request->getPost("city_code");
            $event_type = $this->request->getPost("event_type");


                $name = $keyword;
                if($city_code != ''){
                    $cities = Cities::findFirst($city_code);
                    $ecity .= $cities->city_name;
                    $ecity .= ', '. $cities->state_code_id;
                    $ecity .= ', '.$cities->CountryCodes->country_name;
                }
                if(!empty($keyword)) {
                    $conditions .= ' name LIKE :name:';
                    $bind['name'] = '%'.$keyword.'%';
                }


                $conditions .= ($keyword != '' AND $city_code != '')? 'AND' : '' ;
                if(!empty($city_code)) {
                    $conditions .= ' city LIKE :city:';
                    $bind['city'] = '%'.$city_code.'%';
                }

                $parameters= ['conditions' => $conditions, 'bind' => $bind,'order' => 'id DESC', 'limit' => '20'];

                $qlimit = Events::count(['conditions' => $conditions, 'bind' => $bind]);
                if($qlimit < 20){
                    $trig = 1;
                }



            // if($event_type == 2){
            //    $ticket  =     EventTickets::find(" price = 'F' " );
               
            //    $eventss   =     Events::find(" id = ".$ticket->event_id);
            //    echo '1';
            // }elseif($event_type == 3){
            //    $ticket  =    EventTickets::find(" price !=  'F' " );
            //    $eventss   =    Events::find($ticket->event_id);
            //    echo '1';
            // }else{
            // }
            // die;

        }else{
                    // Events display order by descending 
             $parameters = array('order' => 'id DESC', 'limit' => '12');
            
        }

  //       if($event_type == 2 OR $event_type == 3){
  //           $event = $eventss;
  //       }else{
  // }
         $event = Events::find($parameters);
      
        $cc = count($event);
        $this->view->setVars([
            'events' => $event,
            'ename' => $name,
            'ecity' => $ecity,
            'ecity_code' => $city_code,
            'counter' => $cc,
            'trigger' => $trig,
            'event_type' => $event_type
        ]);




    }//end of indexAction


    public function eventTypeAction($id = null) {


       $event_type = '';
        if ($this->request->isPost()) {
            $event_type = $this->request->getPost("event_type");

            if($event_type == 2){
               $ticket  =     EventTickets::find(" price =  F" );
               $event   =     Events::find($ticket->event_id);
            }elseif($event_type == 3){
               $ticket  =    EventTickets::find(" price !=  F" );
               $event   =    Events::find($ticket->event_id);
            }else{

            }

        }
    }


    public function viewAction($id = null) {

        // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        
        $event = Events::findFirst($id);

        if (!$event) {
            return $this->response->redirect('event/index');
        }

        $this->view->setVar('event', $event);
    }

    public function view2Action($id = null, $name = null) {
        // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        
        $event = Events::findFirst($id);
        if (!$event) {
            return $this->response->redirect('event/index');
        }
        $ticket = EventTickets::find([
            'conditions' => 'event_id = :eid:',
            'bind'  => array('eid' => $id)
        ]);
        $this->view->setVars([
            'event'     => $event,
            'tickets'   => $ticket

        ]);
    }
    
    public function view_directionAction($id = null, $name = null){
        $this->view->setTemplateAfter('default1');
        
        $event = Events::findFirst($id);
        if (!$event) {
            return $this->response->redirect('event/index');
        }
        $this->view->setVar('event', $event);
    }




    public function newAction() {
        $this->view->setTemplateAfter('default1');



        if ($this->request->isPost()) {
   
    

        // $messages = $this->formValidate();
        // if (count($messages)) {
        //  $this->view->disable();
        //  $errorMsg = '';
        //  foreach ($messages as $msg) {
        //    $errorMsg .= $msg . "<br>";       
        //  }
        //     $this->session->set('errorMessasge', $errorMsg);
        //  return $this->response->redirect('event/new/'); 
        // } 



//                 $i = 0;
//                 foreach ($dates as $date) {

// echo $date.$event_start[$i].$event_end[$i]."</br>";

//       $i++;
                    // $eventDatetime = new EventDatetimes();
                    // $eventDatetime->event_id = $id;
                    // $eventDatetime->date = $date;
                    // $eventDatetime->from_time = $fromTimes[$i];
                    // $eventDatetime->to_time = $toTimes[$i];
                    // $i++;

                    // if (!$eventDatetime->create()) {
                    //     $this->view->disable();

                    //     echo "failed to insert event date and time in event_datetimes table. :(";
                    // }
                // }
            // address 
            $street = $this->request->getPost("street");
            $country_id = $this->request->getPost("country_id");
            $state = $this->request->getPost("state");
            $city = $this->request->getPost("city");

            //event information
            $userSession = $this->session->get("userSession");
            $eventName = $this->request->getPost("name");
            $website   = $this->request->getPost("website");
            $telephone = $this->request->getPost("telephone");
            $info      = $this->request->getPost("event_info");
            $category  = $this->request->getPost("category");

            // map location
            $latitude = $this->request->getPost("lat");
            $longitude = $this->request->getPost("lng");


            // schedule
            $dates = $this->request->getPost("date");
            $event_start = $this->request->getPost("event_start");
            $event_end = $this->request->getPost("event_end");


            $event = new Events();
            $event->created = date('Y-m-d H:i:s');
            $event->modified = date('Y-m-d H:i:s');
            $event->member_id = $userSession['id'];
            $event->name = $eventName;
            // $event->event_date = $this->request->getPost("date");
            $event->website = $website;
            $event->telephone = $telephone;
            $event->event_category_id = $category;
            $event->lng = $longitude;
            $event->lat = $latitude;
            $event->eventinfo = $info;

            $event->street = $street;
            $event->city = $city;
            $event->country_code_id = $country_id;
            $event->state = $state;

            if ($event->create()) {

               $i = 0;
                foreach ($dates as $date) {

                    $eventD = new EventDatetimes();
                    $eventD->event_id = $event->id;
                    $eventD->date = $date;
                    $eventD->from_time = $event_start[$i];
                    $eventD->to_time = $event_end[$i];
                   $i++;
                    if (!$eventD->create()) {
                        $this->session->set('errorMessasge', 'failed to insert event date and time');
                    }

                }

                if($this->request->hasFiles() == true) {
                    // $this->folderOrganizer('classfieds', $id);
                    $uploads = $this->request->getUploadedFiles();

                    // upload default to false
                    $isUploaded = false;

                    //allowed file extension
                    $fileImageExt = array('jpeg', 'jpg', 'png');

                    $filePath = 'img/event/';

                    $fileType = '';

                    foreach ($uploads as $upload) {

                        $fileName = $upload->getname() ;

                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        // filename convert to md5
                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'-evt.'.$fileExt;

                        $path = NULL;

                        //
                        if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/event/'.$newFileName;
                        }

                       //move the file and simultaneously check if everything was ok
                       ($path != NULL OR !empty($path)) ? $isUploaded = true : $isUploaded = false;

                        if ($isUploaded) {
                            $thingPhotos = new EventPhotos();
                            $thingPhotos->created = date('Y-m-d H:i:s');
                            $thingPhotos->modified = date('Y-m-d H:i:s');
                            $thingPhotos->member_id = $userSession['id'];
                            $thingPhotos->event_id = $event->id;
                            $thingPhotos->file_path = $filePath;
                            $thingPhotos->filename = $newFileName;
                            $eventPhotos->caption = 'caption';
                            if ($thingPhotos->create()) {
                                $upload->moveTo($path);
                            } else {
                                $this->session->set('errorMessasge', 'failed add image');
                                //print_r($thingPhotos->getMessages());
                            }
                        }

                    }

            
                }




                
                $this->response->redirect('event/view/'.$event->id);
            }else{
                foreach ($event->getMessages() as  $error) {
                    $errorMsg .= $error->getMessage()."</br>";
                }
               $this->session->set('errorMessasge', $errorMsg);
            }

            


           // return $this->response->redirect('event/view/'.$event->id);
        }

        $events = Events::find();
        $categories = EventsCategories::find();
        $countries = Countries::find();
        $this->view->setVar('events', $events);
        $this->view->setVars([
            'events' => $events,
            'categories' => $categories,
            'countries' => $countries
        ]);
    }

   public function updateAction($id = null) {
        $this->view->setTemplateAfter('default1');

        $path = 'img/event/';

        if ($this->request->isPost()) {

          $userSession = $this->session->get("userSession");
                if ($this->request->hasFiles() == true){
                    set_time_limit(1200);
                    $uploads = $this->request->getUploadedFiles();

                    $isUploaded = false;

                    foreach($uploads as $upload) {
                        $imageName = $upload->getKey(); // $this->request->getPost('image', $ctr);
                        $inputRow = str_replace('image', '', $imageName);

                        $checker = EventPhotos::findFirst("event_id = '$id' ");

                        $unlink = $checker->filename;
                        # define a "unique" name and a path to where our file must go
                        $fileName = $upload->getName();
                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'ext1.'.$fileExt;
                            //$fileExt = $upload->getExtension();
                        $fileImageExt = array('jpeg', 'jpg', 'png');
                        $fileType = '';
                        $filePath = '';
                        $path = '';
                         if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/event/'.$newFileName;
                                $filePath = 'img/event/';
                                //$fileType = 'Image';
                        }

                        #move the file and simultaneously check if everything was ok
                        ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;

                        if($isUploaded) {
                                if($checker){
                                    $realtyPhotos = EventPhotos::findFirst("event_id = '$id' ");
                                    $realtyPhotos->modified = date('Y-m-d H:i:s'); 
                                    $realtyPhotos->member_id =  $userSession['id'];
                                    $realtyPhotos->file_path =  $filePath;
                                    $realtyPhotos->filename =  $newFileName;
                                    if($realtyPhotos->update()){
                                        unlink($filePath.$unlink);
                                    }

                                } else {
                                    $realtyPhotos = new EventPhotos();
                                    $realtyPhotos->created = date('Y-m-d H:i:s'); 
                                    $realtyPhotos->modified = date('Y-m-d H:i:s'); 
                                    $realtyPhotos->member_id =  $userSession['id'];
                                    $realtyPhotos->event_id =  $id;
                                    $realtyPhotos->file_path =  $filePath;
                                    $realtyPhotos->filename =  $newFileName;
                                    $realtyPhotos->create();
                                }
                            } // isUploaded
                    } // foreach uploads

                } // has files

            $messages = $this->formValidate();
            if (count($messages)) {
                $this->view->disable();
                $errorMsg = '';
                foreach ($messages as $msg) {
                  $errorMsg .= $msg . "<br>";       
                }
                   $this->session->set('errorMessasge', $errorMsg);
               // return $this->response->redirect('event/'); 
            } 
            $eventdateid = $this->request->getPost("eventdateid");
            $date = $this->request->getPost("date");
            $event_start = $this->request->getPost("event_start");
            $event_end = $this->request->getPost("event_end");





            $c = count($eventdateid);
            for ($i=0; $i < $c; $i++) { 
                $eventdateid[$i];
                $eventdates = EventDatetimes::findFirst($eventdateid[$i]);
                $eventdates->date = $date[$i];
                $eventdates->from_time = $event_start[$i];
                $eventdates->to_time = $event_end[$i];
                $eventdates->update();
            }

            $userSession = $this->session->get("userSession");

            $event = Events::findFirst($id);
            $event->modified = date('Y-m-d H:i:s');
            $event->member_id = $userSession['id'];
            $event->name = $this->request->getPost("name");
            $event->event_date = $this->request->getPost("date");
            $event->website = $this->request->getPost("website");
            $event->telephone = $this->request->getPost("telephone");
            $event->street = $this->request->getPost("street");
            $event->state = $this->request->getPost("state");
            $event->city = $this->request->getPost("city");
            $event->country_code_id = $this->request->getPost("country_id");
            $event->event_category_id = $this->request->getPost("category");
            $event->eventinfo = $this->request->getPost("event_info");

            if ($event->update()) {
         
                return $this->response->redirect('event/update/'.$id);
            } else {
            

                print_r($event->getMessages());
                    $errorMsg = '';
                foreach ($event->getMessages() as  $error) {
                    $errorMsg .= $error->getMessage()."</br>";
                }
               $this->session->set('errorMessasge', $errorMsg);

            }

          //  $this->view->disable();
        }



        $event = Events::findFirst($id);
        $this->view->setVar('event', $event);
        $eventPhotos = EventPhotos::find("event_id = $id");
        $this->view->setVar('eventPhotos', $eventPhotos);

    }


    public function searchAction() {
        // get latitude and longitude via google maps api service. 
        $request =$this->request;
        $this->view->disable();
        $response = new \Phalcon\Http\Response();

        $res = $_GET["location"]; // $_GET parameter
        $address = urlencode($res);
                    
        $Response = array('success' => $res);
        $url = "https://maps.google.com/maps/api/geocode/json?sensor=true&address=$address&key=AIzaSyBSR4ZUyKhxVcN3eMNzQWnm8YSdP-KE8uM";
        $Res = file_get_contents($url);
        $resp = json_decode($Res, true);
        $lat = $resp['results'][0]['geometry']['location']['lat'];
        $lng = $resp['results'][0]['geometry']['location']['lng'];
        $res = array("lat" => $lat, "lng"=> $lng );
        $response->setContent(json_encode($res));
        $response->setContentType('application/json', 'UTF-8');
        return $response;
    }



    public function listAction() {

        $userSession = $this->session->get("userSession");
        $member_id = $userSession['id'];
        $events = Events::find("member_id = '$member_id' ");
        $this->view->setVar('events', $events);
    }




    public function delete_listAction($id = null){

         $query = Events::find($id);
        // $idphoto = $query->AutomotivePhotos->id;
        $name = $query->name;
        if ($query->delete()) {

            $this->session->set('successMessasge', $name.'succesfully deleted');
        }else{
            $this->session->set('errorMessasge', 'A problem has been occurred while deleting your data');
        }
        $this->response->redirect('event/list');
    }




    public function new2Action(){
        $this->view->setTemplateAfter('default1');
        
        $events         = Events::find();
        $categories     = EventsCategories::find();
        $countries      = Countries::find();

        $this->view->setVars([
            'events'        => $events,
            'categories'    => $categories,
            'countries'     => $countries
        ]);

        if ($this->request->isPost()) {


            $userSession = $this->session->get("userSession");

            $event = new Events();
            $event->created             = date('Y-m-d H:i:s');
            $event->modified            = date('Y-m-d H:i:s');
            $event->member_id           = $userSession['id'];
            $event->name                = $this->request->getPost("name");
            $event->website             = $this->request->getPost("website");
            $event->telephone           = $this->request->getPost("telephone");
            $event->event_category_id   = $this->request->getPost("category");
            $event->eventinfo           = $this->request->getPost("event_info");
            $event->location            = $this->request->getPost("location");
            $event->organizer_name      = $this->request->getPost("organizer_name");
            $event->organizer_desc      = $this->request->getPost("organizer_desc");
            $event->lat                 = $this->request->getPost("lat");
            $event->lng                 = $this->request->getPost("lng");

            $event->street              = $this->request->getPost("street");
            $event->city                = $this->request->getPost("city");
            $event->country_code_id     = $this->request->getPost("country_id");
            $event->state               = $this->request->getPost("state");
            $event->status                = 1;

            if($event->create()){


                $mpic = new EventPhotos();
                $mpic->created      = date('Y-m-d H:i:s');
                $mpic->modified     = date('Y-m-d H:i:s');
                $mpic->file_path    = 'img/';
                $mpic->filename     = 'defaultImage.jpg';
                $mpic->member_id    =  $event->member_id;
                $mpic->event_id     =  $event->id;
                $mpic->primary_pic  = 'Yes';
                $mpic->caption      = 'caption Event';
                $mpic->create();


            $ticketArray    = $this->request->getPost("tktname");
            $ticketType     = $this->request->getPost("ticketType");
            $qtyname        = $this->request->getPost("qtyname");
            $tktprice       = $this->request->getPost("tktprice");
            $currency_type  = $this->request->getPost("currency_type");  


            $i = 0;
            if(!empty($ticketArray)){
                foreach ($ticketArray as $ticket) {
                    $tickets = new EventTickets();
                    $tickets->created_at    = date('Y-m-d H:i:s');
                    $tickets->modified_at   = date('Y-m-d H:i:s');
                    $tickets->event_id      = $event->id;
                    $tickets->member_id     = $userSession['id'];
                    $tickets->name          = $ticket;
                    $tickets->quantity      = $qtyname[$i];
                    $tickets->price         = $tktprice[$i];
                    $tickets->currency_type = $currency_type[$i];
                    if($tickets->create()){
                    }else{
                        $err = '';
                        foreach ($tickets->getMessages() as $errno) {
                            $err .= $errno->getMessage(). '</br>';
                        }
                        echo $err;
                        // die;
                    }           
                    $i++;
                }
            }
  
            // schedule
            $datesArray     = $this->request->getPost("date");
            $event_start    = $this->request->getPost("event_start");
            $event_end      = $this->request->getPost("event_end");
            $ix = 0;
            if(!empty($datesArray)){
                foreach ($datesArray as $date) {
                    $eventD             = new EventDatetimes();
                    $eventD->event_id   = $event->id;
                    $eventD->date       = $date;
                    $eventD->from_time  = $event_start[$ix];
                    $eventD->to_time    = $event_end[$ix];
                   $ix++;
                    if (!$eventD->create()) {
                        $this->session->set('errorMessasge', 'failed to insert event date and time');
                    }
                }
            }
                $this->session->set('successMess', 'Event successfully Create. Next step add Event photo');
                $name = str_replace(',', '+', $event->name);
                return $this->response->redirect('event/update2/'.$event->id.'/'.$name);
            }else{
                $errno = '';
                foreach ($event->getMessages() as $err) {
                    $errno .= $err->getMessage(). '</br>';
                }
                $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
                return $this->response->redirect('event/new2');
            }
        }

    }




    public function update2Action($id, $name = null){
        $this->view->setTemplateAfter('default1');

        $event          = Events::findFirstById($id);
        $userSession    = $this->session->get("userSession");
        if(!$event OR $event->member_id != $userSession['id']){
            return $this->response->redirect('event/index');
        }


        if ($this->request->isPost()) {

            $event->created             = date('Y-m-d H:i:s');
            $event->modified            = date('Y-m-d H:i:s');
            $event->member_id           = $userSession['id'];
            $event->name                = $this->request->getPost("name");
            $event->website             = $this->request->getPost("website");
            $event->telephone           = $this->request->getPost("telephone");
            $event->event_category_id   = $this->request->getPost("category");
            $event->eventinfo           = $this->request->getPost("event_info");
            $event->location            = $this->request->getPost("location");
            $event->organizer_name      = $this->request->getPost("organizer_name");
            $event->organizer_desc      = $this->request->getPost("organizer_desc");
            $event->ticket_desc         = $this->request->getPost("ticket_desc");
            $event->check_desc          = $this->request->getPost("check_desc");
            $event->check_ticket_type   = $this->request->getPost("check_ticket_type");
            $event->min                 = $this->request->getPost("min");
            $event->max                 = $this->request->getPost("max");

            $event->street              = $this->request->getPost("street");
            $event->city                = $this->request->getPost("city");
            $event->country_code_id     = $this->request->getPost("country_code");
            $event->state               = $this->request->getPost("state");


            $event->status                = 1;

            if($event->update()){

            $eventticketid  = $this->request->getPost("eventticketid");
            $ticketArray    = $this->request->getPost("tktname");
            $ticketType     = $this->request->getPost("ticketType");
            $qtyname        = $this->request->getPost("qtyname");
            $tktprice       = $this->request->getPost("tktprice");
            $currency_type  = $this->request->getPost("currency_type");  
                    

            if(!empty($eventticketid)){
                $i = 0;
                foreach ($eventticketid as $ticketid) {
                    if($ticketid == 'NEW'){
                        $tickets = new EventTickets();
                        $tickets->created_at    = date('Y-m-d H:i:s');
                        $tickets->modified_at   = date('Y-m-d H:i:s');
                        $tickets->event_id      = $event->id;
                        $tickets->member_id     = $userSession['id'];
                        $tickets->name          = $ticketArray[$i];
                        $tickets->quantity      = $qtyname[$i];
                        $tickets->price         = $tktprice[$i];
                        $tickets->currency_type = $currency_type[$i];
                        if($tickets->create()){
                        }
                    }else{
                        $ticketsUp = EventTickets::findFirst($ticketid);
                        $ticketsUp->modified_at = date('Y-m-d H:i:s');
                        $ticketsUp->event_id    = $event->id;
                        $ticketsUp->member_id   = $userSession['id'];
                        $ticketsUp->name        = $ticketArray[$i];
                        $ticketsUp->quantity    = $qtyname[$i];
                        $ticketsUp->price       = $tktprice[$i];
                        $ticketsUp->currency_type = $currency_type[$i];
                        $ticketsUp->update();
                    }
                    $i++;
                }
            }


            $eventdateid    = $this->request->getPost("eventdateid");
            $date           = $this->request->getPost("date");
            $event_start    = $this->request->getPost("event_start");
            $event_end      = $this->request->getPost("event_end");
            if(!empty($eventdateid)){
                $x = 0;
                foreach ($eventdateid as $datse) {
                    if($datse   == 'NEW'){
                        $eventD             = new EventDatetimes();
                        $eventD->event_id   = $event->id;
                        $eventD->date       = $date[$x];
                        $eventD->from_time  = $event_start[$x];
                        $eventD->to_time    = $event_end[$x];
                        $eventD->create();
                    }else{
                        $eventdates             = EventDatetimes::findFirst($datse);
                        $eventdates->date       = $date[$x];
                        $eventdates->from_time  = $event_start[$x];
                        $eventdates->to_time    = $event_end[$x];
                        $eventdates->update();
                    }
                    $x++;
                }
            }



                $this->session->set('successMess', 'Event successfully Update');
                $name = str_replace(',', '+', $event->name);
                return $this->response->redirect('event/update2/'.$event->id.'/'.$name);

            }else{
                $errno = '';
                foreach ($event->getMessages() as $err) {
                    $errno .= $err->getMessage(). '</br>';
                }
                $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
                return $this->response->redirect('event/new2');
            }


        }


        $countries = CountryCodes::find();
        $cities = Cities::find("country_code_id='".$event->country_code_id."'");
        $states = States::find("country_code_id='".$event->country_code_id."'");

        $this->view->setVars([
            'countries' => $countries,
            'cities' => $cities,
            'states' => $states,
            'event' => $event
        ]); 

    }

    public function add_scheduleAction($id, $name = null){
        $this->view->setTemplateAfter('default1');

        $event          = Events::findFirstById($id);
        $userSession    = $this->session->get("userSession");
        if(!$event OR $event->member_id != $userSession['id']){
            return $this->response->redirect('event/index');
        }

        $this->view->setVar('event', $event);

    }
    // public function update2Action($id, $name = null){
    //     $this->view->setTemplateAfter('default1');
    //     $event          = Events::findFirstById($id);
    //     $userSession    = $this->session->get("userSession");
    //     if(!$event ){
    //         return $this->response->redirect('event/index');
    //     }
    //     $this->view->setVar('event', $event);



    // }





    public function downloadImageAction(){

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
      
        if($this->request->isAjax()) {

            $userSession    = $this->session->get('userSession');
            $userid         = $userSession['id'];
            $encoded        = $this->request->getPost('imagelink');
            $eid            = $this->request->getPost('eid');
            $message    =  ''; 
            $response   = 'ok';
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'YES.png';
                $filePath = 'img/event/';

                $mpic = EventPhotos::findFirst(" event_id =".$eid);
                $filedel =   $mpic->filename;

                $mpic->file_path    = $filePath;
                $mpic->filename     = $newFileName;
                $mpic->member_id    = $userid;
                $mpic->primary_pic  = 'Yes';
                $mpic->caption      = 'Event Caption';
                if ($mpic->update()) {
                    $file = $filePath . $newFileName;
                    if (file_put_contents($file, file_get_contents($encoded))) {
                        $message = 'Event photo successfully updated';
                        if($filedel != 'defualtImage.jpg'){
                            $path = $filePath.$filedel;
                             if (file_exists($path)) {
                                unlink($path);
                                $message = 'deleted';
                              }
                        }

                    }else{
                        $message = 'Ops! something went wrong, please try again';
                        $response = 'failed';
                    }
                }else{
                    $errs = 'erro';
                    foreach ($mpic->getMessages() as $err) {
                        $errs .= $err->getMessage() .'</br>';
                    }
                    $message = $errs;
                    $response = 'failed';
                }
            $output = json_encode(array('type'=> $response, 'text' => $message));
            die($output); //exit script outputting json data
        }
    }

    public function delete_eventAction($id){


        $event          = Events::findFirstById($id);
        $userSession    = $this->session->get("userSession");
        if(!$event OR $event->member_id != $userSession['id']){
            return $this->response->redirect('event/index');
        }

        if($event){

            if($event->delete()){

                $filePath = 'img/event/';

                $mpic = EventPhotos::findFirst(" event_id =".$id);
                if($mpic){
                    $filedel =   $mpic->filename;
                    if($mpic->delete()){
                        if($filedel != 'defualtImage.jpg'){
                            $path = $filePath.$filedel;
                             if (file_exists($path)) {
                                unlink($path);
                              }
                        }
                    }else{
             
                    }
                }
                $tickets = EventTickets::findFirst("event_id = " . $id);
                if($tickets){
                    if($tickets->delete()){

                    }else{
                
                    }
                }
                $eventD  = EventDatetimes::findFirst("event_id = " . $id);
                if($eventD){
                    if($eventD->delete()){
                        
                    }else{
        
                    }
                }
            }
        }

        $this->response->redirect('event');
    }



    public function delete_ticketAction(){


      
        if($this->request->isAjax()) {
            $id         = $this->request->getQuery('ids');
            $message = '';
            $name = '';
            $response = 'ok';
            $failed = 'Failed removing to archived section, please try again';

            $query = EventTickets::findFirstById($id);
            if($query){
                $realtyid = $query->id;
                $name = $query->name;
                if($query->delete()){
                    $message .= 'Ticket : <strong>' .$name .'</strong> successfully deleted!';
                }else{
                    $message .= 'Failed deleting this ticket : <strong>' .$name .'</strong> - please try again';
                    $response = 'failed';
                }
            }

            $output = json_encode(array('type' => $response, 'message' => $message));
            die($output); //exit script outputting json data

         }
    }


    public function event_requestAction($id = null, $name = null){


        if ($this->request->isPost()) {


            $fname      = $this->request->getPost('first_name');
            $lname      = $this->request->getPost('last_name');
            $contact    = $this->request->getPost('contact_number');
            $email      = $this->request->getPost('email');
            $eventid    = $this->request->getPost('eventid');
            $eventname  = $this->request->getPost('eventname');

            $ticket_type    = $this->request->getPost('ticket-name');
            $ticket_id      = $this->request->getPost('ticket-id');
            $ticket_qty     = $this->request->getPost('ticket-qty');
            $bool = false;
            $requestTicket = array();
            if(!empty($ticket_qty)){
                $i = 0;
                foreach ($ticket_qty as $request) {

                    if(!empty($request) OR $request != ''){
                        $message .= $request;
                        $message .= $ticket_type[$i];

                        $ticketRequest = new EventEmailRequests();
                        $ticketRequest->created_at          = date('Y-m-d H:i:s');
                        $ticketRequest->first_name          = $fname;
                        $ticketRequest->last_name           = $lname;
                        $ticketRequest->contact             = $contact;
                        $ticketRequest->email_sender        = $email;
                        $ticketRequest->message             = '';
                        $ticketRequest->event_ticket_id     = $ticket_id[$i];
                        $ticketRequest->event_id            = $eventid;
                        $ticketRequest->ticket_request      = $request;
                        $ticketRequest->status              = 'Y';
                        if($ticketRequest->create()){
                            $bool = true;
                            $requestTicket[] = $ticketRequest->id;
                        }else{
                            $err = '';
                            foreach ($ticketRequest->getMessages() as  $errn) {
                                $err .= $errn->getMessage().'<br>';
                            }
                            $message .= $err;
                        }
                    }
                    $i++;    
                }
                if($bool == true){
                    $fullname = $fname.' '.$lname;
                    $request = $this->getDI()->getMail()->send(
                        'info@mybarangay.com',
                        'Ticket Order',
                        'ticket_request',
                        array('email' => $email, 'eventname' => $eventname, 'contact' => $contact, 'fullname' => $fullname , 'ticket_id' => $requestTicket)
                    );
                    // $fullname = $fname.' '.$lname;
                    // $request = $this->getDI()->getMail()->send(
                    //     'iam.davidheart@gmail.com',
                    //     'Ticket Order',
                    //     'ticket_request',
                    //     array('email' => $email, 'eventname' => $eventname, 'contact' => $contact, 'fullname' => $fullname , 'ticket_id' => $requestTicket)
                    // );

                    if($request){
                         $this->session->set('successMess', 'MyBarangay will contact you to confirm your order. ');
                    }
                }else{
                     $this->session->set('errorMessasge', 'Ops! somethig went wrong.');
                }
            }
            return $this->response->redirect('event/view2/'.$eventid);
        }else{
            return $this->response->redirect('event/index/');
        }
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
    }



    public function eventEmailRequestAction(){
        
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {
   


            $message = 'ok';
            $name = '';
            $response = 'ok';


            $fname      = $this->request->getPost('fnames');
            $lname      = $this->request->getPost('lnames');
            $contact    = $this->request->getPost('contacts');
            $email      = $this->request->getPost('emails');
            $eventid    = $this->request->getPost('eventids');
            $eventname  = $this->request->getPost('eventname');
            $ticket_type    = $this->request->getPost('ticket_type');
            $ticket_id      = $this->request->getPost('ticket_id');
            $ticket_qty     = $this->request->getPost('ticket_qty');
            $bool = false;
            $requestTicket = array();
            if(!empty($ticket_qty)){
                $i = 0;
                foreach ($ticket_qty as $request) {

                    if(!empty($request) OR $request != ''){
                        $message .= $request;
                        $message .= $ticket_type[$i];

                        $ticketRequest = new EventEmailRequests();
                        $ticketRequest->created_at          = date('Y-m-d H:i:s');
                        $ticketRequest->first_name          = $fname;
                        $ticketRequest->last_name           = $lname;
                        $ticketRequest->contact             = $contact;
                        $ticketRequest->email_sender        = $email;
                        $ticketRequest->message             = '';
                        $ticketRequest->event_ticket_id     = $ticket_id[$i];
                        $ticketRequest->event_id            = $eventid;
                        $ticketRequest->ticket_request      = $request;
                        $ticketRequest->status              = 'Y';
                        if($ticketRequest->create()){
                            $bool = true;
                            $requestTicket[] = $ticketRequest->id;
                        }else{
                            $err = '';
                            foreach ($ticketRequest->getMessages() as  $errn) {
                                $err .= $errn->getMessage().'<br>';
                            }
                            $message .= $err;
                        }
                    }
                    $i++;    
                }
                if($bool == true){
                    $fullname = $fname.' '.$lname;
                    $request = $this->getDI()->getMail()->send(
                        'iam.davidheart@gmail.com',
                        'Ticket Order',
                        'ticket_request',
                        array('email' => $email, 'eventname' => $eventname, 'contact' => $contact, 'fullname' => $fullname , 'ticket_id' => $requestTicket)
                    );
                    if($request){
                         $this->session->set('successMess', 'Ticket request successfully delivered');
                    }
                }else{
                     $this->session->set('errorMessasge', 'Ops! somethig went wrong.');
                }

            }


            $output = json_encode(array('type' => $response, 'text' => $message));
            die($output); //exit script outputting json data

         }

    }












}
