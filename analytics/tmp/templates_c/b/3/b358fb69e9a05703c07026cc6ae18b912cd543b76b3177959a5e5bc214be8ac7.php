<?php

/* @Goals/getOverviewView.twig */
class __TwigTemplate_b358fb69e9a05703c07026cc6ae18b912cd543b76b3177959a5e5bc214be8ac7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"plugins/Goals/stylesheets/goals.css\"/>

";
        // line 3
        $this->loadTemplate("@Goals/_titleAndEvolutionGraph.twig", "@Goals/getOverviewView.twig", 3)->display($context);
        // line 4
        $context["sum_nb_conversions"] = (isset($context["nb_conversions"]) ? $context["nb_conversions"] : $this->getContext($context, "nb_conversions"));
        // line 5
        echo "
";
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["goalMetrics"]) ? $context["goalMetrics"] : $this->getContext($context, "goalMetrics")));
        foreach ($context['_seq'] as $context["_key"] => $context["goal"]) {
            // line 7
            echo "    ";
            $context["nb_conversions"] = $this->getAttribute($context["goal"], "nb_conversions", array());
            // line 8
            echo "    ";
            $context["nb_visits_converted"] = $this->getAttribute($context["goal"], "nb_visits_converted", array());
            // line 9
            echo "    ";
            $context["conversion_rate"] = $this->getAttribute($context["goal"], "conversion_rate", array());
            // line 10
            echo "    ";
            $context["name"] = $this->getAttribute($context["goal"], "name", array());
            // line 11
            echo "    <div class=\"goalEntry\" style=\"clear:both\">
        <h2>
            <a href=\"javascript:broadcast.propagateAjax('module=Goals&action=goalReport&idGoal=";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["goal"], "id", array()), "html", null, true);
            echo "')\">
                ";
            // line 14
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Goals_GoalX", (("'" . (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name"))) . "'"))), "html", null, true);
            echo "
            </a>
        </h2>

        ";
            // line 18
            if ( !(isset($context["isWidget"]) ? $context["isWidget"] : $this->getContext($context, "isWidget"))) {
                // line 19
                echo "        <div class=\"row\">
            <div class=\"col-md-6\">
        ";
            }
            // line 22
            echo "
                <div class=\"sparkline\">";
            // line 23
            echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array($this->getAttribute($context["goal"], "urlSparklineConversions", array())));
            echo "
                    ";
            // line 24
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Goals_Conversions", (("<strong>" . (isset($context["nb_conversions"]) ? $context["nb_conversions"] : $this->getContext($context, "nb_conversions"))) . "</strong>")));
            echo "
                    ";
            // line 25
            if ($this->getAttribute($context["goal"], "goalAllowMultipleConversionsPerVisit", array())) {
                // line 26
                echo "                        (";
                echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_NVisits", (("<strong>" . (isset($context["nb_visits_converted"]) ? $context["nb_visits_converted"] : $this->getContext($context, "nb_visits_converted"))) . "</strong>")));
                echo ")
                    ";
            }
            // line 28
            echo "                </div>

        ";
            // line 30
            if ( !(isset($context["isWidget"]) ? $context["isWidget"] : $this->getContext($context, "isWidget"))) {
                // line 31
                echo "            </div>
            <div class=\"col-md-6\">
        ";
            }
            // line 34
            echo "
                <div class=\"sparkline\">";
            // line 35
            echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array($this->getAttribute($context["goal"], "urlSparklineConversionRate", array())));
            echo "
                    ";
            // line 36
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Goals_ConversionRate", (("<strong>" . (isset($context["conversion_rate"]) ? $context["conversion_rate"] : $this->getContext($context, "conversion_rate"))) . "</strong>")));
            echo "
                </div>

        ";
            // line 39
            if ( !(isset($context["isWidget"]) ? $context["isWidget"] : $this->getContext($context, "isWidget"))) {
                // line 40
                echo "            </div>
        </div>
        ";
            }
            // line 43
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['goal'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "
";
        // line 47
        if ((isset($context["displayFullReport"]) ? $context["displayFullReport"] : $this->getContext($context, "displayFullReport"))) {
            // line 48
            echo "    ";
            if (((isset($context["sum_nb_conversions"]) ? $context["sum_nb_conversions"] : $this->getContext($context, "sum_nb_conversions")) != 0)) {
                // line 49
                echo "    <h2 id='titleGoalsByDimension'>
        ";
                // line 50
                if (array_key_exists("idGoal", $context)) {
                    // line 51
                    echo "            ";
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Goals_GoalConversionsBy", (isset($context["goalName"]) ? $context["goalName"] : $this->getContext($context, "goalName")))), "html", null, true);
                    echo "
        ";
                } else {
                    // line 53
                    echo "            ";
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Goals_ConversionsOverviewBy")), "html", null, true);
                    echo "
        ";
                }
                // line 55
                echo "    </h2>
    ";
                // line 56
                echo (isset($context["goalReportsByDimension"]) ? $context["goalReportsByDimension"] : $this->getContext($context, "goalReportsByDimension"));
                echo "
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "@Goals/getOverviewView.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 56,  151 => 55,  145 => 53,  139 => 51,  137 => 50,  134 => 49,  131 => 48,  129 => 47,  126 => 46,  118 => 43,  113 => 40,  111 => 39,  105 => 36,  101 => 35,  98 => 34,  93 => 31,  91 => 30,  87 => 28,  81 => 26,  79 => 25,  75 => 24,  71 => 23,  68 => 22,  63 => 19,  61 => 18,  54 => 14,  50 => 13,  46 => 11,  43 => 10,  40 => 9,  37 => 8,  34 => 7,  30 => 6,  27 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }
}
