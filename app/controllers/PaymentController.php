<?php
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
# use Phalcon\Paginator\Adapter\Model as Paginator;
# use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
use Stripe as Stripe; 
# use Helpers\Sample;
# use Helpers\YelpApi as Yeppie;

class PaymentController extends ControllerBase {
    


    public function initialize()
    {
        parent::initialize();

    }

  /**
   * webhooks - for testing purposes only, for retrieving stripe events from your stripe account 
   * [endpoint] https://mybarangay.com/payment/webhooks
   *
  */
    public function webhooksAction() {
      $stripe = array(
        "secret_key"      => "sk_test_C8DoeYam87t4Gc2aUmAzlHzb",
        "publishable_key" => "pk_test_e1QBeWj2msAUESZujcNOcfdR"
      );

      Stripe\Stripe::setApiKey($stripe['secret_key']);
      $asdas = Stripe\Event::all(array("limit" => 3));
      $azdaz = $asdas->__toArray(true);
        $this->view->disable();
        echo '<pre>';
        echo print_r($azdaz['data']);
        echo '</pre>';

        foreach ($azdaz['data'] as $key => $value) {
          $arr =  $azdaz['data'][$key];
          // echo " this is key : " . $key . "<br>";
          // echo "id: " . $arr['id'] . '<br>';
          // echo "created: " .$arr['created'] . "<br>"; 
        }
    }

  /**
   * pricing - this is the page where you can choose subscription plan (basic, enhanced, premium)
   *
   * @param int $member_id (or String)
   *
   * @return void || response object
   *
  */
    public function pricingAction($member_id = null) {

      is_null($member_id) AND $this->response->redirect($this->request->getHTTPReferer());

      # check if user has already subscribed.
      $checkUser = MemberSubscriptions::count("member_id='".$member_id."'");
      if ($checkUser > 0) { 
        return $this->response->redirect($this->request->getHTTPReferer());
      }

      $this->view->setVar('member_id', $member_id);

    }

    public function upgradeenhancedpricingAction($amount = null, $credit = null ,$truecredit = null,$memberId = null) {
      is_null($amount) AND $this->response->redirect($this->request->getHTTPReferer());
      is_null($memberId) AND $this->response->redirect($this->request->getHTTPReferer());
      is_null($truecredit) AND $this->response->redirect($this->request->getHTTPReferer());
      is_null($credit) AND $this->response->redirect($this->request->getHTTPReferer());
      $this->view->setVars(array(
        'amount' => $amount,
        'member_id' => $memberId,
        'credit' => $credit,
        'truecredit' => $truecredit
      ));
    }
    
    public function upgradepremiumpricingAction($amount = null,$credit = null ,$planType = null, $truecredit = null ,$pointer = null ,$memberId = null)  {
      is_null($amount) AND $this->response->redirect($this->request->getHTTPReferer());
      is_null($planType) AND $this->response->redirect($this->request->getHTTPReferer());
      is_null($memberId) AND $this->response->redirect($this->request->getHTTPReferer());
      is_null($credit) AND $this->response->redirect($this->request->getHTTPReferer());
      is_null($truecredit) AND $this->response->redirect($this->request->getHTTPReferer());
      $this->view->setVars(array(
        'amount' => $amount,
        'member_id' => $memberId,
        'planType' => $planType,
        'credit' => $credit,
        'truecredit' => $truecredit,
        'pointer' => $pointer
      ));
    }

  /**
   * charge - this is the action which stripe's form go, also if the transaction is success stripe throws some parameters here
   *
   * @param POST parameters from Stripe i.e stripeEmail, stripeToken
   *
   * @return depends on the process
   *
   * @throws same as above :p
  */
  public function chargeAction() {
    if ($this->request->isPost()) {

        # sometimes https request has a problem when stripe respond to the request 
        # so it's safe to pass our user id from form to $memberId 
        $userSession = $this->session->get('userSession');
        $userId = $userSession['id'];
        // check if there's a session  
        //if ( $userSession = $this->session->get('userSession') ) {
          
         if ($userSession['type'] == 'Member') {
            $this->session->set('subscription', '2');
            return $this->response->redirect('biz/login');

         }

         $memberId = $userSession['id'];

         if ($this->request->getPost('memberId')) {
          $memberId = $this->request->getPost('memberId');
         }

      //} else {
            //$this->session->set('subscription', '3');
            //return $this->response->redirect('biz/login');
      //}
         
      # check if the user has a subscription already
      $check_subscription = MemberSubscriptions::count("member_id='".$memberId."'");
      if ($check_subscription > 0) {
        $this->session->set('subscription', '1');
        return $this->response->redirect('biz/manage_adu/'. $memberId);
      }

        // <-- api keys 

          # TEST API KEYS
            // $stripe = array(
            //   "secret_key"      => "sk_test_C8DoeYam87t4Gc2aUmAzlHzb",
            //   "publishable_key" => "pk_test_e1QBeWj2msAUESZujcNOcfdR"
            // );

          # LIVE API KEYS
            # used these keys if you completed the test from test keys
            $stripe = array(
              "secret_key" => "sk_live_Zm3AhlglWgo9rN8oXhEyZzzp",
              "publishable_key" => "pk_live_hNyKl1iujBQG9EK0pJtPUAlC" 
            );

        // --> api keys

      # set the stripe api key [your secret key not publishable key]  
      Stripe\Stripe::setApiKey($stripe['secret_key']); 

      # check what is the plan type
      if ($this->request->getPost('planType')) 
      {
        $planType = $this->request->getPost('planType'); # basic plan, enhanced plan, premium plan
        if ($planType == '1') {
          #$amount = $this->request->getPost('timer') *  999;
          #$actualAmount = ;
          # $amount = 6 * 999;
          # $amount2 = 6 * 2999;
          # $amount3 = $amount + $amount2;
          # $floated_amount = $amount3 / 100; // or times by 0.01
          # $raw_gst_amount = '1.05' * $floated_amount;
          # $gst_amount = number_format($raw_gst_amount, 2, '.', '');  
          # $gstamount = str_replace('.', '', $gst_amount);
          $amount = 25187;
          $actualAmount = '239.88';
          $floated_amount = $amount / 100; // or times by 0.01
          $raw_gst_amount = $floated_amount;
          $gstamount = $floated_amount;
          $planName = "Basic Plan";
        } else if ($planType == '2') { # enhanced plan
          //$amount = $this->request->getPost('timer') * 3999;
          //$amount = 9 * 3999;
          $amount = 50387;
          $actualAmount = '479.88';
          $floated_amount = $amount / 100; // or times by 0.01
          //$raw_gst_amount = '1.05' * $floated_amount;
          $raw_gst_amount = $floated_amount;
          $gstamount = $floated_amount;
          //$gst_amount = number_format($raw_gst_amount, 2, '.', '');  
          //$gstamount = str_replace('.', '', $gst_amount);
          $planName = "Enhanced Plan";
        } else if ($planType == '3') { # premium plan
          //$amount = $this->request->getPost('timer') * 9999;
          //$amount = 9 * 9999;
          $amount = 125987;
          $actualAmount = '1199.88';
          $floated_amount = $amount / 100; // or times by 0.01
          //$raw_gst_amount = '1.05' * $floated_amount;
          $raw_gst_amount = $floated_amount;
          //$gst_amount = number_format($raw_gst_amount, 2, '.', '');  
          //$gstamount = str_replace('.', '', $gst_amount);
          $gstamount = $floated_amount;
          $planName = "Premium Plan";
        } else if ($planType == '4') {
          // for testing purposes only :)
          $gstamount = 50;
          $gst_amount = 50;
          $floated_amount = 0.50;
          $planName = "test";
          $amnt = .5;
        }
      }

      # DESCRIPTION, TOKEN, ENTERED_EMAIL
      $desc = "actual amount: $". $actualAmount ." amount with gst: $".$floated_amount." plan type: " . $planName;  
      $token  = $_POST['stripeToken'];
      $enteredEmail = $_POST['stripeEmail'];

      // <-- UPDATE DATABASE 
      try {

          $customer = Stripe\Customer::create(array(
          'email' => $enteredEmail,
          'card'  => $token,
          'description' => $desc,
          //'id'    => $memberId
          ));

          $charge = Stripe\Charge::create(array(
              'customer' => $customer->id,
              'amount'   => $amount,
              'currency' => 'cad', 
              'receipt_email' => $enteredEmail
          ));
          
          $userSession = $this->session->get('userSession');
          $customerId = $customer->id; 
          $getMember = Members::findFirstById($memberId);
          
          if ($getMember == true) {
            if ($getMember->email == true) {
            $userEmail = $getMember->email;
            } else {
              $userEmail = "jopper_jan@yahoo.co.jp"; # if our system doesn't recognize any email 
            }
          } 
          
        $this->session->set('payment_status', 1);
        # $this->view->disable();
        # $memberId = $customerId;
        $payment =  new PaymentLogs();
        $payment->modified = date('Y-m-d H:i:s');
        $payment->created  = date('Y-m-d H:i:s');
        $payment->member_id = $memberId;
        $payment->stripe_customer_id = $customer->id;
        $payment->member_email = $userEmail;//'asdfsf@uahoo.com';//$userSession['email'];
        $payment->entered_email = $enteredEmail;
        $payment->plan_type =  intval($planType);
        $payment->timer  = intval($this->request->getPost('timer'));
        $payment->description = $desc;
        $payment->gst = '1.05';
        $payment->amount = $actualAmount;
        $payment->total_amount = $gstamount;
        # $payment->total_amount = $gstamount / 100;

        if ($payment->save()) {

          # phql doesn't recognized mysql (aggregate)function so we need to set Resultset class
          $query = "INSERT INTO member_subscriptions(created,modified,member_id,subscription,start_date,end_date,for_renew)
                    VALUES(NOW(),NOW(),$memberId,$planType,NOW(),NOW() + INTERVAL 1 YEAR,0)";
          $new_subscriber = new MemberSubscriptions();
          $params = null;
          $result_set = new Resultset(null, $new_subscriber, $new_subscriber->getReadConnection()->query($query, $params)); 
          if ($result_set) {       
            // echo "payment test success";
            //echo "customer_id: - " . $customer->id . "<br>";
            //echo "amount: - " . $amnt . "<br>";
            //echo "entered email: " . $enteredEmail . "<br>";
            // echo "success";

            $this->session->set('subscribe-success', 1);
            return $this->response->redirect('biz/manage_adu/'.$memberId);

          } else {
            $this->view->disable();
            echo "WHOOPS, NOTHING IS HAPPENED";
          }
        } else {
          echo $this->view->disable();
          print_r($payment->getMessages());
        }

        # return $this->response->redirect($this->request->getHTTPReferer());
      } catch (Stripe\Error\Card $e) {
        $this->view->disable();
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
            # $this->session->set('paymentError', 'Card was declined');
            # $this->response->redirect('payment/pricing/'.$memberId);
          # echo "<h2>Card was declined</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', 'whoops!, card was declined');
          return $this->response->redirect('biz/manage_adu/'.$memberId);
      } catch (Stripe\Error\ApiConnection $e) {
          $this->view->disable();
          $e_json = $e->getJsonBody();
          #$error = $e_json['error'];
          # echo "<h2>network problem, perhaps try again </h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', 'network problem, perhaps try again');
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', 'Network Problem, perhaps try again');
            # $this->response->redirect('payment/pricing/'.$memberId);
      } catch (Stripe\Error\InvalidRequest $e) {
          # Card was declined.
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
          # echo "<h2>you screwed up in your programming, shouldn't happen!</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', "you screwed up in your programming, shouldn't happen!");
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', 'Whoops, Server error, perhaps try again');
            # $this->response->redirect('payment/pricing/'.$memberId);
      } catch (Stripe\Error\Api $e) {
          #$this->view->disable();
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
          # echo "<h2>Stripe's servers are down!</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', "stripe's servers are down!");
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', "Payment Gateway's server is down!. please try again later");
            # $this->response->redirect('payment/pricing/'.$memberId);
      }
      // --> UPDATE DATABASE
    } // if request is post
  } // - chargeAction





  public function upgradeeAction() {
    if ($this->request->isPost()) {

        # sometimes https request has a problem when stripe respond to the request 
        # so it's safe to pass our user id from form to $memberId 
        $userSession = $this->session->get('userSession');
        $userId = $userSession['id'];
        // check if there's a session  
        //if ( $userSession = $this->session->get('userSession') ) {
          
           if ($userSession['type'] == 'Member') {
              $this->session->set('subscription', '2');
              return $this->response->redirect('biz/login');

           }

           $memberId = $userSession['id'];

           if ($this->request->getPost('memberId')) {
            $memberId = $this->request->getPost('memberId');
           }

      //} else {
            //$this->session->set('subscription', '3');
            //return $this->response->redirect('biz/login');
      //}
         
      # check if the user has a subscription already
      // $check_subscription = MemberSubscriptions::count("member_id='".$memberId."'");
      // if ($check_subscription > 0) {
      //   $this->session->set('subscription', '1');
      //   return $this->response->redirect('biz/manage_adu/'. $memberId);
      // }

        // <-- api keys 

          # TEST API KEYS
            // $stripe = array(
            //   "secret_key"      => "sk_test_C8DoeYam87t4Gc2aUmAzlHzb",
            //   "publishable_key" => "pk_test_e1QBeWj2msAUESZujcNOcfdR"
            // );

          # LIVE API KEYS
            # used these keys if you completed the test from test keys
            $stripe = array(
              "secret_key" => "sk_live_Zm3AhlglWgo9rN8oXhEyZzzp",
              "publishable_key" => "pk_live_hNyKl1iujBQG9EK0pJtPUAlC" 
            );

        // --> api keys

      # set the stripe api key [your secret key not publishable key]  
      Stripe\Stripe::setApiKey($stripe['secret_key']); 

      # check what is the plan type
      if ($this->request->getPost('planType')) 
      {
        $planType = $this->request->getPost('planType'); # basic plan, enhanced plan, premium plan
        $enteredAmount = $this->request->getPost('this_amount');
        $credit = $this->request->getPost('credit');
        $truecredit = $this->request->getPost('truecredit');
        $amount = $enteredAmount;
        $actualAmount = '479.88';
        $floated_amount = $amount / 100;
        $raw_gst_amount = $floated_amount;
        $gstamount = $floated_amount;
        $planName = 'Enhanced Plan';
      }

      # DESCRIPTION, TOKEN, ENTERED_EMAIL
      $desc = "actual amount: $". $actualAmount ." credit: $".$truecredit." amount with gst: $".$floated_amount." plan type: " . $planName;  
      $token  = $_POST['stripeToken'];
      $enteredEmail = $_POST['stripeEmail'];

      // <-- UPDATE DATABASE 
      try {

          $customer = Stripe\Customer::create(array(
          'email' => $enteredEmail,
          'card'  => $token,
          'description' => $desc,
          //'id'    => $memberId
          ));

          $charge = Stripe\Charge::create(array(
              'customer' => $customer->id,
              'amount'   => $amount,
              'currency' => 'cad', 
              'receipt_email' => $enteredEmail
          ));
          
          $userSession = $this->session->get('userSession');
          $customerId = $customer->id; 
          $getMember = Members::findFirstById($memberId);
          
          if ($getMember == true) {
            if ($getMember->email == true) {
            $userEmail = $getMember->email;
            } else {
              $userEmail = "jopper_jan@yahoo.co.jp"; # if our system doesn't recognize any email 
            }
          } 
          
        $this->session->set('payment_status', 1);
        # $this->view->disable();
        # $memberId = $customerId;
        $payment =  new PaymentLogs();
        $payment->modified = date('Y-m-d H:i:s');
        $payment->created  = date('Y-m-d H:i:s');
        $payment->member_id = $memberId;
        $payment->stripe_customer_id = $customer->id;
        $payment->member_email = $userEmail;//'asdfsf@uahoo.com';//$userSession['email'];
        $payment->entered_email = $enteredEmail;
        $payment->plan_type =  intval($planType);
        $payment->timer  = intval($this->request->getPost('timer'));
        $payment->description = $desc;
        $payment->gst = '1.05';
        $payment->amount = $actualAmount;
        $payment->total_amount = $gstamount;
        # $payment->total_amount = $gstamount / 100;

        if ($payment->save()) {
          # remove previous plan
          $getOldSubscription = MemberSubscriptions::findFirst("member_id='".$memberId."' AND subscription='1'");
          if ($getOldSubscription->delete() == false) {
            $this->response->redirect();
          }
          # phql doesn't recognized mysql (aggregate)function so we need to set Resultset class
          $query = "INSERT INTO member_subscriptions(created,modified,member_id,subscription,start_date,end_date,for_renew)
                    VALUES(NOW(),NOW(),$memberId,$planType,NOW(),NOW() + INTERVAL 1 YEAR,0)";
          $new_subscriber = new MemberSubscriptions();
          $params = null;
          $result_set = new Resultset(null, $new_subscriber, $new_subscriber->getReadConnection()->query($query, $params)); 
          if ($result_set) {       
            // echo "payment test success";
            //echo "customer_id: - " . $customer->id . "<br>";
            //echo "amount: - " . $amnt . "<br>";
            //echo "entered email: " . $enteredEmail . "<br>";
            // echo "success";

            $this->session->set('subscribe-success', 1);
            return $this->response->redirect('biz/manage_adu/'.$memberId);

          } else {
            $this->view->disable();
            echo "WHOOPS, NOTHING IS HAPPENED";
          }
        } else {
          echo $this->view->disable();
          print_r($payment->getMessages());
        }

        # return $this->response->redirect($this->request->getHTTPReferer());
      } catch (Stripe\Error\Card $e) {
        $this->view->disable();
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
            # $this->session->set('paymentError', 'Card was declined');
            # $this->response->redirect('payment/pricing/'.$memberId);
          # echo "<h2>Card was declined</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', 'whoops!, card was declined');
          return $this->response->redirect('biz/manage_adu/'.$memberId);
      } catch (Stripe\Error\ApiConnection $e) {
          $this->view->disable();
          $e_json = $e->getJsonBody();
          #$error = $e_json['error'];
          # echo "<h2>network problem, perhaps try again </h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', 'network problem, perhaps try again');
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', 'Network Problem, perhaps try again');
            # $this->response->redirect('payment/pricing/'.$memberId);
      } catch (Stripe\Error\InvalidRequest $e) {
          # Card was declined.
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
          # echo "<h2>you screwed up in your programming, shouldn't happen!</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', "you screwed up in your programming, shouldn't happen!");
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', 'Whoops, Server error, perhaps try again');
            # $this->response->redirect('payment/pricing/'.$memberId);
      } catch (Stripe\Error\Api $e) {
          #$this->view->disable();
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
          # echo "<h2>Stripe's servers are down!</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', "stripe's servers are down!");
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', "Payment Gateway's server is down!. please try again later");
            # $this->response->redirect('payment/pricing/'.$memberId);
      }
      // --> UPDATE DATABASE
    } // if request is post
  } // - upgradeeAction

   public function upgradepAction() {
    if ($this->request->isPost()) {

        # sometimes https request has a problem when stripe respond to the request 
        # so it's safe to pass our user id from form to $memberId 
        $userSession = $this->session->get('userSession');
        $userId = $userSession['id'];
        // check if there's a session  
        //if ( $userSession = $this->session->get('userSession') ) {
          
           if ($userSession['type'] == 'Member') {
              $this->session->set('subscription', '2');
              return $this->response->redirect('biz/login');

           }

           $memberId = $userSession['id'];

           if ($this->request->getPost('memberId')) {
            $memberId = $this->request->getPost('memberId');
           }

      //} else {
            //$this->session->set('subscription', '3');
            //return $this->response->redirect('biz/login');
      //}
         
      # check if the user has a subscription already
      // $check_subscription = MemberSubscriptions::count("member_id='".$memberId."'");
      // if ($check_subscription > 0) {
      //   $this->session->set('subscription', '1');
      //   return $this->response->redirect('biz/manage_adu/'. $memberId);
      // }

        // <-- api keys 

          # TEST API KEYS
            // $stripe = array(
            //   "secret_key"      => "sk_test_C8DoeYam87t4Gc2aUmAzlHzb",
            //   "publishable_key" => "pk_test_e1QBeWj2msAUESZujcNOcfdR"
            // );

          # LIVE API KEYS
            # used these keys if you completed the test from test keys
            $stripe = array(
              "secret_key" => "sk_live_Zm3AhlglWgo9rN8oXhEyZzzp",
              "publishable_key" => "pk_live_hNyKl1iujBQG9EK0pJtPUAlC" 
            );

        // --> api keys

      # set the stripe api key [your secret key not publishable key]  
      Stripe\Stripe::setApiKey($stripe['secret_key']); 

      # check what is the plan type
      if ($this->request->getPost('planType')) 
      {
        $planType = $this->request->getPost('planType'); # basic plan, enhanced plan, premium plan
        $enteredAmount = $this->request->getPost('this_amount');
        $pointer = $this->request->getPost('pointer');
        $credit = $this->request->getPost('credit');
        $truecredit = $this->request->getPost('truecredit');
        $amount = $enteredAmount;
        $actualAmount = '1188.88';
        $floated_amount = $amount / 100;
        $raw_gst_amount = $floated_amount;
        $gstamount = $floated_amount;
        $planName = 'Premium Plan';
      }

      # DESCRIPTION, TOKEN, ENTERED_EMAIL
      $desc = "actual amount: $". $actualAmount ." credit: $".$credit." amount with gst: $".$floated_amount." plan type: " . $planName;  
      $token  = $_POST['stripeToken'];
      $enteredEmail = $_POST['stripeEmail'];

      // <-- UPDATE DATABASE 
      try {

          $customer = Stripe\Customer::create(array(
          'email' => $enteredEmail,
          'card'  => $token,
          'description' => $desc,
          //'id'    => $memberId
          ));

          $charge = Stripe\Charge::create(array(
              'customer' => $customer->id,
              'amount'   => $amount,
              'currency' => 'cad', 
              'receipt_email' => $enteredEmail
          ));
          
          $userSession = $this->session->get('userSession');
          $customerId = $customer->id; 
          $getMember = Members::findFirstById($memberId);
          
          if ($getMember == true) {
            if ($getMember->email == true) {
            $userEmail = $getMember->email;
            } else {
              $userEmail = "jopper_jan@yahoo.co.jp"; # if our system doesn't recognize any email 
            }
          } 
          
        $this->session->set('payment_status', 1);
        # $this->view->disable();
        # $memberId = $customerId;
        $payment =  new PaymentLogs();
        $payment->modified = date('Y-m-d H:i:s');
        $payment->created  = date('Y-m-d H:i:s');
        $payment->member_id = $memberId;
        $payment->stripe_customer_id = $customer->id;
        $payment->member_email = $userEmail;//'asdfsf@uahoo.com';//$userSession['email'];
        $payment->entered_email = $enteredEmail;
        $payment->plan_type =  intval($planType);
        $payment->timer  = intval($this->request->getPost('timer'));
        $payment->description = $desc;
        $payment->gst = '1.05';
        $payment->amount = $actualAmount;
        $payment->total_amount = $gstamount;
        # $payment->total_amount = $gstamount / 100;

        if ($payment->save()) {
          # remove previous plan
          $getOldSubscription = MemberSubscriptions::findFirst("member_id='".$memberId."' AND subscription='".$pointer."'");
          if ($getOldSubscription->delete() == false) {
            $this->response->redirect();
          }
          # phql doesn't recognized mysql (aggregate)function so we need to set Resultset class
          $query = "INSERT INTO member_subscriptions(created,modified,member_id,subscription,start_date,end_date,for_renew)
                    VALUES(NOW(),NOW(),$memberId,$planType,NOW(),NOW() + INTERVAL 1 YEAR,0)";
          $new_subscriber = new MemberSubscriptions();
          $params = null;
          $result_set = new Resultset(null, $new_subscriber, $new_subscriber->getReadConnection()->query($query, $params)); 
          if ($result_set) {       
            // echo "payment test success";
            //echo "customer_id: - " . $customer->id . "<br>";
            //echo "amount: - " . $amnt . "<br>";
            //echo "entered email: " . $enteredEmail . "<br>";
            // echo "success";

            $this->session->set('subscribe-success', 1);
            return $this->response->redirect('biz/manage_adu/'.$memberId);

          } else {
            $this->view->disable();
            echo "WHOOPS, NOTHING IS HAPPENED";
          }
        } else {
          echo $this->view->disable();
          print_r($payment->getMessages());
        }

        # return $this->response->redirect($this->request->getHTTPReferer());
      } catch (Stripe\Error\Card $e) {
        $this->view->disable();
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
            # $this->session->set('paymentError', 'Card was declined');
            # $this->response->redirect('payment/pricing/'.$memberId);
          # echo "<h2>Card was declined</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', 'whoops!, card was declined');
          return $this->response->redirect('biz/manage_adu/'.$memberId);
      } catch (Stripe\Error\ApiConnection $e) {
          $this->view->disable();
          $e_json = $e->getJsonBody();
          #$error = $e_json['error'];
          # echo "<h2>network problem, perhaps try again </h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', 'network problem, perhaps try again');
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', 'Network Problem, perhaps try again');
            # $this->response->redirect('payment/pricing/'.$memberId);
      } catch (Stripe\Error\InvalidRequest $e) {
          # Card was declined.
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
          # echo "<h2>you screwed up in your programming, shouldn't happen!</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', "you screwed up in your programming, shouldn't happen!");
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', 'Whoops, Server error, perhaps try again');
            # $this->response->redirect('payment/pricing/'.$memberId);
      } catch (Stripe\Error\Api $e) {
          #$this->view->disable();
          $e_json = $e->getJsonBody();
          $error = $e_json['error'];
          # echo "<h2>Stripe's servers are down!</h2><br>";
          # echo $error['message'];
          $this->session->set('subscribe-error', "stripe's servers are down!");
          return $this->response->redirect('biz/manage_adu/'.$memberId);
            # $this->session->set('paymentError', "Payment Gateway's server is down!. please try again later");
            # $this->response->redirect('payment/pricing/'.$memberId);
      }
      // --> UPDATE DATABASE
    } // if request is post
  } // - upgradeeAction
}