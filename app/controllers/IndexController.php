<?php

class IndexController extends ControllerBase{


    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){
        parent::initialize();
        $this->view->setTemplateAfter('main');
    }
    
    /**
     * [show404Action description]
     * @return [type] [description]
     */
    public function show404Action(){

        $this->view->setTemplateAfter('oneheader_default');
        
    }


    public function admin_indexAction()
    {   
        $claimRequests = ClaimRequests::find();
        $this->view->setVar('requests', $claimRequests);
        $this->view->setTemplateAfter('admin_default');
    }

    public function admin_cancel_requestAction($id = null) {
        $this->view->disable();

        $request = ClaimRequests::findFirst($id);

        $request->status = 'pending';

        if ($request->save()) {
            return $this->response->redirect('admincontrol/claimbizreq');
        } else {
            $this->view->disable();
            echo "failed to update data";
        }
    }

    public function admin_reject_requestAction($id = null) {
         $this->view->disable();
         //echo "reject page";
        $request = ClaimRequests::findFirst($id);

        $request->status = 'rejected';

        if ($request->save()) {
            return $this->response->redirect('admincontrol/claimbizreq');
        } else {
            $this->view->disable();
            echo "failed to update data";
        }
    }


//*************************************************************
//                   Customized Controller                    *
//*************************************************************
    /**
     * [tabView description]
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public  function tabView($name){

        $attach = '';
        $name = trim($name);
        switch ($name) {
            case 'autoBizList':
                $attach = ' business_category_id BETWEEN 141 AND 173 GROUP BY business_id ';
                break;
            case 'CommunityBizList':
                $attach = ' (business_category_id BETWEEN 1 AND 94) OR (business_category_id BETWEEN 95 AND 139) OR (business_category_id BETWEEN 564 AND 565) OR (business_category_id BETWEEN 205 AND 209) OR (business_category_id BETWEEN 210 AND 246) 
                            OR (business_category_id BETWEEN 709 AND 721) OR (business_category_id BETWEEN 736 AND 742) OR (business_category_id BETWEEN 617 AND 620) GROUP BY business_id';
                break;
            case 'HBeautyBizList':
                $attach = ' (business_category_id BETWEEN 351 AND 445) OR (business_category_id BETWEEN 174 AND 204) GROUP BY business_id';
                break;
            case 'FinBizList':
                $attach = ' (business_category_id BETWEEN 351 AND 414) OR (business_category_id BETWEEN 280 AND 288) OR (business_category_id BETWEEN 664 AND 708) OR (business_category_id BETWEEN 247 AND 279) GROUP BY business_id';
                break; 
            case 'HTravelBizList':
                $attach = ' business_category_id BETWEEN 519 AND 563 GROUP BY business_id';
                break; 
            case 'RealtyBizList':
                $attach = ' business_category_id BETWEEN 722 AND 735 GROUP BY business_id';
                break; 
            case 'ShoppingBizList':
                $attach = ' business_category_id BETWEEN 1048 AND 1171 GROUP BY business_id';
                break; 
            case 'FoodBizList':
                $attach = ' (business_category_id BETWEEN 289 AND 350) OR (business_category_id BETWEEN 743 AND 1047) OR (business_category_id BETWEEN 621 AND 651) GROUP BY business_id';
                break; 
            default:
               return false;
                break;
        }
        // Business tabs
        $query = "SELECT business_id FROM BusinessCategoryLists WHERE $attach ";

        $queryLists = $this->modelsManager->executeQuery($query);
        $queryIds = array();
        $adquery = array();
        if(count($queryLists) > 0){
            foreach($queryLists as $autoList) {
                $queryIds[] = $autoList->business_id;
            }
            shuffle($queryIds);
            $queryIds = implode(',', $queryIds);
        $adcamps = '';
        $builAds = "SELECT * FROM Adcampaigns WHERE business_id IN (".$queryIds.") ORDER BY FIELD(business_id,".$queryIds.")   ";
        $adcamps = $this->modelsManager->executeQuery($builAds);
        $limit = 18;
        if($adcamps){
          $limit = abs(count($adcamps) - 18);
          foreach ($adcamps as $ad) {
                $adquery[] = $ad->business_id;
            }
        }

            $sa = explode(',', $queryIds);
            $result = array_diff($sa, $adquery);
            $finalresult = implode(',', $result);
            $this->view->setVar($name.'camp', $adcamps);
            $queryBizList = "SELECT * FROM Business WHERE id IN (".$finalresult.") ORDER BY FIELD(id,".$finalresult.")  LIMIT $limit";
            $queryBizListResult = $this->modelsManager->executeQuery($queryBizList);
        } else {
            $queryBizListResult = array();
        }
        $this->view->setVar($name, $queryBizListResult);

    }


    public function indexAction() {
    



    $userSession = $this->session->get('userSession');
    if ($userSession['type'] == 'Business') {
        return $this->response->redirect('biz/page/'. $userSession['id']);
    }   

        $this->view->setTemplateAfter('default1');
    
        $this->view->setVar('home', 'act');
        $array = array(
        'autoBizList',
        'CommunityBizList',
        'HBeautyBizList',
        'HServBizList',
        'FinBizList',
        'HTravelBizList',
        'RealtyBizList',
        'ShoppingBizList',
        'FoodBizList'
        );

        foreach ($array as $key => $value) {
           $this->tabView($value);
        }


        $adcamp = ImageAds::find([
            'columns'     => 'id',
            'conditions'  => 'type = :typ:',
            'bind'        => array('typ'  => 3)
        ]);

       $ipaddress = $this->request->getClientAddress();
        if($ipaddress == $this->session->ipaddress){

            $page = 1;
            $numbers  = $this->persistent->ids;
            $total    = count( $numbers );   
            if(count($adcamp) > $total){
                $dataarray = $this->firstBatch($ipaddress,$adcamp);
            }else{
            //****************************
                $limit    = 8; //per page  
                $totalPages = ceil( $total/ $limit ); //calculate total pages
                $page     = max($page, 1); 
                // $page = min($page, $totalPages);
                if($this->persistent->pageNo){
                   echo $page = $this->persistent->pageNo++;
                    if($page > $totalPages){
                        $page = 1;
                        if($totalPages >= 2){
                             shuffle($numbers);
                            $this->persistent->ids = $numbers;
                            $this->persistent->pageNo = 2;

                        }else{  
                                 shuffle($numbers);
                            $this->persistent->pageNo = 1;
                        }
                    }
                }else{
                    $this->persistent->pageNo = 1;
                }
                $offset = ($page - 1) * $limit;
                if( $offset < 0 ){ $offset = 0; }
                $dataarray = array_slice( $numbers, $offset, $limit );

            }
            $this->view->setVar('sliderChunk', $dataarray);

        }else{
            $dataarray = $this->firstBatch($ipaddress,$adcamp);
            $this->view->setVar('sliderChunk', $dataarray);
        }
    $this->view->setVar('numbers', $dataarray);
    }

    public function firstBatch($ipaddress, $adcamp){
            $this->session->set("ipaddress", $ipaddress);
            $ids = array();
            foreach ($adcamp as $ads) {
                $ids[] = $ads->id;
            }
            shuffle($ids);
            $this->persistent->ids  = $ids;
            $numbers                = $this->persistent->ids;
            $limit                  = 8; //per page    
          return  $dataarray              = array_slice( $numbers, 0, $limit );
    }




    /**
     * [admin_flagsAction description]
     * @return [type] [description]
     */
    public function admin_flagsAction() {

        $flags = Flags::find();
        $this->view->setVar('flags', $flags);
        $this->view->setTemplateAfter('admin_default');

    }
    public function real_escape($value){
        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
        return str_replace($search, $replace, $value);
    }

    /**
     * [searchAction description]
     * @return [type] [description]
     */
   public function searchAction(){
        $this->view->setTemplateAfter('default1');

        $sql = '';
        $type = '1';
        $business = '';
        $autos = '';
        $things = '';
        $jobs = '';
        $realties = '';
        $city = '';
        $city_code = '';
        $ecity = '';
        $keyword = '';
        $btn_search = '';
        $campaign = '';
        $campaign2 = '';
        if (isset($_GET['btn_search'])) {
          $type =  $this->request->getQuery('type');
          if($type == ''){
            $type = 1;
          }
                  $keyword = $this->real_escape($this->request->getQuery('keyword'));
        //$keyword = $this->request->getQuery('keyword');
          $city_code = $this->request->get("city_code");
            if($city_code != ''){
                $cities = Cities::findFirst($city_code);
                $ecity .= $cities->city_name;
                $ecity .= ', '. $cities->state_code_id;
                $ecity .= ', '.$cities->CountryCodes->country_name;
            }

          if($type == '1' or $type == '2'){


         // ********************************************************
            $cond = '';
            $bind = array();
            $city = Cities::findFirst($city_code);
            $cityname = $city->city_name;
            $theId = array(0);
            if(!empty($keyword) AND !empty($city_code)){
            $nbus = $this->modelsManager->createBuilder()
              ->from('Business')
              ->where('name LIKE :name:', ['name' =>  '%'.$keyword.'%'])
              ->andWhere('city = :city:', ['city' => $cityname])
              ->columns('id')
              ->getQuery()
              ->execute();

             foreach ($nbus as $x) {
               $theId[] = $x['id'];
               echo $x['id'].'<br>';
             }


            $rbcl = $this->modelsManager->createBuilder()
              ->from(array('bc' => 'BusinessCategories'))
              ->where('bc.main_title LIKE :fieldname:',
               ['fieldname' => '%'.$keyword.'%'])
              ->innerJoin('BusinessCategoryLists', "bcl.business_category_id = bc.id", 'bcl')
              ->innerJoin('Business', "bs.id = bcl.business_id AND city LIKE '%$cityname%' ", 'bs')
              ->notInWhere('bcl.business_id', $theId)
              ->columns(array('DISTINCT(bcl.business_id)', 'bcl.business_id'))
              ->limit(10)
              ->getQuery()
              ->execute();

                foreach ($rbcl as $bcl) {
                  $theId[] = $bcl->business_id;
                }
                $result = array_unique($theId);
                $cresult = count($result);
                if($cresult > 0 ){

                  foreach ($result as $x) {
                    $idfield[] = $x;
                  }

                  for ($i=0; $i < $cresult; $i++) { 
                       $cond .= ' id = :params'.$i.':';
                       $cond .= ($i == ($cresult - 1))? '' : ' OR ' ;
                  }
                  for ($x=0; $x < $cresult; $x++) { 
                      $n = 'params'.$x;
                      $bind[$n] = $idfield[$x];
                  }
                }


            $idkill = array(0);
            $strcond = str_replace('id', "business_id", $cond);
            $campaign = Adcampaigns::find([
              'columns' => '*',
              'conditions' => $strcond,
              'bind' => $bind,
              'limit' => 3
            ]);
            $rcamp = count($campaign);
              if($rcamp > 0){
              foreach ($campaign as $campid) {
                $idkill[] = $campid->business_id;
                }
              }



              // $crbcl = count($rbcl);
              // $cond .= ($cnbus > 0 AND $crbcl > 0)? 'OR' : ' ' ;
              // if(!empty($crbcl) OR $crbcl > 0 ){

              //   foreach ($rbcl as $bcl) {
              //     $idfield[] = $bcl->id;
              //   }
              //   for ($i=0; $i < $crbcl; $i++) { 
              //        $cond .= ' id = :idfld'.$i.':';
              //        $cond .= ($i == ($crbcl - 1))? '' : ' OR ' ;
              //   }
              //   for ($x=0; $x < $crbcl; $x++) { 
              //       $n = 'idfld'.$x;
              //       $bind[$n] = $idfield[$x];
              //   }
              // }

              // ----DEBUG CODE-----
              // echo count($robots).'--</br>';
              // //print_r($robots->toArray());
              // foreach ($robots as $value) {
              // echo $value->name.' - '. $value->city.' - '. $value->main_title.' - '. $value->title .'</br>';
              // }
              // echo '</br>'.$mt.'</br>';
              // echo $tn.'</br>';
              // echo $st.'</br>';
              // $this->view->disable();
            }else{

           // first check if the name is not empty (if not then search)
          if(!empty($keyword)) {
            $theId = array(0);
            $nbus = $this->modelsManager->createBuilder()
              ->from('Business')
              ->where('name LIKE :name:', ['name' =>  '%'.$keyword.'%'])
              ->columns('id')
              ->getQuery()
              ->execute();
             foreach ($nbus as $x) {
               $theId[] = $x['id'];
               echo $x['id'].'<br>';
             }
              $rbcl = $this->modelsManager->createBuilder()
                ->from(array('bc' => 'BusinessCategories'))
                ->where('bc.title LIKE :fieldname:',
                 ['fieldname' => '%'.$keyword.'%'])
                ->innerJoin('BusinessCategoryLists', "bcl.business_category_id = bc.id", 'bcl')
                ->notInWhere('bcl.business_id', $theId)
                ->columns(array('DISTINCT(bcl.business_id)', 'bcl.business_id'))
                ->getQuery()
                ->execute();

                foreach ($rbcl as $bcl) {
                  $theId[] = $bcl->business_id;
                }
                $result = array_unique($theId);
                $cresult = count($result);
                if($cresult > 0 ){

                  foreach ($result as $x) {
                    $idfield[] = $x;
                  }

                  for ($i=0; $i < $cresult; $i++) { 
                       $cond .= ' id = :params'.$i.':';
                       $cond .= ($i == ($cresult - 1))? '' : ' OR ' ;
                  }
                  for ($x=0; $x < $cresult; $x++) { 
                      $n = 'params'.$x;
                      $bind[$n] = $idfield[$x];
                  }
                }


            $idkill = array(0);
            $strcond = str_replace('id', "business_id", $cond);
            $campaign = Adcampaigns::find([
              'columns' => '*',
              'conditions' => $strcond,
              'bind' => $bind,
              'limit' => 3
            ]);
            $rcamp = count($campaign);
              if($rcamp > 0){
              foreach ($campaign as $campid) {
                $idkill[] = $campid->business_id;
                }
              }

 
          }

            /****************/

            if(!empty($city_code)) {
                $idkill = array(0);
                $cond .= ' city = :city:';
                $bind['city'] = $cityname;
            }

            /****************/
            if(!empty($city_code)) {
        
                $bus = Business::find([
                'columns' => '*',
                'conditions' => 'city = :city:',
                'bind' => array('city' => $cityname)
                ]);

                if($bus){
                    $idbiz = array(0);
                    foreach ($bus as $biz) {
                        $idbiz[] = $biz->id;
                    }
                   
                  $campaign = $this->modelsManager->createBuilder()
                    ->from(array('Adcampaigns'))
                    ->inWhere('business_id', $idbiz)
                    ->columns(['*'])
                    ->getQuery()
                    ->execute();
                }

         }

         }



     

 

            if($cond != '' AND !empty($bind)){
              $order = "IF (name = '{$keyword}', 0, 
                IF (name like '{$keyword}%', 1,
              IF (name like '{$keyword}%', 2, 3))) , name ASC";
              // $business = Business::find([
              //   'columns' => '*',
              //   'conditions' => $cond,
              //   'bind' => $bind,
              //   'order' => $order,
              //   'limit' => 12
              // ]);
              $business = $this->modelsManager->createBuilder()
               ->from('Business')
               ->where($cond,$bind)
               ->notInWhere('id', $idkill)
               ->limit(12)
               ->orderBy($order)
               ->getQuery()
               ->execute();

            }



            // echo $cond;
            // $this->view->disable();
          //********************** end of business search ***********************
          }

          if($type == '1' or $type == '3'){
            $locations = [];
            $getters = [];
            $queries = [];
            $error = [];
            foreach ($_GET as $key => $value) {
               $temp = is_array($value) ? $value : trim($value) ;
               if(!empty($temp)){
                    list($key) = explode("-", $key);
                    if(!in_array($key, $getters)){
                        $getters[$key] = $value;
                    }
               }
            }
           $sql = '';
           $sql = "SELECT * FROM Automotives ";
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                   ${$key} = $value;
                    switch ($key) {
                        case 'keyword':
                       $keyword = $this->real_escape($keyword);
                               array_push($queries, "(name LIKE '%$keyword%' OR brand LIKE '%$keyword%' OR model LIKE '%$keyword%' )");
                            break;
                            case 'city_code':
                                array_push($queries, "(city = $city_code)");
                            break;
                        default:
                            break;
                    }
                }
            }
          $sql = $this->andQuery($queries, $sql);
          $autos = $this->modelsManager->executeQuery($sql);
            $queries = [];
            $jobsql = '';
            $jobsql = "SELECT * FROM Jobs ";
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                   ${$key} = $value;
                    switch ($key) {
                        case 'keyword':
                       $keyword = $this->real_escape($keyword);
                               array_push($queries, "(position LIKE '%$keyword%' OR company LIKE '%$keyword%'  )");
                            break;
                            case 'city_code':
                                array_push($queries, "(city = $city_code)");
                            break;
                        default:
                            break;
                    }
                }
            }
          $jobsql = $this->andQuery($queries, $jobsql);
          $jobs = $this->modelsManager->executeQuery($jobsql);
            $queries = [];
            $realsql = '';
            $realsql = "SELECT * FROM Realties ";
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                   ${$key} = $value;
                    switch ($key) {
                        case 'keyword':
                       $keyword = $this->real_escape($keyword);
                               array_push($queries, "(name LIKE '%$keyword%')");
                            break;
                            case 'city_code':
                                array_push($queries, "(city = $city_code)");
                            break;
                        default:
                            break;
                    }
                }
            }
          $realsql = $this->andQuery($queries, $realsql);
          $realties = $this->modelsManager->executeQuery($realsql);
            $queries = [];
            $thgsql = '';
            $thgsql = "SELECT * FROM Things ";
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                  ${$key} = $value;
                    switch ($key) {
                        case 'keyword':
                       $keyword = $this->real_escape($keyword);
                               array_push($queries, "(name LIKE '%$keyword%')");
                            break;
                            case 'city_code':
                                array_push($queries, "(city = $city_code)");
                            break;
                        default:
                            break;
                    }
                }
            }
          $thgsql = $this->andQuery($queries, $thgsql);
          $things = $this->modelsManager->executeQuery($thgsql);
          }
      }




    $this->view->setVars([
    'campaign' => $campaign,
    'campaign2' => $campaign2,
    'keword' => $keyword, 
    'type' => $type, 
    'btn_search' => $btn_search,
    'city' => $ecity,
    'city_code' => $city_code,
    'business' => $business,
    'autos' => $autos,
    'things' => $things,
    'jobs' => $jobs,
    'realties' => $realties]);

   }

   /**
    * [andQuery description]
    * @param  [type] $queries [description]
    * @param  [type] $sql     [description]
    * @return [type]          [description]
    */
   protected function andQuery($queries, $sql){
          if(!empty($queries)){
          $sql .= " WHERE ";
          $i = 1;
          foreach ($queries as $query) {
              if($i < count($queries)){
                  $sql .= $query." AND ";
              }else{
                  $sql .= $query;
              }
              $i++;
          }
      }

         $sql .= " LIMIT 20 ";
        return $sql;

   }






    /**
     * [searchAction description]
     * @return [type] [description]
     */
   public function search1xAction(){

        $this->view->setTemplateAfter('default1');


        $ename = '';
        $ecity = '';
        $city_code = '';
        // searched variables
        $keyword = $this->request->get("find");
        $location = $this->request->get("near");
     
            $city_code = $this->request->get("city_code");
  
            $ename = $keyword;
            if($city_code != ''){
                $cities = Cities::findFirst($city_code);
                $ecity .= $cities->city_name;
                $ecity .= ', '. $cities->state_code_id;
                $ecity .= ', '.$cities->CountryCodes->country_name;
            }

        $this->view->setVars([
            'ename' => $ename,
            'ecity' => $ecity,
            'ecity_code' => $city_code
        ]);


        // exploding location [address, city]
        $adds = explode(',', $location);

        $no = 0;

    /************************************************************************************/

        $business = '';
    if($no == 2 OR $no == 0){

        $cond = '';
        $bind = array();


        $city = Cities::findFirst($city_code);
        $cityname = $city->city_name;



        // if two search box is not empty 
        // Create a query for this two
        if(!empty($keyword) AND !empty($city_code)){
            // $var = array('mt','tn','st');
            // $mt = 0;
            // $tn = 0;
            // $st = 0;
            // foreach ($var as  $rrbc) {
            //     if($rrbc == 'mt'){
            //         $rbc = BusinessCategories::count([
            //             'columns'    => '*', 
            //             'conditions' => 'main_title LIKE :main_title:', 
            //             'bind' => array('main_title' => '%'.$keyword.'%')
            //         ]);
            //     }elseif($rrbc == 'tn'){
            //         $rbc = BusinessCategories::count([
            //             'columns'    => '*', 
            //             'conditions' => 'title LIKE :title:', 
            //             'bind' => array('title' => '%'.$keyword.'%')
            //         ]);
            //     }elseif($rrbc == 'st'){
            //         $rbc = BusinessCategories::count([
            //             'columns'    => '*', 
            //             'conditions' => 'sub_title LIKE :sub_title:', 
            //             'bind' => array('sub_title' => '%'.$keyword.'%') 
            //         ]);
            //     }
            //     $$rrbc = $rbc;
            // }

        $nbus = $this->modelsManager->createBuilder()
          ->from('Business')
          ->where('name LIKE :name:', ['name' =>  '%'.$keyword.'%'])
          ->andWhere('city = :city:', ['city' => $cityname])
          ->columns('id')
          ->getQuery()
          ->execute();

          $cnbus = count($nbus);
          if(!empty($cnbus) OR $cnbus > 0 ){

            foreach ($nbus as $nbus) {
              $name[] = $nbus->id;
            }
            for ($i=0; $i < $cnbus; $i++) { 
                 $cond .= ' id = :idfld'.$i.':';
                 $cond .= ($i == ($cnbus - 1))? '' : ' OR ' ;
            }
            for ($x=0; $x < $cnbus; $x++) { 
                $n = 'idfld'.$x;
                $bind[$n] = $name[$x];
            }
          }

          // ----DEBUG CODE-----
          // print_r($nbus->toArray());
          // $this->view->disable();

        $rbcl = $this->modelsManager->createBuilder()
          ->from(array('bc' => 'BusinessCategories'))
          ->where('bc.main_title LIKE :fieldname: OR bc.title LIKE :fieldname1: OR bc.sub_title LIKE :fieldname2:',
           ['fieldname' => '%'.$keyword.'%','fieldname1' => '%'.$keyword.'%','fieldname2' => '%'.$keyword.'%'])
          ->innerJoin('BusinessCategoryLists', "bcl.business_category_id = bc.id", 'bcl')
          ->innerJoin('Business', "bs.id = bcl.business_id AND city LIKE '%$cityname%' ", 'bs')
          ->columns(array('bs.id','bs.city','bs.name', 'bc.main_title', 'bc.title'))
          ->getQuery()
          ->execute();

          $crbcl = count($rbcl);

          $cond .= ($cnbus > 0 AND $crbcl > 0)? 'OR' : ' ' ;
          if(!empty($crbcl) OR $crbcl > 0 ){

            foreach ($rbcl as $bcl) {
              $idfield[] = $bcl->id;
            }
            for ($i=0; $i < $crbcl; $i++) { 
                 $cond .= ' id = :idfld'.$i.':';
                 $cond .= ($i == ($crbcl - 1))? '' : ' OR ' ;
            }
            for ($x=0; $x < $crbcl; $x++) { 
                $n = 'idfld'.$x;
                $bind[$n] = $idfield[$x];
            }
          }

          // ----DEBUG CODE-----
          // echo count($robots).'--</br>';
          // //print_r($robots->toArray());
          // foreach ($robots as $value) {
          // echo $value->name.' - '. $value->city.' - '. $value->main_title.' - '. $value->title .'</br>';
          // }
          // echo '</br>'.$mt.'</br>';
          // echo $tn.'</br>';
          // echo $st.'</br>';
          // $this->view->disable();


        }else{

          // first check if the name is not empty (if not then search)
          if(!empty($keyword)) {

            $nbus = $this->modelsManager->createBuilder()
              ->from('Business')
              ->where('name LIKE :name:', ['name' =>  '%'.$keyword.'%'])
              ->columns('id')
              ->getQuery()
              ->execute();

              $cnbus = count($nbus);
              if(!empty($cnbus) OR $cnbus > 0 ){
                foreach ($nbus as $nbus) {
                  $name[] = $nbus->id;
                }
                for ($i=0; $i < $cnbus; $i++) { 
                     $cond .= ' id = :idfld'.$i.':';
                     $cond .= ($i == ($cnbus - 1))? '' : ' OR ' ;
                }
                for ($x=0; $x < $cnbus; $x++) { 
                    $n = 'idfld'.$x;
                    $bind[$n] = $name[$x];
                }
              }


              $rbcl = $this->modelsManager->createBuilder()
                ->from(array('bc' => 'BusinessCategories'))
                ->where('bc.title LIKE :fieldname: ',
                 ['fieldname' => '%'.$keyword.'%'])
                ->innerJoin('BusinessCategoryLists', "bcl.business_category_id = bc.id", 'bcl')
                ->columns(array('DISTINCT(bcl.business_id)', 'bcl.business_id'))
                ->getQuery()
                ->execute();


                echo $crbcl = count($rbcl);

                $cond .= ($cnbus > 0 AND $crbcl > 0)? 'OR' : ' ' ;



                if(!empty($crbcl) OR $crbcl > 0 ){

                  foreach ($rbcl as $bcl) {
                   $idfield[] = $bcl->business_id;
                  }
       
                  for ($i=0; $i < $crbcl; $i++) { 
                       $cond .= ' id = :idfldx'.$i.':';
                       $cond .= ($i == ($crbcl - 1))? '' : ' OR ' ;
                  }
           
                  for ($x=0; $x < $crbcl; $x++) { 
                      $n = 'idfldx'.$x;
                      $bind[$n] = $idfield[$x];
                  }


                }



          //echo count($rbcl).'--</br>';
          //print_r($rbcl->toArray());
          // foreach ($robots as $value) {
          // //echo $value->name.' - '. $value->city.' - '. $value->main_title.' - '. $value->title .'</br>';
          // }

          //$this->view->disable();



          }

          if(!empty($city_code)) {
              $cond .= ' city = :city:';
              $bind['city'] = $cityname;
          }

        }

        if($cond != '' AND !empty($bind)){
          $business = Business::find([
            'columns' => '*',
            'conditions' => $cond,
            'bind' => $bind
          ]);
        }

    }

    $this->view->setVar('business', $business);

/************************************************************************************/

       $autos = '';
 if($no == 3 OR $no == 0){


    // first check if the name is not empty (if not then search)
        $conditions = '';
        $bind = array();
 

           if(!empty($keyword) AND !empty($city_code)  ){

            echo "</br>";
              // check and count searched keywords and location if true on this fields( name,location ) 
              echo 'debug namex  '.$nA = Automotives::count([
                    'conditions' => 'name LIKE :name: AND city LIKE :location:',
                    'bind' => [
                            'name' => '%' . $keyword . '%',
                            'location' => '%' . $city_code . '%'
                            ]
                ]);
                echo "</br>";
              // check and count searched keywords and location if true on this fields( brand,location ) 
              echo 'debug brandx  '.$bA = Automotives::count([
                    'conditions' => 'brand LIKE :brand: AND city LIKE :location:',
                    'bind' => [
                            'brand' => '%' . $keyword . '%',
                            'location' => '%' . $city_code . '%'
                            ]
                ]);
                echo "</br>";
              // check and count searched keywords and location if true on this fields( model,location ) 
              echo 'debug modelx  '.$mA = Automotives::count([
                    'conditions' => 'model LIKE :model: AND city LIKE :location:',
                    'bind' => [
                            'model' => '%' . $keyword . '%',
                            'location' => '%' . $city_code . '%'
                            ]
                ]);

            echo "</br>";
                if($nA > 0 AND $bA > 0 AND  $mA > 0 ){
            echo 'debug 1';
                echo "</br>";
                        $conditions .= ' name LIKE :name: OR brand LIKE :brand: OR model LIKE :model: ';
                        $bind['name'] = '%'.$keyword.'%';
                        $bind['brand'] = '%'.$keyword.'%';
                        $bind['model'] = '%'.$keyword.'%';

                // run this code if the keyword find data inside this fields [name,brand]
                }elseif($nA > 0 AND $bA > 0  ){
            echo 'xam 2';
                echo "</br>";
                        $conditions .= ' name LIKE :name: AND brand LIKE :brand:  ';
                        $bind['name'] = '%'.$keyword.'%';
                        $bind['brand'] = '%'.$keyword.'%';

                // run this code if the keyword find data inside this fields [name,model]
                }elseif($nA > 0 AND $mA > 0  ){
           echo 'debug 3';
                echo "</br>";
                        $conditions .= ' name LIKE :name: AND model LIKE :model: ';
                        $bind['name'] = '%'.$keyword.'%';
                        $bind['model'] = '%'.$keyword.'%';

                // run this code if the keyword find data inside this fields [brand,model]
                }elseif($bA > 0 AND $mA > 0  ){
           echo 'debug 4';
                echo "</br>";
                        $conditions .= ' brand LIKE :brand: AND model LIKE :model: ';
                        $bind['brand'] = '%'.$keyword.'%';
                        $bind['model'] = '%'.$keyword.'%';

                // run this code if the keyword find data inside this name field
                }elseif($nA > 0 ){
           echo 'debug 5';
                echo "</br>";
                        $conditions .= ' name LIKE :name: ';
                        $bind['name'] = '%'.$keyword.'%';

                // run this code if the keyword find data inside this brand field
                }elseif($bA > 0 ){
           echo 'debug 6';
                echo "</br>";
                        $conditions .= ' brand LIKE :brand: ';
                        $bind['brand'] = '%'.$keyword.'%';

                // run this code if the keyword find data inside this brand model
                }elseif($mA > 0 ){
           echo 'debug 7';
                echo "</br>";
                        $conditions .= ' model LIKE :model: ';
                        $bind['model'] = '%'.$keyword.'%';
                }else{
           echo 'debug 8';
                echo "</br>";
                    $conditions .= ' name LIKE :name: OR brand LIKE :brand: OR model LIKE :model: ';
                    $bind['name'] = '%'.$keyword.'%';
                    $bind['brand'] = '%'.$keyword.'%';
                    $bind['model'] = '%'.$keyword.'%';

                }

            }
    
           if(!empty($keyword) AND !empty($city_code)  ){

                if (!empty($city_code)) {
                    $conditions .= ' AND city LIKE :location:';
                    $bind['location'] = '%'.$city_code.'%';
                }
    
            }else{
                if (!empty($city_code)) {
                    echo $city_code;
                    $conditions .= ' city LIKE :location:';
                    $bind['location'] = '%'.$city_code.'%';
                }
               if (!empty($keyword)) {
                echo 'wasabe';
                    $conditions .= ' name LIKE :name: OR brand LIKE :brand: OR model LIKE :model: ';
                    $bind['name'] = '%'.$keyword.'%';
                    $bind['brand'] = '%'.$keyword.'%';
                    $bind['model'] = '%'.$keyword.'%';
               }
            }//end if[1]

            // nag chechek kung empty ba yung two fields find and location
            // if empty two fields return nothing
           if(!empty($keyword) AND !empty($city_code) OR 
            $keyword != NULL AND $city_code != NULL OR 
            $keyword != '' AND $city_code != ''){
            $autos = Automotives::find([
                'columns' => '*',
                'conditions' => $conditions,
                'bind' => $bind,
                'limit' => '6'
            ]);
           }


    }
        $this->view->setVar('autos', $autos);

/************************************************************************************/

        //PENDING 
        // Location
        // Country
        // cities
        // address
        $conditions = '';
        $bind = array();
        $things = '';
        if($no == 3 OR $no == 0){

            if (!empty($keyword)) {
                $conditions = 'name LIKE :name:';
                $bind['name'] = '%'.$keyword.'%';
            }

            $HLP = (!empty($keyword) AND !empty($city_code)) ? 'AND' : '' ;

                if (!empty($city_code)) {
                    echo $city_code;
                    $conditions .= $HLP.' city LIKE :location:';
                    $bind['location'] = '%'.$city_code.'%';
                }

            $things = Things::find([
                'columns' => '*',
                'conditions' => $conditions,
                'bind' => $bind,
                'limit' => '6'
            ]);
        }
       $this->view->setVar('things', $things);


/************************************************************************************/


        //JOBS
        $conditions = ''; 
        $bind = array();
        $jobs = '';
    if($no == 5 OR $no == 0){



            if (!empty($keyword)) {
                $conditions = 'position LIKE :position:';
                $bind['position'] = '%'.$keyword.'%';
            }

            $JHP = (!empty($keyword) AND !empty($city_code)) ? 'AND' : '' ;

                if (!empty($city_code)) {
                    echo $city_code;
                    $conditions .= $JHP.' city LIKE :location:';
                    $bind['location'] = '%'.$city_code.'%';
                }


            $jobs = Jobs::find([
                 'columns'    => '*',
                 'conditions' => $conditions, 
                 'bind' => $bind,
                 'limit' => '6'
            ]);

        }

        $this->view->setVar('jobs', $jobs);

/************************************************************************************/
        //realties
        $conditions = '';
        $bind = array();
        $realties = '';
        if($no == 3 OR $no == 0){

            if (!empty($keyword)) {
                $conditions = 'name LIKE :name:';
                $bind['name'] = '%'.$keyword.'%';
            }

            $RHP = (!empty($keyword) AND !empty($city_code)) ? 'AND' : '' ;

                if (!empty($city_code)) {
                    echo $city_code;
                    $conditions .= $RHP.' city LIKE :location:';
                    $bind['location'] = '%'.$city_code.'%';
                }

            $realties = Realties::find([
                'columns' => '*',
                'conditions' => $conditions, 
                'bind' => $bind,
                'limit' => '6'            
            ]);
        }

        $this->view->setVar('realties', $realties);
/************************************************************************************/

    }//end of function seearch
    

    /**
     * [infoAction description]
     * @return [type] [description]
     */
    public function infoAction(){
        $this->view->setTemplateAfter('info_default');
    } 

    /**
     * [knowl_centerAction description]
     * @return [type] [description]
     */
    public function knowl_centerAction(){
        $this->view->setTemplateAfter('info_default');
    } 

    /**
     * univsersal search for City append State code and country name
     *
     *  form sample
     *
     *   for multiple use : you need to change the id's only
     * 
     *   <div class="input_container" id="universal">
     *   <input type="text" id="country_id" class="city_list form-control  wd" autocomplete="off" name="near">
     *   <input type="hidden" id="city_code" name="city_code" class="city_id">
     *   <ul id="country_list_id" class="show_city"></ul>
     *   </div>
     *   
     * [ajaxAddressAction description]
     * @return [type] [description]
     */
    public function ajaxAddressAction(){

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

        //if ajax request run
        if ($this->request->isAjax()) {

            //get the parameters
            $keyword = $this->request->getQuery('keyword');
            $parent = $this->request->getQuery('parent');

            // variables default
            $conditions = ''; 
            $bind = array();
            $cities = '';
     
                //query string
                $conditions .= ' city_name LIKE :keyword:';
                $bind['keyword'] = '%'.$keyword.'%';
     
            //queryload
            $cities = Cities::find([
                'columns' => '*',
                'conditions' => $conditions,
                'bind' => $bind,
                'order' => 'id asc',
                'limit' => '10'
            ]);

            //fetch the addres list
            foreach ($cities as $rs) {

                // put in bold the written text
                 $country_name = preg_replace("/".$keyword."/i", "<b>\$0</b>", $rs->city_name);

                // get the country name to another table CountryCodes
                 $country = CountryCodes::findFirst("id = '$rs->country_code_id' ");
                
                //display the list: onclick go to designated id input field
                echo '<li id ="'.$keyword.'" onclick="set_item(\''.str_replace("'", "\'", $rs->city_name.', '.$rs->state_code_id).', '.$country->country_name.'\',\''.$parent.'\');set_code(\''.$rs->id.'\',\''.$parent.'\')"><i class="fa fa-map-marker"></i> '.$country_name.', '.$rs->state_code_id.' '.$country->country_name.'</li>';
          

            }//end of for each

        }//end of ajax request


    } //end of function


    /**
     * [ajaxAddressAction description]
     * @return [type] [description]
     */
    public function ajaxAddress2Action(){

        // This is an Ajax response so it doesn't generate any kind of view
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        //if ajax request run
        if ($this->request->isAjax()) {
            $keyword = $this->request->getQuery('keyword');
            // variables default
            $conditions = ''; 
            $bind = array();
            $cities = '';
   
            //query string
            $conditions .= ' city_name LIKE :keyword:';
            $bind['keyword'] = '%'.$keyword.'%';
     
            //queryload
            $cities = Cities::find([
                'columns' => '*',
                'conditions' => $conditions,
                'bind' => $bind,
                'order' => 'id asc',
                'limit' => '10'
            ]);

            //fetch the addres list
            foreach ($cities as $rs) {
                // put in bold the written text
                 $country_name = preg_replace("/".$keyword."/i", "<b>\$0</b>", $rs->city_name);
                // get the country name to another table CountryCodes
                 $country = CountryCodes::findFirst("id = '$rs->country_code_id' ");
                //display the list: onclick go to designated id input field
                echo '<li id ="'.$keyword.'" ><i class="fa fa-map-marker"></i> '.$country_name.', '.$rs->state_code_id.' '.$country->country_name.'</li>';

            }//end of for each
        }//end of ajax request
    } //end of function



    public function remittanceAction(){
        $this->view->setTemplateAfter('default1');
    }

    public function registerremitAction() {
        $this->view->setTemplateAfter('default1');
    }
    
    public function registerremit2Action() {
        $this->view->setTemplateAfter('default1');
    }

    public function remitindexAction() {
        $this->view->setTemplateAfter('default1');
    }

    public function availableLocationsAction() {
       $this->view->setTemplateAfter('default1');
    }
}









