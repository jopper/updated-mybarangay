<?php

class MemberEducation extends \Phalcon\Mvc\Model{

	public $id;
	public $member_id;
	public $education_id;
	public $institute;
	public $major;
	public $year;
	public $score;
	public $start_date;
	public $end_date;

    public function initialize(){
         $this->belongsTo("member_id", "Member", "id");
    }

    /**
     * Independent Column Mapping.
     */
	public function columnMap(){
		return array(
			'id' => 'id', 
			'member_id' => 'member_id', 
			'education_id' => 'education_id', 
			'institute' => 'institute', 
			'major' => 'major', 
			'year' => 'year', 
			'score' => 'score', 
			'start_date' => 'start_date', 
			'end_date' => 'end_date'
		);
	}

}
