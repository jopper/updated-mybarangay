<?php

class MemberWorkexperience extends \Phalcon\Mvc\Model{

	public $id;
	public $member_id;
	public $start_date;
	public $end_date;
	public $company;
	public $job_description;
	public $contact_no;
	public $status;

    public function initialize(){
         $this->belongsTo("member_id", "Member", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'start_date' => 'start_date', 
            'end_date' => 'end_date', 
            'member_id' => 'member_id',
            'company' => 'company',
            'job_description' => 'job_description',
            'contact_no' => 'contact_no',
            'status' => 'status'
        );
    }

}
