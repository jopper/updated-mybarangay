<?php get_header(); ?>     
        <main role="main" class="main">
          <div class="page-home">
            <div class="container">
              <div class="row">
                <div class="col-sm-8">
                  <div class="page-content">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                      <article>
                        <span class="cat-post"><?php the_category(' '); ?></span>
                        <?php the_post_thumbnail(); ?>
                        <h1><?php the_title(); ?></h1>
                        <div class="post-info">by <address class="author"><?php the_author_posts_link(); ?> / <time><?php the_time( get_option( 'date_format' ) ); ?></time></address></div>
                        <?php the_content(); ?>
                        
                      </article>
                      <div class="head-divider">
                         <div class="flag-banner"></div>
                    </div>
                      <?php comments_template(); ?>
                    <?php endwhile; else : ?>
                      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-sm-4">
                  <?php get_sidebar(); ?>
                </div>
              </div>
            </div>
          </div>
        </main>

<?php get_footer(); ?>