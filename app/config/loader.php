<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->libraryDir,
        $config->application->formsDir,
        $config->application->tagHelpers,
        $config->application->stripe,
        $config->application->customHelpers
    )
);

/*
	Register some namespaces
*/
$loader->registerNamespaces(
	array(
		//"Helpers" => $config->application->tagHelpers,
		"Helpers\YelpApi" => $config->application->yelpApi,
		"Stripe" => $config->application->stripe,
        "Facebook" => $config->application->facebookSdk,
        "Abraham\TwitterOAuth" => $config->application->twitterOauth,
        "Abraham\TwitterOAuth\Util" => $config->application->twitterOauthJsonDecoder
	)
);

$loader->register();
