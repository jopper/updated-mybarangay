<?php


class Things extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var integer
     */
    public $member_id;

    /**
     *
     * @var integer
     */
    public $thing_condition_id;

    /**
     *
     * @var integer
     */
    public $thing_category_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $price;

    /**
     *
     * @var string
     */
    public $details;

    /**
     *
     * @var integer
     */
    public $view_hits;




    public $contact_no;
    public $email;
    public $other_name;



    /**
     *
     * @var string
     */
    public $street;

    /**
     *
     * @var string
     */
    public $city;

    /**
     *
     * @var string
     */
    public $country_code_id;


    /**
     *
     * @var string
     */
    public $state;


    /**
     *
     * @var integer
     */
    public $status;


    public $currency_type;


    public function initialize()
    {
        $this->belongsTo("thing_category_id", "ThingCategories", "id");
        $this->belongsTo("thing_condition_id", "ThingConditions", "id");
        $this->belongsTo("member_id", "Members", "id");
        $this->hasMany("id", "ThingPhotos", "thing_id");
        $this->belongsTo('country_code_id', 'CountryCodes', 'id');
    }




    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'created' => 'created',
            'modified' => 'modified',
            'member_id' => 'member_id',
            'name' => 'name',
            'price' => 'price',
            'thing_condition_id' => 'thing_condition_id',
            'thing_category_id' => 'thing_category_id',
            'details' => 'details',
            'view_hits' => 'view_hits',
            'contact_no' => 'contact_no',
            'email' => 'email',
            'other_name' => 'other_name',

            'view_hits' => 'view_hits',
            'contact_no' => 'contact_no',
            'email' => 'email',
            'other_name' => 'other_name',
            'street' => 'street',
            'city' => 'city',
            'country_code_id' => 'country_code_id',
            'state' => 'state',
            'status' => 'status',
            'currency_type' => 'currency_type'

        );
    }

}
