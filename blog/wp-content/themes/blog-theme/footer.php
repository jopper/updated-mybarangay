        <footer role="contentinfo" class="footer">
          <div class="footer-a">
            <div class="container clearfix">
              <ul>
                <?php 
                  wp_list_categories('hide_empty=0&title_li='); 
                ?>
              </ul>
              <a href="<?php echo get_the_permalink(2); ?>">About Us</a>
            </div>
          </div> <!--/ footer-a -->
          <div class="footer-b">
            <div class="container clearfix">
             <?php  wp_nav_menu( array(
                 'depth' => 1,
                 'container' => false,
                 'menu_class' => 'footer-nav',
                 'walker' => new wp_bootstrap_navwalker())
              ); ?>
              <ul class="footer-social">
                <li><a href="<?php echo get_option('general_setting_facebook'); ?>"><i class="fa fa-facebook"></i></a></li>
                <li><a href="<?php echo get_option('general_setting_googleplus'); ?>"><i class="fa fa-google-plus"></i></a></li>
               <!-- <li><a href="<?php //echo get_option('general_setting_linkedin'); ?>"><i class="fa fa-linkedin"></i></a></li> -->
                <li><a href="<?php echo get_option('general_setting_twitter'); ?>"><i class="fa fa-twitter"></i></a></li>
              </ul>
            </div>
          </div> <!--/ footer-b -->
          <div class="footer-c">
            <div class="container clearfix">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_f.png" alt="Image">
              <small>Copyright <?php echo date("Y"); ?></small>
            </div>
          </div>
        </footer>
        
  <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>-->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/jquery-1.11.0.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>
		<?php wp_footer(); ?>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
       <!-- <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>-->
        <script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'mybarangayblog';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>
    </body>
</html>
