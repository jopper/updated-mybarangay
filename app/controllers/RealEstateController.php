<?php
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;
    
class RealEstateController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
    }

  public function getCitiesAction() {
        if ($this->request->isAjax()) {
            $arrCities = array();
             $stateCode = $this->request->getQuery('param1');
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }
            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }





    public function getStatesAction() {

        if ($this->request->isAjax()) {
            $arrStates = array();
            $countryCode = $this->request->getQuery('param');


            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

          

            return $response;
        }
    }


   /**
     * [flagAction description]
     * @return [type] [description]
     */
    public function flagAction(){

        $this->view->disable();



        if($this->request->isAjax()) 
        {
      
            $review = $this->request->getQuery('review');
            $reviewer = $this->request->getQuery('reviewer');
            $flagger = $this->request->getQuery('flagger');
            $flag_type = $this->request->getQuery('flag_type');
            $referer = $this->request->getHTTPReferer (); 
            $controller = 'realestate';

            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }
            
            $arrayName = array('review' => $review, 'reviewer' => $reviewer, 'flagger' => $flagger, 'result' => $resultInfo);
            echo json_encode(array('result' => 'OK', 'items' => $arrayName));
        }


    }











    public function formValidate() {
        $validation = new Phalcon\Validation();
        $validation->add('name', new PresenceOf(array(
            'message' => 'name field is required'   
        )));
        $validation->add('details', new PresenceOf(array(
            'message' => 'details field is required'    
        )));
        $validation->add('price', new PresenceOf(array(
            'message' => 'price field is required'  
        )));
        $validation->add('realty_condition_id', new PresenceOf(array(
            'message' => 'condition field is required'  
        )));
        $validation->add('realty_type_id', new PresenceOf(array(
            'message' => 'type field is required'   
        )));
        $validation->add('house_area', new PresenceOf(array(
            'message' => 'house area field is required' 
        )));
        $validation->add('lot_area', new PresenceOf(array(
            'message' => 'lot area field is required'   
        )));
        $validation->add('bedrooms', new PresenceOf(array(
            'message' => 'bedroom field is required'    
        )));
        $validation->add('bathrooms', new PresenceOf(array(
            'message' => 'bathroom field is required'   
        )));
        $validation->add('parking', new PresenceOf(array(
            'message' => 'parking field is required'    
        )));
        $validation->add('pets_allowed', new PresenceOf(array(
            'message' => 'pet allowed field is required'    
        )));

        return $validation->validate($_POST);
    
    }

   
   /**
     * [indexAction description]
     * @return [type] [description]
     */
    public function indexAction(){
   
       // set default1 template for new design template
        $this->view->setTemplateAfter('default1');

        $realties = '';


        $name = '';
        $realty_category_id = '';
        $realty_condition_id = '';
        $realty_type_id = '';
        $minPrice = '';
        $maxPrice = '';
        $btn_search = '';
        $advertised = '';
        $sql = "SELECT * FROM Realties ";
        if (isset($_GET['btn_search'])) {

            $locations = [];
            $getters = [];
            $queries = [];
            foreach ($_GET as $key => $value) {
               $temp = is_array($value) ? $value : trim($value) ;
               if(!empty($temp)){
                    list($key) = explode("-", $key);
                    if($key == 'name'){
                        array_push($locations, $value);
                    }
                    if(!in_array($key, $getters)){
                        $getters[$key] = $value;
                    }
               }
            }
            if(!empty($locations)){
                $loc_qry = implode(",", $locations);
            }
            if(!empty($getters)){
                foreach ($getters as $key => $value) {
                   ${$key} = $value;
                    switch ($key) {
                        case 'name':
                                $name = $this->real_escape($name);
                               array_push($queries, "(name LIKE '%$name%')");
                            break;
                        case 'realty_category_id':
                               array_push($queries, "(realty_category_id = $realty_category_id)");
                            break;
                       case 'realty_condition_id':
                               array_push($queries, "(realty_condition_id = $realty_condition_id)");
                            break;
                       case 'realty_type_id':
                               array_push($queries, "(realty_type_id = $realty_type_id)");
                            break;
                       case 'advertised':
                               array_push($queries, "(advertised = $advertised)");
                            break;
                        case 'minPrice':
                        break;
                        case 'maxPrice':
                        break;
                        case 'btn_search':
                        if(is_numeric($minPrice) OR is_numeric($maxPrice)){
                            if(!empty($minPrice) AND !empty($maxPrice)){
                                array_push($queries, "(price BETWEEN $minPrice AND $maxPrice )");
                            }else{
                                if(!empty($minPrice)){
                                    array_push($queries, "(price < $minPrice )");
                                }
                                if(!empty($maxPrice)){
                                    array_push($queries, "(price > $maxPrice )");
                                }
                            }
                        }
                        break;   



                        default:
                            # code...
                            break;
                    }
                }
            }
            if(!empty($queries)){
                $sql .= " WHERE ";
                $i = 1;
                foreach ($queries as $query) {
                    if($i < count($queries)){
                        $sql .= $query." AND ";
                    }else{
                        $sql .= $query;
                    }
                    $i++;
                }

            }

     // $this->view->disable();
        }

        $realties = $this->modelsManager->executeQuery($sql);
        $this->view->setVars([
        'realties' => $realties,
        'name' => $name, 
        'advertised' => $advertised, 
        'realty_category_id' => $realty_category_id,
        'realty_condition_id' => $realty_condition_id,
        'realty_type_id' => $realty_type_id,
        'btn_search' => $btn_search,
        'minPrice' => $minPrice,
        'maxPrice' => $maxPrice,
        'city' => $city,
        'city_code' => $city_code
        ]);
        $this->view->realtyCategories = RealtyCategories::find([
            'order' => 'realty_type_id ASC'
            ]);
    }



    public function newAction(){

    $this->view->setTemplateAfter('default1');
        if ($this->request->isPost()) {
        
        $messages = $this->formValidate();
        if (count($messages)) {
            $this->view->disable();
            $errorMsg = '';
            foreach ($messages as $msg) {
              $errorMsg .= $msg . "<br>";       
            }
            $this->session->set('errorMessasge', $errorMsg); 
            // $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);

            //return $this->response->redirect('real_estate/new');   
        } 

            // $countryId = $this->request->getPost('country_id');
            // $country = Countries::findFirst(array('columns'    => '*', 
            //                                  'conditions' => 'id LIKE :id:', 
            //                                  'bind' => array('id' => $countryId)));
            // $countryName = '';
            // if($country) {
            //     $countryName = $country->country;
            // } 
    
            // $address = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            // $userSession = $this->session->get("userSession");
            // $content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            // $json = json_decode($content, true);
            // $lat = $json['results'][0]['geometry']['location']['lat'];
            // $lng = $json['results'][0]['geometry']['location']['lng'];
            //error_log('LAT : '.$lat.' LONG : '.$lng);
            // $property->lat = $lat;
            // $property->lng = $lng;
         $userSession = $this->session->get("userSession");
            $property = new Realties();
            $property->created = date('Y-m-d H:i:s'); 
            $property->modified = date('Y-m-d H:i:s'); 
            $property->member_id =  $userSession['id'];
            $property->name = $this->request->getPost('name');
            $property->advertised =$this->request->getPost("advertised");
            $property->details = $this->request->getPost('details');
            $property->price = str_replace(',', '', $this->request->getPost('price'));
            $property->realty_condition_id = $this->request->getPost('realty_condition_id');
            $property->realty_category_id = $this->request->getPost('realty_category_id');
            $property->realty_type_id = $this->request->getPost('realty_type_id');
            $property->house_area = $this->request->getPost('house_area');
            $property->lot_area = $this->request->getPost('lot_area');
            $property->bedrooms = $this->request->getPost('bedrooms');
            $property->bathrooms = $this->request->getPost('bathrooms');
            $property->parking = $this->request->getPost('parking');
            $property->pets_allowed = $this->request->getPost('pets_allowed');
            $property->contact_no = $this->request->getPost("contact_no");
            $property->email = $this->request->getPost("email");
            $property->other_name =$this->request->getPost("other_name");
            $property->currency_type = $this->request->getPost("currency_type");

            $property->street = $this->request->getPost('street');
            $property->state = $this->request->getPost('state');
            $property->city = $this->request->getPost('city');
            $property->country_code_id = $this->request->getPost('country_code_id');



            if($property->create()){


                $id = $property->id;
                if($this->request->hasFiles() == true) {

                   
                    // $this->folderOrganizer('classfieds', $id);
                    $uploads = $this->request->getUploadedFiles();

                    // upload default to false
                    $isUploaded = false;

                    //allowed file extension
                    $fileImageExt = array('jpeg', 'jpg', 'png');

                    $filePath = 'img/realties/';

                    $fileType = '';

                    foreach ($uploads as $upload) {

                        $fileName = $upload->getname() ;

                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        // filename convert to md5
                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'-re.'.$fileExt;

                        $path = NULL;

                        //
                        if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/realties/'.$newFileName;
                        }

                       //move the file and simultaneously check if everything was ok
                       ($path != NULL OR !empty($path)) ? $isUploaded = true : $isUploaded = false;

                        if ($isUploaded) {
                            $realtyPhotos = new RealtyPhotos();
                            $realtyPhotos->created = date('Y-m-d H:i:s');
                            $realtyPhotos->modified = date('Y-m-d H:i:s');
                            $realtyPhotos->member_id = $userSession['id'];
                            $realtyPhotos->realty_id = $id;
                            $realtyPhotos->file_path = $filePath;
                            $realtyPhotos->filename = $newFileName;
                           
                            if ($realtyPhotos->create()) {
                                $upload->moveTo($path);

                            } else {
                                $this->session->set('errorMessasge', 'Uploading Error');
                                //print_r($realtyPhotos->getMessages());
                            }
                        }

                    }

                }
                $this->session->set('successMessasge', 'New property successfully added');
               return $this->response->redirect('real_estate/view2/'.$property->id);
            }else{
                print_r($property->getMessages());
                $this->view->disable();
            }

        }

        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $realtyConditions = RealtyConditions::find();
        $this->view->setVar('realtyConditions', $realtyConditions);
        $realtyTypes = RealtyTypes::find();
        $this->view->setVar('realtyTypes', $realtyTypes);
        $realtyCategories = RealtyCategories::find();
        $this->view->setVar('realtyCategories', $realtyCategories);
    }




    public function updateAction($id = null){


        $this->view->setTemplateAfter('default1');
        
        if ($this->request->isPost()) {
   

            $messages = $this->formValidate();
            if (count($messages)) {
                $this->view->disable();
                $errorMsg = '';
                foreach ($messages as $msg) {
                  $errorMsg .= $msg . "<br>";       
                }
            $this->session->set('errorMessasge', $errorMsg); 
            return $this->response->redirect('real_estate/update/'.$id);   
       
            } 

            // $countryId = $this->request->getPost('country_id');
            // $country = Countries::findFirst(array('columns'    => '*', 
            //                                  'conditions' => 'id LIKE :id:', 
            //                                  'bind' => array('id' => $countryId)));
            // $countryName = '';
            // if($country) {
            //     $countryName = $country->country;
            // } 



            // $address = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            $userSession = $this->session->get("userSession");
            // $content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            // $json = json_decode($content, true);
            // $lat = $json['results'][0]['geometry']['location']['lat'];
            // $lng = $json['results'][0]['geometry']['location']['lng'];
            //error_log('LAT : '.$lat.' LONG : '.$lng);

            $property = Realties::findFirst($id); 
            $property->modified = date('Y-m-d H:i:s'); 
            $property->member_id =  $userSession['id'];
            $property->name = $this->request->getPost('name');
            $property->advertised =$this->request->getPost("advertised");
            $property->details = $this->request->getPost('details');
            $property->price = str_replace(',', '', $this->request->getPost('price'));
            // $property->street = $this->request->getPost('street');
            // $property->city = $this->request->getPost('city');
            // $property->country_id = $this->request->getPost('country_id');
            // $property->lat = $lat;
            // $property->lng = $lng;
            $property->realty_condition_id = $this->request->getPost('realty_condition_id');
            $property->realty_category_id = $this->request->getPost('realty_category_id');
            $property->realty_type_id = $this->request->getPost('realty_type_id');
            $property->house_area = $this->request->getPost('house_area');
            $property->lot_area = $this->request->getPost('lot_area');
            $property->bedrooms = $this->request->getPost('bedrooms');
            $property->bathrooms = $this->request->getPost('bathrooms');
            $property->parking = $this->request->getPost('parking');
            $property->pets_allowed = $this->request->getPost('pets_allowed');
            $property->contact_no = $this->request->getPost("contact_no");
            $property->email = $this->request->getPost("email");
            $property->other_name =$this->request->getPost("other_name");
            $property->currency_type = $this->request->getPost("currency_type");


            $property->street = $this->request->getPost('location');
            $property->country_code_id = $this->request->getPost("country_id");
            $property->state = $this->request->getPost("state");
            $property->city =$this->request->getPost("city");


            
            if($property->update()){
               
                $this->session->set('successMessasge', 'Information Successfuly updated'); 
      
              return $this->response->redirect('real_estate/update/'.$property->id);
            }else{
                    foreach ($property->getMessages() as $error) {
                       $errorMsg .= $error->getMessage() .'</br>';
                    }
                    $this->session->set('errorMessasge', $errorMsg);
            }

        }



        $property = Realties::findFirst($id);

        $this->view->setVar('property', $property);

        $realtyPhotos = RealtyPhotos::find('realty_id ="'.$id.'"');
        $this->view->setVar('realtyPhotos', $realtyPhotos);
        
        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $realtyConditions = RealtyConditions::find();
        $this->view->setVar('realtyConditions', $realtyConditions);
        $realtyTypes = RealtyTypes::find();
        $this->view->setVar('realtyTypes', $realtyTypes);
        $realtyCategories = RealtyCategories::find();
        $this->view->setVar('realtyCategories', $realtyCategories);
    }


    public function update_photoAction($id = null){

        $this->view->disable();
        if ($this->request->isPost()) {


                if($this->request->hasFiles() == true) {


                    $userSession = $this->session->get("userSession");
                    // $this->folderOrganizer('classfieds', $id);
                    $uploads = $this->request->getUploadedFiles();

                    // upload default to false
                    $isUploaded = false;

                    //allowed file extension
                    $fileImageExt = array('jpeg', 'jpg', 'png');

                    $filePath = 'img/realties/';

                    $fileType = '';

                    foreach ($uploads as $upload) {

                        $fileName = $upload->getname() ;

                        $fileInfo = new SplFileInfo($fileName);

                        $fileExt  = strtolower($fileInfo->getExtension());

                        // filename convert to md5
                        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'-re.'.$fileExt;

                        $path = NULL;

                        //
                        if(in_array($fileExt, $fileImageExt)){
                                $path = 'img/realties/'.$newFileName;
                        }

                       //move the file and simultaneously check if everything was ok
                       ($path != NULL OR !empty($path)) ? $isUploaded = true : $isUploaded = false;

                        if ($isUploaded) {
                            $photo = new RealtyPhotos();
                            $photo->created = date('Y-m-d H:i:s');
                            $photo->modified = date('Y-m-d H:i:s');
                            $photo->member_id = $userSession['id'];
                            $photo->realty_id = $id;
                            $photo->file_path = $filePath;
                            $photo->filename = $newFileName;
                           
                            if ($photo->create()) {
                                $upload->moveTo($path);
                                $this->session->set('successMessasge', 'Photo Succesfuly Added');
                            } else {
                                $error = '';
                                //print_r($photo->getMessages());
                                foreach ($photo->getMessages() as $error) {
                                   $error .= $photo->getMessage.'</br>';
                                }
                                 $this->session->set('errorMessasge', 'Uploading Error </br>' .$error);
                            }
                        }

                    }

                }

            $this->response->redirect('real_estate/update/'.$id);
        }else{

            $this->response->redirect('real_estate/update/'.$id);
        }

    }

    public function delete_photoAction($id = null, $itemid = null){


        $photo = RealtyPhotos::findFirst($id);
        $itemid = $photo->realty_id;
        $trashImg = 'img/realties/'.$photo->filename;
        unlink($trashImg);
        
        if ($photo->delete()) {
            $this->session->set('successMessasge', 'Photo Succesfuly Deleted');

        }else{
            $this->session->set('successMessasge', 'Error can\'t delete');
        }
        $this->response->redirect('real_estate/update/'.$itemid);

    }

    public function viewAction($id = null)
    {
        $property = Realties::findFirst($id);
        if(!$property) {
            return $this->response->redirect('real_estate/index');
        }
        $this->view->setVar('property', $property);
    }

    public function view2Action($id = null)
    {
    $this->view->setTemplateAfter('default1');
        $property = Realties::findFirst($id);
        if(!$property) {
            return $this->response->redirect('real_estate/index');
        }
        $this->view->setVar('property', $property);
    }

}

