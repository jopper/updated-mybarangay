<?php

/* @Overlay/notifyParentIframe.twig */
class __TwigTemplate_84179227ecf674e6bd7d26fd34b4f672808e286f756c14626129c5eab847397a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
<head>
    <title></title>
    <meta name=\"robots\" content=\"noindex,nofollow\">
</head>
<body>
<script type=\"text/javascript\">
    // go up two iframes to find the piwik window
    var piwikWindow = window.parent.parent;
    // notify piwik of location change
    // the location has been passed as the hash part of the url from the overlay session
    piwikWindow.Piwik_Overlay.setCurrentUrl(window.location.hash.substring(1));
</script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "@Overlay/notifyParentIframe.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
