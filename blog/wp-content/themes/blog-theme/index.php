<?php get_header(); ?>     
        <main role="main" class="main">
          <div class="page-home">
            <div class="container">
              <div class="row">
                <div class="col-sm-8">
                  <div class="page-content">

                    <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $the_query = new WP_Query('post_type=post&posts_per_page=3&order_by=date&paged='. $paged); ?>

                    <?php if ( $the_query->have_posts() ) : ?>

                      <!-- the loop -->
                      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                      <article>
                        <span class="cat-post"><?php the_category(' '); ?></span>
                        <?php the_post_thumbnail(); ?>
                        <h1><?php the_title(); ?></h1>

                        <div class="post-info">by <address class="author"><?php the_author_posts_link(); ?> / 
                          <time><?php the_time( get_option( 'date_format' ) ); ?></time></address>
                           <a href="<?php the_permalink(); ?>#disqus_thread" class="comment-count"></a>
                        </div>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>">Read More</a>
                      </article>
                      <hr>
                      <?php endwhile; ?>
                      <!-- end of the loop -->
                      <?php wp_paginate(); ?>
                      <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
  
                  </div>
                </div>
                <div class="col-sm-4">
                  <?php get_sidebar(); ?>
                </div>
              </div>
            </div>
          </div>
        </main>

<?php get_footer(); ?>