<?php


class CountryCodes extends \Phalcon\Mvc\Model
{

    public $id;
    public $country_name;

    public function initialize()
    {
         $this->hasMany("id", "States", "country_code_id");
         $this->hasMany("id", "Cities", "country_code_id");
	 $this->hasMany("id", "Business", "country_code_id");
	 $this->hasMany("id","Members", "country_code_id");
        $this->hasMany('id', 'Events', 'country_code_id');

        $this->hasMany('id', 'Automotives', 'country_code_id');
        $this->hasMany('id', 'Jobs', 'country_code_id');
        $this->hasMany('id', 'Realties', 'country_code_id');
        $this->hasMany('id', 'Things', 'country_code_id');
    }



    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'country_name' => 'country_name'
        );
    }

}
