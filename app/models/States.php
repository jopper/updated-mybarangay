<?php


class States extends \Phalcon\Mvc\Model
{

    public $country_code_id;
    public $id;
    public $state_name;

    public function initialize()
    {
        $this->belongsTo("country_code_id", "CountryCodes", "id");
        $this->hasMany("id", "Cities", "state_code_id");
    }



    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'country_code_id' => 'country_code_id',
            'state_name' => 'state_name'
        );
    }

}