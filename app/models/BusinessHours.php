<?php

class BusinessHours extends \Phalcon\Mvc\Model
{

    public $id;
    public $created;
    public $modified;
    public $business_id;
    public $member_id;
    public $day;
    public $start;
    public $end;
    public $status;


    public function initialize()
    {
        $this->belongsTo("business_id", "Business", "id");
        $this->belongsTo("member_id", "Members", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {   
        return array(
            'id' => 'id', 
            'created' => 'created', 
            'modified' => 'modified', 
            'member_id' => 'member_id',
            'business_id' => 'business_id', 
            'day' => 'day',
            'start' => 'start',
            'end' => 'end',
            'status' => 'status'
        );
    }

}
