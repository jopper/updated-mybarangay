<?php

use Phalcon\Mvc\Controller;

use Phalcon\Mvc\Dispatcher as Dispatcher;

class ControllerBase extends Controller
{

	

	public function initialize()
	{
		$this->view->setTemplateAfter('default1');
		$this->session;
		$this->fbSetDefault;
		$this->cookie_login();
		$userSession = array();
		$pxpic = '';
		if ($this->session->has("userSession")) {
           	//Retrieve its value
        	$userSession = $this->session->get("userSession");
           	//$this->view->setVar("firstName", $userSession['first_name']);
           	$pxpic = $this->userImage($userSession['id']);
        }
        //error_log($userSession['primary_pic']); 

        $this->view->setVars([
        	'userSession'	=> $userSession,
			'pxpic'			=> $pxpic
        	]);
	}


	public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $userSession['type'] = '';
        $userSession['admin_access'] = 0;
		if ($this->session->has("userSession")) {
           	//Retrieve its value
        	$userSession = $this->session->get("userSession");
        } 


        $permission = $this->permission($userSession['type'], $userSession['admin_access']);
        if($permission == false) {
        	//$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Please login.');
			return $this->response->redirect('login');
		}

		$this->response->setExpires(new DateTime("2015-09-03 11:14:15.889342"));

// $di = $dispatcher->getDI();
//     	if($userSession['type'] == 'Business'){
//       if(!$di->get('request')->isSecureRequest()){
//           $httpHost = str_ireplace('www.','',$_SERVER['HTTP_HOST']);
//           $url = "https://" . $httpHost . $_SERVER["REQUEST_URI"];
//           $di->get('response')->redirect($url);
//           return false;
//       }
//     	}else{
// $pos = strpos($di->get('request')->getHttpHost(), 'www.');
// if($pos !== false){
// 	$httpHost = str_ireplace('www.','',$_SERVER['HTTP_HOST']);
// 	$url = "http://" . $httpHost . $_SERVER["REQUEST_URI"];
//           $di->get('response')->redirect($url);
//   		return false;
// }
//       if($di->get('request')->isSecureRequest()){
//           $httpHost = str_ireplace('www.','',$_SERVER['HTTP_HOST']);
//           $url = "http://" . $httpHost . $_SERVER["REQUEST_URI"];
//          $di->get('response')->redirect($url);
//         return false;
//       }
//     	}
    }

	public function permission($type = null, $admin_access = null) {
		$controller = $this->dispatcher->getControllerName();
		$action = $this->dispatcher->getActionName();
		$permission = false;

		// test controllers 
		if($controller == 'payment' && in_array($action, array('testing', 'charge')))
		{
			$permission = true;
		}
		if($controller == 'yeppie' && in_array($action, array('testing')))
		{
			$permission = true;
		}
		// end test controllers

		if($controller == 'vlog' && in_array($action, array('index')))
		{
			$permission = true;
		}

		if($controller == 'admincontrol' && in_array($action, array('login'))){
				$permission = true;
			}

		if($controller == 'index' && in_array($action, array('error401', 'ajaxAddress', 'ajaxAddress2', 'show404','knowl_center','index', 'info', 'admin_index', 'search', 'search1','admin_approve_biz_claim', 'admin_cancel_request','admin_reject_request', 'index2','admin_flags'))){
			$permission = true;
		}
		if($controller == 'member' && in_array($action, array('fbConnect','page','fbTest2','fbTest','user_classified','pageu','profileu','galleryu','signup_success','page', 'photos', 'biz_page', 'advertiser_signup', 'advertiser_login', 'advertiser_emailConfimation','signup', 'gallery', 'resetPassword', 'forgotPassword', 'login'))){
			$permission = true;
		}
		if($controller == 'job' && in_array($action, array('index', 'view2'))){
			$permission = true;
		}
		if($controller == 'real_estate' && in_array($action, array('index', 'view2'))){
			$permission = true;
		}
		if($controller == 'thing' && in_array($action, array('index', 'view2'))){
			$permission = true;
		}
		if($controller == 'review' && in_array($action, array('search_business','search_business2', 'index2', 'search_business1'))){
				$permission = true;
		}
		if($controller == 'business' && in_array($action, array('index', 'view', 'photos', 'admin_claim_business', 'add_photo_cover','admin_approve_biz_claim','reject_biz_claim','cancel_biz_claim', 'filtered_seemore', 'view2', 'gallery', 'getdirections', 'test'))){
				$permission = true;
		}
		if($controller == 'car_and_truck' && in_array($action, array('index',  'view2'))){
			$permission = true;
		}


		if($controller == 'event' && in_array($action, array( 'event_request','load_ajax', 'index', 'view', 'view2', 'view_direction','search'))){
			$permission = true;
		}

		if($controller == 'user' && in_array($action, array('admin_login', 'admin_logout', 'admin_add'))){
			$permission = true;
		}

		if($controller == 'biz' && in_array($action, array('logoutMember','signup', 'login', 'logout', 'emailConfimation', 'business_search', 'claim', 'page', 'respond', 'add_photo', 'biz_claimsearch', 'biz_claimresult','claimreq_success', 'howtoclaim'))){
			$permission = true;
		}


		if($controller == 'forum' && in_array($action, array('latest','popular','viewTopic','list_topic','persistent','load_ajax','test','index','view', 'topic_list', 'search'))){
			$permission = true;
		}

		if($controller == 'ajax' && in_array($action, array('index', 'getCities', 'getStates', 'ajax_classifieds'))){
			$permission = true;
		}

		if($type == 'Admin') {
			// none of your business mofo :p
			$permission = false;
		}

		if($type == 'User') {
			if($controller == 'business' && in_array($action, array('admin_index', 'admin_view', 'admin_pending', 'approve','admin_approve_biz_claim','reject_biz_claim','cancel_biz_claim'))){
				$permission = true;
			}
			if($controller == 'business_update' && in_array($action, array('admin_view', 'approve', 'deny'))){
				$permission = true;
			}


		}
		
		if($type == 'Business' OR $admin_access == 1) {
			if($controller == 'payment' && in_array($action, array('testing', 'template', 'pricetable', 'pay','process','proceed', 'pricing', 'charge', 'pricing2', 'webhooks', 'upgradeenhancedpricing','upgradepremiumpricing')))
			{
				$permission = true;
			}
		 	if($controller == 'biz' && in_array($action, array('subscript','business_profile_photo', 'business_gallery','confirmEmail','update_email','change_password','profile','adcampaignu','update_businessu','manage_ad_crop','manage_ad_camp','statistic','manage_adu','pageu','update_business','page1', 'add_photo', 'add_photo1', 'claimreq_success', 'manage_ad', 'add_ad', 'homepage_banner', 'homepage_banner2', 'subpage_banner', 'adcampaign', 'mobile_banner', 'website_banner', 'crop_homepagebanner','howtoclaim', 'delete_leaderboard'))){
		 		$permission = true;
		 	}
		 	if($controller == 'business' && in_array($action, array('add_photo', 'update', 'view2', 'add_image', 'add_image_cover', 'gallery', 'photos','getdirections','new_biz_hours', 'add_image2'))){
				$permission = true;
			}
			if($controller == 'review' && in_array($action, array('search_business', 'add', 'update', 'new_business', 'update_business','getStates', 'getCities'))){
				$permission = true;
			}

			if($controller == 'member' && in_array($action, array('add_photo'))){
			$permission = true;
			}

			if($controller == 'add' && in_array($action, array('new_ad'))){
			$permission = true;
			}

			if($controller == 'index' && in_array($action, array('remittance','registerremit','index'))){
			$permission = true;
			}

			if($controller == 'payment' && in_array($action, array('template'))){
			$permission = true;
			}
		}


		if($type == 'Advertiser') {
		 	// if($controller == 'member' && in_array($action, array('adv','add_photo'))){
		 	// 	$permission = true;
		 	// }
		 }

		if($type == 'Member') {

			if($controller == 'forum' && in_array($action, array('view_edit','topicList','editTopic','deleteTopic','deleteReplyTopic','editReplyTopic','addTopic','reply','compliment','add'))){
				$permission = true;
			}
			if($controller == 'index' && in_array($action, array('availableLocations','remitindex','registerremit','registerremit2','remittance','index', 'index1', 'admin_cancel_request','admin_reject_request','index2'))){
				$permission = true;
			}
			if($controller == 'member' && in_array($action, array('twitterTokenRemover','twitterConnect','classfiedDel', 'memWorkExp', 'memEduc', 'downloadImage', 'updateu','update_photo','update', 'add_photo', 'set_primary_photo', 'delete_photo', 'update_photo_caption','signup', 'change_password', 'gallery', 'update_image_caption', 'add_image', 'upload_image','fbAuth','loginWithFacebook'))){
				$permission = true;
			}
			if($controller == 'business' && in_array($action, array('update_business', 'add_business_photo' ,'add_business','add_photo', 'add_photo1', 'update', 'add_image','view2', 'add_video', 'cropit', 'compliment', 'gallery', 'view3', 'flag', 'new_biz_hours', 'picture_editor', 'getdirections'))){
				$permission = true;
			}
			if($controller == 'review' && in_array($action, array('add_review_option','update_reviews','addu','updateu','search_business','search_business2', 'search_business1', 'add', 'update', 'new_business', 'getStates', 'getCities'))){
				$permission = true;
			}
			if($controller == 'job' && in_array($action, array('index',  'index1', 'new', 'update', 'view2','flag','update_photo'))){
				$permission = true;
			}
			if($controller == 'real_estate' && in_array($action, array('index', 'new', 'update', 'view2','flag','update_photo','delete_photo'))){
				$permission = true;
			}
			if($controller == 'thing' && in_array($action, array('getCities', 'getStates', 'index', 'new', 'update', 'view2','flag','update_photo','delete_photo'))){
				$permission = true;
			}
			if($controller == 'car_and_truck' && in_array($action, array('delete_list', 'list','index', 'new',  'update', 'flag','update_photo','delete_photo'))){
				$permission = true;
			}
			if($controller == 'biz' && in_array($action, array('claimreq_success','howtoclaim'))){
		 		$permission = true;
		 	}

			if($controller == 'event' && in_array($action, array('delete_event','update2','add_schedule','new2','load_ajax', 'index', 'view', 'new', 'search', 'update', 'list', 'delete_list'))){
				$permission = true;
			}

			if($controller == 'user' && in_array($action, array('testFacebook'))){
				$permission = true;
			}

			if($controller == 'admincontrol' && in_array($action, array('show404','control', 'index','claimbizreq','getuserdata','user_profile', 'reviewflag','getbiz_flag_data','classifiedflag','jobflag', 'thingflag', 'analytics','subscribers', 'memberlist', 'classifiedlist','bizownerlist', 'emailbizowner','postemailbizowner','memberpage', 'bizlist', 'subscribers', 'stripe', 'login', 'adminmvlgli', 'grantAdminAccess', 'bizcategories','bizlistwcategory', 'bizownerpage', 'paymentlogs', 'delete_classified'))){
				
				$permission = ($admin_access == '1') ? true : false;
			}

		}

		return $permission;
	}
	
	public function cookie_login(){
		if(isset($_COOKIE['mid']) && isset($_COOKIE['e']) && isset($_COOKIE['token'])) {
			$id = $this->decrypt($_COOKIE['mid']);
			$email = $this->decrypt($_COOKIE['e']);
			$token = $this->decrypt($_COOKIE['token']);
			$member = Members::findFirst(array('id = "'.trim($id).'"', 'email = "'.trim($email).'"'));
			//$member =  Members::findFirst(array('id= "'.$id.'"', 'email="Yes"'));
			if ($member == true && $this->security->checkHash($token, $member->cookie_token)) {
				$userSession = get_object_vars($member);
				$profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
				//$userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
				return $this->session->set('userSession', $userSession);
			}
		}
	}
	

	function encrypt($pure_string) {
		$encryption_key = 'sMeynBiaprpainlgiyhP';
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
		return $encrypted_string;
	}

	function decrypt($encrypted_string) {
		$encryption_key = 'sMeynBiaprpainlgiyhP';
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
		return $decrypted_string;
    }

    public function real_escape($value){
        $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
        $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
        return str_replace($search, $replace, $value);
    }

    public function sessionChecker($id){
    	$main = FALSE;
    	$userSession = $this->session->get("userSession");

        if(isset($userSession)){
            if($userSession['id'] == $id){
                $main = TRUE;
            }
        }
        return $main;
    }

    protected function userImage($id){
    	$html = '';
        $px =  MemberPhotos::findFirst([
                'conditions' => 'member_id = :id: AND primary_pic = :type:',
                'bind' => array('id' => $id, 'type' => 'Yes'),
                'order' => 'id desc'
        ]);
        if ($px) {
            $html .=  $this->url->getBaseUri() . $px->file_path . $px->filename;
        }else{
            $html .= "/img/profilepic.jpg"; 
        }
        return $html;
    }


    /**
     * [folderOrganizer description]
     * @param  [type] $id     [description]
     * @param  [type] $folder [description]
     * @return [type]         [description]
     */
    public function folderOrganizer($id, $folder ){

		$path = '/img/userFiles/'.$id.'/'.$folder;

		if(!file_exists($path)){
			mkdir($path, 0775, true);
		}

		return true;
    }
    
	public function word_break($string, $limit, $break=".", $pad="...") {   
	    // return with no change if string is shorter than $limit    
	    if(strlen($string) <= $limit)   
	        return $string;   
	    // is $break present between $limit and the end of the string?    
	    if(false !== ($breakpoint = strpos($string, $break, $limit))) {  
	        if($breakpoint < strlen($string) - 1) {   
	            $string = substr($string, 0, $breakpoint) . $pad;   
	        }   
	    }  
	    return $string;   
	}  




    public function flagAllAction($pagetype = null) {


	$this->view->disable();



        if($this->request->isAjax()) 
        {
            $arrFlag = array();



            if($pagetype == '1auto'){

	            $review = $this->request->getQuery('review');
	            $reviewer = $this->request->getQuery('reviewer');
	            $flagger = $this->request->getQuery('flagger');
	            $flag_type = $this->request->getQuery('flag_type');
	            $referer = $this->request->getHTTPReferer (); 
	            $controller = 'car_and_truck';
            	
            }




            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }


            $arrFlag[] = ["review" => $review, "reviewer" => $reviewer, "flagger" => $flagger, 'result' => $resultInfo];

            $payload     = $arrFlag; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }




            // $review = $this->request->getQuery('review');
            // $reviewer = $this->request->getQuery('reviewer');
            // $flagger = $this->request->getQuery('flagger');
            // $flag_type = $this->request->getQuery('flag_type');
            // $referer = $this->request->getHTTPReferer (); 
            // $controller = 'business';

            // $newFlag = new Flags();
            // $newFlag->created = date('Y-m-d H:i:s');
            // $newFlag->modified = date('Y-m-d H:i:s');
            // $newFlag->review_id = $review;
            // $newFlag->member_id = $flagger;
            // $newFlag->target_id = $reviewer;
            // $newFlag->flag_type_id = $flag_type;
            // $newFlag->location = $controller; 
            // $newFlag->status = '0';
            // $newFlag->page = $referer;

            // if ($newFlag->create()) {
            //     $resultInfo = 'success';
            // } else {
            //     $resultInfo = 'failed';
            // }

            // $arrFlag[] = ["review" => $review, "reviewer" => $reviewer, "flagger" => $flagger, 'result' => $resultInfo];

            // $payload     = $arrFlag; 
            // $status      = 200;
            // $description = 'OK';
            // $headers     = array();
            // $contentType = 'application/json';
            // $content     = json_encode($payload);

            // $response = new \Phalcon\Http\Response();

            // $response->setStatusCode($status, $description);
            // $response->setContentType($contentType, 'UTF-8');
            // $response->setContent($content);

            // // Set the additional headers
            // foreach ($headers as $key => $value) {
            //    $response->setHeader($key, $value);
            // }

            // $this->view->disable();

            // return $response;

        } else {
            $this->view->disable();
            echo "flag action failed";
        }
    }





	public function timeAgo($time_ago){
		$cur_time 	= time();
		$time_elapsed 	= $cur_time - $time_ago;
		$seconds 	= $time_elapsed ;
		$minutes 	= round($time_elapsed / 60 );
		$hours 		= round($time_elapsed / 3600);
		$days 		= round($time_elapsed / 86400 );
		$weeks 		= round($time_elapsed / 604800);
		$months 	= round($time_elapsed / 2600640 );
		$years 		= round($time_elapsed / 31207680 );
		// Seconds
		if($seconds <= 60){
			echo "$seconds seconds ago";
		}
		//Minutes
		else if($minutes <=60){
			if($minutes==1){
				echo "one minute ago";
			}
			else{
				echo "$minutes minutes ago";
			}
		}
		//Hours
		else if($hours <=24){
			if($hours==1){
				echo "an hour ago";
			}else{
				echo "$hours hours ago";
			}
		}
		//Days
		else if($days <= 7){
			if($days==1){
				echo "yesterday";
			}else{
				echo "$days days ago";
			}
		}
		//Weeks
		else if($weeks <= 4.3){
			if($weeks==1){
				echo "a week ago";
			}else{
				echo "$weeks weeks ago";
			}
		}
		//Months
		else if($months <=12){
			if($months==1){
				echo "a month ago";
			}else{
				echo "$months months ago";
			}
		}
		//Years
		else{
			if($years==1){
				echo "one year ago";
			}else{
				echo "$years years ago";
			}
		}
	}






}
