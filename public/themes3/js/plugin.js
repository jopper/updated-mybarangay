$(function(){

	//***************** GALLERY PHOTOS  *****************
    $(".deleteGal").on("click",function(){
        var deleteid      =   $(this).attr('id');
        notyDelete(deleteid);
        return false;
    });

});





//***************** GALLERY PHOTOS  *****************
function notyDelete(id){
    console.log('record : '+ id );
    noty({
        text: 'Do you want to Delete This Image ?',
        layout: 'center',
        buttons: [
	        {addClass: 'btn btn-primary btn-clean', text: 'Delete', onClick: function($noty) {
		            $noty.close();
		           
		            $.ajax({
		                url: "https://mybarangay.com/biz/deleteGalleryPhoto",
		                dataType: 'json',
		                data:{ids: id},
		                beforeSend: function () {
		                    $before = noty({text: 'deleting ...' , layout: 'center', type: 'success' });
		                    console.log('loading...');
		                },
		                success: function(response){
		                     $before.close();
		                    var message = response.message;
		                    var types = 'error';
		                    if(response.type == 'ok'){
		                        types = 'success';
		                    }
		                    $succ = noty({text: message , layout: 'center', type: types });
		              
		                           $('#'+id).parents(".gallery-thumbnail").fadeOut(1000,function(){
		                                $(this).remove();
		                                $succ.close();
		                            });
		                   
		                },
		                error: function (xhr, ajaxOptions, thrownError) {
		                    noty({text: 'Error Deleting this Image : Sorry for the inconvenience the administrator will be notfied thanks.', layout: 'center', type: 'error'});

		                },
		                complete: function () {
		                   console.log('Complete!!!');
		                }
		            });
		    }
		},
		{addClass: 'btn btn-default btn-clean', text: 'Cancel', onClick: function($noty) {
		            $noty.close();
		    }
		}]
    });             
}