<?php
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;

class ReviewController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {

    }
    public function getCitiesAction($stateCode = null) {
        if ($this->request->isAjax()) {
            $arrCities = array();
            $cities = Cities::find("state_code_id='" . $stateCode . "'");

            foreach ($cities as $city) {
                $arrCities[] = ['id' => $city->id, 'name' => $city->city_name];
            }

            $payload     = $arrCities; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }
    public function getStatesAction($countryCode = null) {
        if ($this->request->isAjax()) {
            $arrStates = array();
            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }
    
    public function index2Action()
    {
        $this->view->setTemplateAfter('default1');

            $ename = '';
            $ecity = '';
            $city_code = '';

        $searchWords = '';
        $business = array();
        if(isset($_GET["page"])){
            $currentPage = (int) $_GET["page"];
        } else {
            $currentPage = 1;
        }
        if ($this->request->isPost()) {



            $businessName = $this->request->getPost('name');
            $businessAddress = $this->request->getPost('city');
            $businessCategoryId = $this->request->getPost('business_category_id');

            $city = Cities::findFirst($city_code);
            $cityname = $city->city_name;

            $ename = $keyword;
            $this->view->setVars([
                'ename' => $ename,
                'ecity' => $ecity,
                'ecity_code' => $city_code
            ]);


            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'country LIKE :country:', 
                                             'bind' => array('country' => $businessAddress)));
          
            $countryId = '';
            if($country) {
                $countryId = $country->id;
            } 

            $businessCategoryLists = BusinessCategoryLists::find(array('columns'    => '*', 
                                             'conditions' => 'business_category_id = :business_category_id:',
                                             'bind' => array('business_category_id' => $businessCategoryId)));
            $conditions = '';
            if(!empty($businessCategoryLists)) {
                foreach ($businessCategoryLists as $key => $businessCategoryList) {
                    $conditions .= ' OR id = :'.$key.':';
                    $bind[$key] = $businessCategoryList->business_id;
                }//$searchWords .= ', '.$businessName;
            }

            if(!empty($businessName)) {
                $conditions .= ' OR name LIKE :name:';
                $bind['name'] = '%'.$businessName.'%';
                $searchWords .= ', '.$businessName;
            }

            if(!empty($businessAddress)) {
                $conditions .= ' OR street LIKE :street: OR city LIKE :city:';
                $bind['street'] = '%'.$businessAddress.'%';
                $bind['city'] = '%'.$businessAddress.'%';
                $searchWords .= ', '.$businessAddress;
            }

            if(!empty($countryId)) {
                $conditions .= ' OR country_id = :country_id:';
                $bind['country_id'] = $countryId;
                $searchWords .= ', '.$country->country;
            }

            $searchWords = substr($searchWords, 2); 
            $business = Business::find(array('columns'    => '*', 
                'conditions' => substr($conditions, 3), 
                'bind' => $bind
            ));

            $this->view->setVar('business', $business);



        } else {
            $business = Business::find(array('order'    => 'id DESC'));
        }

        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $business,
                "limit"=> 12,
                "page" => $currentPage
            )
        );

        $this->view->setVar('name', $page);

        $page = $paginator->getPaginate();
        $this->view->setVar('business', $page);
        //$this->view->setVar('business', $business);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);


    }


    public function addAction($id = null)
    {
       $this->view->setTemplateAfter('default1');
        $userSession = $this->session->get("userSession");
        $business = Business::findFirstById($id);
        //$mainCategory = $business->BusinessCategories->main_title;
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        //$this->view->setVar('business', $business);
        $reviews = Reviews::findByBusinessId($id);
        //$this->view->setVar('reviews', $reviews);
        
        $this->view->setVars([
            'business' => $business,
            'reviews' => $reviews,
            'mainCategory' => ''//$mainCategory
        ]);

        if ($this->request->isPost()) {

    $validation = new Phalcon\Validation();
    $validation->add('review', new PresenceOf(array(
        'message' => 'review content is required'   
    )));
    $validation->add('rate', new PresenceOf(array(
        'message' => 'rate is required' 
    )));
    
    $messages = $validation->validate($_POST);
    if (count($messages)) {
        $this->view->disable();
        $errorMsg = '';
        foreach ($messages as $msg) {
          $errorMsg .= $msg . "<br>";       
        }
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">Ã—</button>'.$errorMsg);
        return $this->response->redirect('review/add/'.$id);    
    } else {
        $this->view->disable();
        echo "validation is either failed or passed";
    }
            $content = $this->request->getPost('review');
            $rate = $this->request->getPost('rate');
            $review = new Reviews();
            $review->created = date('Y-m-d H:i:s');
            $review->modified = date('Y-m-d H:i:s');
            $review->member_id = $userSession['id'];
            $review->business_id = $id;
            $review->content = $content;
            $review->rate = $rate;
            $review->status = 'Pending';
            $review->visibility = '0';
        
            if($review->create()) {
                $mainTitle = $this->request->getPost('mainTitle');
            if ($mainTitle == 'Restaurants') {

                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');
                
                $newShopping->wifi = $this->request->getPost('wifi');
                $newShopping->hastv = $this->request->getPost('hastv');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                $newShopping->creditcard = $this->request->getPost('creditcard');
                $newShopping->parking = $this->request->getPost('parking');
                $newShopping->bike_parking = $this->request->getPost('bike_parking');
                $newShopping->attire = $this->request->getPost('attire');
                $newShopping->groups = $this->request->getPost('groups');
                $newShopping->kids = $this->request->getPost('kids');
                $newShopping->reservations = $this->request->getPost('reservations');
                $newShopping->delivery = $this->request->getPost('delivery');
                $newShopping->takeaway = $this->request->getPost('takeaway');
                $newShopping->outdoor = $this->request->getPost('outdoor');
                $newShopping->noise = $this->request->getPost('noise');
                $newShopping->waiter = $this->request->getPost('waiter');
                $newShopping->alcohol = $this->request->getPost('alcohol');
                $newShopping->ambience  = $this->request->getPost('ambience');
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
                
            } else if ($mainTitle == 'Automotive') {
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');
                $newShopping->hastv = $this->request->getPost('hastv');
                $newShopping->creditcard = $this->request->getPost('creditcard');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                $newShopping->parking = $this->request->getPost("parking");
                $newShopping->bike_parking = $this->request->getPost("bike_parking");

                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
            } else if ($mainTitle == 'Active Life') {
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');;
                $newShopping->modified = date('Y-m-d H:i:s');

                $newShopping->groups = $this->request->getPost('groups');
                $newShopping->kids = $this->request->getPost('kids');
                $newShopping->attire = $this->request->getPost('attire');
                $newShopping->parking = $this->request->getPost("parking");
                $newShopping->bike_parking = $this->request->getPost("bike_parking");
                
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
            } else if ($mainTitle == 'Hotels & Travel') {
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');

                $newShopping->creditcard = $this->request->getPost('creditcard');
                $newShopping->reservations = $this->request->getPost('reservations');
                $newShopping->ambience  = $this->request->getPost('ambience');

                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }

            }  else if ( $mainTitle == 'Arts & Entertainment') {
                
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');;
                $newShopping->modified = date('Y-m-d H:i:s');

                $newShopping->creditcard = $this->request->getPost('creditcard');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }

            } else if ( $mainTitle == 'Shopping') { 
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');
                $newShopping->wifi = $this->request->getPost('wifi');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
            } else if ($mainTitle == 'Food') {
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');
                $newShopping->creditcard = $this->request->getPost('creditcard');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
                // $this->view->disable();
                // echo "what happened.";          
            } else if ($mainTitle == 'Event Planning & Services') {
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');
                $newShopping->creditcard = $this->request->getPost('creditcard');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
            } else if ($mainTitle == 'Health & Travel') {
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');
                $newShopping->creditcard = $this->request->getPost('creditcard');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
            } else if ($mainTitle == 'Local Services') {
                $newShopping = new ReviewOptions();
                $newShopping->business_id = $id;
                $newShopping->member_id = $userSession['id'];
                $newShopping->created = date('Y-m-d H:i:s');
                $newShopping->modified = date('Y-m-d H:i:s');
                $newShopping->pricerange = $this->request->getPost('pricerange');
                if ($newShopping->create()) {
                    // do nothing
                } else {
                    $this->view->disable();
                }
            } else {
              // do nothing
            }
                $averageRate = Reviews::average(array(
                                                "column" => "rate",
                                                "conditions" => "business_id = '".$id."'"
                                            ));
                $totalReview = Reviews::count(array(
                                                "column" => "id",
                                                "conditions" => "business_id = '".$id."'"
                                            ));
                $business->total_review = $totalReview;
                $business->average_rate = $averageRate;
                
                if($business->update() == true) {
            
                } else { $this->view->disable(); echo "failed"; }
                
                $this->flash->success('<button type="button" class="close" data-dismiss="alert">Ã—</button>You\'re review has been submitted.');
                return $this->response->redirect('business/view2/'.$id);
            } else {
                $this->view->disable();

                print_r($review->getMessages());
            }
        } 
        
    }



    public function addformValidate() {

        $validation = new Phalcon\Validation();

        $validation->add('review', new PresenceOf(array(
            'message' => 'review content is required'   
        )));

        $validation->add('rate', new PresenceOf(array(
            'message' => 'rate is required' 
        )));
        
        return $validation->validate($_POST);
    }



    public function adduAction($id){

        $this->view->setTemplateAfter('default1');
        $userSession = $this->session->get("userSession");
        if(empty($userSession['id']) or $userSession['id'] == '' ){
            return $this->response->redirect('member/login/'); 
       
         }

        $this->view->seschecker = $this->sessionChecker($userSession['id']);
        $this->view->ids = $userSession['id'];


        
        $business = Business::findFirstById($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }

        $this->view->setVars([
            'business' => $business,
            'reviews' => Reviews::findByBusinessId($id),
            'mainCategory' => ''
        ]);

        if ($this->request->isPost()) {
            $messages = $this->addformValidate();
            if (count($messages)) {
                $this->response->redirect('review/addu/'.$id);
            }// end of error redirect
            $this->response->redirect('review/addu/'.$id);

        }//end of request post


    }




    public function updateAction($id = null, $reviewId = null)
    {
        $this->view->setTemplateAfter('default1');
        $userSession = $this->session->get("userSession");
        $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $reviewUpdate = Reviews::findFirst($reviewId);
        if(!$reviewUpdate) {
            return $this->response->redirect('review/search_business');
        }
        if($reviewUpdate->member_id != $userSession['id']) {
            return $this->response->redirect('review/search_business');
        }
        $this->view->setVar('reviewUpdate', $reviewUpdate);
        $this->view->setVar('business', $business);
        $reviews = Reviews::findByBusinessId($id);
        $this->view->setVar('reviews', $reviews);

        if ($this->request->isPost()) {
            $content = $this->request->getPost('review');
            $rate = $this->request->getPost('rate');
            $review = Reviews::findFirst($reviewId);
            $review->created = date('Y-m-d H:i:s');
            $review->modified = date('Y-m-d H:i:s');
            $review->member_id = $userSession['id'];
            $review->business_id = $id;
            $review->content = $content;
            $review->rate = $rate;
            if($review->update()) {
                $averageRate = Reviews::average(array(
                                                "column" => "rate",
                                                "conditions" => "business_id = '".$id."'"
                                            ));
                $totalReview = Reviews::count(array(
                                                "column" => "id",
                                                "conditions" => "business_id = '".$id."'"
                                            ));
                $business->total_review = $totalReview;
                $business->average_rate = $averageRate;
                $business->update();
                 //Set a session variable
                $this->session->set("abra", $content);
                $this->session->set("cadabra", $reviewId);
                //$this->flash->success('<button type="button" class="close" data-dismiss="alert">Ã—</button>You\'re review has been submitted.');
                return $this->response->redirect('business/view2/'.$id);
            }
        } 
        
    }



    public function updateuAction($id, $reviewId){

        $this->view->setTemplateAfter('default1');
        $userSession = $this->session->get("userSession");
        $this->view->seschecker = $this->sessionChecker($userSession['id']);
        $this->view->ids = $userSession['id'];

        
        $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $reviewUpdate = Reviews::findFirst($reviewId);
        if(!$reviewUpdate) {
            return $this->response->redirect('review/search_business');
        }
        if($reviewUpdate->member_id != $userSession['id']) {
            return $this->response->redirect('review/search_business');
        }


        $businessCategoryLists = BusinessCategoryLists::find('business_id="'.$id.'"');
        $reviews = Reviews::find([
            'conditions'    =>  'business_id = :busid:',
            'bind'          =>  array('busid' => $id),
            'order'         =>  'id DESC',
            'limit'         =>  '5'
        ]);



        $revopt = ReviewOptions::findFirst([
            'conditions'    =>  'business_id = :busid: AND member_id = :memid:',
            'bind'          =>  array('busid' => $business->id, 'memid' => $userSession['id'] )
        ]);

        $this->view->setVars([
            'reviewUpdate'  => $reviewUpdate,
            'business'      => $business,
            'reviews'       => $reviews,
            'buscatlist'    => $businessCategoryLists,
            'revopt'        => $revopt
        ]);



        if ($this->request->isPost()) {

            $content = $this->request->getPost('review');
            $rate = $this->request->getPost('rate');
            $review = Reviews::findFirst($reviewId);
            $review->created = date('Y-m-d H:i:s');
            $review->modified = date('Y-m-d H:i:s');
            $review->member_id = $userSession['id'];
            $review->business_id = $id;
            $review->content = $content;
            $review->rate = $rate;
            if($review->update()) {
                $averageRate = Reviews::average(array(
                                                "column" => "rate",
                                                "conditions" => "business_id = '".$id."'"
                                            ));
                $totalReview = Reviews::count(array(
                                                "column" => "id",
                                                "conditions" => "business_id = '".$id."'"
                                            ));
                $business->total_review = $totalReview;
                $business->average_rate = $averageRate;
                $business->update();
                 //Set a session variable
                $this->session->set("abra", $content);
                $this->session->set("cadabra", $reviewId);
                return $this->response->redirect('business/view2/'.$id);
            }else{
                foreach ($review->getMessages() as $error) {
                    echo $error->getMessage(). '</br>';
                
                }
            }
        } 
    }

    public function add_review_optionAction($id, $name = null){

        $this->view->setTemplateAfter('default1');

        $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
		$userSession 	= $this->session->get("userSession");
        $businessCategoryLists = BusinessCategoryLists::find('business_id="'.$id.'"');

        $revopt = ReviewOptions::findFirst([
            'conditions'    =>  'business_id = :busid: AND member_id = :memid:',
            'bind'          =>  array('busid' => $business->id, 'memid' => $userSession['id'] )
        ]);

        if($business->member_id != $userSession['id']){
        	return $this->response->redirect('review/search_business');
        }

        // if($revopt === true){
        // 	return $this->response->redirect('business/view/'.$id);
        // }

        $this->view->setVars([
            'business'      => $business,
            'buscatlist'    => $businessCategoryLists,
            'revopt'        => $revopt
        ]);


    }


    public function update_reviewsAction(){
    
        if ($this->request->isPost()) {

                // if(!$this->sesChecker() OR !$this->idChecker($id)){
                //    return $this->response->redirect('biz/login');
                // }

                $userSession 	= $this->session->get("userSession");
                $businessid     = $this->request->getPost('businessid');
                $revoptions     = $this->request->getPost('revoptions');
                
        		$revidcreate 	= '';
                $revidcreate    = $this->request->getPost('revidcreate');
        		$revid 			= '';
				$revid          = $this->request->getPost('revid');
        		$busname 		= '';
				$revid          = $this->request->getPost('revid');


                // $revoptCheck = ReviewOptions::findFirst([
                //     'conditions'    =>  'business_id = :busid: AND member_id = :memid:',
                //     'bind'          =>  array('busid' => $businessid, 'memid' => $userSession['id'] )
                // ]);
                $revoptCheck = ReviewOptions::findFirstById($revoptions);

                if($revoptCheck){
                    $revopt = $revoptCheck;
                }else{
                    $revopt = new ReviewOptions();
                }

                $revopt->business_id    = $businessid;
                $revopt->member_id      = $userSession['id'];
                $revopt->created        = date('Y-m-d H:i:s');
                $revopt->modified       = date('Y-m-d H:i:s');
                $revopt->caters         = $this->request->getPost('reviews_caters');
                $revopt->wifi           = $this->request->getPost('reviews_wifi');
                $revopt->hastv          = $this->request->getPost('reviews_hastv');
                $revopt->pricerange     = $this->request->getPost('reviews_pricerange');
                $revopt->creditcard     = $this->request->getPost('reviews_creditcard');
                $revopt->parking        = $this->request->getPost('reviews_parking');
                $revopt->bike_parking   = $this->request->getPost('reviews_bike_parking');
                $revopt->attire         = $this->request->getPost('reviews_attire');
                $revopt->groups         = $this->request->getPost('reviews_groups');
                $revopt->kids           = $this->request->getPost('reviews_kids');
                $revopt->reservations   = $this->request->getPost('reviews_reservations');
                $revopt->delivery       = $this->request->getPost('reviews_delivery');
                $revopt->takeaway       = $this->request->getPost('reviews_takeaway');
                $revopt->outdoor        = $this->request->getPost('reviews_outdoor');
                $revopt->goodfor        = $this->request->getPost('reviews_goodfor');
                $revopt->noise          = $this->request->getPost('reviews_noise');
                $revopt->waiter         = $this->request->getPost('reviews_waiter');
                $revopt->alcohol        = $this->request->getPost('reviews_alcohol');
                $revopt->ambience       = $this->request->getPost('reviews_ambience');


                if($revoptCheck){
                    if($revopt->update()){
                        $this->session->set('successMess', 'Reviews successfully updated. ');
                    }else{
                        $err = '';
                        foreach ($revopt->getMessages() as $message) {
                            $err .= $message->getMessage() . " <br>";
                        }
                       $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
                    }

                    if($revidcreate != '' OR $revidcreate != null){

                    	$this->response->redirect('review/add_review_option/'.$businessid.'/'.$busname);
                    }else{
                    	$this->response->redirect('review/updateu/'.$businessid.'/'.$revid);
                    }
                    
                }else{
                    if($revopt->create()){
                        $this->session->set('successMess', 'Reviews successfully created. ');
                    }else{
                        $err = '';
                        foreach ($revopt->getMessages() as $message) {
                            $err .= $message->getMessage() . " <br>";
                        }
                       $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
                    }

                    if($revidcreate != '' OR $revidcreate != null){
                    	$this->session->set('successMess', 'Reviews successfully created. You can now add photos(Recommended)  ');
                    	$this->response->redirect('business/add_business_photo/'.$businessid.'/'.$busname);
                    }else{
                    	$this->response->redirect('review/updateu/'.$businessid.'/'.$revid);
                    }
                    
                }

        }else{
            $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again.');
            $this->response->redirect('member/page/1');
        }

    }





  public function search_business2Action() {
   $this->view->setTemplateAfter('default1');
        // set breadcrum for page identification
        $this->view->setVar('breadcmp', 'Write a review');
        if (isset($_GET["page"])) {
          $currentPage = (int) $_GET["page"];
        } else {
          $currentPage = 1;
        }
      
        $currentPage = $this->request->getQuery("page", "int");
      
        $business = '';
        $catg = '';
        if (isset($_GET["cat"])) {

            $cats = $_GET["cat"];
            if($cats == ''){
                return $this->response->redirect('review/search_business');
            }

            $query = $this->getQueryStmt($_GET["cat"]);
            if($query == false){
                return $this->response->redirect('review/search_business');
            }

        $items = $this->modelsManager->executeQuery($query);
        $fuckingBizId = array();
        foreach ($items as $fuckThisBiz) {
            $fuckingBizId[] = $fuckThisBiz->business_id;
        }
        $ids = join(',',$fuckingBizId);  
        $sql = "SELECT * FROM Business WHERE id IN ($ids)";
        $business = $this->modelsManager->executeQuery($sql);


        $catg = $_GET["cat"];

        } else {
      
         return $this->response->redirect('review/search_business');
        }

  


        $paginator = new Paginator(
            [
                "data" => $business,
                "limit"=> 12,
                "page" => $currentPage
            ]
        );

  		$this->view->currentPage = $currentPage;

        $page = $paginator->getPaginate();
        $this->view->setVar('business', $page);
        $this->view->setVar('counter', $business);
        $this->view->setVar('cat', $catg);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);









  } // end of search business 2

  public function getQueryStmt($mc = null) {

    if ($mc == 'automotive') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 141 AND 173 GROUP BY business_id";
      return $query;
    } else if ($mc == 'lifestyle') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 1 AND 94) OR (business_category_id BETWEEN 95 AND 139) OR (business_category_id BETWEEN 564 AND 565) OR (business_category_id BETWEEN 205 AND 209) OR (business_category_id BETWEEN 210 AND 246) 
                            OR (business_category_id BETWEEN 709 AND 721) OR (business_category_id BETWEEN 736 AND 742) OR (business_category_id BETWEEN 617 AND 620) GROUP BY business_id";
      return $query;
    } else if ($mc == 'food') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 289 AND 350) OR (business_category_id BETWEEN 743 AND 1047) OR (business_category_id BETWEEN 621 AND 651) GROUP BY business_id";
      return $query;
    } else if ($mc == 'health') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 351 AND 445) OR (business_category_id BETWEEN 174 AND 204) GROUP BY business_id";
      return $query;
    } else if ($mc == 'professional') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 351 AND 414) OR (business_category_id BETWEEN 280 AND 288) OR (business_category_id BETWEEN 664 AND 708) OR (business_category_id BETWEEN 247 AND 279) GROUP BY business_id";
        return $query;
    } else if ($mc == 'hotel') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 519 AND 563 GROUP BY business_id";
      return $query;
    } else if ($mc == 'realestate') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 722 AND 735 GROUP BY business_id";
      return $query;
    } else if ($mc == 'shopping') {
      $query = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 1048 AND 1171 GROUP BY business_id";
      return $query;
    }

  } // end setQueryStmt
    /**
     * [search_businessAction description]
     * @return [type] [description]
     */
   public function search_businessAction(){
      
        // set default1 template for new design template
        $this->view->setTemplateAfter('default1');
        // set breadcrum for page identification
        $this->view->setVar('breadcmp', 'Write a review');


            $keyword = '';

            $businessCategoryId = '';
            $businessName = '';
            $businessAddress = '';
            $ename = '';
            $ecity = '';
            $city_code = '';
            $cond = '';

            $bind = array();
            $parameters = array();

        $currentPage = 1;

        if ($this->request->isPost()) {

            $keyword = $this->request->getPost('name');
            $city_code = $this->request->getPost('city_code');
            $businessCategoryId = $this->request->getPost('business_category_id');

            $city = Cities::findFirst($city_code);
            $cityname = $city->city_name;
            $ename = $keyword;
            $currentPage = $this->request->getQuery("page", "int");

            if(!empty($keyword) AND !empty($city_code)  AND !empty($businessCategoryId) ){



             $rbcl = $this->modelsManager->createBuilder()
                  ->from(array('bs' => 'Business'))
                  ->where('bs.name LIKE :fieldname: AND bs.city LIKE :fieldname1:  ',
                   ['fieldname' => '%'.$keyword.'%','fieldname1' => '%'.$cityname.'%'])
                  ->innerJoin('BusinessCategoryLists', "bcl.business_id = bs.id AND  bcl.business_category_id = '$businessCategoryId' ", 'bcl')
                  ->columns(array('bs.id','bs.city','bs.name'))
                  ->getQuery()
                  ->execute();

                 $crbcl = count($rbcl);
            
                  if(!empty($crbcl) OR $crbcl > 0 ){

                    foreach ($rbcl as $bcl) {
                      $idfield[] = $bcl->id;
                    }
                    for ($i=0; $i < $crbcl; $i++) { 
                         $cond .= ' id = :idfld'.$i.':';
                         $cond .= ($i == ($crbcl - 1))? '' : ' OR ' ;
                    }
                    for ($x=0; $x < $crbcl; $x++) { 
                        $n = 'idfld'.$x;
                        $bind[$n] = $idfield[$x];
                    }
                  }



            }elseif(!empty($keyword) AND !empty($businessCategoryId)){

                $nbus = $this->modelsManager->createBuilder()
                  ->from('Business')
                  ->where('name LIKE :name:', ['name' =>  '%'.$keyword.'%'])
                  ->columns('id')
                  ->getQuery()
                  ->execute();

                  $cnbus = count($nbus);
                  if(!empty($cnbus) OR $cnbus > 0 ){
                    foreach ($nbus as $nbus) {
                      $name[] = $nbus->id;
                    }
                    for ($i=0; $i < $cnbus; $i++) { 
                         $cond .= ' id = :idfld'.$i.':';
                         $cond .= ($i == ($cnbus - 1))? '' : ' OR ' ;
                    }
                    for ($x=0; $x < $cnbus; $x++) { 
                        $n = 'idfld'.$x;
                        $bind[$n] = $name[$x];
                    }
                  }



                $rbcl = $this->modelsManager->createBuilder()
                  ->from(array('bs' => 'Business'))
                  ->innerJoin('BusinessCategoryLists', "bcl.business_id = bs.id AND bcl.business_category_id = '$businessCategoryId' ", 'bcl')
                  ->columns(array('DISTINCT(bs.id)', 'bs.id'))
                  ->getQuery()
                  ->execute();

                      $crbcl = count($rbcl);

                      $cond .= ($cnbus > 0 AND $crbcl > 0)? 'OR' : ' ' ;
                      if(!empty($crbcl) OR $crbcl > 0 ){

                        foreach ($rbcl as $bcl) {
                          $idfield[] = $bcl->id;
                        }
                        for ($i=0; $i < $crbcl; $i++) { 
                             $cond .= ' id = :idfld'.$i.':';
                             $cond .= ($i == ($crbcl - 1))? '' : ' OR ' ;
                        }
                        for ($x=0; $x < $crbcl; $x++) { 
                            $n = 'idfld'.$x;
                            $bind[$n] = $idfield[$x];
                        }
                      }




            }elseif(!empty($keyword) AND !empty($city_code)){

                $nbus = $this->modelsManager->createBuilder()
                  ->from('Business')
                  ->where('name LIKE :name:', ['name' =>  '%'.$keyword.'%'])
                  ->andWhere('city = :city:', ['city' => $cityname])
                  ->columns('id')
                  ->getQuery()
                  ->execute();

                  $cnbus = count($nbus);
                  if(!empty($cnbus) OR $cnbus > 0 ){

                    foreach ($nbus as $nbus) {
                      $name[] = $nbus->id;
                    }
                    for ($i=0; $i < $cnbus; $i++) { 
                         $cond .= ' id = :idfld'.$i.':';
                         $cond .= ($i == ($cnbus - 1))? '' : ' OR ' ;
                    }
                    for ($x=0; $x < $cnbus; $x++) { 
                        $n = 'idfld'.$x;
                        $bind[$n] = $name[$x];
                    }
                  }


            }elseif(!empty($city_code) AND !empty($businessCategoryId)){

                $rbcl = $this->modelsManager->createBuilder()
                  ->from(array('bcl' => 'BusinessCategoryLists'))
                  ->where('bcl.business_category_id = :fieldname:',['fieldname' => $businessCategoryId])
                  ->innerJoin('Business', "bs.id = bcl.business_id AND city LIKE '%$cityname%' ", 'bs')
                  ->columns(array('bs.id','bs.name'))
                  ->getQuery()
                  ->execute();
                  echo $crbcl = count($rbcl);

                  //$cond .= ($cnbus > 0 AND $crbcl > 0)? 'OR' : ' ' ;
                  if(!empty($crbcl) OR $crbcl > 0 ){

                    foreach ($rbcl as $bcl) {
                      $idfield[] = $bcl->id;
                    }
                    for ($i=0; $i < $crbcl; $i++) { 
                         $cond .= ' id = :idfld'.$i.':';
                         $cond .= ($i == ($crbcl - 1))? '' : ' OR ' ;
                    }
                    for ($x=0; $x < $crbcl; $x++) { 
                        $n = 'idfld'.$x;
                        $bind[$n] = $idfield[$x];
                    }
                  }

            }else{


               if(!empty($keyword)){
                    $nbus = $this->modelsManager->createBuilder()
                      ->from('Business')
                      ->where('name LIKE :name:', ['name' =>  '%'.$keyword.'%'])
                      ->columns('id')
                      ->getQuery()
                      ->execute();

                      $cnbus = count($nbus);
                      if(!empty($cnbus) OR $cnbus > 0 ){
                        foreach ($nbus as $nbus) {
                          $name[] = $nbus->id;
                        }
                        for ($i=0; $i < $cnbus; $i++) { 
                             $cond .= ' id = :idfld'.$i.':';
                             $cond .= ($i == ($cnbus - 1))? '' : ' OR ' ;
                        }
                        for ($x=0; $x < $cnbus; $x++) { 
                            $n = 'idfld'.$x;
                            $bind[$n] = $name[$x];
                        }
                      }
                }

                if(!empty($businessCategoryId)){

                    $rbcl = $this->modelsManager->createBuilder()
                      ->from(array('bs' => 'Business'))
                      ->innerJoin('BusinessCategoryLists', "bcl.business_id = bs.id AND bcl.business_category_id = '$businessCategoryId' ", 'bcl')
                      ->columns(array('DISTINCT(bs.id)', 'bs.id'))
                      ->getQuery()
                      ->execute();

                      $cnbus = count($rbcl);
                      if(!empty($cnbus) OR $cnbus > 0 ){
                        foreach ($rbcl as $rbcl) {
                          $name[] = $rbcl->id;
                        }
                        for ($i=0; $i < $cnbus; $i++) { 
                             $cond .= ' id = :idfld'.$i.':';
                             $cond .= ($i == ($cnbus - 1))? '' : ' OR ' ;
                        }
                        for ($x=0; $x < $cnbus; $x++) { 
                            $n = 'idfld'.$x;
                            $bind[$n] = $name[$x];
                        }
                      }
                }


              if(!empty($city_code)) {
                  $cond .= ' city LIKE :city:';
                  $bind['city'] = '%'.$cityname.'%';
              }


            }


   

            if(!$cond == '' or !empty($bind)){
                $parameters = array('conditions' => $cond, 'bind' => $bind);
            }else{
                $parameters = array('conditions' => 'id = :ss:', 'bind' => ['ss' => '1xxsxxiii']);
            }


        } else {
            $currentPage = $this->request->getQuery("page", "int");
        }


        $business = Business::find($parameters);
        $countBiz = Business::count($parameters);
  
        
        $paginator = new Paginator(
            [
                "data" => $business,
                "limit"=> 18,
                "page" => $currentPage
            ]
        );

        $page = $paginator->getPaginate();
        $this->view->setVar('business', $page);



        $this->view->setVar('counter', $business);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);

        $this->view->setVar('name', $businessName);
        $this->view->setVar('address', $businessAddress);
        $this->view->setVar('busId', $businessCategoryId);

            $this->view->setVars([
                'ename' => $ename,
                'ecity' => $ecity,
                'ecity_code' => $city_code
            ]);

    }// end of search_businessAction



   public function search_businessOldAction()
    {
        $this->view->setTemplateAfter('default1');
        $businessName = '';
        $businessAddress = '';
        $businessCategoryId = '';



        $this->view->setVar('breadcmp', 'Write A Review');
        $searchWords = '';
        $business = array();
        if(isset($_GET["page"])){
            $currentPage = (int) $_GET["page"];
        } else {
            $currentPage = 1;
        }
        if ($this->request->isPost()) {
            $businessName = $this->request->getPost('name');
            $businessAddress = $this->request->getPost('address');
            $businessCategoryId = $this->request->getPost('business_category_id');

            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'country LIKE :country:', 
                                             'bind' => array('country' => $businessAddress)));
          
            $countryId = '';
            if($country) {
                $countryId = $country->id;
            } 

            $businessCategoryLists = BusinessCategoryLists::find(array('columns'    => '*', 
                                             'conditions' => 'business_category_id = :business_category_id:',
                                             'bind' => array('business_category_id' => $businessCategoryId)));
            $conditions = '';
            if(!empty($businessCategoryLists)) {
                foreach ($businessCategoryLists as $key => $businessCategoryList) {
                    $conditions .= ' OR id = :'.$key.':';
                    $bind[$key] = $businessCategoryList->business_id;
                }//$searchWords .= ', '.$businessName;
            }

            if(!empty($businessName)) {
                $conditions .= ' OR name LIKE :name:';
                $bind['name'] = '%'.$businessName.'%';
                $searchWords .= ', '.$businessName;
            }

            if(!empty($businessAddress)) {
                $conditions .= ' OR street LIKE :street: OR city LIKE :city:';
                $bind['street'] = '%'.$businessAddress.'%';
                $bind['city'] = '%'.$businessAddress.'%';
                $searchWords .= ', '.$businessAddress;
            }

            if(!empty($countryId)) {
                $conditions .= ' OR country_id = :country_id:';
                $bind['country_id'] = $countryId;
                $searchWords .= ', '.$country->country;
            }

            $searchWords = substr($searchWords, 2); 
            $business = Business::find(array('columns'    => '*', 
                                     'conditions' => substr($conditions, 3), 
                                     'bind' => $bind
                                        ));

            $this->view->setVar('business', $business);
        } else {
            $business = Business::find(array('order'    => 'id DESC'));
        }
        $this->view->setVar('name', $businessName);
        $this->view->setVar('address', $businessAddress);
        $this->view->setVar('busId', $businessCategoryId);



        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $business,
                "limit"=> 12,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        $this->view->setVar('business', $page);
        //$this->view->setVar('business', $business);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);
    }



    public function new_businessAction()
    {
    $this->view->setTemplateAfter('default1');
        if ($this->request->isPost()) {
            $countryId = $this->request->getPost('country_id');
            $stateId = $this->request->getPost('state');
            $cityId  = $this->request->getPost('city');
            $checkCountry = CountryCodes::count("id='". $countryId ."'");
      
            //$country = Countries::findFirst(array('columns'    => '*', 
            //                                 'conditions' => 'id LIKE :id:', 
            //                                 'bind' => array('id' => $countryId)));Id
        $checkState = States::count("id='". $stateId ."' AND country_code_id='".$countryId."'");
        $checkCity = Cities::count("id='". $cityId ."' AND country_code_id='".$countryId."'");
      

        $countryName = '';
        $stateName = '';
        $cityName = '';
            if($checkCountry > 0) {
                $country = CountryCodes::findFirst("id='". $countryId ."'");
                $countryName = $country->country_name;
            } else {
                $countryName = 'Philippines';
            }
    
            if ($checkState > 0) {
                $state = States::findFirst("id='". $stateId ."' AND country_code_id='".$countryId."'");
                $stateName = $state->state_name;
            } else {
                $stateName = "National Capital Region";
            }
  
            if ($checkCity > 0) {
                $city = Cities::findFirst("id='". $cityId ."' AND country_code_id='".$countryId."'");
                $cityName = $city->city_name;
            } else {
                $cityName = 'Manila';
            }

      $getCountryName = 
            $address = str_replace(' ', '+', $this->request->getPost('street').'+'.$cityName.'+'.$countryName);
            $userSession = $this->session->get("userSession");
            $content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            $json = json_decode($content, true);
            $lat = $json['results'][0]['geometry']['location']['lat'];
            $lng = $json['results'][0]['geometry']['location']['lng'];
            //error_log('LAT : '.$lat.' LONG : '.$lng);
            $validation = new Phalcon\Validation();
            $validation->add('name', new PresenceOf(array(
                'message' => 'name field is required is required'   
            )));

        $validation->add('street', new PresenceOf(array(
                'message' => 'street field is required' 
            )));
        $validation->add('city', new PresenceOf(array(
                'message' => 'city field is required' 
            )));
            $validation->add('country_id', new PresenceOf(array(
                'message' => 'country field is required' 
            )));
        $validation->add('business_category_ids', new PresenceOf(array(
        'message' => 'category is required'
        )));
        $validation->add('state', new PresenceOf(array(
        'message' => 'state is required'
        )));
        // $validation->add('postal_code', new PresenceOf(array(
        // 'message' => 'postal code is required'
        // )));
        // $validation->add('rate', new PresenceOf(array(
     //            'message' => 'rate field is required' 
     //        )));

        $messages = $validation->validate($_POST);
        if (count($messages)) {
            $this->view->disable();
            $errorMsg = '';
            foreach ($messages as $msg) {
              $errorMsg .= $msg . "<br>";       
            }
            $this->flash->error('<button type="button" class="close" data-dismiss="alert">Ã—</button>'.$errorMsg);       
            $oldInput = array();
            $oldInput['name'] = $this->request->getPost('name');
            $oldInput['website'] = $this->request->getPost('website');
            $oldInput['telephone'] = $this->request->getPost('telephone');
            $oldInput['email'] = $this->request->getPost('email');
            $oldInput['street'] = $this->request->getPost('street');
            $oldInput['city'] = $this->request->getPost('city');
            $oldInput['state'] = $this->request->getPost('state');
           // $oldInput['postal_code'] = $this->request->getPost('postal_code');
            //$oldInput['country_id'] = $this->request->getPost('country_id');
            $oldInput['country_code_id'] = $this->request->getPost('country_id');
            $oldInput['rate'] = $this->request->getPost('rate');
            $oldInput['review'] = $this->request->getPost('review');
            $oldInput['opened'] = $this->request->getPost('opened');
            
            $this->session->set("new_business_validation", $errorMsg);
            $this->session->set('oldInputs', $oldInput);
            return $this->response->redirect('review/new_business/');   
        } //else {
            //$this->view->disable();
            //echo "validation is either failed or passed";
        //}
            
            $business = new Business();
            $business->created = date('Y-m-d H:i:s'); 
            $business->modified = date('Y-m-d H:i:s'); 
            $business->member_id =  $userSession['id'];
            $business->name = $this->request->getPost('name');
            $business->website = $this->request->getPost('website');
            $business->telephone = $this->request->getPost('telephone');
            $business->email = $this->request->getPost('email');
            $business->apt_no = $this->request->getPost('apt_no');
            $business->street = $this->request->getPost('street');
            $business->city = $cityName; //$this->request->getPost('city');
            $business->state = $stateName; //$this->request->getPost('state');
            $business->postal_code = $this->request->getPost('postal_code');
            // $business->country_id = $this->request->getPost('country_id');
            $business->country_code_id = $this->request->getPost('country_id');
            $business->lat = $lat;
            $business->lng = $lng;
            $business->average_rate = $this->request->getPost('rate');
            $business->subscription = '0';
            if(!empty($this->request->getPost('review'))) {
                $business->total_review = 1;
            }
            $opened = '';
            if(!empty($this->request->getPost('opened'))) { $opened = 'Opened'; };
            if(empty($this->request->getPost('opened'))) { $opened = 'Opening Soon'; };
            $business->opened = $opened;
            //$business->business_category_id = $this->request->getPost('business_category_id');
            // $business->business_sub_category_id = $this->request->getPost('business_sub_category_id');
            // $business->business_asub_category_id = $this->request->getPost('business_asub_category_id');
            if($business->create()){
                $id = $business->id;
                if(!empty($this->request->getPost('business_category_ids'))) {
                    $bCtegories = $this->request->getPost('business_category_ids');
                    $bCtegoryIds = explode(',', $bCtegories);
                    foreach ($bCtegoryIds as $key => $bCtegoryId) {
                        $businessCategoryLists = new BusinessCategoryLists();
                        $businessCategoryLists->created = date('Y-m-d H:i:s');
                        $businessCategoryLists->business_id = $id;
                        $businessCategoryLists->business_category_id = $bCtegoryId;
                        $businessCategoryLists->create();
                    }
                }

                $this->flash->success('<button type="button" class="close" data-dismiss="alert">Ã—</button>New business has been created');
                
                if(!empty($this->request->getPost('review'))) {
                    $review = new Reviews();
                    $review->created = date('Y-m-d H:i:s'); 
                    $review->modified = date('Y-m-d H:i:s'); 
                    $review->member_id =  $userSession['id'];
                    $review->business_id = $business->id;
                    $review->content = $this->request->getPost('review');
                    $review->rate = $this->request->getPost('rate');
                    if($review->create()){
                        $this->flash->success('<button type="button" class="close" data-dismiss="alert">Ã—</button>You\'re review has been submitted.');
                    } else {
      $this->view->disable();
      echo "whoops!! something went wrong :( insert review to db failed..,";
        }
                }
                return $this->response->redirect('business/view2/'.$id);
            } else {
                $this->view->disable();
                print_r($business->getMessages());
            }
        }
        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);
    }


    public function update_businessXXAction($businessId = null)
    {
        $business = Business::findFirst($businessId);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        if ($this->request->isPost()) {
            $countryId = $this->request->getPost('country_id');
            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'id LIKE :id:', 
                                             'bind' => array('id' => $countryId)));
            $countryName = '';
            if($country) {
                $countryName = $country->country;
            } 

            $address = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            $userSession = $this->session->get("userSession");
            $content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            $json = json_decode($content, true);
            $lat = $json['results'][0]['geometry']['location']['lat'];
            $lng = $json['results'][0]['geometry']['location']['lng'];
            $businessUpdate = Business::findFirst(" id = " . $businessId);
        //$member = Members::findFirstById($this->request->getPost('id'));
            //$businessUpdate = new BusinessUpdates();
            if ($this->request->hasPost('city') == true) {
                if ($this->request->getPost('city') != '') {
                    $getCityName = Cities::findFirst("id='".$this->request->getPost('city')."'");
                    $cityName = $getCityName->city_name;
                } else {
                    $cityName = '';
                }
            } else {
                $cityName = '';
            }

            if ($this->request->hasPost('state') == true) {
                if ($this->request->getPost('state') != '') {
                 $getStateName = States::findFirst("id='".$this->request->getPost('state')."'");
                 $stateName = $getStateName->state_name;
                } else {
                    $stateName = '';
                }
            } else {
                $stateName = '';
            }
           
            $businessUpdate->created = date('Y-m-d H:i:s'); 
            $businessUpdate->modified = date('Y-m-d H:i:s'); 
            $businessUpdate->member_id =  $userSession['id'];
            $businessUpdate->business_id =  $businessId;
            $businessUpdate->name = $this->request->getPost('name');
            $businessUpdate->website = $this->request->getPost('website');
            $businessUpdate->telephone = $this->request->getPost('telephone');
            $businessUpdate->street = $this->request->getPost('street');
            $businessUpdate->apt_no = $this->request->getPost('apt_no');
            $businessUpdate->city = $cityName;
            $businessUpdate->state = $stateName;
            $businessUpdate->postal_code = $this->request->getPost('postal_code');
            $businessUpdate->country_code_id = $this->request->getPost('country_code');
            $businessUpdate->lat = $lat;
            $businessUpdate->lng = $lng;
            //$businessUpdate->status = 'Pending';
            if(!empty($this->request->getPost('review'))) {
                $businessUpdate->total_review = 1;
            }
            $opened = '';
            if(!empty($this->request->getPost('opened'))) { $opened = 'Opened'; };
            if(empty($this->request->getPost('opened'))) { $opened = 'Opening Soon'; };
            $businessUpdate->opened = $opened;
            if($businessUpdate->update()){
        
                // $id = $businessUpdate->id;
                // if(!empty($this->request->getPost('business_category_ids'))) {
                //     $bCtegories = $this->request->getPost('business_category_ids');
                //     $bCtegoryIds = explode(',', $bCtegories);
                //     foreach ($bCtegoryIds as $key => $bCtegoryId) {
                //         $businessCategoryLists = new BusinessCategoryListUpdates();
                //         $businessCategoryLists->created = date('Y-m-d H:i:s');
                //         $businessCategoryLists->business_update_id = $businessId;
                //         $businessCategoryLists->business_category_id = $bCtegoryId;
                //         $businessCategoryLists->create();
                //     }
                // }
                if(!empty($this->request->getPost('business_category_ids'))) {
                    $bCtegories = $this->request->getPost('business_category_ids');
                    $bCtegoryIds = explode(',', $bCtegories);
                    BusinessCategoryLists::find('business_id="'.$businessId.'"')->delete();
                    foreach ($bCtegoryIds as $key => $bCtegoryId) {
                        $businessCategoryLists = new BusinessCategoryLists();
                        $businessCategoryLists->created = date('Y-m-d H:i:s');
                        $businessCategoryLists->business_id = $businessId;
                        $businessCategoryLists->business_category_id = $bCtegoryId;
                        $businessCategoryLists->create();
                    }
                }

                //$this->flash->success('<button type="button" class="close" data-dismiss="alert">Ã—</button>Business upates has been submitted');
                $this->session->set('update_business', 'business has been updated successfully!');
                return $this->response->redirect('business/view2/'.$businessId);
            } else {
                $this->view->disable();
                foreach ($businessUpdate->getMessages() as $message) {
                    echo $message . " <br>";
                }
            }
        }
        $this->view->setVar('business', $business);
        $countries = CountryCodes::find();
        $cities = Cities::find("country_code_id='".$business->country_code_id."'");
        $states = States::find("country_code_id='".$business->country_code_id."'");
        $this->view->setVar('cities', $cities);
        $this->view->setVar('states', $states);
        $this->view->setVar('countries', $countries);
        $businessCategoryLists = BusinessCategoryLists::find('business_id="'.$businessId.'"');
        $this->view->setVar('businessCategoryLists', $businessCategoryLists);
    }











}