<?php

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;



class BusinessController extends ControllerBase
{

    public function indexAction()
    {
        $searchWords = '';
        $business = array();
        if(isset($_GET["page"])){
            $currentPage = (int) $_GET["page"];
        } else {
            $currentPage = 1;
        }
        if ($this->request->isPost()) {
            $businessName = $this->request->getPost('name');
            $businessAddress = $this->request->getPost('address');
            $businessCategoryId = $this->request->getPost('business_category_id');
            $businessCategory = BusinessCategories::findFirst($businessCategoryId);
            $country = Countries::findFirst(array('columns'    => '*', 
                                             'conditions' => 'country LIKE :country:', 
                                             'bind' => array('country' => $businessAddress)));
          
            $countryId = '';
            if($country) {
                $countryId = $country->id;
            } 

            $conditions = ''; 
            $bind = array();
            if(!empty($businessName)) {
                $conditions .= ' OR name LIKE :name:';
                $bind['name'] = '%'.$businessName.'%';
                $searchWords .= ', '.$businessName;
            }
            if(!empty($businessAddress)) {
                $conditions .= ' OR street LIKE :street:';
                $bind['street'] = '%'.$businessAddress.'%';
                $conditions .= ' OR city LIKE :city:';
                $bind['city'] = '%'.$businessAddress.'%';
                $searchWords .= ', '.$businessAddress;
            }
            if(!empty($countryId)) {
                $conditions .= ' OR country_id LIKE :country_id:';
                $bind['country_id'] = $countryId;
                $searchWords .= ', '.$country->country;
            }
            if(!empty($businessCategoryId)) {
                $conditions .= ' OR business_category_id = :business_category_id:';
                $bind['business_category_id'] = $businessCategoryId;
                $searchWords .= ', '.$businessCategory->name;
            }
            

            $business = Business::find(array('columns'    => '*', 
                                            'conditions' => substr($conditions, 3), 
                                            'bind' => $bind
                                            ));

        } else {
            $business = Business::find();
        }
         // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $business,
                "limit"=> 10,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        $this->view->setVar('searchWords', $searchWords);
        $this->view->setVar('business', $page);

        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);
    }

    public function getdirectionsAction($businessId = null) {
        $this->view->setTemplateAfter('default3');
        $business = Business::findFirst($businessId);
        $this->view->setVar('business', $business);
    }
    public function flagAction() {
        if($this->request->isAjax()) 
        {
            $arrFlag = array();

            $review = $this->request->getQuery('review');
            $reviewer = $this->request->getQuery('reviewer');
            $flagger = $this->request->getQuery('flagger');
            $flag_type = $this->request->getQuery('flag_type');
            $referer = $this->request->getHTTPReferer (); 
            $controller = 'business';

            $newFlag = new Flags();
            $newFlag->created = date('Y-m-d H:i:s');
            $newFlag->modified = date('Y-m-d H:i:s');
            $newFlag->review_id = $review;
            $newFlag->member_id = $flagger;
            $newFlag->target_id = $reviewer;
            $newFlag->flag_type_id = $flag_type;
            $newFlag->location = $controller; 
            $newFlag->status = '0';
            $newFlag->page = $referer;

            if ($newFlag->create()) {
                $resultInfo = 'success';
            } else {
                $resultInfo = 'failed';
            }

            $arrFlag[] = ["review" => $review, "reviewer" => $reviewer, "flagger" => $flagger, 'result' => $resultInfo];

            $payload     = $arrFlag; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;

        } else {
            $this->view->disable();
            echo "flag action failed";
        }
    }

public function add_image_coverAction($id = null) {
  $business = Business::findFirstById($id);
  if (!$business) {
    return $this->request->redirect('business/view/'. $id);
  }

  $this->view->setVar('business', $business);
    }
    public function add_photo_coverAction($id = null) {
      $business = Business::findFirstById($id);
      if (!$business) {
        return $this->request->redirect('business/view/'. $id);
      }
      $this->view->setVar('business', $business);

      // if ($this->request->isPost() && $this->request->hasFiles() == true) {
      //   $userSession = $this->session->get("userSession");

      //    $imgCovers = new BusinessBackgroundCovers();

      //    if (!$imgCovers->delete()) {
      //      $this->view->disable();
      //      print_r($e->getMessages());
      //    }
     //            //ini_set('upload_max_filesize', '64M');
     //            set_time_limit(1200);
     //            $uploads = $this->request->getUploadedFiles();

     //            $isUploaded = false;
     //            #do a loop to handle each file individually
     //            foreach($uploads as $upload){
     //                #define a “unique” name and a path to where our file must go
     //                $fileName = $upload->getname();
     //                $fileInfo = new SplFileInfo($fileName);
     //                $fileExt = $fileInfo->getExtension();
     //                $fileExt = strtolower($fileExt);
     //                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
     //                //$fileExt = $upload->getExtension();
     //                $fileImageExt = array('jpeg', 'jpg', 'png');
     //                //error_log("File Extension :".$fileExt, 0);
     //                $fileType = '';
     //                $filePath = '';
     //                $path = '';
     //                //$path = ''.$newFileName;
     //                if(in_array($fileExt, $fileImageExt)){
     //                    $path = 'img/cover/'.$newFileName;
     //                    $filePath = 'img/cover/';
     //                    //$fileType = 'Image';
     //                }
     //                #move the file and simultaneously check if everything was ok
     //                ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
     //            }

     //            #if any file couldn't be moved, then throw an message
     //            if($isUploaded) {
     //                $businessImages = new BusinessBackgroundCovers();
     //                $businessImages->created = date('Y-m-d H:i:s'); 
     //                $businessImages->modified = date('Y-m-d H:i:s'); 
     //                $businessImages->member_id =  $userSession['id'];
     //                $businessImages->business_id =  $id;
     //                $businessImages->file_path =  $filePath;
     //                $businessImages->filename =  $newFileName;
     //                //$businessImages->caption = $this->request->getPost('caption');
     //                $businessImages->primary_pic = 'Yes';
     //                if($businessImages->create()){
     //                    return $this->response->redirect('business/view/'.$id);
     //                } else {
     //                 $this->view->disable();
     //                 print_r($businessImages->getMessages());
     //                }
     //            }
      // } 

      if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/business/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new BusinessImages();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $id;
                    $businessImages->file_path =  $filePath;
                    if ($this->request->getPost('review_photo') != '0') {
                        $businessImages->review_id = $this->request->getPost('review_photo');
                    }
                    $businessImages->filename =  $newFileName;
                    $businessImages->caption = $this->request->getPost('caption');
                    $businessImages->primary_pic = 'No';
                    if($businessImages->create()){
      $this->view->disable();
      echo "oops something went wrong";
      $msg = "new photo has been added to your business page";
                        //return $this->response->redirect('business/photos/'.$id);
                    } else {
                        $this->view->disable();
      $msg = "inserting image data failed..";
                        print_r($businessImages->getMessages());
                    }
            } else {
        $this->view->disable();
        echo "whoops something went wrong!!";
        }

        }
    } 

    public function delete_photoAction($bizImageId = null) {
      if ($bizImageId == null) {
        return $this->response->redirect();
      } 
      $bizImage = BusinessImages::findFirstById($bizImageId);
      if (unlink($bizImage->file_path. $bizImage->filename)) {
        $bizImage->delete();
        $this->session->set('delete_photo', 1);
        return $this->response->redirect($this->request->getHTTPReferer());
      } else {
        $this->session->set('delete_photo', 0);
        return $this->response->redirect($this->request->getHTTPReferer());
      }
    }

  public function add_image2Action($businessImg = null, $id = null, $review_id = null) {
    $this->view->setTemplateAfter('default1');
    $this->view->setVar('referer', $this->request->getHTTPReferer());
    $photo = BusinessImages::findFirstById($businessImg);
    $business = Business::findFirstById($id);
    if(!$business) {
        return $this->response->redirect('review/search_business');
    }
    if (!$photo) {
        return $this->response->redirect('business/view2/'. $id);
    }
    $this->view->setVar('photo', $photo);
    $this->view->setVar('business', $business);
        if ($review_id != null) {
            $this->view->setVar('review_id', $review_id);
        } 
    if($this->request->isPost() && $this->request->hasFiles() == true){
        $userSession = $this->session->get("userSession");
            //ini_set('upload_max_filesize', '64M');
            set_time_limit(1200);
            $uploads = $this->request->getUploadedFiles();

            $isUploaded = false;
            #do a loop to handle each file individually
            foreach($uploads as $upload){
                #define a “unique” name and a path to where our file must go
                $fileName = $upload->getname();
                $fileInfo = new SplFileInfo($fileName);
                $fileExt = $fileInfo->getExtension();
                $fileExt = strtolower($fileExt);
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
                //$fileExt = $upload->getExtension();
                $fileImageExt = array('jpeg', 'jpg', 'png');
                //error_log("File Extension :".$fileExt, 0);
                $fileType = '';
                $filePath = '';
                $path = '';
                //$path = ''.$newFileName;
                if(in_array($fileExt, $fileImageExt)){
                    $path = 'img/business/'.$newFileName;
                    $filePath = 'img/business/';
                    //$fileType = 'Image';
                }
                #move the file and simultaneously check if everything was ok
                ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
            }

            #if any file couldn't be moved, then throw an message
            if($isUploaded) {
                $businessImages = new BusinessImages();
                $businessImages->created = date('Y-m-d H:i:s'); 
                $businessImages->modified = date('Y-m-d H:i:s'); 
                $businessImages->member_id =  $userSession['id'];
                $businessImages->business_id =  $id;
                $businessImages->file_path =  $filePath;
                $businessImages->filename =  $newFileName;
                $businessImages->caption = $this->request->getPost('caption');
                $businessImages->primary_pic = 'No';
                if($businessImages->create()){
                    return $this->response->redirect('business/view2/'.$id);
                } else {
                  $this->view->disable();
                  print_r($businessImages->getMessages());
                }
            }
    }    
  }

    // revised version of add_photoAction.
  public function add_imageAction($id = null, $review_id = null) {
    $this->view->setTemplateAfter('default1');
    $this->view->setVar('referer', $this->request->getHTTPReferer());
    $business = Business::findFirstById($id);
    if(!$business) {
        return $this->response->redirect('review/search_business');
    }
    $this->view->setVar('business', $business);
        if ($review_id != null) {
            $this->view->setVar('review_id', $review_id);
        } 
    if($this->request->isPost() && $this->request->hasFiles() == true){
        $userSession = $this->session->get("userSession");
            //ini_set('upload_max_filesize', '64M');
            set_time_limit(1200);
            $uploads = $this->request->getUploadedFiles();

            $isUploaded = false;
            #do a loop to handle each file individually
            foreach($uploads as $upload){
                #define a “unique” name and a path to where our file must go
                $fileName = $upload->getname();
                $fileInfo = new SplFileInfo($fileName);
                $fileExt = $fileInfo->getExtension();
                $fileExt = strtolower($fileExt);
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
                //$fileExt = $upload->getExtension();
                $fileImageExt = array('jpeg', 'jpg', 'png');
                //error_log("File Extension :".$fileExt, 0);
                $fileType = '';
                $filePath = '';
                $path = '';
                //$path = ''.$newFileName;
                if(in_array($fileExt, $fileImageExt)){
                    $path = 'img/business/'.$newFileName;
                    $filePath = 'img/business/';
                    //$fileType = 'Image';
                }
                #move the file and simultaneously check if everything was ok
                ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
            }

            #if any file couldn't be moved, then throw an message
            if($isUploaded) {
                $businessImages = new BusinessImages();
                $businessImages->created = date('Y-m-d H:i:s'); 
                $businessImages->modified = date('Y-m-d H:i:s'); 
                $businessImages->member_id =  $userSession['id'];
                $businessImages->business_id =  $id;
                $businessImages->file_path =  $filePath;
                $businessImages->filename =  $newFileName;
                $businessImages->caption = $this->request->getPost('caption');
                $businessImages->primary_pic = 'No';
                if($businessImages->create()){
                    return $this->response->redirect('business/view2/'.$id);
                } else {
                  $this->view->disable();
                  print_r($businessImages->getMessages());
                }
            }
    }    
  }
/* crop it 2*/
    public function cropit2Action($id = null) {
  //$msg = null;
        if ($this->request->isAjax()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('imagelink');
            $id = $this->request->get('businessId');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/business/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $currentPhotos = BusinessImages::find('business_id="'.$id.'"');
                      // update all photos of this business change primary_pic into 'No'
                      foreach ($currentPhotos as $key => $currentPhoto) {
                        $currentPhoto->primary_pic = 'No';
                        $currentPhoto->update();
                      }
                    $userSession = $this->session->get("userSession");
                    $businessImages = new BusinessImages();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $id;
                    $businessImages->file_path =  $filePath;
                    if ($this->request->getPost('review_photo') != '0') {
                        $businessImages->review_id = $this->request->getPost('review_photo');
                    }
                    $businessImages->filename =  $newFileName;
                    $businessImages->caption = $this->request->getPost('caption');
                    $businessImages->primary_pic = 'Yes';
                    if($businessImages->create()) {
                      $msg = array();
                      $msg['status'] = "photo has been set to primary";
                      $payload     = $msg; 
                      $status      = 200;
                      $description = 'OK';
                      $headers     = array();
                      $contentType = 'application/json';
                      $content     = json_encode($payload);

                      $response = new \Phalcon\Http\Response();

                      $response->setStatusCode($status, $description);
                      $response->setContentType($contentType, 'UTF-8');
                      $response->setContent($content);

                      // Set the additional headers
                      foreach ($headers as $key => $value) {
                         $response->setHeader($key, $value);
                      }

                      $this->view->disable();

                      return $response;
                        //return $this->response->redirect('business/photos/'.$id);
                    } else {
                        $this->view->disable();
                          $msg = "inserting image data failed..";
                        print_r($businessImages->getMessages());
                    }
            } else {
              $this->view->disable();
              echo "whoops something went wrong!!";
            }

        } else {
          $this->view->disable();
          echo "whoops, something went wrong!";
        }
  
      if (isset($msg)) {
        $this->view->setVar('addPhotoMsg', $msg);
      }
  
    return $this->response->redirect('business/view2/'. $id);
  }
/* crop it */
    public function cropitAction($id = null) {
  //$msg = null;
        if ($this->request->isPost()) {
            
            $this->view->disable();
            if ($this->request->hasFiles() == true) 
            {
                $this->view->disable();
                echo "has file disable()";
            }
            $encoded = $this->request->getPost('image-data');
            $decoded = urldecode($encoded);
            $exp = explode(',', $decoded);
            $base64 = array_pop($exp);
            $data = base64_decode($base64);
            $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
            $filePath = 'img/business/';

            $file = $filePath . $newFileName;
            //echo $encoded;
            //echo '<img src="'.$encoded.'">';
            //file_put_contents($file, $data);
            if (file_put_contents($file, file_get_contents($encoded))) {
                    $userSession = $this->session->get("userSession");
                    $businessImages = new BusinessImages();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $id;
                    $businessImages->file_path =  $filePath;
                    if ($this->request->getPost('review_photo') != '0') {
                        $businessImages->review_id = $this->request->getPost('review_photo');
                    }
                    $businessImages->filename =  $newFileName;
                    $businessImages->caption = $this->request->getPost('caption');
                    $businessImages->primary_pic = 'No';
                    if($businessImages->create()){
      $this->view->disable();
      echo "oops something went wrong";
      $msg = "new photo has been added to your business page";
                        //return $this->response->redirect('business/photos/'.$id);
                    } else {
                        $this->view->disable();
      $msg = "inserting image data failed..";
                        print_r($businessImages->getMessages());
                    }
            } else {
    $this->view->disable();
    echo "whoops something went wrong!!";
      }

        } else {
    $this->view->disable();
    echo "whoops, something went wrong!";
  }
  
  if (isset($msg)) {
    $this->view->setVar('addPhotoMsg', $msg);
  }
  
  return $this->response->redirect('business/view2/'. $id);
    }
    

    
    public function add_photoAction($id = null, $review_id = null)
    { 
        $this->view->setTemplateAfter('default1');
        $business = Business::findFirstById($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
  
    //$this->view->disable();
    if ($review_id != null) {
    $this->view->setVar('review_id', $review_id);
    } 

        $this->view->setVar('business', $business);
        if($this->request->isPost() && $this->request->hasFiles() == true){
            $userSession = $this->session->get("userSession");
                //ini_set('upload_max_filesize', '64M');
                set_time_limit(1200);
                $uploads = $this->request->getUploadedFiles();

                $isUploaded = false;
                #do a loop to handle each file individually
                foreach($uploads as $upload){     
                    #define a “unique” name and a path to where our file must go
                    $fileName = $upload->getname();
                    $fileInfo = new SplFileInfo($fileName);
                    $fileExt = $fileInfo->getExtension();
                    $fileExt = strtolower($fileExt);
                    $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
                    //$fileExt = $upload->getExtension();
                    $fileImageExt = array('jpeg', 'jpg', 'png');
                    //error_log("File Extension :".$fileExt, 0);
                    $fileType = '';
                    $filePath = '';
                    $path = '';
                    //$path = ''.$newFileName;
                    if(in_array($fileExt, $fileImageExt)){
                        $path = 'img/business/'.$newFileName;
                        $filePath = 'img/business/';
                        //$fileType = 'Image';
                    }
                    #move the file and simultaneously check if everything was ok
                    ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
                }

                #if any file couldn't be moved, then throw an message
                if($isUploaded) {
                    $businessImages = new BusinessImages();
                    $businessImages->created = date('Y-m-d H:i:s'); 
                    $businessImages->modified = date('Y-m-d H:i:s'); 
                    $businessImages->member_id =  $userSession['id'];
                    $businessImages->business_id =  $id;
                    $businessImages->file_path =  $filePath;
                    if ($this->request->getPost('review_photo') != '0') {
                      $businessImages->review_id = $this->request->getPost('review_photo');
                    }
                    $businessImages->filename =  $newFileName;
                    $businessImages->caption = $this->request->getPost('caption');
                    $businessImages->primary_pic = 'No';
                    if($businessImages->create()){
                        return $this->response->redirect('business/view2/'.$id);
                    } else {
                      $this->view->disable();
                      print_r($businessImages->getMessages());
                    }
                }
        }    

    }
    
    public function view3Action($id = null) {
    $userSession = $this->session->get("userSession");
        $this->view->setTemplateAfter('default1');
        $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $averageRate = $business->average_rate;
        $businessCategoryLists = BusinessCategoryLists::find('business_id = "'.$id.'"');
        $businessUpdates = Business::findFirst(array('id="'.$id.'"', "order" => "id DESC"));
        if($businessUpdates){
            $business = $businessUpdates;
            //$businessCategoryLists = BusinessCategoryListUpdates::find('business_update_id = "'.$businessUpdates->id.'"');
            $business->id = $id;
            $business->average_rate = $averageRate;
        }
        $this->view->setVar('business', $business);
        
        $this->view->setVar('businessCategoryLists', $businessCategoryLists);
        $query = $this->modelsManager->createQuery("SELECT * from BusinessImages where business_id=$id and primary_pic='yes'");
        //$businessLastImg = $query->execute();
        $businessLastImg = BusinessImages::findFirst(array('business_id="'.$id.'" and primary_pic="Yes"'));
        $businessImgCover = BusinessBackgroundCovers::findFirst(array('business_id="'.$id.'" and primary_pic="Yes"'));
        //$businessLastImg = BusinessImages::findFirst(array('business_id = "'.$id.'"', "order" => "id DESC"));
        $this->view->setVar('businessLastImg', $businessLastImg);
        $businessImages = BusinessImages::find(array('business_id = "'.$id.'"', "order" => "id DESC", "limit" => 7));
        $this->view->setVar('businessImages', $businessImages);
        $reviews = Reviews::find(array('business_id = "'.$id.'"', "order" => "id DESC"));
        $this->view->setVar('reviews', $reviews);
        $this->view->setVar('businessImgCover', $businessImgCover);
    }

    public function view2Action($id = null) {
  $userSession = $this->session->get("userSession");
        $this->view->setTemplateAfter('default1');
        $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $averageRate = $business->average_rate;
        $businessCategoryLists = BusinessCategoryLists::find('business_id = "'.$id.'"');
        $businessUpdates = Business::findFirst(array('id="'.$id.'"', "order" => "id DESC"));
        if($businessUpdates){
            $business = $businessUpdates;
            //$businessCategoryLists = BusinessCategoryListUpdates::find('business_update_id = "'.$businessUpdates->id.'"');
            $business->id = $id;
            $business->average_rate = $averageRate;
        }
        $this->view->setVar('business', $business);
        
        $this->view->setVar('businessCategoryLists', $businessCategoryLists);
        $query = $this->modelsManager->createQuery("SELECT * from BusinessImages where business_id=$id and primary_pic='yes'");
        //$businessLastImg = $query->execute();
        $businessLastImg = BusinessImages::findFirst(array('business_id="'.$id.'" and primary_pic="Yes"'));
        $businessImgCover = BusinessBackgroundCovers::findFirst(array('business_id="'.$id.'" and primary_pic="Yes"'));
        //$businessLastImg = BusinessImages::findFirst(array('business_id = "'.$id.'"', "order" => "id DESC"));
        $this->view->setVar('businessLastImg', $businessLastImg);
        $businessImages = BusinessImages::find(array('business_id = "'.$id.'"', "order" => "id DESC", "limit" => 7));
        $this->view->setVar('businessImages', $businessImages);
        $reviews = Reviews::find(array('business_id = "'.$id.'" AND visibility="0"', "order" => "id DESC"));
        $this->view->setVar('reviews', $reviews);
        $this->view->setVar('businessImgCover', $businessImgCover);
    }
    
    public function picture_editorAction() {
        $this->view->setTemplateAfter('default1');
    }
    public function viewAction($id = null)
    {
      $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $averageRate = $business->average_rate;
        $businessCategoryLists = BusinessCategoryLists::find('business_id = "'.$id.'"');
        $businessUpdates = BusinessUpdates::findFirst(array('business_id="'.$id.'"', "order" => "id DESC"));
        if($businessUpdates){
            $business = $businessUpdates;
            //$businessCategoryLists = BusinessCategoryListUpdates::find('business_update_id = "'.$businessUpdates->id.'"');
            $business->id = $id;
            $business->average_rate = $averageRate;
        }
        $this->view->setVar('business', $business);
        
        $this->view->setVar('businessCategoryLists', $businessCategoryLists);
        $query = $this->modelsManager->createQuery("SELECT * from BusinessImages where business_id=$id and primary_pic='yes'");
    //$businessLastImg = $query->execute();
        $businessLastImg = BusinessImages::findFirst(array('business_id="'.$id.'" and primary_pic="Yes"'));
        $businessImgCover = BusinessBackgroundCovers::findFirst(array('business_id="'.$id.'" and primary_pic="Yes"'));
        //$businessLastImg = BusinessImages::findFirst(array('business_id = "'.$id.'"', "order" => "id DESC"));
        $this->view->setVar('businessLastImg', $businessLastImg);
        $businessImages = BusinessImages::find(array('business_id = "'.$id.'"', "order" => "id DESC", "limit" => 7));
        $this->view->setVar('businessImages', $businessImages);
        $reviews = Reviews::find(array('business_id = "'.$id.'"', "order" => "id DESC"));
        $this->view->setVar('reviews', $reviews);
        $this->view->setVar('businessImgCover', $businessImgCover);
        //error_log("WWWWWW ".print_r($businessCategoryLists));
        // $this->view->setVar('businessCategoryLists', $businessCategoryLists);
        // $reviews = Reviews::find('business_id = "'.$id.'"');
        // $this->view->setVar('reviews', $reviews);
    }
    public function new_biz_hoursAction($id = null) {
  
  $business = Business::findFirst("id='".$id."'");
        $this->view->setTemplateAfter('default1');
  $this->view->setVar('business', $business);

      if ($this->request->isPost()) {
        $start1 = $this->request->getPost('start1');
        $end1 = $this->request->getPost('end1');
        $start2 = $this->request->getPost('start2');
        $end2 = $this->request->getPost('end2');
        $start3 = $this->request->getPost('start3');
        $end3 = $this->request->getPost('end3');
        $start4 = $this->request->getPost('start4');
        $end4 = $this->request->getPost('end4');
        $start5 = $this->request->getPost('start5');
        $end5 = $this->request->getPost('end5');
        $start6 = $this->request->getPost('start6');
        $end6 = $this->request->getPost('end6');
        $start7 = $this->request->getPost('start7');
        $end7 = $this->request->getPost('end7');
        $businessId = $this->request->getPost("business_id");
        $countHour = BusinessHours::count("business_id='".$businessId."'");
        if ($countHour > 0) {
          foreach (BusinessHours::find("business_id='".$businessId."'") as $time) {
            if ($time->delete() == true) {
              // do nothing
            }
          }
        }
        $userSession = $this->session->get("userSession");
        $monday = new BusinessHours();
        $monday->created = date('Y-m-d H:i:s'); 
        $monday->modified = date('Y-m-d H:i:s'); 
        $monday->business_id = $businessId;
        $monday->member_id = $userSession['id'];
        $monday->day = 'monday';
        $monday->start = $start1;
        $monday->end= $end1;
        if ($this->request->getPost('status1') == 'open' || $this->request->getPost('status1') == '') {
           $monday->status = 0;
        } else {
          $monday->status = 1;
        }

        $monday->create();
        $tuesday = new BusinessHours();
        $tuesday->created = date('Y-m-d H:i:s'); 
        $tuesday->modified = date('Y-m-d H:i:s'); 
        $tuesday->business_id = $businessId;
        $tuesday->member_id = $userSession['id'];
        $tuesday->day = 'tuesday';
        $tuesday->start = $start2;
        $tuesday->end= $end2;
        if ($this->request->getPost('status2') == 'open' || $this->request->getPost('status2') == '') {
           $tuesday->status = 0;
        } else {
          $tuesday->status = 1;
        }

        $tuesday->create();

        $wednesday = new BusinessHours();
        $wednesday->created = date('Y-m-d H:i:s'); 
        $wednesday->modified = date('Y-m-d H:i:s'); 
        $wednesday->business_id = $businessId;
        $wednesday->member_id = $userSession['id'];
        $wednesday->day = 'wednesday';
        $wednesday->start = $start3;
        $wednesday->end= $end3;
        if ($this->request->getPost('status3') == 'open' || $this->request->getPost('status3') == '') {
           $wednesday->status = 0;
        } else {
          $wednesday->status = 1;
        }

        $wednesday->create();

        $thursday = new BusinessHours();
        $thursday->created = date('Y-m-d H:i:s'); 
        $thursday->modified = date('Y-m-d H:i:s'); 
        $thursday->business_id = $businessId;
        $thursday->member_id = $userSession['id'];
        $thursday->day = 'thursday';
        $thursday->start = $start4;
        $thursday->end= $end4;
        if ($this->request->getPost('status4') == 'open' || $this->request->getPost('status4') == '') {
           $thursday->status = 0;
        } else {
          $thursday->status = 1;
        }

        $thursday->create();

        $friday = new BusinessHours();
        $friday->created = date('Y-m-d H:i:s'); 
        $friday->modified = date('Y-m-d H:i:s'); 
        $friday->business_id = $businessId;
        $friday->member_id = $userSession['id'];
        $friday->day = 'friday';
        $friday->start = $start5;
        $friday->end= $end5;
        if ($this->request->getPost('status5') == 'open' || $this->request->getPost('status5') == '') {
           $friday->status = 0;
        } else {
          $friday->status = 1;
        }

        $friday->create();

        $saturday = new BusinessHours();
        $saturday->created = date('Y-m-d H:i:s'); 
        $saturday->modified = date('Y-m-d H:i:s'); 
        $saturday->business_id = $businessId;
        $saturday->member_id = $userSession['id'];
        $saturday->day = 'saturday';
        $saturday->start = $start6;
        $saturday->end= $end6;
        if ($this->request->getPost('status6') == 'open' || $this->request->getPost('status6') == '') {
           $saturday->status = 0;
        } else {
          $saturday->status = 1;
        }

        $saturday->create();

        $sunday = new BusinessHours();
        $sunday->created = date('Y-m-d H:i:s'); 
        $sunday->modified = date('Y-m-d H:i:s'); 
        $sunday->business_id = $businessId;
        $sunday->member_id = $userSession['id'];
        $sunday->day = 'sunday';
        $sunday->start = $start7;
        $sunday->end= $end7;
        if ($this->request->getPost('status7') == 'open' || $this->request->getPost('status7') == '') {
           $sunday->status = 0;
        } else {
          $sunday->status = 1;
        }

        if ($sunday->create() == true) {
          return $this->response->redirect('business/view2/'.$businessId);
        } else {
          $this->view->disable();
          echo "failed";
        }
        
        
      }
    }
    public function photosAction($id = null, $owner = null)
    {
  $this->view->setTemplateAfter('default1');
        $business = array();
        if(isset($_GET["page"])){
            $currentPage = (int) $_GET["page"];
        } else {
            $currentPage = 1;
        }
        $business = Business::findFirstById($id);
        if ($owner == null) {
            $businessImages = BusinessImages::find(array('business_id = "'.$id.'"', "order" => "id DESC"));
        } else {
            $businessImages = BusinessImages::find(array('business_id = "'.$id.'" and member_id="'.$owner.'"', "order" => "id DESC"));
        }
        
  if (count($businessImages) <= 0) {
    return $this->response->redirect($this->request->getHTTPReferer()); 
  }
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
        $this->view->setVar('business', $business);
        $paginator = new \Phalcon\Paginator\Adapter\Model(
                array(
                    "data" => $businessImages,
                    "limit"=> 10,
                    "page" => $currentPage
                )
            );
        $userSession = $this->session->get('userSession');
        if ($userSession['type'] == 'Member') {
          $isMember = '1';
          $this->view->setVar('isMember', '1');
        } else if ($userSession['type'] == 'Business') {
          $this->view->setVar('isMember', '2');
        } 
        $this->view->setVar('userId', $userSession['id']);
        $page = $paginator->getPaginate();
        $this->view->setVar('businessImages', $page);
        if ($owner == null) {
            $this->view->setVar('owner', 0);
            $businessFirstImg = BusinessImages::findFirst(array('business_id = "'.$id.'"', "order" => "id DESC"));
        } else {
            $this->view->setVar('owner', 1);
            $businessFirstImg = BusinessImages::findFirst(array('business_id = "'.$id.'" and member_id="'.$owner.'"', "order" => "id DESC"));
        }
        

        $this->view->setVar('businessFirstImg', $businessFirstImg);
        // $businessLastImg = BusinessImages::findFirst(array('business_id = "'.$id.'"', "order" => "id DESC"));
        // $this->view->setVar('businessLastImg', $businessLastImg);
    }

    public function suggested_changesAction($id = null)
    {
        $business = Business::findFirstById($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }

        $businessUpdates = BusinessUpdates::find(array('business_id = "'.$id.'"', "order" => "id DESC"));
         // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $businessUpdates,
                "limit"=> 10,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        $this->view->setVar('searchWords', $searchWords);
        $this->view->setVar('business', $page);

        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);
    }
    
    
    
    public function admin_indexAction(){
        $this->view->setTemplateAfter('admin_default');
       $business = Business::find();
       $this->view->setVar('business', $business);
    }

    public function admin_viewAction($id = null){
        $this->view->setTemplateAfter('admin_default');
        $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('admin/business');
        }
        $this->view->setVar('business', $business);

        $businessUpdates = BusinessUpdates::find(array('business_id="'.$id.'"', "order" => "id DESC"));
        //$businessCategoryLists = BusinessCategoryListUpdates::find('business_update_id = "'.$businessUpdates->id.'"');
        $this->view->setVar('businessUpdates', $businessUpdates);
        //$this->view->setVar('businessCategoryLists', $businessCategoryLists);
    }

    public function admin_pendingAction(){
        $this->view->setTemplateAfter('admin_default');
        $business = Business::find('status="Pending"');
        $this->view->setVar('business', $business);
    }

    public function approveAction($id = null){
        $this->view->disable();
        $business = Business::findFirst($id);
        if(!$business) {
            return $this->response->redirect('admin/business');
        }
        $business->status = 'Approved';
        $business->update();
        return $this->response->redirect('admin/business/view/'.$business->id);
    }

    public function admin_claim_businessAction() {
        echo "admin claim business";
    }

    public function set_primary_photoAction($id = null, $business_id = null) {
        $this->view->disable();
        $photos = BusinessImages::findFirst($id);
        $userSession = $this->session->get('userSession');
        if(!$photos) {
            // if(!$photos || $userSession['id'] != $photos->member_id) {
            return $this->response->redirect('business/photos/'.$business_id);
        } else { 
            $currentPhotos = BusinessImages::find('business_id = "'.$business_id.'"');
            foreach ($currentPhotos as $key => $currentPhoto) {
                $currentPhoto->primary_pic = 'No';
                $currentPhoto->update();             
            }

            $photos->modified = date('Y-m-d H:i:s');
            $photos->primary_pic = 'Yes';
            if($photos->update()) {
                $userSession['primary_pic'] = $photos->file_path.$photos->filename;
                $this->session->set('userSession', $userSession);
                $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo has been set as primary.');
                return $this->response->redirect('business/view2/'.$business_id);
            } else {
                $this->view->disable();
                echo "failed to modified data. ";
            }
        }
    }

    public function change_bground_coverAction($id ) {
      // change background cover .

    }

    public function admin_approve_biz_claimAction($id = null, $business = null, $email = null, $firstName = null, $lastName =  null) {
        // $this->view->disable();
        // echo $id;
        $request = ClaimRequests::findFirst($id);

        $request->status = 'approved';

        if ($request->save()) {
                        $this->getDI()->getMail()->send(
                        array($email => $firstName.' '.$lastName),
                        'Welcome to MyBarangay for Business Owners',
                        'approve_biz',
                        array( 'confirmUrl' => 'business/view2/'. $business, 'fullName' => $firstName.' '.$lastName)
                    );
            $checkClaimStatus = EmailConfirmations::findFirst("user_id='".$request->member_id."'");
            $checkClaimStatus->claim_status = '1';
            $checkClaimStatus->save();
            $this->session->set('claimadminaction', 1);
            return $this->response->redirect('admincontrol/claimbizreq');
        } else {
            $this->view->disable();
            echo "failed to update data";
        }
    }

    public function reject_biz_claimRequest($id = null) {
        $request = ClaimRequests::findFirst($id);

        $request->status = 'rejected';

        if ($request->save()) {
            return $this->response->redirect('index/admin_index/');
        } else {
            $this->view->disable();
            echo "failed to update data";
        }
    }

    public function cancel_biz_claim($id = null) {
        $request = ClaimRequests::findFirst($id);

        $request->status = 'pending';

        if ($request->save()) {
            return $this->response->redirect('index/admin_index/');
        } else {
            $this->view->disable();
            echo "failed to update data";
        }
    }

  public function filtered_seemoreAction($category = null) {
    if ($category == 'automotive') {
        //AUTOMOTIVE
        $phqlAutoList = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 141 AND 173 GROUP BY business_id";
        $autoLists = $this->modelsManager->executeQuery($phqlAutoList);
        $autoIds = array();
        if(count($autoLists) > 0){
            foreach($autoLists as $autoList) {
          $autoIds[] = $autoList->business_id;
            }
            $autoIds = implode(',', $autoIds);
            $phqlAutoBizList = "SELECT * FROM Business WHERE id IN (".$autoIds.") ORDER BY id DESC LIMIT 7";
            $autoBizList = $this->modelsManager->executeQuery($phqlAutoBizList);
        } else {
            $autoBizList = array();
        }
        $this->view->setVar('bizList', $autoBizList); 
    } else if ($category == 'community') {
        //COMMUNITY
        $phqlCommunityList = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 1 AND 94) OR (business_category_id BETWEEN 95 AND 139) OR (business_category_id BETWEEN 564 AND 565) OR (business_category_id BETWEEN 205 AND 209) OR (business_category_id BETWEEN 210 AND 246) 
        OR (business_category_id BETWEEN 709 AND 721) OR (business_category_id BETWEEN 736 AND 742) OR (business_category_id BETWEEN 617 AND 620) GROUP BY business_id";
        $CommunityLists = $this->modelsManager->executeQuery($phqlCommunityList);
        $CommunityIds = array();
        if(count($CommunityLists) > 0){
            foreach($CommunityLists as $CommunityList) {
          $CommunityIds[] = $CommunityList->business_id;
            }
            $CommunityIds = implode(',', $CommunityIds);
            $phqlCommunityBizList = "SELECT * FROM Business WHERE id IN (".$CommunityIds.") ORDER BY id DESC LIMIT 7";
            $CommunityBizList = $this->modelsManager->executeQuery($phqlCommunityBizList);
        } else {
            $CommunityBizList = array();
        }
        $this->view->setVar('bizList', $CommunityBizList);
    } else if ($category == 'food') {
        //FOOD - Food
        $phqlFoodList = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 289 AND 350) OR (business_category_id BETWEEN 743 AND 1047) OR (business_category_id BETWEEN 621 AND 651) GROUP BY business_id";
        $FoodLists = $this->modelsManager->executeQuery($phqlFoodList);
        $FoodIds = array();
        if(count($FoodLists) > 0){
            foreach($FoodLists as $FoodList) {
          $FoodIds[] = $FoodList->business_id;
            }
            $FoodIds = implode(',', $FoodIds);
            $phqlFoodBizList = "SELECT * FROM Business WHERE id IN (".$FoodIds.") ORDER BY id DESC LIMIT 7";
            $FoodBizList = $this->modelsManager->executeQuery($phqlFoodBizList);
        } else {
            $FoodBizList = array();
        }
        $this->view->setVar('bizList', $FoodBizList);
    } else if ($category == 'healthnbeauty') {
      //HEALTH & BEAUTY
      $phqlHBeautyList = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 351 AND 445) OR (business_category_id BETWEEN 174 AND 204) GROUP BY business_id";
      $HBeautyLists = $this->modelsManager->executeQuery($phqlHBeautyList);
      $HBeautyIds = array();
      if(count($HBeautyLists) > 0){
          foreach($HBeautyLists as $HBeautyList) {
        $HBeautyIds[] = $HBeautyList->business_id;
          }
          $HBeautyIds = implode(',', $HBeautyIds);
          $phqlHBeautyBizList = "SELECT * FROM Business WHERE id IN (".$HBeautyIds.") ORDER BY id DESC LIMIT 7";
          $HBeautyBizList = $this->modelsManager->executeQuery($phqlHBeautyBizList);
      } else {
          $HBeautyBizList = array();
      }
      $this->view->setVar('bizList', $HBeautyBizList);
    } else if ($category == 'homenservices') {
      //HOME & SERVICES
      $phqlHServList = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 446 AND 518) OR (business_category_id BETWEEN 566 AND 616) OR (business_category_id BETWEEN 652 AND 663) GROUP BY business_id";
      $HServLists = $this->modelsManager->executeQuery($phqlHServList);
      $HServIds = array();
      if(count($HServLists) > 0){
          foreach($HServLists as $HServList) {
        $HServIds[] = $HServList->business_id;
          }
          $HServIds = implode(',', $HServIds);
          $phqlHServBizList = "SELECT * FROM Business WHERE id IN (".$HServIds.") ORDER BY id DESC LIMIT 7";
          $HServBizList = $this->modelsManager->executeQuery($phqlHServBizList);
      } else {
          $HServBizList = array();
      }
      $this->view->setVar('bizList', $HServBizList);
    } else if ($category == 'financial') {
      //FINANCIAL
      $phqlFinList = "SELECT business_id FROM BusinessCategoryLists WHERE (business_category_id BETWEEN 280 AND 288) OR (business_category_id BETWEEN 664 AND 708) OR (business_category_id BETWEEN 247 AND 279) GROUP BY business_id";
      $FinLists = $this->modelsManager->executeQuery($phqlFinList);
      $FinIds = array();
      if(count($FinLists) > 0){
          foreach($FinLists as $FinList) {
        $FinIds[] = $FinList->business_id;
          }
          $FinIds = implode(',', $FinIds);
          $phqlFinBizList = "SELECT * FROM Business WHERE id IN (".$FinIds.") ORDER BY id DESC LIMIT 7";
          $FinBizList = $this->modelsManager->executeQuery($phqlFinBizList);
      } else {
          $FinBizList = array();
      }
      $this->view->setVar('bizList', $FinBizList);
    } else if ($category == 'hotelntravel') {
       //Hotels & Travel
      $phqlHTravelList = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 519 AND 563 GROUP BY business_id";
      $HTravelLists = $this->modelsManager->executeQuery($phqlHTravelList);
      $HTravelIds = array();
      if(count($HTravelLists) > 0){
          foreach($HTravelLists as $HTravelList) {
        $HTravelIds[] = $HTravelList->business_id;
          }
          $HTravelIds = implode(',', $HTravelIds);
          $phqlHTravelBizList = "SELECT * FROM Business WHERE id IN (".$HTravelIds.") ORDER BY id DESC LIMIT 7";
          $HTravelBizList = $this->modelsManager->executeQuery($phqlHTravelBizList);
      } else {
          $HTravelBizList = array();
      }
      $this->view->setVar('bizList', $HTravelBizList);
    } else if ($category == 'realestate') {
      //REAL ESTATE
      $phqlRealtyList = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 722 AND 735 GROUP BY business_id";
      $RealtyLists = $this->modelsManager->executeQuery($phqlRealtyList);
      $RealtyIds = array();
      if(count($RealtyLists) > 0){
          foreach($RealtyLists as $RealtyList) {
        $RealtyIds[] = $RealtyList->business_id;
          }
          $RealtyIds = implode(',', $RealtyIds);
          $phqlRealtyBizList = "SELECT * FROM Business WHERE id IN (".$RealtyIds.") ORDER BY id DESC LIMIT 7";
          $RealtyBizList = $this->modelsManager->executeQuery($phqlRealtyBizList);
      } else {
          $RealtyBizList = array();
      }
      $this->view->setVar('bizList', $RealtyBizList);
    } else if ($category == 'shopping') {
       //SHOPPING
      $phqlShoppingList = "SELECT business_id FROM BusinessCategoryLists WHERE business_category_id BETWEEN 1048 AND 1171 GROUP BY business_id";
      $ShoppingLists = $this->modelsManager->executeQuery($phqlShoppingList);
      $ShoppingIds = array();
      if(count($ShoppingLists) > 0){
          foreach($ShoppingLists as $ShoppingList) {
        $ShoppingIds[] = $ShoppingList->business_id;
          }
          $ShoppingIds = implode(',', $ShoppingIds);
          $phqlShoppingBizList = "SELECT * FROM Business WHERE id IN (".$ShoppingIds.") ORDER BY id DESC LIMIT 7";
          $ShoppingBizList = $this->modelsManager->executeQuery($phqlShoppingBizList);
      } else {
          $ShoppingBizList = array();
      }
      $this->view->setVar('bizList', $ShoppingBizList);
    }

  }

  public function add_videoAction($id = null, $review_id =null) {
        $business = Business::findFirstById($id);
        if(!$business) {
            return $this->response->redirect('review/search_business');
        }
    
          //$this->view->disable();
          if ($review_id != null) {
            $this->view->setVar('review_id', $review_id);
          } 

        $this->view->setVar('business', $business);
    }

    public function complimentAction($rate = null, $business_id = null, $review_id = null, $reviewer_id = null) {

        if ($rate == null) {return $this->response->redirect('review/search_business'); }
        if ($business_id == null) {return $this->response->redirect('review/search_business'); }
        if ($review_id == null) {return $this->response->redirect('review/search_business'); }

        $this->view->disable();
        $userSession = $this->session->get("userSession");
        $compliment = new ReviewCompliments();
        $compliment->created = date('Y-m-d H:i:s');
        $compliment->modified = date('Y-m-d H:i:s');
        $compliment->rate = $rate; 
        $compliment->business_id = $business_id;
        $compliment->review_id = $review_id;
        $compliment->member_id = $userSession['id'];
        $compliment->reviewer_id = $reviewer_id;
        
        if ($compliment->save()) {
            return $this->response->redirect($this->request->getHTTPReferer());
        } else {
            return $this->response->redirect('review/search_business');
        }     
    }

    public function galleryAction($id = null) {
  $businessImages = BusinessImages::find(array('business_id = "'.$id.'"', "order" => "id DESC", "limit" => 7));
  $this->view->setVars([
    'businessImages' => $businessImages,
    'businessId' => $id
  ]);
    }
    
    public function testAction() {
        $business = Business::find();
        $businessCategoryLists = BusinessCategoryLists::find();
        $this->view->disable();
        //$this->view->setVar('business', $business);
        /*foreach ($business as $biz) {
            echo $biz->city . "<br>";
        }*/
        
        foreach ($businessCategoryLists as $category) {
           echo  $category->Business->name . "<br>";
        }
    }






    /***************************************************************************************/


    /**
     * [update_businessuAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function update_businessAction($id = null){

        $this->view->setTemplateAfter('default1');

        $business = Business::findFirst($id);
        if(!$business){
            return $this->response->redirect('review/business_search');
        }
        

        $revopt = ReviewOptions::findFirst([
            'conditions'    =>  'business_id = :busid: AND member_id = :memid:',
            'bind'          =>  array('busid' => $claimRequests->business_id, 'memid' => $claimRequests->member_id )
        ]);

 
        if ($this->request->isPost()) {


            $messages = $this->add_business_Validate();
            if (count($messages)) {
                $errorMsg = '';
                foreach ($messages as $msg) {
                  $errorMsg .= $msg . "<br>";       
                }
                $this->session->set('errorMess', $errorMsg);
                return $this->response->redirect('review/business_search/');
            } 


            $countryId = $this->request->getPost('country_id');
            $businessId = $this->request->getPost('businessId');

            $country = Countries::findFirst([
                'columns'    => '*', 
                'conditions' => 'id LIKE :id:', 
                'bind' => array('id' => $countryId)
            ]);

            $countryName = '';
            if($country) {
                $countryName = $country->country;
            } 

            $cityName = '';
            if ($this->request->getPost('city') != '') {
                $getCityName = Cities::findFirst("id='".$this->request->getPost('city')."'");
                $cityName = $getCityName->city_name;
            }
     
            $stateName = '';
            if ($this->request->getPost('state') != '') {
                $getStateName = States::findFirst("id='".$this->request->getPost('state')."'");
                $stateName = $getStateName->state_name;
            } 
     
            $address                    = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            $userSession                = $this->session->get("userSession");
            $content                    = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            $json                       = json_decode($content, true);
            $lat                        = $json['results'][0]['geometry']['location']['lat'];
            $lng                        = $json['results'][0]['geometry']['location']['lng'];


            $business->member_id        = $userSession['id'];
            $business->name             = $this->request->getPost('name');
            $business->website          = $this->request->getPost('website');
            $business->telephone        = $this->request->getPost('telephone');
            $business->street           = $this->request->getPost('street');
            $business->city             = $cityName;
            $business->state            = $stateName;
            $business->postal_code      = $this->request->getPost('postal_code');
            $business->country_code_id  = $this->request->getPost('country_code');
            $business->lat              = $lat;
            $business->lng              = $lng;
            $opened = '';

            if(!empty($this->request->getPost('opened'))) { $opened = 'Opened'; };
            if(empty($this->request->getPost('opened'))) { $opened = 'Opening Soon'; };
            $business->opened = $opened;
            if($business->update()){
                if(!empty($this->request->getPost('business_category_ids'))) {
                    $bCtegories = $this->request->getPost('business_category_ids');
                    $bCtegoryIds = explode(',', $bCtegories);
                    BusinessCategoryLists::find('business_id="'.$businessId.'"')->delete();
                    foreach ($bCtegoryIds as $key => $bCtegoryId) {
                        $businessCategoryLists = new BusinessCategoryLists();
                        $businessCategoryLists->created = date('Y-m-d H:i:s');
                        $businessCategoryLists->business_id = $businessId;
                        $businessCategoryLists->business_category_id = $bCtegoryId;
                        $businessCategoryLists->create();
                    }
                }
         
                $this->session->set('successMess', 'business successfully updated!');
                $busname = str_replace(" ","+",$business->name);
                return $this->response->redirect('business/update_business/'.$id.'/'.$busname);


            }else{
                $err = '';
                foreach ($business->getMessages() as $message) {
                    $err .= $message->getMessage() . " <br>";
                }
               $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
               return $this->response->redirect('business/update_business/'.$id);

            }
        }


        $countries = CountryCodes::find();
        $cities = Cities::find("country_code_id='".$business->country_code_id."'");
        $states = States::find("country_code_id='".$business->country_code_id."'");
        $businessCategoryLists = BusinessCategoryLists::find('business_id="'.$business->id.'"');


        $this->view->setVars([
            'businessCategoryLists' => $businessCategoryLists,
            'cities'                => $cities,
            'states'                => $states,
            'countries'             => $countries,
            'ids'                   => $id,
            'business'              => $business,
            'revopt'                => $revopt
        ]);


    }


    public function add_business_Validate() {

        $validation = new Phalcon\Validation();

        $validation->add('name', new PresenceOf(array(
            'message' => 'Business Name field is required'   
        )));

        $validation->add('street', new PresenceOf(array(
            'message' => 'street field is required' 
        )));

        $validation->add('city', new PresenceOf(array(
            'message' => 'city field is required' 
        )));

        $validation->add('country_code', new PresenceOf(array(
            'message' => 'country field is required' 
        )));

        $validation->add('business_category_ids', new PresenceOf(array(
            'message' => 'category is required'
        )));

        $validation->add('state', new PresenceOf(array(
            'message' => 'state is required'
        )));

        return $validation->validate($_POST);
    }



    public function add_businessAction(){
        $this->view->setTemplateAfter('default1');

        $countries = Countries::find();
        $this->view->setVar('countries', $countries);
        $businessCategories = BusinessCategories::find();
        $this->view->setVar('businessCategories', $businessCategories);

        if ($this->request->isPost()) {

            $messages = $this->add_business_Validate();
            if (count($messages)) {
                $errorMsg = '';
                foreach ($messages as $msg) {
                  $errorMsg .= $msg . "<br>";       
                }
                $this->session->set('errorMess', $errorMsg);
                return $this->response->redirect('business/add_business/');
            } 



            $countryId = $this->request->getPost('country_id');


            $country = Countries::findFirst([
                'columns'    => '*', 
                'conditions' => 'id LIKE :id:', 
                'bind' => array('id' => $countryId)
            ]);

            $countryName = '';
            if($country) {
                $countryName = $country->country;
            } 

            $cityName = '';
            if ($this->request->getPost('city') != '') {
                $getCityName = Cities::findFirst("id='".$this->request->getPost('city')."'");
                $cityName = $getCityName->city_name;
            }
     
            $stateName = '';
            if ($this->request->getPost('state') != '') {
                $getStateName = States::findFirst("id='".$this->request->getPost('state')."'");
                $stateName = $getStateName->state_name;
            } 

     
            $address                    = str_replace(' ', '+', $this->request->getPost('street').'+'.$this->request->getPost('city').'+'.$countryName);
            $userSession                = $this->session->get("userSession");
            $content                    = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyAbpLPfBH8sNdVSzMULD_BZN9qrAqbL3V8');
            $json                       = json_decode($content, true);
            $lat                        = $json['results'][0]['geometry']['location']['lat'];
            $lng                        = $json['results'][0]['geometry']['location']['lng'];

            $business = new Business();

            $business->member_id        = $userSession['id'];
            $business->created          = date('Y-m-d H:i:s');
            $business->modified         = date('Y-m-d H:i:s');
            $business->name             = $this->request->getPost('name');
            $business->website          = $this->request->getPost('website');
            $business->telephone        = $this->request->getPost('telephone');
            $business->street           = $this->request->getPost('street');
            $business->city             = $cityName;
            $business->state            = $stateName;
            $business->postal_code      = $this->request->getPost('postal_code');
            $business->country_code_id  = $this->request->getPost('country_code');
            $business->lat              = $lat;
            $business->lng              = $lng;
            $opened = '';

            if(!empty($this->request->getPost('opened'))) { $opened = 'Opened'; };
            if(empty($this->request->getPost('opened'))) { $opened = 'Opening Soon'; };
            $business->opened = $opened;

            if($business->create()){
                $id = $business->id;
    
                if(!empty($this->request->getPost('business_category_ids'))) {
                    $bCtegories = $this->request->getPost('business_category_ids');
                    $bCtegoryIds = explode(',', $bCtegories);
                    BusinessCategoryLists::find('business_id="'.$id.'"')->delete();
                    foreach ($bCtegoryIds as $key => $bCtegoryId) {
                        $businessCategoryLists                          = new BusinessCategoryLists();
                        $businessCategoryLists->created                 = date('Y-m-d H:i:s');
                        $businessCategoryLists->business_id             = $id;
                        $businessCategoryLists->business_category_id    = $bCtegoryId;
                        $businessCategoryLists->create();
                    }
                }

                // $revOpt                  = new ReviewOptions();
                // $revOpt->created         = date('Y-m-d H:i:s');
                // $revOpt->modified        = date('Y-m-d H:i:s');
                // $revOpt->member_id       = $business->member_id;
                // $revOpt->business_id     = $business->id;
                // $revOpt->create();

                $this->session->set('successMess', 'New business has been created!');
                $busname = str_replace(" ","+",$business->name);
                return $this->response->redirect('review/add_review_option/'.$id.'/'.$busname);

            }else{
                $err = '';
                foreach ($business->getMessages() as $message) {
                    $err .= $message->getMessage() . " <br>";
                }
               $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
               return $this->response->redirect('business/add_business/');
            }
        }

    }




    public function add_business_photoAction($id, $name = null){
        $this->view->setTemplateAfter('default1');

        $business = Business::findFirst($id);
        $busname = '';
        if(!$business){
            $this->session->set('errorMess', 'Oops! Something went wrong.. Please try again. <br>'.$err);
        }else{
            $busname = str_replace(" ","+",$business->name);
        }
        
        $this->view->setVars([
            'business' => $business
        ]);

        $data = array();
        // if(isset( $_POST['image_upload'] ) && !empty( $_FILES['images'] )){
        if($this->request->isPost() && $this->request->hasFiles() == true){
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
            $userSession = $this->session->get("userSession");

            //get the structured array
            $images = $this->restructure_array(  $_FILES );
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            
            foreach ( $images as $key => $value){
                $i = $key+1;
                //create directory if not exists
                // if (!file_exists('images')) {
                //     mkdir('images', 0777, true);
                // }
                $image_name = $value['name'];
                //get image extension
                $ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
                //assign unique name to image
                $name = $i*time().'.'.$ext;
                //$name = $image_name;
                //image size calcuation in KB
                $image_size = $value["size"] / 1024;
                $image_flag = true;
                //max image size
                $max_size = 512;
                if( in_array($ext, $allowedExts) && $image_size < $max_size ){
                    $image_flag = true;
                } else {
                    $image_flag = false;
                    $data[$i]['error'] = 'Maybe '.$image_name. ' exceeds max '.$max_size.' KB size or incorrect file extension';
                } 
                
                if( $value["error"] > 0 ){
                    $image_flag = false;
                    $data[$i]['error'] = '';
                    $data[$i]['error'].= '<br/> '.$image_name.' Image contains error - Error Code : '.$value["error"];
                }
                
                if($image_flag){

                    $filePath = 'img/business/';

    
                    $businessImages = new BusinessImages();
                    $businessImages->created        = date('Y-m-d H:i:s'); 
                    $businessImages->modified       = date('Y-m-d H:i:s'); 
                    $businessImages->member_id      =  $userSession['id'];
                    $businessImages->business_id    =  $id;
                    $businessImages->file_path      =  $filePath;
                    // if ($this->request->getPost('review_photo') != '0') {
                    //     $businessImages->review_id = $this->request->getPost('review_photo');
                    // }
                    $businessImages->filename       =   'thumbnail_'.$name;
                    $businessImages->caption        =   $this->request->getPost('caption');
                    $businessImages->primary_pic    =   'No';
                    if($businessImages->create()){

                        move_uploaded_file($value["tmp_name"], "img/business/xxx".$name);
                        $src = "img/business/xxx".$name;
                        $dist = "img/business/thumbnail_".$name;
                        $data[$i]['success'] = $thumbnail = 'thumbnail_'.$name;
                        $this->thumbnail($src, $dist, 400);

                    } else {
                        $err = '';
                        foreach ($businessImages->getMessages() as $message) {
                        $err .= $message->getMessage() . " <br>";
                        }

                        $data[$i]['error'].= '<br/> '.$image_name.' Oops! Something went wrong.. Please try again. ';
                    }

                }
            }
            $output = json_encode($data);
            die($output); //exit script outputting json data
            } else {
                $data[] = 'No Image Selected..';
            }


    }


    public function restructure_array(array $images){
        $result = array();

        foreach ($images as $key => $value) {
            foreach ($value as $k => $val) {
                for ($i = 0; $i < count($val); $i++) {
                    $result[$i][$k] = $val[$i];
                }
            }
        }
        return $result;
    }


    public function thumbnail($src, $dist, $dis_width = 100 ){
        $img = '';
        $extension = strtolower(strrchr($src, '.'));
        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                $img = @imagecreatefromjpeg($src);
                break;
            case '.gif':
                $img = @imagecreatefromgif($src);
                break;
            case '.png':
                $img = @imagecreatefrompng($src);
                break;
        }
        $width = imagesx($img);
        $height = imagesy($img);

        $dis_height = $dis_width * ($height / $width);

        $new_image = imagecreatetruecolor($dis_width, $dis_height);
        imagecopyresampled($new_image, $img, 0, 0, 0, 0, $dis_width, $dis_height, $width, $height);

        $imageQuality = 100;

        switch($extension)
        {
            case '.jpg':
            case '.jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($new_image, $dist, $imageQuality);
                }
                break;

            case '.gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($new_image, $dist);
                }
                break;

            case '.png':
                $scaleQuality = round(($imageQuality/100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                    imagepng($new_image, $dist, $invertScaleQuality);
                }
                break;
        }
        imagedestroy($new_image);
    }





}

