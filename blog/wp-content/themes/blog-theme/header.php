<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="<?bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="favicon.ico">
     <!--    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'> -->
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/main.css">
		<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
   		<?php
		 // if (class_exists('CSSDropDownMenu')){
		   //   $myMenu = new CSSDropDownMenu();
		     // $myMenu->show();
		 // }
		?>
		<?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
      <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=293644353986423";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header role="banner" class="header clearfix">
            <nav class="navbar navbar-inverse" role="navigation">
              <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button class="navbar-search">
                    <i class="fa fa-search"></i>
                  </button>
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="logo">
                  </a>
                </div>

                <form action="/" method="get" class="clearfix" role="search">
                   <input type="text"  name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search MyBarangay">
                  <button type="submit"><i class="fa fa-search"></i></button>
                </form> 

                <div class="navbar-blog cleafix">
                  <div class="sec-top">
                    <h1>BLOG</h1>
                    <ul>
                      <li><a href="<?php echo get_option('general_setting_facebook'); ?>"><i class="fa fa-facebook"></i></a></li>
                     <li><a href="<?php echo get_option('general_setting_googleplus'); ?>"><i class="fa fa-google-plus"></i></a></li>
                      <!-- <li><a href="<?php // echo get_option('general_setting_linkedin'); ?>"><i class="fa fa-linkedin"></i></a></li> -->
                      <li><a href="<?php echo get_option('general_setting_twitter'); ?>"><i class="fa fa-twitter"></i></a></li> 
                      <li><a href="<?php echo get_the_permalink(2); ?>">About Us</a></li>
                    </ul>
                  </div>
                  <div class="sec-bottom">
                    <ul>
                      <?php wp_list_categories('hide_empty=0&title_li=');  ?>
                    </ul>
                  </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <div class="container">
                    <?php 
                          wp_nav_menu( array(
                            'depth' => 1,
                            'container' => false,
                            'menu_class' => 'nav navbar-nav',
                            'walker' => new wp_bootstrap_navwalker())
                          );
                    ?>
                    <form action="<?php bloginfo('home'); ?>" method="get" class="clearfix" role="search">
                      <input type="text"  name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search MyBarangay">
                      <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                  </div>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-->
            </nav>
            <div class="head-divider">
               <div class="container">
                 <div class="flag-banner"></div>
               </div>
            </div>
        </header>
