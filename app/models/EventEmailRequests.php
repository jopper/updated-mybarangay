<?php


class EventEmailRequests extends \Phalcon\Mvc\Model{


    public $id;
    public $created_at;
    public $first_name;
    public $last_name;
    public $contact;
    public $email_sender;
    public $message;
    public $event_ticket_id;
    public $event_id;
    public $ticket_request;
    public $status;


    public function initialize(){
        $this->belongsTo("event_id", "Events", "id");
    }


}
