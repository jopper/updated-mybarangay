<?php return array (
  'lifetime' => 1448682894,
  'data' => 
  array (
    'PluginCorePluginsAdminMetadata' => 
    array (
      'description' => 'CorePluginsAdmin_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreAdminHomeMetadata' => 
    array (
      'description' => 'CoreAdminHome_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreHomeMetadata' => 
    array (
      'description' => 'CoreHome_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginWebsiteMeasurableMetadata' => 
    array (
      'description' => 'Analytics for the web: lets you measure and analyze Websites.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'WebsiteMeasurable',
    ),
    'PluginDiagnosticsMetadata' => 
    array (
      'description' => 'Performs diagnostics to check that Piwik is installed and runs correctly.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreVisualizationsMetadata' => 
    array (
      'description' => 'CoreVisualizations_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginProxyMetadata' => 
    array (
      'description' => 'Proxy services',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginAPIMetadata' => 
    array (
      'description' => 'API_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExamplePluginMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to create widgets, menus, scheduled tasks, a custom archiver, plugin tests, and a AngularJS component.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'email' => '',
          'homepage' => '',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '0.1.0',
      'theme' => false,
      'require' => 
      array (
        'piwik' => '>=PIWIK_VERSION',
      ),
      'name' => 'ExamplePlugin',
    ),
    'PluginWidgetizeMetadata' => 
    array (
      'description' => 'Widgetize_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginTransitionsMetadata' => 
    array (
      'description' => 'Transitions_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLanguagesManagerMetadata' => 
    array (
      'description' => 'LanguagesManager_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginActionsMetadata' => 
    array (
      'description' => 'Actions_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDashboardMetadata' => 
    array (
      'description' => 'Dashboard_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMultiSitesMetadata' => 
    array (
      'description' => 'MultiSites_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik PRO',
          'homepage' => 'http://piwik.pro',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginReferrersMetadata' => 
    array (
      'description' => 'Referrers_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserLanguageMetadata' => 
    array (
      'description' => 'UserLanguage_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDevicesDetectionMetadata' => 
    array (
      'description' => 'DevicesDetection_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik PRO',
          'homepage' => 'http://piwik.pro',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginGoalsMetadata' => 
    array (
      'description' => 'Goals_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginEcommerceMetadata' => 
    array (
      'description' => 'Ecommerce_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'Ecommerce',
    ),
    'PluginSEOMetadata' => 
    array (
      'description' => 'SEO_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginEventsMetadata' => 
    array (
      'description' => 'Events_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserCountryMetadata' => 
    array (
      'description' => 'UserCountry_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitsSummaryMetadata' => 
    array (
      'description' => 'VisitsSummary_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitFrequencyMetadata' => 
    array (
      'description' => 'VisitFrequency_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitTimeMetadata' => 
    array (
      'description' => 'VisitTime_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginVisitorInterestMetadata' => 
    array (
      'description' => 'VisitorInterest_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginExampleAPIMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to create an API for your plugin to let your users export your data in multiple formats.',
      'homepage' => 'http://piwik.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'email' => 'hello@piwik.org',
          'homepage' => 'http://piwik.org',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleAPI',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'api',
      ),
    ),
    'PluginExampleRssWidgetMetadata' => 
    array (
      'description' => 'Piwik Platform showcase: how to create a new widget that displays a user submitted RSS feed.',
      'homepage' => 'http://piwik.org',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'email' => 'hello@piwik.org',
          'homepage' => 'http://piwik.org',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '1.0',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'ExampleRssWidget',
      'keywords' => 
      array (
        0 => 'example',
        1 => 'feed',
        2 => 'widget',
      ),
    ),
    'PluginProviderMetadata' => 
    array (
      'description' => 'Provider_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginFeedbackMetadata' => 
    array (
      'description' => 'Feedback_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMonologMetadata' => 
    array (
      'description' => 'Adds logging capabilities to Piwik.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLoginMetadata' => 
    array (
      'description' => 'Login_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUsersManagerMetadata' => 
    array (
      'description' => 'UsersManager_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginSitesManagerMetadata' => 
    array (
      'description' => 'SitesManager_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginInstallationMetadata' => 
    array (
      'description' => 'Installation_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreUpdaterMetadata' => 
    array (
      'description' => 'CoreUpdater_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCoreConsoleMetadata' => 
    array (
      'description' => 'CoreConsole_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginScheduledReportsMetadata' => 
    array (
      'description' => 'ScheduledReports_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginUserCountryMapMetadata' => 
    array (
      'description' => 'UserCountryMap_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLiveMetadata' => 
    array (
      'description' => 'Live_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCustomVariablesMetadata' => 
    array (
      'description' => 'CustomVariables_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginPrivacyManagerMetadata' => 
    array (
      'description' => 'PrivacyManager_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginImageGraphMetadata' => 
    array (
      'description' => 'ImageGraph_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginAnnotationsMetadata' => 
    array (
      'description' => 'Annotations_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMobileMessagingMetadata' => 
    array (
      'description' => 'MobileMessaging_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginOverlayMetadata' => 
    array (
      'description' => 'Overlay_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginSegmentEditorMetadata' => 
    array (
      'description' => 'SegmentEditor_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginInsightsMetadata' => 
    array (
      'description' => 'Insights_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginZenModeMetadata' => 
    array (
      'description' => 'ZenMode_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginLeftMenuMetadata' => 
    array (
      'description' => 'LeftMenu_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginMorpheusMetadata' => 
    array (
      'description' => 'Morpheus is the default theme of Piwik 2 designed to help you focus on your analytics. In Greek mythology, Morpheus is the God of dreams. In the Matrix movie, Morpheus is the leader of the rebel forces who fight to awaken humans from a dreamlike reality called The Matrix. ',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => true,
      'require' => 
      array (
      ),
      'name' => 'Morpheus',
      'stylesheet' => 'stylesheets/main.less',
    ),
    'PluginContentsMetadata' => 
    array (
      'description' => 'Contents_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginBulkTrackingMetadata' => 
    array (
      'description' => 'Provides ability to send several Tracking API requests in one bulk request. Makes importing a lot of data in Piwik faster.',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
      'name' => 'BulkTracking',
    ),
    'PluginResolutionMetadata' => 
    array (
      'description' => 'Resolution_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginDevicePluginsMetadata' => 
    array (
      'description' => 'DevicePlugins_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginIntlMetadata' => 
    array (
      'description' => 'Intl_PluginDescription',
      'homepage' => 'http://piwik.org/',
      'authors' => 
      array (
        0 => 
        array (
          'name' => 'Piwik',
          'homepage' => 'http://piwik.org/',
        ),
      ),
      'license' => 'GPL v3+',
      'license_homepage' => 'http://www.gnu.org/licenses/gpl.html',
      'version' => '2.14.3',
      'theme' => false,
      'require' => 
      array (
      ),
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/CoreHome/Columns/IdSite.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\IdSite',
      '/var/www/analytics/plugins/CoreHome/Columns/UserId.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\UserId',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitFirstActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitFirstActionTime',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitGoalBuyer.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalBuyer',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitGoalConverted.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitGoalConverted',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitLastActionTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitLastActionTime',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitorDaysSinceFirst.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceFirst',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitorDaysSinceOrder.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorDaysSinceOrder',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitorReturning.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitorReturning',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitsCount.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitsCount',
      '/var/www/analytics/plugins/CoreHome/Columns/VisitTotalTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\VisitTotalTime',
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/Actions/Columns/EntryPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageTitle',
      '/var/www/analytics/plugins/Actions/Columns/EntryPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\EntryPageUrl',
      '/var/www/analytics/plugins/Actions/Columns/ExitPageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageTitle',
      '/var/www/analytics/plugins/Actions/Columns/ExitPageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ExitPageUrl',
      '/var/www/analytics/plugins/Actions/Columns/VisitTotalActions.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalActions',
      '/var/www/analytics/plugins/Actions/Columns/VisitTotalSearches.php' => 'Piwik\\Plugins\\Actions\\Columns\\VisitTotalSearches',
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/Referrers/Columns/Campaign.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Campaign',
      '/var/www/analytics/plugins/Referrers/Columns/Keyword.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Keyword',
      '/var/www/analytics/plugins/Referrers/Columns/ReferrerName.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerName',
      '/var/www/analytics/plugins/Referrers/Columns/ReferrerType.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerType',
      '/var/www/analytics/plugins/Referrers/Columns/ReferrerUrl.php' => 'Piwik\\Plugins\\Referrers\\Columns\\ReferrerUrl',
      '/var/www/analytics/plugins/Referrers/Columns/Website.php' => 'Piwik\\Plugins\\Referrers\\Columns\\Website',
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/UserLanguage/Columns/Language.php' => 'Piwik\\Plugins\\UserLanguage\\Columns\\Language',
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/DevicesDetection/Columns/BrowserEngine.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserEngine',
      '/var/www/analytics/plugins/DevicesDetection/Columns/BrowserName.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserName',
      '/var/www/analytics/plugins/DevicesDetection/Columns/BrowserVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\BrowserVersion',
      '/var/www/analytics/plugins/DevicesDetection/Columns/DeviceBrand.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceBrand',
      '/var/www/analytics/plugins/DevicesDetection/Columns/DeviceModel.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceModel',
      '/var/www/analytics/plugins/DevicesDetection/Columns/DeviceType.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\DeviceType',
      '/var/www/analytics/plugins/DevicesDetection/Columns/Os.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\Os',
      '/var/www/analytics/plugins/DevicesDetection/Columns/OsVersion.php' => 'Piwik\\Plugins\\DevicesDetection\\Columns\\OsVersion',
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/Events/Columns/TotalEvents.php' => 'Piwik\\Plugins\\Events\\Columns\\TotalEvents',
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/UserCountry/Columns/City.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\City',
      '/var/www/analytics/plugins/UserCountry/Columns/Country.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Country',
      '/var/www/analytics/plugins/UserCountry/Columns/Latitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Latitude',
      '/var/www/analytics/plugins/UserCountry/Columns/Longitude.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Longitude',
      '/var/www/analytics/plugins/UserCountry/Columns/Provider.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Provider',
      '/var/www/analytics/plugins/UserCountry/Columns/Region.php' => 'Piwik\\Plugins\\UserCountry\\Columns\\Region',
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/VisitTime/Columns/LocalTime.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\LocalTime',
      '/var/www/analytics/plugins/VisitTime/Columns/ServerTime.php' => 'Piwik\\Plugins\\VisitTime\\Columns\\ServerTime',
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/VisitorInterest/Columns/VisitsByDaysSinceLastVisit.php' => 'Piwik\\Plugins\\VisitorInterest\\Columns\\VisitsByDaysSinceLastVisit',
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginExampleRssWidgetColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginProviderColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/Provider/Columns/Provider.php' => 'Piwik\\Plugins\\Provider\\Columns\\Provider',
    ),
    'PluginFeedbackColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginZenModeColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginLeftMenuColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/Resolution/Columns/Resolution.php' => 'Piwik\\Plugins\\Resolution\\Columns\\Resolution',
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginCookie.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginCookie',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginDirector.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginDirector',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginFlash.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginFlash',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginGears.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginGears',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginJava.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginJava',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginPdf.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginPdf',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginQuickTime.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginQuickTime',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginRealPlayer.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginRealPlayer',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginSilverlight.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginSilverlight',
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginWindowsMedia.php' => 'Piwik\\Plugins\\DevicePlugins\\Columns\\PluginWindowsMedia',
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\VisitDimension' => 
    array (
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/analytics/plugins/CoreHome/Columns/ServerTime.php' => 'Piwik\\Plugins\\CoreHome\\Columns\\ServerTime',
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/analytics/plugins/Actions/Columns/ClickedUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\ClickedUrl',
      '/var/www/analytics/plugins/Actions/Columns/DownloadUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\DownloadUrl',
      '/var/www/analytics/plugins/Actions/Columns/PageTitle.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageTitle',
      '/var/www/analytics/plugins/Actions/Columns/PageUrl.php' => 'Piwik\\Plugins\\Actions\\Columns\\PageUrl',
      '/var/www/analytics/plugins/Actions/Columns/SearchKeyword.php' => 'Piwik\\Plugins\\Actions\\Columns\\SearchKeyword',
      '/var/www/analytics/plugins/Actions/Columns/TimeSpentRefAction.php' => 'Piwik\\Plugins\\Actions\\Columns\\TimeSpentRefAction',
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/analytics/plugins/Events/Columns/EventAction.php' => 'Piwik\\Plugins\\Events\\Columns\\EventAction',
      '/var/www/analytics/plugins/Events/Columns/EventCategory.php' => 'Piwik\\Plugins\\Events\\Columns\\EventCategory',
      '/var/www/analytics/plugins/Events/Columns/EventName.php' => 'Piwik\\Plugins\\Events\\Columns\\EventName',
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginExampleRssWidgetColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginProviderColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginFeedbackColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginZenModeColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginLeftMenuColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
      '/var/www/analytics/plugins/Contents/Columns/ContentInteraction.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentInteraction',
      '/var/www/analytics/plugins/Contents/Columns/ContentName.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentName',
      '/var/www/analytics/plugins/Contents/Columns/ContentPiece.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentPiece',
      '/var/www/analytics/plugins/Contents/Columns/ContentTarget.php' => 'Piwik\\Plugins\\Contents\\Columns\\ContentTarget',
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\ActionDimension' => 
    array (
    ),
    'PluginCorePluginsAdminColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreAdminHomeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreHomeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginWebsiteMeasurableColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDiagnosticsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreVisualizationsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginProxyColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginAPIColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginExamplePluginColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginWidgetizeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginTransitionsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLanguagesManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginActionsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDashboardColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMultiSitesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginReferrersColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserLanguageColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDevicesDetectionColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginGoalsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
      '/var/www/analytics/plugins/Goals/Columns/IdGoal.php' => 'Piwik\\Plugins\\Goals\\Columns\\IdGoal',
    ),
    'PluginEcommerceColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueDiscount.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueDiscount',
      '/var/www/analytics/plugins/Ecommerce/Columns/Revenue.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\Revenue',
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueShipping.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueShipping',
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueSubtotal.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueSubtotal',
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueTax.php' => 'Piwik\\Plugins\\Ecommerce\\Columns\\RevenueTax',
    ),
    'PluginSEOColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginEventsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserCountryColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitsSummaryColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitFrequencyColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitTimeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginVisitorInterestColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginExampleAPIColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginExampleRssWidgetColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginProviderColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginFeedbackColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMonologColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLoginColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUsersManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginSitesManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginInstallationColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreUpdaterColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCoreConsoleColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginScheduledReportsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginUserCountryMapColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLiveColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginCustomVariablesColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginPrivacyManagerColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginImageGraphColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginAnnotationsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMobileMessagingColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginOverlayColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginSegmentEditorColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginInsightsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginZenModeColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginLeftMenuColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginMorpheusColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginContentsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginBulkTrackingColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginResolutionColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginDevicePluginsColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'PluginIntlColumns\\Piwik\\Plugin\\Dimension\\ConversionDimension' => 
    array (
    ),
    'AllDimensionModifyTime' => 
    array (
      '/var/www/analytics/plugins/Actions/Columns/ClickedUrl.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/DestinationPage.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/DownloadUrl.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/EntryPageTitle.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/EntryPageUrl.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/ExitPageTitle.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/ExitPageUrl.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/Keyword.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/KeywordwithNoSearchResult.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/PageTitle.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/PageUrl.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/SearchCategory.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/SearchDestinationPage.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/SearchKeyword.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/SearchNoResultKeyword.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/TimeSpentRefAction.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/VisitTotalActions.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/VisitTotalSearches.php' => 1439969667,
      '/var/www/analytics/plugins/Contents/Columns/ContentInteraction.php' => 1439969736,
      '/var/www/analytics/plugins/Contents/Columns/ContentName.php' => 1439969736,
      '/var/www/analytics/plugins/Contents/Columns/ContentPiece.php' => 1439969736,
      '/var/www/analytics/plugins/Contents/Columns/ContentTarget.php' => 1439969736,
      '/var/www/analytics/plugins/CoreHome/Columns/IdSite.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/ServerTime.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/UserId.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitFirstActionTime.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitGoalBuyer.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitGoalConverted.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitLastActionTime.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitorDaysSinceFirst.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitorDaysSinceOrder.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitorReturning.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitsCount.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/VisitTotalTime.php' => 1439969717,
      '/var/www/analytics/plugins/CustomVariables/Columns/CustomVariableName.php' => 1439969657,
      '/var/www/analytics/plugins/CustomVariables/Columns/CustomVariableValue.php' => 1439969657,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginCookie.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginDirector.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginFlash.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginGears.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginJava.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginPdf.php' => 1439969680,
      '/var/www/analytics/plugins/DevicePlugins/Columns/Plugin.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginQuickTime.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginRealPlayer.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginSilverlight.php' => 1439969681,
      '/var/www/analytics/plugins/DevicePlugins/Columns/PluginWindowsMedia.php' => 1439969680,
      '/var/www/analytics/plugins/DevicesDetection/Columns/Base.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/BrowserEngine.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/BrowserName.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/BrowserVersion.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/DeviceBrand.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/DeviceModel.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/DeviceType.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/Os.php' => 1439969731,
      '/var/www/analytics/plugins/DevicesDetection/Columns/OsVersion.php' => 1439969731,
      '/var/www/analytics/plugins/Ecommerce/Columns/BaseConversion.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/ProductCategory.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/ProductName.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/ProductSku.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueDiscount.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/Revenue.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueShipping.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueSubtotal.php' => 1439969678,
      '/var/www/analytics/plugins/Ecommerce/Columns/RevenueTax.php' => 1439969678,
      '/var/www/analytics/plugins/Events/Columns/EventAction.php' => 1439969687,
      '/var/www/analytics/plugins/Events/Columns/EventCategory.php' => 1439969687,
      '/var/www/analytics/plugins/Events/Columns/EventName.php' => 1439969687,
      '/var/www/analytics/plugins/Events/Columns/TotalEvents.php' => 1439969687,
      '/var/www/analytics/plugins/ExampleTracker/Columns/ExampleActionDimension.php' => 1439969712,
      '/var/www/analytics/plugins/ExampleTracker/Columns/ExampleConversionDimension.php' => 1439969712,
      '/var/www/analytics/plugins/ExampleTracker/Columns/ExampleDimension.php' => 1439969712,
      '/var/www/analytics/plugins/ExampleTracker/Columns/ExampleVisitDimension.php' => 1439969712,
      '/var/www/analytics/plugins/Goals/Columns/DaysToConversion.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/IdGoal.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/VisitsUntilConversion.php' => 1439969722,
      '/var/www/analytics/plugins/MultiSites/Columns/Website.php' => 1439969676,
      '/var/www/analytics/plugins/Provider/Columns/Provider.php' => 1439969701,
      '/var/www/analytics/plugins/Referrers/Columns/Base.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/Campaign.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/Keyword.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/ReferrerName.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/Referrer.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/ReferrerType.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/ReferrerUrl.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/SearchEngine.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/SocialNetwork.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/WebsitePage.php' => 1439969696,
      '/var/www/analytics/plugins/Referrers/Columns/Website.php' => 1439969696,
      '/var/www/analytics/plugins/Resolution/Columns/Configuration.php' => 1439969704,
      '/var/www/analytics/plugins/Resolution/Columns/Resolution.php' => 1439969704,
      '/var/www/analytics/plugins/UserCountry/Columns/Base.php' => 1439969664,
      '/var/www/analytics/plugins/UserCountry/Columns/City.php' => 1439969664,
      '/var/www/analytics/plugins/UserCountry/Columns/Continent.php' => 1439969664,
      '/var/www/analytics/plugins/UserCountry/Columns/Country.php' => 1439969664,
      '/var/www/analytics/plugins/UserCountry/Columns/Latitude.php' => 1439969664,
      '/var/www/analytics/plugins/UserCountry/Columns/Longitude.php' => 1439969664,
      '/var/www/analytics/plugins/UserCountry/Columns/Provider.php' => 1439969664,
      '/var/www/analytics/plugins/UserCountry/Columns/Region.php' => 1439969664,
      '/var/www/analytics/plugins/UserLanguage/Columns/Language.php' => 1439969686,
      '/var/www/analytics/plugins/VisitorInterest/Columns/PagesPerVisit.php' => 1439969650,
      '/var/www/analytics/plugins/VisitorInterest/Columns/VisitDuration.php' => 1439969650,
      '/var/www/analytics/plugins/VisitorInterest/Columns/VisitsByDaysSinceLastVisit.php' => 1439969650,
      '/var/www/analytics/plugins/VisitorInterest/Columns/VisitsbyVisitNumber.php' => 1439969650,
      '/var/www/analytics/plugins/VisitTime/Columns/DayOfTheWeek.php' => 1439969674,
      '/var/www/analytics/plugins/VisitTime/Columns/LocalTime.php' => 1439969674,
      '/var/www/analytics/plugins/VisitTime/Columns/ServerTime.php' => 1439969674,
      '/var/www/analytics/plugins/Actions/Columns/Metrics/AveragePageGenerationTime.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/Metrics/AverageTimeOnPage.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/Metrics/BounceRate.php' => 1439969667,
      '/var/www/analytics/plugins/Actions/Columns/Metrics/ExitRate.php' => 1439969667,
      '/var/www/analytics/plugins/Contents/Columns/Metrics/InteractionRate.php' => 1439969736,
      '/var/www/analytics/plugins/CoreHome/Columns/Metrics/ActionsPerVisit.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/Metrics/AverageTimeOnSite.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/Metrics/BounceRate.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/Metrics/CallableProcessedMetric.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/Metrics/ConversionRate.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/Metrics/EvolutionMetric.php' => 1439969717,
      '/var/www/analytics/plugins/CoreHome/Columns/Metrics/VisitsPercent.php' => 1439969717,
      '/var/www/analytics/plugins/Events/Columns/Metrics/AverageEventValue.php' => 1439969687,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/AverageOrderRevenue.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/AveragePrice.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/AverageQuantity.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/GoalSpecificProcessedMetric.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/ProductConversionRate.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/RevenuePerVisit.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/GoalSpecific/AverageOrderRevenue.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/GoalSpecific/ConversionRate.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/GoalSpecific/Conversions.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/GoalSpecific/ItemsCount.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/GoalSpecific/RevenuePerVisit.php' => 1439969722,
      '/var/www/analytics/plugins/Goals/Columns/Metrics/GoalSpecific/Revenue.php' => 1439969722,
      '/var/www/analytics/plugins/MultiSites/Columns/Metrics/EcommerceOnlyEvolutionMetric.php' => 1439969676,
      '/var/www/analytics/plugins/VisitFrequency/Columns/Metrics/ReturningMetric.php' => 1439969647,
    ),
    'PluginCorePluginsAdminRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreAdminHomeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreHomeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginWebsiteMeasurableRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDiagnosticsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreVisualizationsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginProxyRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginAPIRendererPiwik\\API\\ApiRenderer' => 
    array (
      '/var/www/analytics/plugins/API/Renderer/Console.php' => 'Piwik\\Plugins\\API\\Renderer\\Console',
      '/var/www/analytics/plugins/API/Renderer/Csv.php' => 'Piwik\\Plugins\\API\\Renderer\\Csv',
      '/var/www/analytics/plugins/API/Renderer/Html.php' => 'Piwik\\Plugins\\API\\Renderer\\Html',
      '/var/www/analytics/plugins/API/Renderer/Json2.php' => 'Piwik\\Plugins\\API\\Renderer\\Json2',
      '/var/www/analytics/plugins/API/Renderer/Json.php' => 'Piwik\\Plugins\\API\\Renderer\\Json',
      '/var/www/analytics/plugins/API/Renderer/Original.php' => 'Piwik\\Plugins\\API\\Renderer\\Original',
      '/var/www/analytics/plugins/API/Renderer/Php.php' => 'Piwik\\Plugins\\API\\Renderer\\Php',
      '/var/www/analytics/plugins/API/Renderer/Rss.php' => 'Piwik\\Plugins\\API\\Renderer\\Rss',
      '/var/www/analytics/plugins/API/Renderer/Tsv.php' => 'Piwik\\Plugins\\API\\Renderer\\Tsv',
      '/var/www/analytics/plugins/API/Renderer/Xml.php' => 'Piwik\\Plugins\\API\\Renderer\\Xml',
    ),
    'PluginExamplePluginRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginWidgetizeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginTransitionsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLanguagesManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginActionsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDashboardRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMultiSitesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginReferrersRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserLanguageRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDevicesDetectionRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginGoalsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginEcommerceRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSEORendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginEventsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserCountryRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitsSummaryRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitFrequencyRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitTimeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginVisitorInterestRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginExampleAPIRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginExampleRssWidgetRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginProviderRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginFeedbackRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMonologRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLoginRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUsersManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSitesManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginInstallationRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreUpdaterRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCoreConsoleRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginScheduledReportsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginUserCountryMapRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLiveRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCustomVariablesRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginPrivacyManagerRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginImageGraphRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginAnnotationsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMobileMessagingRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginOverlayRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginSegmentEditorRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginInsightsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginZenModeRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginLeftMenuRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginMorpheusRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginContentsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginBulkTrackingRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginResolutionRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginDevicePluginsRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginIntlRendererPiwik\\API\\ApiRenderer' => 
    array (
    ),
    'PluginCorePluginsAdminTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\CorePluginsAdmin\\Tasks',
    'PluginCoreAdminHomeTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\CoreAdminHome\\Tasks',
    'PluginCoreHomeTasksPiwik\\Plugin\\Tasks' => false,
    'PluginWebsiteMeasurableTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDiagnosticsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCoreVisualizationsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginProxyTasksPiwik\\Plugin\\Tasks' => false,
    'PluginAPITasksPiwik\\Plugin\\Tasks' => false,
    'PluginExamplePluginTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\ExamplePlugin\\Tasks',
    'PluginWidgetizeTasksPiwik\\Plugin\\Tasks' => false,
    'PluginTransitionsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginLanguagesManagerTasksPiwik\\Plugin\\Tasks' => false,
    'PluginActionsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDashboardTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMultiSitesTasksPiwik\\Plugin\\Tasks' => false,
    'PluginReferrersTasksPiwik\\Plugin\\Tasks' => false,
    'PluginUserLanguageTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDevicesDetectionTasksPiwik\\Plugin\\Tasks' => false,
    'PluginGoalsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginEcommerceTasksPiwik\\Plugin\\Tasks' => false,
    'PluginSEOTasksPiwik\\Plugin\\Tasks' => false,
    'PluginEventsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginUserCountryTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\UserCountry\\Tasks',
    'PluginVisitsSummaryTasksPiwik\\Plugin\\Tasks' => false,
    'PluginVisitFrequencyTasksPiwik\\Plugin\\Tasks' => false,
    'PluginVisitTimeTasksPiwik\\Plugin\\Tasks' => false,
    'PluginVisitorInterestTasksPiwik\\Plugin\\Tasks' => false,
    'PluginExampleAPITasksPiwik\\Plugin\\Tasks' => false,
    'PluginExampleRssWidgetTasksPiwik\\Plugin\\Tasks' => false,
    'PluginProviderTasksPiwik\\Plugin\\Tasks' => false,
    'PluginFeedbackTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMonologTasksPiwik\\Plugin\\Tasks' => false,
    'PluginLoginTasksPiwik\\Plugin\\Tasks' => false,
    'PluginUsersManagerTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\UsersManager\\Tasks',
    'PluginSitesManagerTasksPiwik\\Plugin\\Tasks' => false,
    'PluginInstallationTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCoreUpdaterTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\CoreUpdater\\Tasks',
    'PluginCoreConsoleTasksPiwik\\Plugin\\Tasks' => false,
    'PluginScheduledReportsTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\ScheduledReports\\Tasks',
    'PluginUserCountryMapTasksPiwik\\Plugin\\Tasks' => false,
    'PluginLiveTasksPiwik\\Plugin\\Tasks' => false,
    'PluginCustomVariablesTasksPiwik\\Plugin\\Tasks' => false,
    'PluginPrivacyManagerTasksPiwik\\Plugin\\Tasks' => 'Piwik\\Plugins\\PrivacyManager\\Tasks',
    'PluginImageGraphTasksPiwik\\Plugin\\Tasks' => false,
    'PluginAnnotationsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMobileMessagingTasksPiwik\\Plugin\\Tasks' => false,
    'PluginOverlayTasksPiwik\\Plugin\\Tasks' => false,
    'PluginSegmentEditorTasksPiwik\\Plugin\\Tasks' => false,
    'PluginInsightsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginZenModeTasksPiwik\\Plugin\\Tasks' => false,
    'PluginLeftMenuTasksPiwik\\Plugin\\Tasks' => false,
    'PluginMorpheusTasksPiwik\\Plugin\\Tasks' => false,
    'PluginContentsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginBulkTrackingTasksPiwik\\Plugin\\Tasks' => false,
    'PluginResolutionTasksPiwik\\Plugin\\Tasks' => false,
    'PluginDevicePluginsTasksPiwik\\Plugin\\Tasks' => false,
    'PluginIntlTasksPiwik\\Plugin\\Tasks' => false,
    'availableLanguages' => 
    array (
      0 => 
      array (
        'code' => 'am',
        'name' => 'አማርኛ',
        'english_name' => 'Amharic',
      ),
      1 => 
      array (
        'code' => 'ar',
        'name' => 'العربية',
        'english_name' => 'Arabic',
      ),
      2 => 
      array (
        'code' => 'be',
        'name' => 'Беларуская',
        'english_name' => 'Belarusian',
      ),
      3 => 
      array (
        'code' => 'bg',
        'name' => 'Български',
        'english_name' => 'Bulgarian',
      ),
      4 => 
      array (
        'code' => 'bn',
        'name' => 'বাংলা',
        'english_name' => 'Bengali',
      ),
      5 => 
      array (
        'code' => 'bs',
        'name' => 'Bosanski',
        'english_name' => 'Bosnian',
      ),
      6 => 
      array (
        'code' => 'ca',
        'name' => 'Català',
        'english_name' => 'Catalan',
      ),
      7 => 
      array (
        'code' => 'cs',
        'name' => 'Čeština',
        'english_name' => 'Czech',
      ),
      8 => 
      array (
        'code' => 'cy',
        'name' => 'Cymraeg',
        'english_name' => 'Welsh',
      ),
      9 => 
      array (
        'code' => 'da',
        'name' => 'Dansk',
        'english_name' => 'Danish',
      ),
      10 => 
      array (
        'code' => 'de',
        'name' => 'Deutsch',
        'english_name' => 'German',
      ),
      11 => 
      array (
        'code' => 'el',
        'name' => 'Ελληνικά',
        'english_name' => 'Greek',
      ),
      12 => 
      array (
        'code' => 'en',
        'name' => 'English',
        'english_name' => 'English',
      ),
      13 => 
      array (
        'code' => 'es',
        'name' => 'Español',
        'english_name' => 'Spanish',
      ),
      14 => 
      array (
        'code' => 'et',
        'name' => 'Eesti',
        'english_name' => 'Estonian',
      ),
      15 => 
      array (
        'code' => 'eu',
        'name' => 'Euskara',
        'english_name' => 'Basque',
      ),
      16 => 
      array (
        'code' => 'fa',
        'name' => 'فارسی',
        'english_name' => 'Persian',
      ),
      17 => 
      array (
        'code' => 'fi',
        'name' => 'Suomi',
        'english_name' => 'Finnish',
      ),
      18 => 
      array (
        'code' => 'fr',
        'name' => 'Français',
        'english_name' => 'French',
      ),
      19 => 
      array (
        'code' => 'gl',
        'name' => 'Galego',
        'english_name' => 'Galician',
      ),
      20 => 
      array (
        'code' => 'he',
        'name' => 'עברית',
        'english_name' => 'Hebrew',
      ),
      21 => 
      array (
        'code' => 'hi',
        'name' => 'हिन्दी',
        'english_name' => 'Hindi',
      ),
      22 => 
      array (
        'code' => 'hr',
        'name' => 'Hrvatski',
        'english_name' => 'Croatian',
      ),
      23 => 
      array (
        'code' => 'hu',
        'name' => 'Magyar',
        'english_name' => 'Hungarian',
      ),
      24 => 
      array (
        'code' => 'id',
        'name' => 'Bahasa Indonesia',
        'english_name' => 'Indonesian',
      ),
      25 => 
      array (
        'code' => 'is',
        'name' => 'Íslenska',
        'english_name' => 'Icelandic',
      ),
      26 => 
      array (
        'code' => 'it',
        'name' => 'Italiano',
        'english_name' => 'Italian',
      ),
      27 => 
      array (
        'code' => 'ja',
        'name' => '日本語',
        'english_name' => 'Japanese',
      ),
      28 => 
      array (
        'code' => 'ka',
        'name' => 'ქართული',
        'english_name' => 'Georgian',
      ),
      29 => 
      array (
        'code' => 'ko',
        'name' => '한국어',
        'english_name' => 'Korean',
      ),
      30 => 
      array (
        'code' => 'lt',
        'name' => 'Lietuvių',
        'english_name' => 'Lithuanian',
      ),
      31 => 
      array (
        'code' => 'lv',
        'name' => 'Latviešu',
        'english_name' => 'Latvian',
      ),
      32 => 
      array (
        'code' => 'nb',
        'name' => 'Norsk bokmål',
        'english_name' => 'Norwegian Bokmål',
      ),
      33 => 
      array (
        'code' => 'nl',
        'name' => 'Nederlands',
        'english_name' => 'Dutch',
      ),
      34 => 
      array (
        'code' => 'nn',
        'name' => 'Nynorsk',
        'english_name' => 'Norwegian Nynorsk',
      ),
      35 => 
      array (
        'code' => 'pl',
        'name' => 'Polski',
        'english_name' => 'Polish',
      ),
      36 => 
      array (
        'code' => 'pt-br',
        'name' => 'Português do Brasil',
        'english_name' => 'Brazilian Portuguese',
      ),
      37 => 
      array (
        'code' => 'pt',
        'name' => 'Português',
        'english_name' => 'Portuguese',
      ),
      38 => 
      array (
        'code' => 'ro',
        'name' => 'Română',
        'english_name' => 'Romanian',
      ),
      39 => 
      array (
        'code' => 'ru',
        'name' => 'Русский',
        'english_name' => 'Russian',
      ),
      40 => 
      array (
        'code' => 'sk',
        'name' => 'Slovenčina',
        'english_name' => 'Slovak',
      ),
      41 => 
      array (
        'code' => 'sl',
        'name' => 'Slovenščina',
        'english_name' => 'Slovenian',
      ),
      42 => 
      array (
        'code' => 'sq',
        'name' => 'Shqip',
        'english_name' => 'Albanian',
      ),
      43 => 
      array (
        'code' => 'sr',
        'name' => 'Српски',
        'english_name' => 'Serbian',
      ),
      44 => 
      array (
        'code' => 'sv',
        'name' => 'Svenska',
        'english_name' => 'Swedish',
      ),
      45 => 
      array (
        'code' => 'ta',
        'name' => 'தமிழ்',
        'english_name' => 'Tamil',
      ),
      46 => 
      array (
        'code' => 'te',
        'name' => 'తెలుగు',
        'english_name' => 'Telugu',
      ),
      47 => 
      array (
        'code' => 'th',
        'name' => 'ไทย',
        'english_name' => 'Thai',
      ),
      48 => 
      array (
        'code' => 'tl',
        'name' => 'Filipino',
        'english_name' => 'Tagalog',
      ),
      49 => 
      array (
        'code' => 'tr',
        'name' => 'Türkçe',
        'english_name' => 'Turkish',
      ),
      50 => 
      array (
        'code' => 'uk',
        'name' => 'Українська',
        'english_name' => 'Ukrainian',
      ),
      51 => 
      array (
        'code' => 'vi',
        'name' => 'Tiếng Việt',
        'english_name' => 'Vietnamese',
      ),
      52 => 
      array (
        'code' => 'zh-cn',
        'name' => '简体中文',
        'english_name' => 'Simplified Chinese',
      ),
      53 => 
      array (
        'code' => 'zh-tw',
        'name' => '繁體中文',
        'english_name' => 'Traditional Chinese',
      ),
    ),
  ),
);