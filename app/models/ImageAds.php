<?php

class ImageAds extends \Phalcon\Mvc\Model
{
    public $id;
    public $created;
    public $modified;
    public $member_id;
    public $business_id;
    public $type;
    public $file_path;
    public $file_name;
    public $title;
    public $content;
    public $bground_color;
    public $side_text;
    public $hit;
    public $impression;
 public $status;
    public function initialize()
    {
        $this->belongsTo("member_id", "Members", "id");
        $this->belongsTo("business_id", "Business", "id");
    }

    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'created' => 'created', 
            'modified' => 'modified', 
            'member_id' => 'member_id',
            'business_id' => 'business_id',
            'file_path' => 'file_path',
            'file_name' => 'file_name', 
            'title' => 'title',
            'type' => 'type',
            'content' => 'content',
            'bground_color' => 'bground_color',
            'side_text' => 'side_text',
            'hit' => 'hit',
            'impression' => 'impression',
            'status' => 'status',


        );
    }
}
?>
