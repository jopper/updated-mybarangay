<?php

/* empty.twig */
class __TwigTemplate_137a3c9c1687ae7e0957e5891a83796e8776930a0d7843fe273765cafc9ca044 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('content', $context, $blocks);
    }

    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "empty.twig";
    }

    public function getDebugInfo()
    {
        return array (  20 => 1,);
    }
}
