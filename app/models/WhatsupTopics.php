<?php

class WhatsupTopics extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $subject;

    /**
     *
     * @var date
     */
    public $created_at;


    /**
     *
     * @var integer
     */
    public $member_id;


    /**
     *
     * @var integer
     */
    public $whatsup_category_id;


    /**
     *
     * @var integer
     */
    public $status;




    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){

         $this->belongsTo("member_id", "WhatsupCategories", "id");
         $this->belongsTo("member_id", "Members", "id");
         $this->hasMany("id", "WhatsupReplies", "whatsup_topic_id");
         $this->hasMany("id", "WhatsupCompliments", "whatsup_topic_id");
    }






}