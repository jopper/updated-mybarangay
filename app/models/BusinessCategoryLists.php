<?php

class BusinessCategoryLists extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;
     public $city_id;
    /**
     *
     * @var integer
     */
    public $business_id;

    /**
     *
     * @var string
     */
    public $business_category_id;

     public function initialize()
    {
         $this->belongsTo("business_id", "Business", "id");
         $this->belongsTo("business_category_id", "BusinessCategories", "id");
    }
   

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'created' => 'created', 
            'modified' => 'modified',
            'city_id' => 'city_id',
            'business_id' => 'business_id',
            'business_category_id' => 'business_category_id'
        );
    }

}
