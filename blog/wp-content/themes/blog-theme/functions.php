<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */

automatic_feed_links();

include('bootstrap-walker.php');

add_action( 'after_setup_theme', 'register_my_menus' );
 
function register_my_menus() {
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'your_theme_name' ),
	) );
}

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '',
		'after_title' => '',
	));

add_theme_support('category-thumbnails');
add_theme_support( 'post-thumbnails' );
add_post_type_support('page', 'excerpt');

function get_the_excerpt_here($post_id)
{
  global $wpdb;
  $query = "SELECT post_excerpt FROM $wpdb->posts WHERE ID = $post_id LIMIT 1";
  $result = $wpdb->get_results($query, ARRAY_A);
  return $result[0]['post_excerpt'];
}



// —————Add Settings to General Settings—————–
function social_settings_api_init() {
// Add the section to general settings so we can add our
// fields to it
add_settings_section('social_setting_section',
'Social sites on the web',
'social_setting_section_callback_function',
'general');
// Add the field with the names and function to use for our new
// settings, put it in our new section
add_settings_field('general_setting_facebook',
'Facebook Page',
'general_setting_facebook_callback_function',
'general',
'social_setting_section');
// Register our setting so that $_POST handling is done for us and
// our callback function just has to echo the <input>
register_setting('general','general_setting_facebook');
add_settings_field('general_setting_twitter',
'Twitter Account',
'general_setting_twitter_callback_function',
'general',
'social_setting_section');
register_setting('general','general_setting_twitter');
add_settings_field('general_setting_googleplus',
'Google Plus Page',
'general_setting_googleplus_callback_function',
'general',
'social_setting_section');
register_setting('general','general_setting_googleplus');
add_settings_field('general_setting_youtube',
'YouTube Page',
'general_setting_youtube_callback_function',
'general',
'social_setting_section');
register_setting('general','general_setting_youtube');
add_settings_field('general_setting_linkedin',
'LinkedIn Page',
'general_setting_linkedin_callback_function',
'general',
'social_setting_section');
register_setting('general','general_setting_linkedin');
}
add_action('admin_init', 'social_settings_api_init');


// —————-Settings section callback function———————-
function social_setting_section_callback_function() {
echo '<p>This section is where you can save the social sites where readers can find you on the Internet.</p>';
}
function general_setting_facebook_callback_function() {
echo '<input name="general_setting_facebook" id="general_setting_facebook" type="text" value="'. get_option('general_setting_facebook') .'" />';
}
function general_setting_twitter_callback_function() {
echo '<input name="general_setting_twitter" id="general_setting_twitter" type="text" value="'. get_option('general_setting_twitter') .'" />';
}
function general_setting_googleplus_callback_function() {
echo '<input name="general_setting_googleplus" id="general_setting_googleplus" type="text" value="'. get_option('general_setting_googleplus') .'" />';
}
function general_setting_youtube_callback_function() {
echo '<input name="general_setting_youtube" id="general_setting_youtube" type="text" value="'. get_option('general_setting_youtube') .'" />';
}
function general_setting_linkedin_callback_function() {
echo '<input name="general_setting_linkedin" id="general_setting_linkedin" type="text" value="'. get_option('general_setting_linkedin') .'" />';
}


/* ---------------------------------------------------------------------- */
/*	Check the current post for the existence of a short code
/* ---------------------------------------------------------------------- */

if ( !function_exists('radium_has_shortcode') ) {

function radium_has_shortcode($shortcode = '') {

global $post;
$post_obj = get_post( $post->ID );
$found = false;

if ( !$shortcode )
return $found;
if ( stripos( $post_obj->post_content, '[' . $shortcode ) !== false )
$found = true;

// return our results
return $found;

}
}

/*Then I can call this function from anywhere like so

if( radium_has_shortcode('gallery') ) {
//do some stuff here
}*/

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

//changing the logo
// function my_custom_login_logo() {
//     echo '<style type="text/css">
//     h1 a { background:url('.get_bloginfo('template_directory').'/images/rmdc-logo.png) !important; background-size: contain!important; }
//     </style>';
//     }
//     add_action('login_head', 'my_custom_login_logo');

    // changing the login page URL
    function put_my_url(){
    return ('http://www.ronniemoralesdesign.com/'); // putting my URL in place of the WordPress one
    }
    add_filter('login_headerurl', 'put_my_url');

// changing the login page URL hover text
    function put_my_title(){
    return ('Powered by AOB.'); // changing the title from "Powered by WordPress" to whatever you wish
    }
    add_filter('login_headertitle', 'put_my_title');
    
    function wpse_edit_footer()
{
    add_filter( 'admin_footer_text', 'wpse_edit_text', 11 );
}

function wpse_edit_text($content) {
    return "Thank you for choosing AOB.";
}
add_action( 'admin_init', 'wpse_edit_footer' );
    
function has_children($post_id) {
    $children = get_pages("child_of=$post_id");
    if( count( $children ) != 0 ) { return true; } // Has Children
    else { return false; } // No children
}

/* Custom ajax loader */
add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
	return  get_bloginfo('stylesheet_directory') . '/images/ajax-loader.gif';
}

?>
