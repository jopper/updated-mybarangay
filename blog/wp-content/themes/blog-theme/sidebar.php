<div class="sidebar">
                    <div class="side-about">
                      <?php echo get_field('about',69); ?>
                    </div> <!--/ side-about -->
                    <div class="side-popular">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/s_img1.png" alt="Most Popular">
                      

                    <?php  $the_query = new WP_Query('post_type=post&posts_per_page=3&order_by=date&meta_key=wpb_post_views_count&orderby=meta_value_num'); ?>

                    <?php if ( $the_query->have_posts() ) : ?>

                      <!-- the loop -->
                      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                      <div class="side-post">
                        <span class="cat-post"><?php the_category(' '); ?></span>
                        <h3><?php the_title(); ?></h3>
                        <?php the_post_thumbnail(); ?>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>">Read More</a>
                      </div>
                      <hr>
                      <?php endwhile; ?>
                      <!-- end of the loop -->

                      <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                    </div> <!--/ side-popular -->
                    <div class="side-facebook">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/s_img2.png" alt="Image">
                      <div class="fb-page" data-href="https://www.facebook.com/pages/MyBarangay/605719569528125?__mref=message_bubble" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pages/MyBarangay/605719569528125?__mref=message_bubble"><a href="https://www.facebook.com/pages/MyBarangay/605719569528125?__mref=message_bubble">MyBarangay</a></blockquote></div></div>
                    </div> <!--/ side-facebook -->
                    <div class="site-ad">
                      <a href="<?php echo get_field('link',69); ?>">
                        <img src="<?php echo get_field('image',69); ?>">
                      </a>
                    </div> <!--/ side-ad -->
                    <div class="side-contact">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/s_img3.png" alt="Image">
                      <div class="add-email">
                        <span>Email all inquiries to:</span>
                        <a href="mailto:<?php echo get_field('email', 2); ?>"><?php echo get_field('email', 2); ?></a>
                      </div> <!-- add-email -->
                      <div class="add-tel">
                        <span>Call us at:</span>
                        <?php echo get_field('contact_number', 2); ?>
                      </div> <!--/ add-tell -->
                      <div class="add-office">
                        <span>Our Main Office:</span>
                        <?php echo get_field('address', 2); ?>
                      </div>
                      <a href="<?php echo get_the_permalink(2); ?>">About Us</a>
                    </div> <!--/ side-contact -->
                  </div>