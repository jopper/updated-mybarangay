<?php


class EventTickets extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $modified_at;


    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $event_id;

    /**
     *
     * @var integer
     */
    public $member_id;


    /**
     *
     * @var integer
     */
    public $quantity;

    /**
     *
     * @var integer
     */
    public $price;

    /**
     *
     * @var $integer
     */
    public $status;


    public $ticket_desc;
    public $check_desc;
    public $check_ticket_type;
    public $min;
    public $max;
    public $currency_type;
    public function initialize(){
        $this->belongsTo("event_id", "Events", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id'            => 'id',
            'created_at'    => 'created_at',
            'modified_at'   => 'modified_at',
            'name'          => 'name',          
            'event_id'      => 'event_id',              
            'member_id'     => 'member_id',
            'quantity'      => 'quantity',
            'price'         => 'price',
            'status'        => 'status',
            'ticket_desc'           => 'ticket_desc',
            'check_desc'            => 'check_desc',
            'check_ticket_type'     => 'check_ticket_type',
            'min'                   => 'min',
            'max'                   => 'max',
            'currency_type'         => 'currency_type'


        );
    }

}
