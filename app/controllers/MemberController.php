<?php

//use Facebook\FacebookSession;
//use Facebook\FacebookRequest;
// facebook sdk
use Facebook\Facebook;
use Abraham\TwitterOAuth\TwitterOAuth;
// use Facebook\FacebookSession;
// use Facebook\FacebookRedirectLoginHelper;
// use Facebook\FacebookRequest;
// use Facebook\FacebookResponse;
// use Facebook\FacebookSDKException;
// use Facebook\FacebookRequestException;
// use Facebook\FacebookAuthorizationException;
// use Facebook\GraphObject;
// use Facebook\Entities\AccessToken;
// use Facebook\HttpClients\FacebookCurlHttpClient;
// use Facebook\HttpClients\FacebookHttpable;
// -- facebook sdk
use Google\Client;
use Google\Service\Plus;
use Google\Http\Request;
use Google\IO\Curl;
use Phalcon\Tag;
use Phalcon\Paginator\Adapter\Model as Paginator;

define('CONSUMER_KEY', 'Sea502gYwxs5CK13RIlVxen3s');
define('CONSUMER_SECRET', 'dJCTTpAT4z6pBhYy4Y1aIcQ5MafOXuueAe4W0JNql2p4GNB087');
define('OAUTH_CALLBACK', 'https://mybarangay.com/member/twitterConnect');

class MemberController extends ControllerBase
{



    /**
     * [initialize description]
     * @return [type] [description]
     */
    public function initialize(){
        parent::initialize();
        $this->view->setTemplateAfter('default1');
        if(isset($this->view->userSession['type'])){
            if($this->view->userSession['type'] == 'Business'){
                return $this->response->redirect('biz/pageu/');
            }
        }

        //$this->view->typesession = $this->session->get('userSession');
    }



  public function signup_successAction(){

      $this->view->setTemplateAfter('oneheader_default');


  }



    /**
     * [galleryuAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function galleryuAction($id){

        $currentPage = isset($_GET["page"])? (int) $_GET["page"] : 1 ;

        $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $paginator = new \Phalcon\Paginator\Adapter\Model([
            "data" => $photos,
            "limit"=> 12,
            "page" => $currentPage
        ]);
        $page = $paginator->getPaginate();
        $this->view->seschecker = $this->sessionChecker($id);

      
        $this->view->setVars([
            'photos' => $page,
            'ids' => $id
        ]);


    }


  public function eventuAction($id){

      $this->view->setTemplateAfter('default1');
        $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $this->view->setVar('photos', $photos);


  }



    /**
     * [pageuAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function pageuAction($id){


        $this->view->seschecker = $this->sessionChecker($id);
        $currentPage = 1;

         // $userSession = $this->session->get('userSession');
         // if($userSession){
         //    $id = $userSession['id'];
         //    return $this->response->redirect('member/pageu/'.$id);
         // }else{
         //  return $this->response->redirect('member/login/');
         // }


      $member = Members::findFirstById($id);

        if(!$member){
            return $this->response->redirect('member/login/');
        }
      $this->view->setVar('member', $member);

      $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));

      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));


        $currentPage = $this->request->getQuery("page", "int");
        $paginator = new Paginator(
            [
                "data" => $reviews,
                "limit"=> 10,
                "page" => $currentPage
            ]
        );

        $page = $paginator->getPaginate();

        $this->view->setVars([
        'reviews' => $page,
        'ids' => $id
        ]);


    }

    /**
     * [profileu description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function profileuAction($id){
        $this->view->setTemplateAfter('default1');

        $this->view->seschecker = $this->sessionChecker($id);
        $member = Members::findFirstById($id);

        if(!$member){
            return $this->response->redirect('member/login/');        
        }
        $photos = '';
        $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
        $this->view->setVars([
            'photos' => $photos,
            'member' => $member,
            'ids' => $id
        ]);
    }



  /**
   * [pageAction description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
    public function pageAction($id = null){


    if($id == null or $id == ''){
       $userSession = $this->session->get('userSession');
       if($userSession){
          $id = $userSession['id'];
       }else{
        return $this->response->redirect('member/login/');
       }
    }

      //
      $this->view->setTemplateAfter('default1');

      // 
      $member = Members::findFirstById($id);
      $this->view->setVar('member', $member);

        $currentPage = isset($_GET["page"])? (int) $_GET["page"] : 1 ;

      //
       // $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
       // $paginator = new \Phalcon\Paginator\Adapter\Model(
       //      array(
       //          "data" => $reviews,
       //          "limit"=> 3,
       //          "page" => $currentPage
       //      )
       //  );

       //  $page = $paginator->getPaginate();

        //print_r($parameters);
       $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
       $paginator = new \Phalcon\Paginator\Adapter\Model(
            [
                "data" => $reviews,
                "limit"=> 6,
                "page" => $currentPage
            ]
        );
        $topics = WhatsupTopics::find("member_id = '".$id."'");
        //paginator
        $page = $paginator->getPaginate();
        $this->view->setVar('counter', $reviews);
        $this->view->setVar('reviews', $page);
        $this->view->setVar('topics',  $topics);
        
        $this->view->ids = $id;

      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
    }
    




  // temporary
  public function biz_pageAction() {
      $member = Members::findFirstById($id);
      $this->view->setVar('member', $member);

      $reviews = Reviews::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('reviews', $reviews);

      $claimRequests = ClaimRequests::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
 }
    
  public function aboutAction($id = null)
  {
      $member = Members::findFirst($id);
      $this->view->setVar('member', $member);

  }
    
  public function update_image_captionAction($id = null) 
  {
  $this->view->setTemplateAfter('default1');
  $photo = MemberPhotos::findFirst($id);
      $userSession = $this->session->get('userSession');
      if(!$photo || $userSession['id'] != $photo->member_id) {
        return $this->response->redirect('member/gallery/'.$userSession['id']);
      } 
      $this->view->setVar('photo', $photo);
      if($this->request->isPost()){
        $photo->modified = date('Y-m-d H:i:s');
        $photo->caption = $this->request->getPost('caption');
        if($photo->update()) {
          $userSession['primary_pic'] = $photo->file_path.$photo->filename;
        $this->session->set('userSession', $userSession);
          $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo caption has been updated.');
        return $this->response->redirect('member/update_image_caption/'.$id);
      }
      }
      $photos = MemberPhotos::find(array('member_id = "'.$photo->member_id.'"', 'order' => 'id DESC'));
      $this->view->setVar('photos', $photos);
      $member = Members::findFirst($photo->member_id);
      $this->view->setVar('member', $member);
    }
    public function update_photo_captionAction($id = null)
    {
      $photo = MemberPhotos::findFirst($id);
      $userSession = $this->session->get('userSession');
      if(!$photo || $userSession['id'] != $photo->member_id) {
        return $this->response->redirect('member/gallery/'.$userSession['id']);
      } 
      $this->view->setVar('photo', $photo);
      if($this->request->isPost()){
        $photo->modified = date('Y-m-d H:i:s');
        $photo->caption = $this->request->getPost('caption');
        if($photo->update()) {
          $userSession['primary_pic'] = $photo->file_path.$photo->filename;
        $this->session->set('userSession', $userSession);
          $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo caption has been updated.');
        return $this->response->redirect('member/update_photo_caption/'.$id);
      }
      }
      $photos = MemberPhotos::find(array('member_id = "'.$photo->member_id.'"', 'order' => 'id DESC'));
      $this->view->setVar('photos', $photos);
      $member = Members::findFirst($photo->member_id);
      $this->view->setVar('member', $member);
    }

    public function set_primary_photoAction($memberId = null , $id = null)
    {
      $this->view->disable();
      $photos = MemberPhotos::findFirst($id);

      return $this->response->redirect('member/add_photo/'.$memberId.'/'.$id);
      $userSession = $this->session->get('userSession');
      if(!$photos || $userSession['id'] != $photos->member_id) {
        return $this->response->redirect('member/gallery/'.$userSession['id']);
      } else { 
        $currentPhotos = MemberPhotos::find('member_id = "'.$userSession['id'].'"');
        foreach ($currentPhotos as $key => $currentPhoto) {
          $currentPhoto->primary_pic = 'No';
          $currentPhoto->update();
        }

        $photos->modified = date('Y-m-d H:i:s');
        $photos->primary_pic = 'Yes';
        if($photos->update()) {
          $userSession['primary_pic'] = $photos->file_path.$photos->filename;
        $this->session->set('userSession', $userSession);
          //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo has been set as primary.');
        return $this->response->redirect('member/gallery/'.$userSession['id']);
      }
      }

    }

    public function delete_photoAction($id = null)
    {
      $this->view->disable();
      $photos = MemberPhotos::findFirst($id);
      $userSession = $this->session->get('userSession');
      if(!$photos || $userSession['id'] != $photos->member_id) {
        return $this->response->redirect('member/add_photo/'.$userSession['id']);
      } else {
        if(unlink($photos->file_path.$photos->filename)) {
          $photos->delete();
          $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Photo has been deleted.');
        return $this->response->redirect('member/gallery/'.$userSession['id']);
      }
      }

    }
    public function getStatesAction($countryCode = null) {
        if ($this->request->isAjax()) {
            $arrStates = array();
            $states = States::find("country_code_id='" . $countryCode . "'");

            foreach ($states as $state) {
                $arrStates[] = ['id' => $state->id, 'name' => $state->state_name];
            }

            $payload     = $arrStates; //array(1, 2, 3);
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

  public function galleryAction($id = null) {
    $this->view->setTemplateAfter('default1');
    $referer = $this->request->getHTTPReferer();
    $this->view->setVar('referer', $referer);
  
  
    //$userSession = $this->session->get('userSession');
      //$id = $userSession['id'];
      $member = Members::findFirst($id);
      
      if(!$member) {
        $this->response->redirect('member/gallery/'.$id);
      }

    $this->view->setVar('member', $member);

    $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
    $this->view->setVar('photos', $photos);
  }

  public function add_imageAction($id = null) 
  {
    $this->view->setTemplateAfter('default1');
  }

  public function upload_imageAction($id = null) {
        $this->view->setTemplateAfter('default1');
        $referer = $this->request->getHTTPReferer();
        $userSession = $this->session->get('userSession');
        $id = $userSession['id'];
        $member = Members::findFirst($id);
      
        if(!$member) {
          $this->response->redirect('member/gallery/'.$userSession['id']);
        }
        $this->view->setVar('member', $member); 
         $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
         $this->view->setVar('photos', $photos);
        if ($this->request->isPost()) {
           $uploads = $this->request->getUploadedFiles(); 
           $isUploaded = false;
                  #do a loop to handle each file individually
                  foreach($uploads as $upload){
                      #define a “unique” name and a path to where our file must go
                      $fileName = $upload->getname();
                      $fileInfo = new SplFileInfo($fileName);
                      $fileExt = $fileInfo->getExtension();
                      $fileExt = strtolower($fileExt);
                      //$newFileName = 'test'.'.'.$fileExt;
                      $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
                      //$fileExt = $upload->getExtension();
                      $fileImageExt = array('jpeg', 'jpg', 'png');
                      //error_log("File Extension :".$fileExt, 0);
                      $fileType = '';
                      $filePath = '';
                      $path = '';
                      //$path = ''.$newFileName;
                      if(in_array($fileExt, $fileImageExt)){
                          $path = 'img/member/'.$newFileName;
                          $filePath = 'img/member/';
                          //$fileType = 'Image';
                      }
                      #move the file and simultaneously check if everything was ok
                      ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
        
                      if($isUploaded) {
                      $memberPhotos = new MemberPhotos();
                      $memberPhotos->created = date('Y-m-d H:i:s'); 
                      $memberPhotos->modified = date('Y-m-d H:i:s'); 
                      $memberPhotos->member_id =  $userSession['id'];
                      $memberPhotos->file_path =  $filePath;
                      $memberPhotos->filename =  $newFileName;
                      $memberPhotos->caption = $this->request->getPost('caption');
                      $memberPhotos->type = '1';
                      $memberPhotos->primary_pic = 'No';
                
              
                      if($memberPhotos->create()){
                          $this->view->disable();  
                          echo "success";
                          //return $this->response->redirect('member/add_photo/'.$id);
                      }
                  
                      }   
                  } // end foreach 
        }
    }
    public function add_photoAction($id = null, $currentImage = null)
    { 
      $this->view->setTemplateAfter('default1');
    $referer = $this->request->getHTTPReferer();
    $this->view->setVar('referer', $referer);

      $userSession = $this->session->get('userSession');
      $id = $userSession['id'];
      $member = Members::findFirst($id);
      
      if(!$member) {
        $this->response->redirect('member/gallery/'.$userSession['id']);
      }

      if ($currentImage != null) {
  $currentPhoto = MemberPhotos::findFirst('member_id ="' .$id.'" AND id="'.$currentImage.'"');
      } else {
  $currentPhoto = MemberPhotos::findFirst('member_id ="' .$id.'" AND primary_pic="Yes"'); 
      }
      $this->view->setVar('currentPhoto', $currentPhoto);
      $this->view->setVar('member', $member);
      // $photos = MemberPhotos::find(array('columns'    => '*', 
     //                                 'conditions' => 'member_id LIKE :member_id: AND primary_pic LIKE :primary_pic: ',
     //                                 'order' => 'id DESC', 
     //                                 'bind' => array('member_id' => $id, 'primary_pic' => 'yes' )
     //                                    ));
      $photos = MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC'));
      $this->view->setVar('photos', $photos);
      
      if($this->request->isPost()){
        $encoded = $this->request->getPost('image-data');
        $decoded = urldecode($encoded);
        $exp = explode(',', $decoded);
        $base64 = array_pop($exp);
        $data = base64_decode($base64);
        $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.png';
        $filePath = 'img/member/';
      
        $file = $filePath . $newFileName;

                if (file_put_contents($file, file_get_contents($encoded))) {
    $currentPrimaryPic = MemberPhotos::findFirst('primary_pic="Yes" AND member_id="'.$userSession['id'].'"');

    if (unlink($currentPrimaryPic->file_path.$currentPrimaryPic->filename)) {
       $currentPrimaryPic->delete();
    }
                $userSession = $this->session->get("userSession");
                $memberPhotos = new MemberPhotos();
                $memberPhotos->created = date('Y-m-d H:i:s'); 
                $memberPhotos->modified = date('Y-m-d H:i:s'); 
                $memberPhotos->member_id =  $userSession['id'];
                $memberPhotos->file_path =  $filePath;
                $memberPhotos->filename =  $newFileName;
    $memberPhotos->type = "0";
                $memberPhotos->caption = $this->request->getPost('caption');
                //if(count($photos) > 0) {
                  //  $memberPhotos->primary_pic = 'No';
          //} else {
        $memberPhotos->primary_pic = 'Yes';
        //$userSession['primary_pic'] = $filePath.$newFileName;
        //$this->session->set('userSession', $userSession);
          //}
                if($memberPhotos->save()){
                    return $this->response->redirect('member/gallery/'.$id);
                } else {
                    $this->view->disable();
        $msg = "inserting image data failed..";
                    print_r($memberPhotos->getMessages());
                }
                 } else {
      $this->view->disable();
      return;
      echo "failed";
       }
            //ini_set('upload_max_filesize', '64M');
            /*set_time_limit(1200);
            $uploads = $this->request->getUploadedFiles();

            $isUploaded = false;
            #do a loop to handle each file individually
            foreach($uploads as $upload){
                #define a “unique” name and a path to where our file must go
                $fileName = $upload->getname();
                $fileInfo = new SplFileInfo($fileName);
                $fileExt = $fileInfo->getExtension();
                $fileExt = strtolower($fileExt);
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'.'.$fileExt;
                //$fileExt = $upload->getExtension();
                $fileImageExt = array('jpeg', 'jpg', 'png');
                //error_log("File Extension :".$fileExt, 0);
                $fileType = '';
                $filePath = '';
                $path = '';
                //$path = ''.$newFileName;
                if(in_array($fileExt, $fileImageExt)){
                    $path = 'img/member/'.$newFileName;
                    $filePath = 'img/member/';
                    //$fileType = 'Image';
                }
                #move the file and simultaneously check if everything was ok
                ($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
            }

            #if any file couldn't be moved, then throw an message
            if($isUploaded) {
                $memberPhotos = new MemberPhotos();
                $memberPhotos->created = date('Y-m-d H:i:s'); 
                $memberPhotos->modified = date('Y-m-d H:i:s'); 
                $memberPhotos->member_id =  $userSession['id'];
                $memberPhotos->file_path =  $filePath;
                $memberPhotos->filename =  $newFileName;
                $memberPhotos->caption = $this->request->getPost('caption');
                if(count($photos) > 0) {
                  $memberPhotos->primary_pic = 'No';
                } else {
                  $memberPhotos->primary_pic = 'Yes';
                  $userSession['primary_pic'] = $filePath.$newFileName;
          $this->session->set('userSession', $userSession);
                }
                
                if($memberPhotos->create()){
                    return $this->response->redirect('member/add_photo/'.$id);
                }
            }*/
        }

        
    }

    public function updateAction($id = null)
    {
      $this->view->setTemplateAfter('default1');
      $member = Members::findFirstById($id);
      
      $userSession = $this->session->get('userSession');
      if(!$member || $userSession['id'] != $member->id) {
        $this->response->redirect('member/upload_photo/'.$userSession['id']);
      }
      $oldEmail = $member->email;
      $this->view->setVar('member', $member);
      if ($this->request->isPost()) {
        $member = Members::findFirstById($this->request->getPost('id'));
      $member->modified = date('Y-m-d H:i:s');
      $member->first_name = $this->request->getPost('first_name');
      $member->last_name = $this->request->getPost('last_name');
      $member->street = $this->request->getPost('street');
      $member->city = $this->request->getPost('city');
      $member->country_code_id = $this->request->getPost('country_id');
      $member->email = $this->request->getPost('email');
      $member->mobile = $this->request->getPost('mobile');
      //email cookie  
      if(!empty($this->request->getPost('email'))){
        $cookie_name = "e";
        $cookie_value = $this->request->getPost('email');
        setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
      }
      if($member->update()){
        if($oldEmail != $this->request->getPost('email')){
          $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
          $emailConfimation = new EmailConfirmations();
          $emailConfimation->created = date('Y-m-d H:i:s');
          $emailConfimation->modified = date('Y-m-d H:i:s');
          $emailConfimation->user_id = $member->id;
          $emailConfimation->email = $this->request->getPost('email');
          $emailConfimation->token = $activationToken;
          $emailConfimation->confirmed = 'N';
          if($emailConfimation->save()){
            $this->getDI()->getMail()->send(
                          array($this->request->getPost('email') => $member->first_name.' '.$member->last_name),
                          'Please confirm your email',
                          'email_update_confirmation',
                          array( 'confirmUrl' => 'member/emailConfimation/'. $member->id .'/'. $this->request->getPost('email') .'/'. $activationToken)
                      );
          }
        }
        //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You profile has been updated.');
	$this->session->set("updateProfile", "Your profile has been updated successfully!");
	return $this->response->redirect('member/update/'.$this->request->getPost('id'));
        //return $this->response->redirect('member/page/'.$this->request->getPost('id'));
      }
    } 

    
      $countries = Countries::find();
      $this->view->setVar('countries', $countries);
    }
  
  public function change_passwordAction($id = null) 
  { 
    $this->view->setTemplateAfter('default1');
    $member = Members::findFirstById($id);
      
        $userSession = $this->session->get('userSession');
        if(!$member || $userSession['id'] != $member->id) {
          $this->response->redirect('member/upload_photo/'.$userSession['id']);
        }
    if ($this->request->isPost()) {
      $oldPassword = $this->request->getPost('current_password');
      $newPassword = $this->request->getPost('new_password');
      $confirmPassword = $this->request->getPost('confirm_password');
//$member->password = $this->security->hash($password);
      if ($this->security->checkHash($oldPassword, $member->password)) {
        
        if ($newPassword == $confirmPassword) {
          
          if (empty($newPassword) && empty($confirmPassword)) {
            $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>your new password is empty');
            $this->response->redirect('member/change_password/'.$userSession['id']);
          } else {  

            $member->modified = date('Y-m-d H:i:s');
            $member->password = $this->security->hash($newPassword);

              if ($member->update()) {

              $this->getDI()->getMail()->send(
                    array($member->email => $member->first_name.' '.$member->last_name),
                    'Your password has been changed',
                    'change_password',
                  array('old' => $oldPassword, 'new' => $newPassword)
                );
              //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Your password has been updated.');
              return $this->response->redirect('member/page/'.$this->request->getPost('id'));
              } else {
                $this->view->disable();
                echo "whoops, something went wrong :( <br>
                updating password failed!";
              }
            
            
          }
        } else {
          $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>password doesnt match.');
        }
      } else {
        //$this->view->disable();
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Your current password is not correct.');
      }
    }
    $this->view->setVar('member', $member);
  }
    public function signupAction()
    {

          $this->view->setTemplateAfter('oneheader_default');
          //<-- facebook login
          $fb = new Facebook([
          'app_id' => '775156722610275',
          'app_secret' => '7d3a299ec12af9e08ed61e1e20df6dc7',
          'default_graph_version' => 'v2.4',
          ]);

          $helper = $fb->getRedirectLoginHelper();

          $permissions = ['email']; // Optional permissions
          $loginUrl = $helper->getLoginUrl('https://mybarangay.com/member/fbTest', $permissions);

          $this->view->setVar('loginUrl', $loginUrl);
          //echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
          //--? facebook login
      if ($this->request->isPost()) {
      $error = 0;
      // if($this->security->checkToken() == false){
      //  $error = 1;
      //  $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Invalid CSRF Token');
      //  return $this->response->redirect('signup');
      // }
      $firstName = $this->request->getPost('first_name');
      $lastName = $this->request->getPost('last_name');
      $email = $this->request->getPost('email'); 
      $password = $this->request->getPost('password');
      $confirmPassword = $this->request->getPost('confirm_password');
      
      if(empty($firstName) || empty($lastName)  || empty($email)  || empty($password)  || empty($confirmPassword)){ 
        $this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>All fields required');

         $this->session->set('errorMessage', 'All fields required');
      	return $this->response->redirect('member/signup'); 


      }

      if($password != $confirmPassword){ 

         $this->session->set('errorMessage', 'password does not match');
      	return $this->response->redirect('member/signup'); 


      }


      if(!empty($email) && Members::findFirstByEmail($email)){


         $this->session->set('errorMessage', 'This email address is already used by a Member or another Business Owner in MyBarangay. Please use another email address');
      	return $this->response->redirect('member/signup'); 
      }

      $member = new Members();
      $member->created = date('Y-m-d H:i:s');
      $member->modified = date('Y-m-d H:i:s');
      $member->first_name = $firstName;
      $member->last_name = $lastName;
      $member->email = $email;
      $member->type = 'Member';
      $member->password = $this->security->hash($password);
      
      if($member->create()){
        $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
        $emailConfimation = new EmailConfirmations();
        $emailConfimation->created = date('Y-m-d H:i:s');
        $emailConfimation->modified = date('Y-m-d H:i:s');
        $emailConfimation->user_id = $member->id;
        $emailConfimation->email = $email;
        $emailConfimation->token = $activationToken;
        $emailConfimation->confirmed = 'N';
        if($emailConfimation->save()){
          $this->getDI()->getMail()->send(
                        array($email => $firstName.' '.$lastName),
                        'Please confirm your email',
                        'confirmation',
                        array( 'confirmUrl' => 'member/emailConfimation/'. $member->id .'/'. $email .'/'. $activationToken)
                    );
        }
      
        //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You\'ve successfully created a MyBarangay account. We sent a confirmation email to '.$email.'.');
	// $this->session->set("user-name", "Michael");
         $this->session->set('successMessage', $email.'successfully created a MyBarangay account');
        return $this->response->redirect('member/signup_success'); 
      } else {
         $this->session->set('errorMessage', 'Registration failed. Please try again.');
      	return $this->response->redirect('member/signup'); 
      }
    } 

    }
    
    public function fbConnectAction() {
        //<-- facebook login
        $fb = new Facebook([
        'app_id' => '775156722610275',
        'app_secret' => '7d3a299ec12af9e08ed61e1e20df6dc7',
        'default_graph_version' => 'v2.4',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('https://mybarangay.com/member/fbTest', $permissions);

        return $this->response->redirect($loginUrl);
        
        //echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
        //--? facebook login
    }
    /**
     * Login user
     * @return \Phalcon\Http\ResponseInterface
     */
    public function loginAction(){

        $userSession = $this->session->get('userSession');
        echo $userSession['id'];
        if(!empty($userSession['id']) or !$userSession['id'] == '' ){
            return $this->response->redirect('member/page/'. $userLog); 
        }
        $email = '';
        $this->view->setTemplateAfter('oneheader_default');

        //<-- facebook login
        $fb = new Facebook([
        'app_id' => '775156722610275',
        'app_secret' => '7d3a299ec12af9e08ed61e1e20df6dc7',
        'default_graph_version' => 'v2.4',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('https://mybarangay.com/member/fbTest', $permissions);

        $this->view->setVar('loginUrl', $loginUrl);
        //echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
        //--? facebook login

        if ($this->request->isPost()) {

            // if($this->security->checkToken() == false){
            //  $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Invalid CSRF Token');
            //  return $this->response->redirect('login');
            // }

    
            $email = $this->request->getPost('email'); // $_POST

            $password = $this->request->getPost('password');

            if(empty($email) || empty($password)){ 
                $this->session->set('errorMessasge', 'error');
                return $this->response->redirect('member/login'); 
            }

            $member = Members::findFirstByEmail($email);
      
            if ($member == true && $this->security->checkHash($password, $member->password)) {

                $emaiConfirmed = EmailConfirmations::findFirst(array('columns'    => '*', 
                                         'conditions' => 'user_id = ?1 AND email=?2 AND confirmed = ?3', 
                                         'bind' => array(1 => $member->id, 2 => $email, 3 => 'Y')));

                if(!$emaiConfirmed) {
                    $this->session->set('errorMessasge', 'error');
                    return $this->response->redirect('member/login'); 
                }

                $userSession = get_object_vars($member);
                $userSession['type'] = 'Member';
                $profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
                error_log('WWWTTTTFFFF : '.$profilePic->file_path.$profilePic->filename );
                $userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
                $this->session->set('userSession', $userSession);
                //member id
                $cookie_name = "mid";
                $cookie_value = $userSession['id'];
                $date_of_expiry = time() + 60 * 60 * 24 * 90;
                setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
                //email 
                $cookie_name = "e";
                $cookie_value = $userSession['email'];
                setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
                //cookie token
                $cookie_name = "token";
                $cookie_token = substr(md5(uniqid(rand(), true)), 0, 20);
                setcookie($cookie_name, $this->encrypt($cookie_token), $date_of_expiry, "/"); 
                $member->modified = date('Y-m-d H:i:s');
                $member->cookie_token = $this->security->hash($cookie_token);
                if($member->update()){
                      $this->response->redirect('member/page/'.$userSession['id']);
                }

            } else {
                $this->session->set('errorMessasge', 'error');
                //return $this->response->redirect('member/login'); 
            }

        }
        $this->view->email = $email;

    
    }
    

    function encrypt($pure_string) {
    $encryption_key = 'sMeynBiaprpainlgiyhP';
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return $encrypted_string;
    }

    /**
     * Login with Facebook account
     */
    public function loginWithFacebookAction()
    {
      $this->view->disable();
      $helper = $this->fbRedirectLoginHelper;
      $scope = array('public_profile', 'email', 'user_friends');
      return $this->response->redirect($helper->getLoginUrl(), true);
    }
    
    public function fbAuthAction()
    {
      $this->view->disable();
      $params = array(
        'client_id' => FacebookSession::_getTargetAppId($this->config->facebook->appId),
        'redirect_uri' => $this->config->facebook->redirect,
        'client_secret' =>
          FacebookSession::_getTargetAppSecret($this->config->facebook->secret),
        'code' => isset($_GET['code']) ? $_GET['code'] : null
        );
      
    $response = (new FacebookRequest(
      FacebookSession::newAppSession($this->config->facebook->appId, $this->config->facebook->secret),
      'GET',
      '/oauth/access_token',
      $params
    ))->execute()->getResponse();
    if (isset($response['access_token'])) {
      $session = new FacebookSession($response['access_token']);
    }

    if( isset($session) ){
      $userSession['access_token'] = $response['access_token'];
      $request = new FacebookRequest( $session, 'GET', '/me' );
      $response = $request->execute();
      // get response
      $graphObject = $response->getGraphObject();
      $email = $graphObject->getProperty('email');
      
      $fbId = $graphObject->getProperty('id');
      $verified = $graphObject->getProperty('verified');
      $firstName = $graphObject->getProperty('first_name');
      $lastName = $graphObject->getProperty('last_name');
      $fullName = $graphObject->getProperty('name');
      $gender = $graphObject->getProperty('gender');
      $profileLink = $graphObject->getProperty('link');
      $bday = $graphObject->getProperty('birthday');
      $email = $graphObject->getProperty('email');
      //$city = $graphObject->getProperty('location')->getProperty('name');
      $city = 'unknown';
      if(!isset($email)) {
        $email = $fbId.'@facebook.com';
      }
      if(isset($bday)) {
        $bday = date('Y-m-d', strtotime($bday));
      }
      $socialData = array(
                'social_network'  =>  'Facebook',
                'social_id'     =>  $fbId,
                'first_name'    =>  $firstName,
                'last_name'     =>  $lastName,
                'full_name'     =>  $fullName,
                'gender'      =>  $gender,
                'profile_link'    =>  $profileLink,
                'birthday'      =>  $bday,
                'email'       =>  $email,
                'city'        =>  $city,
                'access_token'    =>  $userSession['access_token']
              );
      $this->view->disable();
      var_dump($socialData);
      // if( $member = Members::findFirstByEmail($email) ) {
      //   $userSession = get_object_vars($member);
      //   if(!$socialProfile = SocialProfiles::findFirstByEmail($email) ) {
      //     $socialData['member_id'] = $member->id;
      //     $this->newSocialProfile($socialData); 
      //     $userSession['Member'];
      //   }
      // } else {
        
      //   $memberId = $this->newMember($socialData);
      //   $socialData['id'] = $memberId;
      //   $userSession = $socialData;
      //   $socialData['member_id'] = $memberId;
      //   $this->newSocialProfile($socialData);
      //   $userSession['Member'];

      // }
      
      //print_r($graphObject);

      //$this->session->set('userSession', $userSession);
    }
    
    //return $this->response->redirect();
      
    }


    /*
     *
     * Test with google account
     */

    public function gplusConnectAction() {
      $clientId = '355779783719-72knrbjabrdt96ivso5t25vmljmniv1j.apps.googleusercontent.com'; //Google CLIENT ID
      $clientSecret = 'AMifOn7tHlJRl6hYRCX4q7TX '; //Google CLIENT SECRET
      $redirectUrl = 'https://mybarangay.com/member/gplusConnect';  //return url (url to script)
      $homeUrl = 'https://mybarangay.com';  //return to home
      $devKey = 'AIzaSyAHbk7KQzyPhE0iVlNO161ikk9GJFByogU';

      $client = new Google_Client();
      $client->setApplicationName("MyBarangay");
      $client->setDeveloperKey($devKey);
      $client->setClientId($clientId);
      $client->setClientSecret($clientSecret);
      $client->setRedirectUri($redirectUrl);
      $scope[] = 'https://www.googleapis.com/auth/plus.me';
      $scope[] = 'https://www.googleapis.com/auth/userinfo.email';
      $scope[] = 'https://www.googleapis.com/auth/userinfo.profile';
      $client->setScopes($scope); 
    }

    /**
     * Login with Google account
     */

    public function loginWithGoogleAction()
    {
      if ($this->request->isPost()) {
        $this->view->disable();
        $code = $this->request->getPost('code');
        $token = $this->request->getPost('access_token');
        $client = $this->googleClient;  
        $client->authenticate($code);
        $gPlusService = new Google_Service_Plus($client);
        $me = $gPlusService->people->get("me");
        //print_r($me);
      // Exchange the OAuth 2.0 authorization code for user credentials.
      // Verify the token
      $reqUrl = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' . $token;
      $req = new Google_Http_Request($reqUrl);

      if($req) {
        $userSession['access_token'] = $token;
        $tokenInfo = $client->getIo()->executeRequest($req);
        $obj = json_decode($tokenInfo[0], false);

        // Make sure the token we got is for the intended user.
        if ($obj->user_id != $me['id']) {
          print_r("Token's user ID doesn't match given user ID");
        }
        // Make sure the token we got is for our app.
        if ($obj->audience != $this->config->google->appId) {
          print_r("Token's client ID does not match app's. 401");
        }
        //print_r($me['modelData']);
        $me['email'] = $me['modelData']['emails'][0]['value'];

        if(isset($me['birthday'])) {
          $me['birthday'] = date('Y-m-d', strtotime($me['birthday']));
        }
        $socialData = array(
                  'social_network'  =>  'Google',
                  'social_id'     =>  $me['id'],
                  'first_name'    =>  $me['modelData']['name']['givenName'],
                  'last_name'     =>  $me['modelData']['name']['familyName'],
                  'full_name'     =>  $me['displayName'],
                  'gender'      =>  $me['gender'],
                  'profile_link'    =>  $me['url'],
                  'birthday'      =>  $me['birthday'],
                  'email'       =>  $me['email'],
                  'city'        =>  $me['currentLocation'],
                  'access_token'    =>  $token
                );

        if( $member = Members::findFirstByEmail($me['email']) ) {
          $userSession = get_object_vars($member);
          if(!$socialProfile = SocialProfiles::findFirstByEmail($me['email']) ) {
            $socialData['member_id'] = $member->id;
            $this->newSocialProfile($socialData); 
          }
          $this->session->set('userSession', $userSession);
        } else {

          $memberId = $this->newMember($socialData);
          $socialData['id'] = $memberId;
          $userSession = $socialData;
          $socialData['member_id'] = $memberId;
          $this->newSocialProfile($socialData);
          $this->session->set('userSession', $userSession);
        }
        $result = array('status' => 'OK');  
        //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged in.');
        
      } else {
        $result = array('status' => 'FAILED');  
      }
      echo json_encode($result);  
      
      //$this->response->redirect();
    }
    }
    
    public function newSocialProfile($socialData = null){
      $this->view->disable();
      $socialProfile = new SocialProfiles();
      $socialData['created'] = date('Y-m-d H:i:s');
      $socialData['modified'] = date('Y-m-d H:i:s');    
      if( $socialProfile->save($socialData) ) { 

      } else {
        return $this->response->redirect();
      }
    }
    
    public function newMember($socialData = null){
      $this->view->disable();
      $member = new Members();
      $member->created = date('Y-m-d H:i:s');
      $member->modified = date('Y-m-d H:i:s');
      $member->first_name = $socialData['first_name'];
      $member->last_name  = $socialData['last_name'];
      $member->gender = $socialData['gender'];
      $member->birthday = $socialData['birthday'];
      $member->city = $socialData['city'];
      $member->email = $socialData['email'];
      $member->type = 'Member';
      if($member->save()){
        return $member->id;
      }     
    }

    
     
    public function logoutAction() {
      $this->view->disable();
      $this->session->destroy();
      $date_of_expiry = time() - 60 ;
      setcookie( "mid", "anonymous", $date_of_expiry, "/");
      setcookie( "e", "anonymous", $date_of_expiry, "/");
      setcookie( "token", "anonymous", $date_of_expiry, "/");
      $this->view->setVar('userSession', '');
      $this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged out.');
      
      return $this->response->redirect();
    }

    public function emailConfimationAction($userId = null, $email =null, $activationToken = null){
      $emaiConfirmed = EmailConfirmations::findFirst(array('columns'    => '*', 
                                 'conditions' => 'user_id = ?1 AND email=?2 AND token = ?3 AND confirmed = ?4', 
                                 'bind' => array(1 => $userId, 2 => $email, 3 => $activationToken, 4 => 'N')));
	      if($emaiConfirmed) {
	        $emaiConfirmed->confirmed = 'Y';
	        $emaiConfirmed->update();
	         //$this->getDI()->getMail()->send(
               //         array($member->email => $member->first_name.' '.$member->last_name),
                 //       "MyBarangay - You're now Kabarangay",
                   //     'youarenowkbrgy',
                     //   array( 'loginurl' => 'member/login/')
                    //);
	       	//$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button><H4>You \'re email has been confirmed.</H4>You\'re now officially part of the <strong>MyBarangay</strong> community. Mabuhay!');
	        $this->session->set('confirmedEmail', "<H4>You are now Kabarangay</H4>You\'re now officially part of the <strong>MyBarangay</strong> community. Mabuhay!");
	      	return $this->response->redirect('member/login'); 

	    } else {

	        $this->session->set('errorMessasge', 'Email confirmation error');
	   		return $this->response->redirect('member/login');
	    }
    }

    public function forgotPasswordAction(){
        $this->view->setTemplateAfter('default1');
    if ($this->request->isPost()) {
      if($member = Members::findFirstByEmail($this->request->getPost('email'))) {
        $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
        $resetPassword = ResetPasswords::findFirstByEmail($this->request->getPost('email'));
        if(!$resetPassword){
          $resetPassword = new ResetPasswords();
          $resetPassword->created = date('Y-m-d H:i:s');
          $resetPassword->user_id = $member->id;
          $resetPassword->email = $member->email;
        }
        $resetPassword->modified = date('Y-m-d H:i:s');
        $resetPassword->token = $activationToken;
        $resetPassword->used = 'N';
        if($resetPassword->save()){ 
          $this->getDI()->getMail()->send(
                        array($member->email => $member->first_name.' '.$member->last_name),
                        'MyBarangay password assistance',
                        'reset_password',
                        array( 'resetUrl' => 'member/resetPassword/'. $member->id .'/'. $member->email .'/'. $activationToken)
                    );
          //$this->flash->notice('<button type="button" class="close" data-dismiss="alert">×</button>We send a special reset password link to your inbox.');
          $this->session->set("forgotPassword", "We sent a password reset link to your email ");
            return $this->response->redirect();
                }
        
      } else {
        $this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Email is not registered. Please try again.');
                return $this->response->redirect();
      }
    }
    //return $this->response->redirect();
  }
  
  
  public function resetPasswordAction($userId = null, $email = null, $activationToken = null, $resetPasswordId = null){
    $this->view->setTemplateAfter('default1');
    if ($this->request->isPost()) {
      $error = 0;
      if(empty($this->request->getPost('new_password')) || empty($this->request->getPost('confirm_password'))){
        $this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>All fields required. Please try again.');
        $error = 1;
      }

      if($this->request->getPost('new_password') != $this->request->getPost('confirm_password')){
        $this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>Password fields does not match. Please try again.');
        $error = 1;
      }

      if($error == 0){
        if(isset($resetPasswordId)) {
          
          $member = Members::findFirstById($userId);
          $member->modified = date('Y-m-d H:i:s');
          $member->password = $this->security->hash($this->request->getPost('new_password'));
          if($member->update()){  
            $resetPassword = ResetPasswords::findFirstById($resetPasswordId);
            $resetPassword->modified = date('Y-m-d H:i:s');
            $resetPassword->used = 'Y';
            if($resetPassword->update()){
              //$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Password has been updated. You can now login');
              $this->session->set("resetPassword", "Your password has been updated, you can now login");
              return $this->response->redirect();
            }
          }
        }
      }
    } else {
      $tomorrow = new DateTime('tomorrow');
      $resetPassword = ResetPasswords::findFirst(array('columns'    => '*', 
                                 'conditions' => 'user_id = ?1 AND email=?2 AND token = ?3 AND used = ?4 AND modified BETWEEN ?5 AND ?6', 
                                 'bind' => array(1 => $userId, 2 => $email, 3 => $activationToken, 4 => 'N', 5 => date('Y-m-d 00:00:00'), 6 => $tomorrow->format('Y-m-d 00:00:00'))));
      
      if($resetPassword){
        $this->view->setVar('referer', 'member/resetPassword/'.$userId.'/'.$email.'/'.$activationToken.'/'.$resetPassword->id);
        //$this->view->setVar('resetPassword', $resetPassword);
      } else {
        //$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Reset password link you use is either incorrect or no longer valid. Please, try again.');
        $this->session->set("resetPassword", "Reset password link you use is either incorrent or no longer valid, Please, try again ");
        return $this->response->redirect();
      }

    }

  }

    /**
     * [user_classifiedAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function user_classifiedAction($id = null){

        if($id == null or $id == ''){
            return $this->response->redirect('error401');
        }

        $this->view->seschecker = $this->sessionChecker($id);

        $this->view->setVars([
            'autos'     =>  Automotives::find("member_id = '$id' "),
            'jobs'      =>  Jobs::find("member_id = '$id' "),
            'realty'    =>  Realties::find("member_id = '$id' "),
            'things'    =>  Things::find("member_id = '$id' "),
            'photos'    =>  MemberPhotos::find(array('member_id = "'.$id.'"', 'order' => 'id DESC')),
            'ids'       =>  $id
        ]);

    }


    /**
     * 
     */
    public function galleryuDelAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $id         = $this->request->getQuery('ids');
            $message    = '';
            $name       = '';
            $response   = 'ok';
            if(!empty($id)){
                $photdel = MemberPhotos::findFirst($id);
                if($photdel){
                     $path      =   $photdel->file_path;
                     $filename  =   $photdel->filename;
               
                    if(!empty($path) OR !empty($filename)) { // optional
                        $pathtodel = $path.$filename;
                        if($photdel->delete()){
                            if (file_exists($pathtodel)) {
                                unlink($pathtodel);
                            }
                            $message .= 'Image successfully deleted!';
                        }else{
                            $message .= 'Failed deleting Image';
                            $response = 'failed';
                        }
                    }
                }
            }
            $output = json_encode(array('type' => $response, 'message' => $message));
            die($output); //exit script outputting json data

        }
    }




    /**
     * [classfiedDelAction description]
     * @return [type] [description]
     */
    public function classfiedDelAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $id         = $this->request->getQuery('ids');
            $codenae    = $this->request->getQuery('codenae');
            $message = '';
            $name = '';
            $response = 'ok';
            if(!empty($id)){
               switch ($codenae) {
                    case 'auto':
                            $query = Automotives::findFirstById($id);
                            if($query){
                                $automotiveid = $query->id;
                                $name = $query->name;
                                if($query->delete()){
                                    $queryPhotoDel = AutomotivePhotos::find(" automotive_id = '$automotiveid' ")->filter(function($photdel){
                                        if(!empty($photdel->file_path) OR !empty($photdel->filename)) { // optional
                                            $pathtodel = $photdel->file_path.$photdel->filename;
                                            if($photdel->delete()){
                                                if (file_exists($pathtodel)) {
                                                    unlink($pathtodel);
                                                }
                                            }
                                        }
                                    });
                                    $message .= 'Automotive name : <strong>' .$name .'</strong> successfully deleted!';
                                }else{
                                    $message .= 'Failed deleting : <strong>' .$name .'</strong> - please try again';
                                    $response = 'failed';
                                }
                            }
                        break;

                    case 'job':
                            $query = Jobs::findFirstById($id);
                            if($query){
                                $jobid = $query->id;
                                $name = $query->position;
                                if($query->delete()){
                                    $queryPhotoDel = JobLogos::find(" job_id = '$jobid' ")->filter(function($photdel){
                                        if(!empty($photdel->file_path) OR !empty($photdel->filename)) { // optional
                                            $pathtodel = $photdel->file_path.$photdel->filename;
                                            if($photdel->delete()){
                                                if (file_exists($pathtodel)) {
                                                    unlink($pathtodel);
                                                }
                                            }
                                        }
                                    });
                                    $message .= 'Job Position : <strong>' .$name .'</strong> successfully deleted!';
                                }else{
                                    $message .= 'Failed deleting : <strong>' .$name .'</strong> - please try again';
                                    $response = 'failed';
                                }
                            }
                        break;

                    case 'realty':
                            $query = Realties::findFirstById($id);
                            if($query){
                                $realtyid = $query->id;
                                $name = $query->name;
                                if($query->delete()){
                                    $queryPhotoDel = RealtyPhotos::find(" realty_id = '$realtyid' ")->filter(function($photdel){
                                        if(!empty($photdel->file_path) OR !empty($photdel->filename)) { // optional
                                            $pathtodel = $photdel->file_path.$photdel->filename;
                                            if($photdel->delete()){
                                                if (file_exists($pathtodel)) {
                                                    unlink($pathtodel);
                                                }
                                            }
                                        }
                                    });
                                    $message .= 'Realty name : <strong>' .$name .'</strong> successfully deleted!';
                                }else{
                                    $message .= 'Failed deleting : <strong>' .$name .'</strong> - please try again';
                                    $response = 'failed';
                                }
                            }
                        break;

                    case 'thing':
                            $query = Things::findFirstById($id);
                            if($query){
                                $thingid = $query->id;
                                $name = $query->name;
                                if($query->delete()){

                                    $queryPhotoDel = ThingPhotos::find(" thing_id = '$thingid' ")->filter(function($photdel){
                                        if(!empty($photdel->file_path) OR !empty($photdel->filename)) { // optional
                                            $pathtodel = $photdel->file_path.$photdel->filename;
                                            if($photdel->delete()){
                                                if (file_exists($pathtodel)) {
                                                    unlink($pathtodel);
                                                }
                                            }
                                        }
                                    });
                                    $message .= 'Item name : <strong>' .$name .'</strong> successfully deleted!';
                                }else{
                                    $message .= 'Failed deleting : <strong>' .$name .'</strong> - please try again';
                                    $response = 'failed';
                                }
                            }
                        break;
                    default:
                        $message .= 'Failed! please try again...';
                        $response = 'failed';
                        break;
                }
            }else{

            }
 
            $output = json_encode(array('type' => $response, 'message' => $message));
            die($output); //exit script outputting json data

        }
    }


    /**
     * [classfiedArcAction description]
     * @return [type] [description]
     */
    public function classfiedArcAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {


            $id         = $this->request->getQuery('ids');
            $codenae    = $this->request->getQuery('codenae');
            $message = '';
            $name = '';
            $response = 'ok';
            $failed = 'Failed removing to archived section, please try again';
            switch ($codenae) {
                case 'auto':
                    $query = Automotives::findFirstById($id);
                    if($query){
                        $name = $query->name;
                        $query->status = 5;
                         if($query->update()){
                            $message .= 'Automotive - name: <strong>' .$name .' successfully removed!';
                         }else{
                            $message .= $failed;
                            $response = 'failed';
                         }
                    }
                break;
                case 'job':
                    $query = Jobs::findFirstById($id);
                    if($query){
                        $name = $query->position;
                        $query->status = 5;
                         if($query->update()){
                            $message .= 'Job - Position: <strong>' .$name .' successfully removed!';
                         }else{
                            $message .= $failed;
                            $response = 'failed';
                         }
                    }
                break;
                case 'realty':
                    $query = Realties::findFirstById($id);
                    if($query){
                        $name = $query->name;
                        $query->status = 5;
                         if($query->update()){
                            $message .= 'Real Estate - name: <strong>' .$name .' successfully removed!';
                         }else{
                            $message .= $failed;
                            $response = 'failed';
                         }
                    }
                break;
                case 'thing':
                    $query = Things::findFirstById($id);
                    if($query){
                        $name = $query->name;
                        $query->status = 5;
                         if($query->update()){
                            $message .= 'Item - name: <strong>' .$name .' successfully removed!';
                         }else{
                            $message .= $failed;
                            $response = 'failed';
                         }
                    }
                break;
                default:
                        $message .= 'Failed! please try again...';
                        $response = 'failed';
                    break;
            }

            $output = json_encode(array('type' => $response, 'message' => $message));
            die($output); //exit script outputting json data

        }

    }



    /**
     * [updateuAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function updateuAction($id = null){
        $this->view->seschecker = $this->sessionChecker($id);
        $this->view->setTemplateAfter('default1');
        $userSession = $this->session->get('userSession');
        if($id == null or $id == ''){
            if($userSession){
                $id = $userSession['id'];
            }else{
                return $this->response->redirect('member/login/');
            }
        }
        $member = Members::findFirstById($id);
        if(!$member || $userSession['id'] != $member->id) {
            $this->response->redirect('member/updateu/'.$userSession['id']);
        }

        $oldEmail = $member->email;
        $this->view->setVar('member', $member);

        if ($this->request->isPost()) {
            $member = Members::findFirstById($this->request->getPost('id'));
            $member->modified = date('Y-m-d H:i:s');
            $member->first_name = $this->request->getPost('first_name');
            $member->last_name = $this->request->getPost('last_name');
            $member->street = $this->request->getPost('street');
            $member->city = $this->request->getPost('city');
            $member->country_code_id = $this->request->getPost('country_id');
            $member->email = $this->request->getPost('email');
            $member->mobile = $this->request->getPost('mobile');
            $member->state = $this->request->getPost('state');
            //email cookie  
            if(!empty($this->request->getPost('email'))){
                $cookie_name = "e";
                $cookie_value = $this->request->getPost('email');
                $date_of_expiry = time() + 60 * 60 * 24 * 90;
                setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
            }
            if($member->update()){
                if($oldEmail != $this->request->getPost('email')){
                    $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
                    $emailConfimation = new EmailConfirmations();
                    $emailConfimation->created = date('Y-m-d H:i:s');
                    $emailConfimation->modified = date('Y-m-d H:i:s');
                    $emailConfimation->user_id = $member->id;
                    $emailConfimation->email = $this->request->getPost('email');
                    $emailConfimation->token = $activationToken;
                    $emailConfimation->confirmed = 'N';

                    if($emailConfimation->save()){
                        $this->getDI()->getMail()->send(
                        array($this->request->getPost('email') => $member->first_name.' '.$member->last_name),
                        'Please confirm your email',
                        'email_update_confirmation',
                        array( 'confirmUrl' => 'member/emailConfimation/'. $member->id .'/'. $this->request->getPost('email') .'/'. $activationToken)
                        );
                    }
                }
               
                return $this->response->redirect('member/updateu/'.$this->request->getPost('id'));
            }else{
                $mess = '';
                foreach ($member->getMessages() as $err) {
                   $mess .= $err->getMessage().'<br>'; 
                }

                echo $mess;
                $this->view->disable();
            }
        } 

      $countries = Countries::find();
      $this->view->setVar('countries', $countries);
      $this->view->setVar('ids', $id);
  }


    /**
     * [update_photoAction description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
   public function update_photoAction($id = null) {

        $this->view->setTemplateAfter('default1');

        if($id == null or $id == ''){
            $userSession = $this->session->get('userSession');
            if($userSession){
                $id = $userSession['id'];
            }else{
                return $this->response->redirect('member/login/');
            }
        }
        $this->view->seschecker = $this->sessionChecker($id);
        $profilepic = MemberPhotos::findFirst([
            'conditions' => 'member_id = :id: AND primary_pic = :type:',
            'bind' => array('id' => $id, 'type' => 'Yes'),
            'order' => 'id desc' 
        ]);
        $this->view->setVar('profilepic', $profilepic);
        $this->view->setVar('ids', $id);

        if($this->request->isAjax()) {
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

            $userSession = $this->session->get('userSession');
            $userid = $userSession['id'];
            $img = '';
            $profilepic = MemberPhotos::findFirst([
                'conditions' => 'member_id = :id: AND primary_pic = :type:',
                'bind' => array('id' => $userid, 'type' => 'Yes'),
                'order' => 'id desc' 
            ]);

            if ($profilepic) {
                $profilePic = $this->url->getBaseUri() . $profilepic->file_path . $profilepic->filename;
                $img .= '<img src="'.$profilePic.'">';
            }else{
                $img .= Phalcon\Tag::image(array('img/default_large.png', 'alt' => 'Profile')); 
            }    
                        
            $output = json_encode(array('type'=>'ok', 'html' => $img));
            die($output); //exit script outputting json data

        }

   }

    public function memWorkExpAction(){

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $myError = array();
            $educ = new MemberEducation();
            $educ->member_id    =  $this->request->getQuery();
            $educ->education_id =  $this->request->getQuery();
            $educ->institute    =  $this->request->getQuery();
            $educ->major        =  $this->request->getQuery();
            $educ->year         =  $this->request->getQuery();
            $educ->score        =  $this->request->getQuery();
            $educ->start_date   =  $this->request->getQuery();
            $educ->end_date     =  $this->request->getQuery();
            if($educ->create()){

            }else{
                $err = '';
                foreach ($educ->getMessages() as $errno) {
                    $err .= $errno->getMessage(). '</br>';
               }       

               $myError[] = $err;      
            }

            $output = json_encode(array('type'=>'ok', 'text' => ''));
            die($output); //exit script outputting json data
        }
    }




    /**
     * [memEducAction description]
     * @return [type] [description]
     */
    public function memEducAction(){

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {

            $myError = array();
            $workexp = new MemberWorkexperience();
            $workexp->member_id         =  $this->request->getQuery();
            $workexp->start_date        =  $this->request->getQuery();
            $workexp->end_date          =  $this->request->getQuery();
            $workexp->company           =  $this->request->getQuery();
            $workexp->job_description   =  $this->request->getQuery();
            $workexp->contact_no        =  $this->request->getQuery();
            $workexp->status            =  $this->request->getQuery();
            if($workexp->create()){

            }else{
                $err = '';
                foreach ($workexp->getMessages() as $errno) {
                    $err .= $errno->getMessage(). '</br>';
               }       
               $myError[] = $err;      
            }

            $output = json_encode(array('type'=>'ok', 'text' => ''));
            die($output); //exit script outputting json data
        }
    }



    /**
     * [memSkillAction description]
     * @return [type] [description]
     */
    public function memSkillAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {
            $output = json_encode(array('type'=>'ok', 'text' => ''));
            die($output); //exit script outputting json data
        }
    }

    /**
     * [memLanguageAction description]
     * @return [type] [description]
     */
    public function memLanguageAction(){
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if($this->request->isAjax()) {
            $output = json_encode(array('type'=>'ok', 'text' => ''));
            die($output); //exit script outputting json data
        }
    }



    public function downloadImageAction(){

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
      
        if($this->request->isAjax()) {

            $userSession = $this->session->get('userSession');
            $userid = $userSession['id'];

            // if (!file_exists('img/userFiles/'.$userid.'/gallery')) {
            //     mkdir('img/userFiles/'.$userid.'/gallery', 0775, true);
            // }

                $encoded = $this->request->getPost('imagelink');
                $newFileName = substr(md5(uniqid(rand(), true)), 0, 10).date('ymdhis').'YES.png';
                // $filePath = 'img/userFiles/'.$userid.'/gallery/';
                $filePath = 'img/userFiles/memberphoto/primary/';
                $ups =  MemberPhotos::findFirst([
                        'conditions' => 'member_id = :id: AND primary_pic = :type:',
                        'bind' => array('id' => $userid, 'type' => 'Yes'),
                        'order' => 'id desc'
                ]);

                if($ups){
                    $mpic = MemberPhotos::findFirst($ups->id);
                    $filedel = $mpic->filename;
                }else{
                    $mpic = new MemberPhotos();
                    $filedel = '';
                    $mpic->created      = date('Y-m-d H:i:s');
                }

                
                $mpic->modified     = date('Y-m-d H:i:s');
                $mpic->file_path    = $filePath;
                $mpic->filename     = $newFileName;
                $mpic->member_id    = $userid;
                $mpic->primary_pic  = 'Yes';
                $mpic->caption      = 'profile pic';
                if ($mpic->save()) {
                    $file = $filePath . $newFileName;
                    if (file_put_contents($file, file_get_contents($encoded))) {
                        $message = 'success';
                        if($filedel != ''){
                            $path = $filePath.$filedel;
                             if (file_exists($path)) {
                                unlink($path);
                                $message = 'deleted';
                              }
                        }

                    }else{
                        $message = 'Error Mr. MERCADO !!!!';
                    }
                }else{
                    $errs = 'erro';
                    foreach ($mpic->getMessages() as $err) {
                        $errs .= $err->getMessage() .'</br>';
                    }
                    $message = $errs;
                }
            $output = json_encode(array('type'=>'ok', 'text' => $message));
            die($output); //exit script outputting json data
        }
    }

    public function fbTest2Action() {
      $this->view->disable();
      $fb = new Facebook([
      'app_id' => '775156722610275',
      'app_secret' => '7d3a299ec12af9e08ed61e1e20df6dc7',
      'default_graph_version' => 'v2.4',
      ]);

      $helper = $fb->getRedirectLoginHelper();

      $permissions = ['email']; // Optional permissions
      $loginUrl = $helper->getLoginUrl('https://mybarangay.com/member/fbTest', $permissions);


      echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
    }

    public function fbTestAction() {
      $fb = new Facebook([  
        'app_id' => '775156722610275',
        'app_secret' => '7d3a299ec12af9e08ed61e1e20df6dc7',
        'default_graph_version' => 'v2.4', 
        ]);  
        
      $helper = $fb->getRedirectLoginHelper();  
        
      try {  
        $accessToken = $helper->getAccessToken();  
      } catch(Facebook\Exceptions\FacebookResponseException $e) {  
        // When Graph returns an error  
        $this->view->disable();
        $ErrMsg =  'Graph returned an error: ' . $e->getMessage();  
        $this->session->set('fbSdkErr', $ErrMsg);
        //exit;
        return $this->response->redirect();  
      } catch(Facebook\Exceptions\FacebookSDKException $e) {  
        // When validation fails or other local issues 
        $this->view->disable(); 
        $ErrMsg = 'Facebook SDK returned an error: ' . $e->getMessage();  
        $this->session->set('fbSdkErr', $ErrMsg);
        return $this->response->redirect();
        //exit;  
      }  

      if (! isset($accessToken)) {  
        if ($helper->getError()) {  
          header('HTTP/1.0 401 Unauthorized');  
          echo "Error: " . $helper->getError() . "\n";
          echo "Error Code: " . $helper->getErrorCode() . "\n";
          echo "Error Reason: " . $helper->getErrorReason() . "\n";
          echo "Error Description: " . $helper->getErrorDescription() . "\n";
        } else {  
          header('HTTP/1.0 400 Bad Request');  
          echo 'Bad request';  
        }  
        exit;  
      }  

      // Logged in  
        //echo '<h3>Access Token</h3>';  
        //var_dump($accessToken->getValue());  
        
      // The OAuth 2.0 client handler helps us manage access tokens  
      $oAuth2Client = $fb->getOAuth2Client();  

      // Get the access token metadata from /debug_token  
      // $tokenMetadata = $oAuth2Client->debugToken($accessToken);  
      // echo '<h3>Metadata</h3>';  
      // var_dump($tokenMetadata);  
        
      // Validation (these will throw FacebookSDKException's when they fail)  
       // $tokenMetadata->validateAppId($config['app_id']);  
      // If you know the user ID this access token belongs to, you can validate it here  
      // $tokenMetadata->validateUserId('123');  
       // $tokenMetadata->validateExpiration();   
         
      if (! $accessToken->isLongLived()) {  
        // Exchanges a short-lived access token for a long-lived one  
        try {  
          $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);  
        } catch (Facebook\Exceptions\FacebookSDKException $e) {  
          echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>";  
          exit;  
        } 
        echo '<h3>Long-lived</h3>';  
        var_dump($accessToken->getValue());  
      }

      try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get('/me?fields=id,name,email', $accessToken);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }
      //$_SESSION['fb_access_token'] = (string) $accessToken;  
        //echo (string) $accessToken;
      $user = $response->getGraphUser();
      $this->view->disable();
      $str = explode(" ", $user['name']);
      $email = $user['email'];
      $first_name = $str[0];
      $last_name = $str[1];

      $checkEmailAvailability = Members::count("email='". $email."'");
      if ($checkEmailAvailability > 0) {
        $getUser = Members::findFirst("email='".$email."'");
        if ($getUser->type == 'Business') {
          $this->view->disable();
          $this->session->set('fbSdkErr', "Sorry " . $first_name . ", It seems that your Facebook's email address is already used as a Business Owner Account.");
          return $this->response->redirect('https://mybarangay.com/');
        } else if ($getUser->type == 'Member') {
          $this->view->disable();
          $member = Members::findFirst("email='".$email."'");
            $userSession = get_object_vars($member);
            $userSession['type'] = 'Member';
            $profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
            error_log('WWWTTTTFFFF : '.$profilePic->file_path.$profilePic->filename );
            $userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
            $this->session->set('userSession', $userSession);
            //member id
            $cookie_name = "mid";
            $cookie_value = $userSession['id'];
            $date_of_expiry = time() + 60 * 60 * 24 * 90;
            setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
            //email 
            $cookie_name = "e";
            $cookie_value = $userSession['email'];
            setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
            //cookie token
            $cookie_name = "token";
            $cookie_token = substr(md5(uniqid(rand(), true)), 0, 20);
            setcookie($cookie_name, $this->encrypt($cookie_token), $date_of_expiry, "/"); 
            $member->modified = date('Y-m-d H:i:s');
            $member->cookie_token = $this->security->hash($cookie_token);
            if($member->update()){
                  $this->response->redirect('member/page/'.$userSession['id']);
            } else {
                  $this->view->disable();
                  echo "oops, something went wrong";
            }
        }
      } else {
        $member = new Members();
        $member->created = date('Y-m-d H:i:s');
        $member->modified = date('Y-m-d H:i:s');
        $member->first_name = $first_name;
        $member->last_name = $last_name;
        $member->email = $email;
        $member->type = 'Member';
        $member->password = $this->security->hash($password);

        if ($member->save()) {
          $activationToken = substr(str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ), 0, 50);
          $emailConfimation = new EmailConfirmations();
          $emailConfimation->created = date('Y-m-d H:i:s');
          $emailConfimation->modified = date('Y-m-d H:i:s');
          $emailConfimation->user_id = $member->id;
          $emailConfimation->email = $email;
          $emailConfimation->token = $activationToken;
          $emailConfimation->confirmed = 'Y';

          if ($emailConfimation->save()) {
            $userSession = get_object_vars($member);
            $userSession['type'] = 'Member';
            $profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
            error_log('WWWTTTTFFFF : '.$profilePic->file_path.$profilePic->filename );
            $userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
            $this->session->set('userSession', $userSession);
            //member id
            $cookie_name = "mid";
            $cookie_value = $userSession['id'];
            $date_of_expiry = time() + 60 * 60 * 24 * 90;
            setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
            //email 
            $cookie_name = "e";
            $cookie_value = $userSession['email'];
            setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
            //cookie token
            $cookie_name = "token";
            $cookie_token = substr(md5(uniqid(rand(), true)), 0, 20);
            setcookie($cookie_name, $this->encrypt($cookie_token), $date_of_expiry, "/"); 
            $member->modified = date('Y-m-d H:i:s');
            $member->cookie_token = $this->security->hash($cookie_token);
            if($member->update()){
                  $this->response->redirect('member/page/'.$userSession['id']);
            }
          } else {
            $this->view->disable();
            $this->session->set('fbSdkErr', 'user account confirmation failed');
            $member->delete();
            return $this->response->redirect();
          }
        } else {
          $this->view->disable();
          $this->session->set('fbSdkErr', 'creating new user failed');
          return $this->response->redirect();
        }

        
      }
      // User is logged in with a long-lived access token.  
      // You can redirect them to a members-only page.  
      // header('Location: https://example.com/members.php');
    }
    public function twitterTokenRemoverAction() {
      unset($_SESSION['oauth_token']);
      unset($_SESSION['oauth_token_secret']);
    }
    // TWITTER OAUTH
    public function twitterConnectAction() {
      // unset($_SESSION['oauth_token']);
      // unset($_SESSION['oauth_token_secret']);
      if (CONSUMER_KEY === '' || CONSUMER_SECRET === '') {
        $this->view->disable();
        echo "Oops!., You need a consumer key and secret";
      }

      if (isset( $_SESSION['oauth_token'])) {
        $request_token = [];
        $request_token['oauth_token'] = $_SESSION['oauth_token'];
        $request_token['oauth_token_secret'] = $_SESSION['oauth_token_secret'];
        $twitter_connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
        $access_token = $twitter_connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier'] ));
        $twitter_connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
        //$twitter_connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
        $_SESSION['access_token'] = $access_token;
        $content = $twitter_connection->get("account/verify_credentials");

        
        //$_SESSION['access_token'] = $access_token;
        $this->view->disable();
        var_dump($content);
        //$twitter_connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);


      } else {

        $twitter_connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
        $request_token = $twitter_connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));

        $this->view->disable();
        print_r($request_token);

        $_SESSION['oauth_token'] = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

        $url = $twitter_connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
        return $this->response->redirect($url);
      }

    }


}
