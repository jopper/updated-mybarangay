<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <title>MyBarangay | Venue for all of us to follow our hearts and come together to share, connect and feel at home. </title>

    <link href="comingsoon/css/bootstrap.css" rel="stylesheet" />
  <link href="comingsoon/css/coming-sssoon.css" rel="stylesheet" />    
    
    <!--     Fonts     -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>

    <?php 
      $message = "";
      if(isset($_POST['subscribeBtn'])) {
        if(!empty($_POST['email'])){
            try{
              $conn = new PDO('mysql:host=localhost;dbname=beta_subscription', 'root', 'MyBarangay012215'); //connect to database
              $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              //query to database
              $data = $conn->query('INSERT INTO subscribers (created, email) VALUES 
                          ( '.$conn->quote(date('Y-m-d H:i:s')).' , '.$conn->quote($_POST['email']).') ');

              if($data) {
                  $message = "Thanks. You are now subscribed.";
              }
            } catch(PDOException $e) {
                echo 'ERROR: ' . $e->getMessage();
            }
        }
      }
    ?>
  
</head>

<body>
<!-- <div id="fb-root"></div>
<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=10152791908818500&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script> -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=861054253944755&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- <nav class="navbar navbar-transparent navbar-fixed-top" role="navigation">
  <div class="container"> -->
    <!-- Brand and toggle get grouped for better mobile display -->
    <!-- <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div> -->

    <!-- Collect the nav links, forms, and other content for toggling -->
    <!-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> -->
      <!-- <ul class="nav navbar-nav">
         <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                <img src="comingsoon/images/flags/US.png"/>
                English(US) 
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#"><img src="comingsoon/images/flags/DE.png"/> Deutsch</a></li>
                <li><a href="#"><img src="comingsoon/images/flags/GB.png"/> English(UK)</a></li>
                <li><a href="#"><img src="comingsoon/images/flags/FR.png"/> Français</a></li>
                <li><a href="#"><img src="comingsoon/images/flags/RO.png"/> Română</a></li>
                <li><a href="#"><img src="comingsoon/images/flags/IT.png"/> Italiano</a></li>
                
                <li class="divider"></li>
                <li><a href="#"><img src="comingsoon/images/flags/ES.png"/> Español <span class="label label-default">soon</span></a></li>
                <li><a href="#"><img src="comingsoon/images/flags/BR.png"/> Português <span class="label label-default">soon</span></a></li>
                <li><a href="#"><img src="comingsoon/images/flags/JP.png"/> 日本語 <span class="label label-default">soon</span></a></li>
                <li><a href="#"><img src="comingsoon/images/flags/TR.png"/> Türkçe <span class="label label-default">soon</span></a></li>
             
              </ul>
        </li>

      </ul> -->
      <!-- <ul class="nav navbar-nav navbar-left">
            <li>
                
            </li>
             <li>
                <a href="#"> 
                    <i class="fa fa-twitter"></i>
                    Tweet
                </a>
            </li>
             <li>
                <a href="#"> 
                    <i class="fa fa-gittip"></i>
                    Gittip
                </a>
            </li>
       </ul> -->
      
    <!-- </div>
</nav> -->
<div class="main" style="background-image: url('comingsoon/images/banig.jpg')">

<!--    Change the image source '/images/restaurant.jpg')" with your favourite image.     -->
    
    <div class="cover black" data-color="black"></div>
     
<!--   You can change the black color for the filter with those colors: blue, green, red, orange       -->
    
    <div class="container">
        
        <div class="row">
        <div class="logo">
        
        <div class="panel col-md-10 col-md-offset-1 col-sm6-6 col-sm-offset-3">
    <img src="comingsoon/images/mybarangay_logo.png"  class="img-responsive">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-8">
          <h5 class="info-text" style="color: black;">
                         Your wait is over! www.MyBarangay.com will be open to the public during its official launch on June 13-14,2015 at the Waterfront Park, North Vancouver.
    </h5>

<h5 class="info-text" style="color: black;"> 
    Join us as we celebrate Philippine Days Festival and the launch of the global online community we’ve all been waiting for. Come and enjoy a day of festivities from 9am to 6pm. Feast your eyes on the richness of our culture, have a taste of delicious Pinoy delicacies, and catch up and have fun with our kababayans. 
</h5> 


 
<h5 class="info-text" style="color: black;">
    We look forward to seeing all our friends there. Visit us again at www.mybarangay.com starting June 13, 2015. Maraming salamat po.
</h5>

<h5 class="info-text" style="color: black;">
    For more details on Philippine Days Festival, check out <a target="_blank" href="http://www.philippinedasysfestival.com"><small style="color: #00005C;">www.philippinedasysfestival.com</small></a> 
                </h5> 
        </div>
        <div class="col-md-4">
                  <div class="fb-like-box" data-href="https://www.facebook.com/pages/MyBarangay/605719569528125" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
        </div>
      </div>
    </div>
        
          <!-- <div class="fb-like" data-href="http://mybarangay.cloudapp.net" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div> -->

          
        </div>
        </div>
        </div>
        <!-- <h1 class="logo cursive">
            MyBarangay
        </h1> -->
<!--  H1 can have 2 designs: "logo" and "logo cursive"           -->
        
        <div class="content">
           <!--  <h5 class="motto">Isang Mundo. Isang Wika. Isang Barangay.</h5> -->
            <!-- <h4 class="motto">Soon...</h4> -->
            <div class="subscribe">
            <?php if(!empty($message)) { ?>
              <div class="row">
                <div class="col-md-4 col-md-offset-4">
                  <div class="alert alert-dismissible alert-success text-center">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p><?php echo $message; ?></p>
                  </div>
                </div>
              </div>
            <?php } ?>


                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm6-6 col-sm-offset-3 ">
                        <form action="index.php" method="post" class="form-inline" role="form">
                          <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                            <input type="email" name="email" class="form-control transparent" placeholder="Your email here..." required>
                          </div>
                          <button type="submit" name="subscribeBtn" class="btn btn-primary btn-fill">Notify Me</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
      <div class="container">
            
      </div>
    </div>
 </div>

</body>
    <script src="comingsoon/js/jquery-1.10.2.js" type="text/javascript"></script>
  <script src="comingsoon/js/bootstrap.min.js" type="text/javascript"></script>
  
</html>
