<?php

class MemberSubscriptions extends \Phalcon\Mvc\Model {
	
	/*
	 *
	 * @var integer 
	 */
	public $id;

	/*
	 *
	 * @var string 
	 */
	public $created;

	/*
	 *
	 * @var string
	 */
	public $modified;

	/*
	 *
	 * @var integer 
	 */
	public $member_id;

	/*
	 *
	 * @var integer 
	 */
	public $subscription;

	/*
	 *
	 * @var integer 
	 */
	public $start_date;

	/*
	 *
	 * @var integer 
	 */
	public $end_date;
	/*
	 * @var integer 
	 */
	public $for_renew;

	// relationship with other table(s) foreign_key, model, primary_key
	public function initialize() {
		$this->belongsTo("member_id", "Members", "id");
	}

	/**
     * Independent Column Mapping.
     */
	public function columnMap() {
		return array(
            'id' => 'id', 
            'created' => 'created', 
            'modified' => 'modified', 
            'member_id' => 'member_id',
            'subscription' => 'subscription',
            'start_date' => 'start_date',
            'end_date' => 'end_date',
            'for_renew' => 'for_renew'
        );
	}
}