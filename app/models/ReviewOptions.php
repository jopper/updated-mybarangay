<?php

class ReviewOptions extends \Phalcon\Mvc\Model
{

    public $id;
    public $modified;
    public $created;
    public $member_id;
    public $business_id;
    public $caters;
    public $wifi;
    public $hastv;
    public $pricerange;
    public $creditcard;
    public $parking;
    public $bike_parking;
    public $attire;
    public $groups;
    public $kids;
    public $reservations;
    public $delivery;
    public $takeaway;
    public $waiter;
    public $outdoor;
    public $goodfor;
    public $alcohol;
    public $noise;
    public $ambience;

    public function initialize()
    {
         $this->belongsTo("business_id", "Business", "id");
         $this->belongsTo("member_id", "Members", "id");
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'created' => 'created', 
            'modified' => 'modified', 
            'member_id' => 'member_id', 
            'business_id' => 'business_id', 
            'caters' => 'caters',
            'wifi' => 'wifi',
            'hastv' => 'hastv',
            'pricerange' => 'pricerange',
            'creditcard' => 'creditcard',
            'parking' => 'parking',
            'bike_parking' => 'bike_parking',
            'attire' => 'attire',
            'groups' => 'groups',
            'kids' => 'kids',
            'reservations' => 'reservations',
            'delivery' => 'delivery',
            'takeaway' => 'takeaway',
            'waiter' => 'waiter',
            'outdoor' => 'outdoor',
            'goodfor' => 'goodfor',
            'alcohol' => 'alcohol',
            'noise' => 'noise',
            'ambience' => 'ambience'
        ); 
    }

}
