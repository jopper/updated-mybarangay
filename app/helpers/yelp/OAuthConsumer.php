<?php 
namespace Helpers\YelpApi;
use Helpers\YelpApi\OAuthException;
use Helpers\YelpApi\OAuthToken;
use Helpers\YelpApi\OAuthSignatureMethod;
use Helpers\YelpApi\OAuthSignatureMethod_HMAC_SHA1;
use Helpers\YelpApi\OAuthSignatureMethod_PLAINTEXT;
use Helpers\YelpApi\OAuthSignatureMethod_RSA_SHA1;
use Helpers\YelpApi\OAuthRequest;
use Helpers\YelpApi\OAuthServer;
use Helpers\YelpApi\OAuthDataStore;
use Helpers\YelpApi\OAuthUtil;

class OAuthConsumer {
  public $key;
  public $secret;
  function __construct($key, $secret, $callback_url=NULL) {
    $this->key = $key;
    $this->secret = $secret;
    $this->callback_url = $callback_url;
  }
  function __toString() {
    return "OAuthConsumer[key=$this->key,secret=$this->secret]";
  }
}

?>