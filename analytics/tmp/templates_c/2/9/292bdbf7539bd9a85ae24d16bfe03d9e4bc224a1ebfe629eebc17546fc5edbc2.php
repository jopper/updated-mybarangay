<?php

/* @Insights/moversAndShakersOverviewWidget.twig */
class __TwigTemplate_292bdbf7539bd9a85ae24d16bfe03d9e4bc224a1ebfe629eebc17546fc5edbc2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["reports"]) ? $context["reports"] : $this->getContext($context, "reports")), "getRowsCount", array())) {
            // line 2
            echo "    ";
            $context["allMetadata"] = $this->getAttribute($this->getAttribute((isset($context["reports"]) ? $context["reports"] : $this->getContext($context, "reports")), "getFirstRow", array()), "getAllTableMetadata", array());
            // line 3
            echo "
    ";
            // line 4
            $context["consideredGrowth"] = call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Insights_TitleConsideredMoversAndShakersGrowth", $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "metricName", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "lastTotalValue", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "totalValue", array()), call_user_func_array($this->env->getFilter('prettyDate')->getCallable(), array($this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "lastDate", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "period", array()))), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "evolutionTotal", array())));
            // line 5
            echo "    ";
            $context["consideredChanges"] = call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Insights_TitleConsideredMoversAndShakersChanges", $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "minGrowthPercentPositive", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "minGrowthPercentNegative", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "minNewPercent", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "minIncreaseNew", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "minDisappearedPercent", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "minDecreaseDisappeared", array()), $this->getAttribute((isset($context["allMetadata"]) ? $context["allMetadata"] : $this->getContext($context, "allMetadata")), "totalValue", array())));
        } else {
            // line 7
            echo "    ";
            $context["allMetadata"] = array();
            // line 8
            echo "
    ";
            // line 9
            $context["consideredGrowth"] = "";
            // line 10
            echo "    ";
            $context["consideredChanges"] = "";
        }
        // line 12
        echo "
";
        // line 13
        $this->loadTemplate("@Insights/overviewWidget.twig", "@Insights/moversAndShakersOverviewWidget.twig", 13)->display($context);
    }

    public function getTemplateName()
    {
        return "@Insights/moversAndShakersOverviewWidget.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 13,  45 => 12,  41 => 10,  39 => 9,  36 => 8,  33 => 7,  29 => 5,  27 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
