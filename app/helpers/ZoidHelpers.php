<?php

//namespace Helpers;
use Phalcon\Tag;
class ZoidHelpers extends Tag {


     /**
      * $category = array('0' => 'Monthly', '1' => 'Daily','2' => 'Piece-rate');
      * ZoidHelpers::StaticDropDown('add_category', $category, $x['Category']);
      * 
      * [StaticDropDown description]
      * @param [type] $name  [description]
      * @param [type] $array [description]
      * @param [type] $v     [description]
      */
    static public function staticDropDown($name, $array , $vk = null, $default = ' -- select -- '){
        echo "<select id=\"$name\" name=\"$name\" class=\" form-control\"   >";
        echo  "<option value=\"\">$default</option>";
        foreach ($array as $k => $v) {
                echo '<option value="'.$k.'"';
                if(trim($k) == $vk){echo ' selected';}
                echo '>'. $v . '</option>';
        }
        echo "</select>";
     }

   /**
     * [DynamicDropDown description]
     * @param [type] $zoid [description]
     * @param [type] $name [description]
     * @param string $opt  [description]
     * @param [type] $code [description]
     * @param [type] $desc [description]
     */
    static public function dynamicDropDown($zoid, $name, $opt = '', $default = ' -- select -- '){
        echo "<select id=\"$name\" name=\"$name\" class=\" form-control\" >";
        echo  "<option value=\"\">$default</option>";
        foreach ($zoid as $condition) {
            if ($condition->id == $opt) {
                echo '<option value="'.$condition->id.'" selected>'.$condition->name.'</option>';
            } else {
                echo '<option value="'.$condition->id.'">'.$condition->name.'</option>';
            }
        }
        echo '</select>';
    }


    /**
     * [dateYearDropDown description]
     * @param  integer $year_limit [description]
     * @return [type]              [description]
     */
   static public function dateYearDropDown($year_limit = 0){
        $html = '<div id="date_select" >'."\n";
        $html .= '<label for="date_day">Date of birth:</label>'."\n";
        /*days*/
        $html .= '<select name="date_day" id="day_select" class="form-control">'."\n";
            for ($day = 1; $day <= 31; $day++) {
                $html .= '               <option>' . $day . '</option>'."\n";
            }
        $html .= '</select>'."\n";
        /*months*/
        $html .= '<select name="date_month" id="month_select" class="form-control">'."\n";
        $months = array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            for ($month = 1; $month <= 12; $month++) {
                $html .= '               <option value="' . $month . '">' . $months[$month] . '</option>'."\n";
            }
        $html .= '</select>'."\n";
        /*years*/
        $html .= '<select name="date_year" id="year_select" class="form-control">'."\n";
            for ($year = 1900; $year <= (date("Y") - $year_limit); $year++) {
                $html .= '               <option>' . $year . '</option>'."\n";
            }
        $html .= '</select>'."\n";
        $html .= '</div>'."\n";
        return $html;
    }






}


?>