<?php

class ReviewCompliments extends \Phalcon\Mvc\Model
{

    public $id;

    public $created;

    public $modified;

    public $member_id;

    public $rate;

    public $review_id;

    public $business_id;

    public $reviewer_id;

    public function initialize()
    {
         $this->belongsTo("review_id", "Reviews", "id");
         $this->belongsTo("member_id", "Members", "id");
         $this->belongsTo("business_id", "Business", "id");
         
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'created' => 'created',
            'modified' => 'modified',
            'member_id' => 'member_id',
            'rate' => 'rate',
            'review_id' => 'review_id',
            'business_id' => 'business_id',
            'reviewer_id' => 'reviewer_id'
        );
    }

}
