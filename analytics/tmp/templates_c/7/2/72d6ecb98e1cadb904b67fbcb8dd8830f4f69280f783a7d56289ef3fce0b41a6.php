<?php

/* @UserCountry/index.twig */
class __TwigTemplate_72d6ecb98e1cadb904b67fbcb8dd8830f4f69280f783a7d56289ef3fce0b41a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">

    <div class=\"col-md-6\">
        ";
        // line 4
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), array("Template.leftColumnUserCountry"));
        echo "

        <h2 piwik-enriched-headline>";
        // line 6
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UserCountry_Continent")), "html", null, true);
        echo "</h2>
        ";
        // line 7
        echo (isset($context["dataTableContinent"]) ? $context["dataTableContinent"] : $this->getContext($context, "dataTableContinent"));
        echo "

        <div class=\"sparkline\">
            ";
        // line 10
        echo call_user_func_array($this->env->getFunction('sparkline')->getCallable(), array((isset($context["urlSparklineCountries"]) ? $context["urlSparklineCountries"] : $this->getContext($context, "urlSparklineCountries"))));
        echo "
            ";
        // line 11
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UserCountry_DistinctCountries", (("<strong>" . (isset($context["numberDistinctCountries"]) ? $context["numberDistinctCountries"] : $this->getContext($context, "numberDistinctCountries"))) . "</strong>")));
        echo "
        </div>
        <div style=\"clear:left\"></div>

        ";
        // line 15
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), array("Template.footerUserCountry"));
        echo "
    </div>

    <div class=\"col-md-6\">
        <h2 piwik-enriched-headline>";
        // line 19
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UserCountry_Country")), "html", null, true);
        echo "</h2>
        ";
        // line 20
        echo (isset($context["dataTableCountry"]) ? $context["dataTableCountry"] : $this->getContext($context, "dataTableCountry"));
        echo "

        <h2 piwik-enriched-headline>";
        // line 22
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UserCountry_Region")), "html", null, true);
        echo "</h2>
        ";
        // line 23
        echo (isset($context["dataTableRegion"]) ? $context["dataTableRegion"] : $this->getContext($context, "dataTableRegion"));
        echo "

        <h2 piwik-enriched-headline>";
        // line 25
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UserCountry_City")), "html", null, true);
        echo "</h2>
        ";
        // line 26
        echo (isset($context["dataTableCity"]) ? $context["dataTableCity"] : $this->getContext($context, "dataTableCity"));
        echo "
    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "@UserCountry/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 26,  75 => 25,  70 => 23,  66 => 22,  61 => 20,  57 => 19,  50 => 15,  43 => 11,  39 => 10,  33 => 7,  29 => 6,  24 => 4,  19 => 1,);
    }
}
