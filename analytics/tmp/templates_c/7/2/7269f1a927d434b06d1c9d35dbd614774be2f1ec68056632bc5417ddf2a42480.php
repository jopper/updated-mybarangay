<?php

/* @Events/index.twig */
class __TwigTemplate_7269f1a927d434b06d1c9d35dbd614774be2f1ec68056632bc5417ddf2a42480 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["leftMenuReports"]) ? $context["leftMenuReports"] : $this->getContext($context, "leftMenuReports"));
        echo "

";
    }

    public function getTemplateName()
    {
        return "@Events/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
