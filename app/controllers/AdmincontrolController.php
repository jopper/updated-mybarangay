<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset; # used for raw mysql that phql can't handle||recognize
class AdmincontrolController extends ControllerBase
{

    # used for ajax request 
    private $payload;
    private $status;
    private $headers;
    private $description;
    private $contentType;
    private $content;
    private $ajaxResponse;

    private function securityGuard($login = null) {    
        $userSession = $this->session->get('userSession');
        if ($userSession == false) {
            if ($login != null) {
                return true;
            } else {
                return $this->response->redirect($this->request->getHTTPReferer());
            }
        }
        
        $checkMember = false;
        $member = Members::findFirstById("id='".$userSession['id']."'");
        if ($member->admin_access == '1') {
            $checkMember = false;
        } else {
            $checkMember = true;
        }
        
        if ($checkMember == false) {
            return $this->response->redirect($this->request->getHTTPReferer());
        } else {
            if ($login != null)  {
                if ($userSession == true) {
                    return $this->response->redirect('admincontrol/');
                }
            }
            return true;
        }   
    }

    # not working right now
    private function ajaxRequestHandler($arr = null) {
        
        is_null($arr) AND false;

        $this->payload = $arr;
        $this->status = 200;
        $this->description = 'OK';
        $this->headers = array();
        $this->contentType = 'application/json';
        $this->content = json_encode(array("data" => $this->payload));

        $this->ajaxResponse = new \Phalcon\Http\Response();
        $this->ajaxResponse->setStatusCode($this->status, $this->description);
        $this->ajaxResponse->setContentType($this->contentType, 'UTF-8');
        $this->ajaxResponse->setContent($this->content);

        # Set the additional headers
        foreach ($headers as $key => $value) {
           $this->ajaxResponse->setHeader($key, $value);
        }
        
        $this->view->disable();
        return $this->ajaxResponse;
    }

    private function returnOrigin() {
        return $this->response->redirect($this->request->getHTTPReferer());
    }

	public function indexAction() {
        $this->securityGuard();
		$countUsers = Members::count();
		$countReviews = Reviews::count();
		$countFlags = Flags::count();
		$countBusiness = Business::count();
		$this->view->setVars(['numMember' => $countUsers, 'numReview' => $countReviews, 
			'numFlag' => $countFlags, 'numBiz' => $countBusiness]);
		$this->view->setTemplateAfter('admin-panel');
	}

    public function loginAction() {
        $this->securityGuard('1');
        $userSession = $this->session->get('userSession');

        $this->view->setTemplateAfter('admin-panel2');

        if ($this->request->isPost()) {
            $oldInputs = array();
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');

            if (empty($email) || empty($password)) {
                $this->session->set('errorMessage', 'empty-field');
                return $this->response->redirect('admincontrol/login');
            }

            $member = Members::findFirstByEmail($email);

            if ($member == true && $this->security->checkHash($password, $member->password)) {
                $emailConfirmed = EmailConfirmations::findFirst(array('columns'    => '*', 
                                         'conditions' => 'user_id = ?1 AND email=?2 AND confirmed = ?3', 
                                         'bind' => array(1 => $member->id, 2 => $email, 3 => 'Y')));

                if (!$emailConfirmed) {
                    $this->session->set('errorMessage', 'not-confirmed');
                    $oldInputEmail = $email;
                    $this->session->set('oldInputEmail', $oldInputEmail);
                    return $this->response->redirect('admincontrol/login');
                }


                $userSession = get_object_vars($member);
                $userSession['type'] = 'Member';
                $userSession['admin'] = 1;
                $profilePic = MemberPhotos::findFirst(array('member_id="'.$userSession['id'].'"', 'primary_pic="Yes"'));
                error_log('WWWTTTTFFFF : '.$profilePic->file_path.$profilePic->filename );
                $userSession['primary_pic'] = $profilePic->file_path.$profilePic->filename;
                $this->session->set('userSession', $userSession);
                //member id
                $cookie_name = "mid";
                $cookie_value = $userSession['id'];
                $date_of_expiry = time() + 60 * 60 * 24 * 90;
                setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
                //email 
                $cookie_name = "e";
                $cookie_value = $userSession['email'];
                setcookie($cookie_name, $this->encrypt($cookie_value), $date_of_expiry, "/");
                //cookie token
                $cookie_name = "token";
                $cookie_token = substr(md5(uniqid(rand(), true)), 0, 20);
                setcookie($cookie_name, $this->encrypt($cookie_token), $date_of_expiry, "/"); 
                $member->modified = date('Y-m-d H:i:s');
                $member->cookie_token = $this->security->hash($cookie_token);
                if($member->update()){
                    //$this->response->redirect('member/page/'.$userSession['id']);
                    $checkAdminAccess = Admins::findFirst("member_id='".$userSession['id']."' AND grant='1'");
                    if ($checkAdminAccess == true) {
                        return $this->response->redirect('admincontrol/');
                    } else {
                        return $this->response->redirect('member/page/'. $userSession['id']);
                    }
                }

            } else {
                $this->session->set('errorMessage', 'error');
                $oldInputEmail = $email;
                $this->session->set('oldInputEmail', $oldInputEmail);
                return $this->response->redirect('admincontrol/login');
            }
        }
    }

    public function memberlistAction() {
        $this->securityGuard();
        $countUsers = Members::count();
        $countReviews = Reviews::count();
        $countFlags = Flags::count();
        $countBusiness = Business::count();
        $this->view->setVars(['numMember' => $countUsers, 'numReview' => $countReviews, 
            'numFlag' => $countFlags, 'numBiz' => $countBusiness]);

        $this->view->setTemplateAfter('admin-panel');
    }

    public function bizcategoriesAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }

    public function stripeAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }
    
    public function classifiedlistAction() {
        $this->securityGuard();
        $autos = Automotives::find();
        $jobs = Jobs::find();
        $realestates = Realties::find();
        $things = Things::find();

        $this->view->setVars([
            'autos' => $autos,
            'jobs'  => $jobs,
            'realties' => $realestates,
            'things' => $things
        ]);
        $this->view->setTemplateAfter('admin-panel');
    }

    public function bizownerlistAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }
	public function claimbizreqAction() {
        $this->securityGuard();
		$this->view->setTemplateAfter('admin-panel');
		$claimRequests = ClaimRequests::find();
        $this->view->setVar('requests', $claimRequests);
	}

    public function analyticsAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }
    
    public function getUserCarsDataAction() {
        if ($this->request->isAjax()) {
            $userCarArr = array();
            $memberId = $this->request->getPost('member_id');
            
            if ($getUserCar = Automotives::find("member_id='".$memberId."'")) {
                foreach ($autos as $auto) {
                    $autoLink = $this->url->getBaseUri() . 'car_and_truck/view2/' . $auto->id;
                    $userCarArr[] = [
                        $auto->created,
                        $auto->name,
                        $auto->price,
                        $auto->model,
                        $auto->contact_no,
                        $auto->email,
                        "<a><button class='btn btn-xs btn-danger'>View</button></a>"
                    ];
                }
                
                $this->ajaxRequestHandler($userCarArr);

            }

        }
    }

	public function getuserdataAction() {
		if ($this->request->isAjax()) {
            $userArr = array();
            $members = Members::find("type='Member'");

            foreach ($members as $member) {
                $emailLink = $this->url->getBaseUri(). 'admincontrol/emailbizowner/'.$member->id.'/2';
            	$viewLink = $this->url->getBaseUri() . 'member/page/'.$member->id;
                $memberLink = $this->url->getBaseUri() . 'admincontrol/memberpage/'.$member->id;
                $userArr[] = [$member->first_name,$member->last_name,
                $member->street,$member->city,
                $member->country_id,$member->email,
                "<a href='".$memberLink."'><button class='btn btn-xs btn-primary'>View</button></a></span>"
                ];
                #"<a href='#'><i class='icon-eye'></i></a>"];
            }

            $payload     = $userArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("data" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
	}


    public function getbizcategoriesAction() {
        if ($this->request->isAjax()) {
            $categoryArr = array();
            $query = "SELECT DISTINCT `business_categories`.`id`,`business_categories`.`main_title`,
                        `business_categories`.`title`,
                        `business_categories`.`sub_title`,
                        `business_categories`.`unique_word`
                        FROM `business_categories`
                        INNER JOIN `business_category_lists`
                        ON `business_categories`.`id`=`business_category_lists`.`business_category_id`";
            $params = null;
            $categories = new BusinessCategories();
            $result_set = new Resultset(null, $categories, $categories->getReadConnection()->query($query, $params)); 
            
            foreach ($result_set as $result) {
                $bizIds = BusinessCategoryLists::find("business_category_id='".$result->id."'");
                $bizCounter = 0;
                foreach ($bizIds as $bizId) {
                    $checkBusiness = Business::count("id='".$bizId->business_id."'");
                    if ($checkBusiness > 0) {
                        // do mothing
                    } else {
                        $bizCounter += 1;
                    }
                }
                $countBiz = BusinessCategoryLists::count("business_category_id='".$result->id."'");
                $viewLink = $this->url->getBaseUri() . 'admincontrol/bizlistwcategory/'.$result->id;
                $categoryArr[] = [                  
                    $result->main_title,
                    $result->title,
                    $result->sub_title,
                    $result->unique_word,
                    "<a href='".$viewLink."'><button class='btn btn-xs btn-primary'>View Business (".$countBiz.")</button></a>",
                    $bizCounter
                
                ];
            }



            $payload     = $categoryArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("data" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function getbizuserdataAction() {
        if ($this->request->isAjax()) {
            $userArr = array();
            $members = Members::find("type='Business'");

            foreach ($members as $member) {
                $subscription = null;
                $creditDay = null;
                $endDate = null;
                $checkSubscription = MemberSubscriptions::count("member_id='".$member->id."'");
                if ($checkSubscription > 0) {
                    $getSubscription = MemberSubscriptions::findFirst("member_id='".$member->id."'");
                    if ($getSubscription->subscription == '1') {
                        $subscription = 'Basic';
                    } else if ($getSubscription->subscription == '2') {
                        $subscription = 'Enhanced';
                    } else if ($getSubscription->subscription == '3') {
                        $subscription = 'Premium';
                    } else {
                        $subscription = 'Unknown';
                    }
                    $query = "SELECT `end_date`,DATEDIFF(`end_date`,NOW()) AS diffdays FROM `member_subscriptions` WHERE `member_id`='".$member->id."'";
                    $subscriber = new MemberSubscriptions();
                    $result_set = new Resultset(null, $subscriber, $subscriber->getReadConnection()->query($query, $params)); 
                    
                        foreach ($result_set as $result) {
                            $creditDay = ($result->diffdays > 0) ? $result->diffdays . ' days' : 'expired';
                            $endDate = $result->end_date;
                         }
                    //$creditDay = "n/a";
                    //$endDate = "n/a";
                } else {
                    $subscription = "unsubscribed";
                    $creditDay = "n/a";
                    $endDate = "n/a";
                }
                $viewLink = $this->url->getBaseUri() . 'admincontrol/user_profile/'.$member->id;
                $emailLink = $this->url->getBaseUri() . 'admincontrol/emailbizowner/'.$member->id.'/1';
                $memberLink = $this->url->getBaseUri() . 'admincontrol/bizownerpage/'.$member->id;
                $userArr[] = [
                $member->first_name,$member->last_name,
                $member->email,
                $subscription,
                $creditDay,
                $endDate,
                //"<a href='".$viewLink."'><i class='icon-eye'></i></a>"];
                "<span><a href='".$emailLink."'><button class='btn btn-xs btn-primary'>Email</button></a><a href='".$memberLink."'><button class='btn btn-xs btn-primary'>View</button></a></span>"];
            }

            $payload     = $userArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("data" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    /* 
     * email business owner 
     * @param int member_id
     */    
    public function emailbizownerAction($memberId = null, $type = null) { 
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
        if ($memberId == null) {    
            return $this->response->redirect('admincontrol/index');
        } else {
            is_null($type) AND $this->response->redirect($this->request->getHTTPReferer());
            if ($type == 1) {
               $countBiz = Members::count("id='".$memberId."' AND type='Business'");
                if ($countBiz <= 0) {
                    return $this->response->redirect('admincontrol/index');
                } else {
                    $biz = Members::findFirst("id='".$memberId."' AND type='Business'");
                    $this->view->setVars(array('biz' => $biz, 'type' => 1, 'referer' => $this->request->getHTTPReferer()));

                } 
            } else if ($type == 2) {
               $countBiz = Members::count("id='".$memberId."' AND type='Member'");
                if ($countBiz <= 0) {
                    return $this->response->redirect('admincontrol/index');
                } else {
                    $biz = Members::findFirst("id='".$memberId."' AND type='Member'");
                    $this->view->setVars(array('biz' => $biz, 'type' => 2, 'referer' => $this->request->getHTTPReferer()));

                } 
            }
            
        }
    }

    public function grantAdminAccessAction() {
        if ($this->request->isAjax()) {
             $arr = array();
            $memberId = $this->request->getPost("member_id");
            $updateMember = Members::findFirstById($memberId);
            if ($updateMember == true) {
                if ($updateMember->admin_access == '1') {
                    $how = "0";
                    $updateMember->admin_access = '0';
                } else {
                    $how = "1";
                    $updateMember->admin_access = '1';
                }

                if ($updateMember->update()) {
                    $arr[] = ["status"=>"ok", "how" => $how];
                } else {
                    $arr[] = ["status"=>"failed"];
                }
            } else {
                $arr[] = ["status"=>"failed"];
            }

           
           
            $payload     = $arr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($payload);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }
    public function adminmvlgliAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }

    public function bizlistAction() {
        $this->securityGuard();
        $business = Business::find();
        $countBiz = Business::count();
        $this->view->setTemplateAfter('admin-panel');
        $this->view->setVars(['business' => $business, 'totalBiz' => $countBiz]);
    }

    public function bizlistwcategoryAction($businessCategoryId = null) {
        $this->securityGuard();
        $query = "SELECT * FROM `business` WHERE EXISTS (SELECT * FROM `business_category_lists` WHERE `business_category_lists`.`business_id`=`business`.`id` AND `business_category_lists`.`business_category_id`='".$businessCategoryId."')";
        $params = null;
        $categories = new BusinessCategories();
        $result_set = new Resultset(null, $categories, $categories->getReadConnection()->query($query, $params)); 
        $this->view->setTemplateAfter('admin-panel');
        $this->view->setVar('business', $result_set);
    }

    public function subscribersAction() {
        $this->securityGuard();
        $subscribers = MemberSubscriptions::find();
        $this->view->setVar('subscribers', $subscribers);
        $this->view->setTemplateAfter('admin-panel');
    }

    public function bizownerpageAction($memberId = null) {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');

        is_null($memberId) AND $this->response->redirect($this->request->getHTTPReferer()); 

        $member = Members::findFirstById($memberId);
        if ( $member == false ) {
            return $this->response->redirect($this->request->getHTTPReferer());
        }
        // if ($member->type = 'Member') {
        //     return $this->response->redirect($this->url->getBaseUri() . 'admincontrol/memberpage/' . $member->id);
        // }
        $hasBiz = (ClaimRequests::findFirst("member_id='".$member->id."' AND status='approved'")) ? 1 : 0;
        $hasSubscription = (MemberSubscriptions::findFirst("member_id='".$member->id."'")) ? 1 : 0;
        $this->view->setVars(array(
            'hasBiz' => $hasBiz,
            'hasSubscription' => $hasSubscription,
            'member' => $member
        ));

    }

    public function memberpageAction($memberId = null) {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
        
        is_null($memberId) AND $this->response->redirect("admincontrol/memberlist");

        $member = Members::findFirstById($memberId);
        // if ($member->type = 'Business') {
        //     return $this->response->redirect($this->url->getBaseUri() . 'admincontrol/bizownerpage/' . $member->id);
        // }
        $hasAuto = (Automotives::find("member_id='".$member->id."'")) ? 1 : 0;
        $hasRealty = (Realties::find("member_id='".$member->id."'")) ? 1 : 0;
        $hasJob = (Jobs::find("member_id='".$member->id."'")) ? 1 : 0;
        $hasThing = (Things::find("member_id='".$member->id."'")) ? 1 : 0;
        $hasEvents = (Events::find("member_id='".$member->id."'")) ? 1 : 0;
        $hasTopics = (WhatsupTopics::find("member_id='".$member->id."'")) ? 1 : 0;

        $this->view->setVars([
            'member' => $member,
            'hasAuto' => $hasAuto,
            'hasThing' => $hasThing,
            'hasJob' => $hasJob,
            'hasRealty' => $hasRealty,
            'hasEvents' => $hasEvents,
            'hasTopics' => $hasTopics
        ]);
    }

    public function paymentlogsAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
        
        $paymentLogs = PaymentLogs::find();

        $this->view->setVar('paymentLogs', $paymentLogs);
    }
    
	/*
	 * AJAX REQUESTS
	 *
	 */
	public function getbiz_flag_dataAction() {
		if ($this->request->isAjax()) {
            $reqArr = array();
			$flagTypeId = $this->request->getQuery("flag_type_id");
            $requests = Flags::find("flag_type_id'".$flagTypeId."' AND location='business'");
			
            foreach ($requests as $req) {
            	$reviewId = $req->review_id;
            	$reporter = $req->members->first_name.' '.$req->members->last_name;
				$target = Members::findFirst("id='".$req->target_id."'");
            	$targetOne = $target->first_name.' '.$target->last_name;
            	$location = $req->page;
				if ($req->status == 0) {
					$getStatus = 'pending';
				} else if ($req->status == 1) {
					$getStatus = 'approved';
				} else if ($req->status == 2) {
					$getStatus = 'rejected';
				}
            	$status = $getStatus;
            	$actionLink = '<a href="#" class="btn btn-square btn-success btn-xs"><i class="icon-checkmark-2"></i>
						                	</a> | <a href="#" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i>
						                	</a>';
				$reportDate = $req->created;
				$getReview = Reviews::findFirst("id='".$flag->review_id."'");
				//$dateOfPost = Reviews::findFirst("id='".$reviewId."'");
				$postDate = $getReview->created;
				$getBusiness = Business::findFirst("id='".$getReview->business_id."'");
				$businessName = $getBusiness->name;
				// reportedby, fullname, business name, location(page), date of report, date of post, status , action
                $reqArr[] = [$reporter, $targetOne, $businessName, $location, $req->created, $postDate, $status, $actionLink];
            }
	
			$payload     = $reqArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("data" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);
			
            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }
			//$this->view->disable();
            $this->view->disable();
            return $response;
        }
	}
	public function get_flag_type_detailAction() {
		if ($this->request->isAjax()) {
            $reqArr = array();
			$flagTypeId = $this->request->getQuery("flag_type_id");
            $request = FlagTypes::findFirst('id = "'.$flagTypeId.'"');
			
            $reqArr[] = $request->detail;

            $payload     = $reqArr; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode(array("type_name" => $payload));

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
	}
	// public function getclaimrequestAction() {
	// 	if ($this->request->isAjax()) {
 //            $reqArr = array();
 //            $requests = ClaimRequests::find('status = "pending"');

 //            foreach ($requests as $req) {
 //            	$bizId = $req->business->id;
 //            	$bizName = $req->business->name;
 //            	$fullName = $req->members->first_name.' '.$req->members->last_name;
 //            	$email = $req->members->email;
 //            	$status = $req->status;
 //            	$contact = $req->members->telephone;
 //            	$actionLink = '<a href="#" class="btn btn-square btn-success btn-xs"><i class="icon-checkmark-2"></i>
	// 					                	</a> | <a href="#" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i>
	// 					                	</a>';
 //                $reqArr[] = [$fullName, $bizName, $email, $contact, $req->created,
 //                $status, $actionLink];
 //            }

 //            $payload     = $reqArr; 
 //            $status      = 200;
 //            $description = 'OK';
 //            $headers     = array();
 //            $contentType = 'application/json';
 //            $content     = json_encode(array("data" => $payload));

 //            $response = new \Phalcon\Http\Response();

 //            $response->setStatusCode($status, $description);
 //            $response->setContentType($contentType, 'UTF-8');
 //            $response->setContent($content);

 //            // Set the additional headers
 //            foreach ($headers as $key => $value) {
 //               $response->setHeader($key, $value);
 //            }

 //            $this->view->disable();

 //            return $response;
 //        }
	// }

	public function user_profileAction($id = null) {
        $this->securityGuard();
		$this->view->setTemplateAfter('admin-panel');
	}
	
	/*
	 * FLAGS
	 */
	public function reviewflagAction() {
        $this->securityGuard();
		$this->view->setTemplateAfter('admin-panel');
	}

    public function classifiedflagAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }
	
    public function jobflagAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }

    public function thingflagAction() {
        $this->securityGuard();
        $this->view->setTemplateAfter('admin-panel');
    }

    public function approvethingflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Things::findFirst($reviewId);
            $review->status = 1;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '1';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function approvejobflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Jobs::findFirst($reviewId);
            $review->status = 1;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '1';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function approveautoflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Automotives::findFirst($reviewId);
            $review->status = 1;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '1';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

	public function approveflagAction($reviewId = null) {
		if ($this->request->isAjax()) {
			$arrResult = array();
				$reviewId = $this->request->getQuery('review_id');
				$flagId = $this->request->getQuery('flag_id');
			$review = Reviews::findFirst($reviewId);
			$review->visibility = 1;
			$actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
			if ($review->update()) {
				$result = $review->visibility;
			} else {
				$result = "failed";
			}
			$flag = Flags::findFirst($flagId);
			$flag->status = '1';
			if ($flag->update()) {
				$result2 = $flag->status;
			} else {
				$result2 = "failed";
			}
			$newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
			$arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
			
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
		}
	}
	
    public function rejectthingflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Things::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function rejectjobflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Jobs::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

    public function rejectautoflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Automotives::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }
    }

	public function rejectflagAction($reviewId = null) {
		if ($this->request->isAjax()) {
			$arrResult = array();
				$reviewId = $this->request->getQuery('review_id');
				$flagId = $this->request->getQuery('flag_id');
			$review = Reviews::findFirst($reviewId);
			$review->visibility = 0;
			$actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="cancel('.$reviewId.','.$flagId.')"><i class="icon-close"></i></span>';
			if ($review->update()) {
				$result = $review->visibility;
			} else {
				$result = "failed";
			}
			$flag = Flags::findFirst($flagId);
			$flag->status = '2';
			if ($flag->update()) {
				$result2 = $flag->status;
			} else {
				$result2 = "failed";
			}
			$newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
			$arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
			
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
		}
	}
	
    public function cancelthingflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Things::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }   
    }

    public function canceljobflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Jobs::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }   
    }

    public function cancelautoflagAction($reviewId = null) {
        if ($this->request->isAjax()) {
            $arrResult = array();
                $reviewId = $this->request->getQuery('review_id');
                $flagId = $this->request->getQuery('flag_id');
            $review = Automotives::findFirst($reviewId);
            $review->status = 0;
            $actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
            if ($review->update()) {
                $result = $review->status;
            } else {
                $result = "failed";
            }
            $flag = Flags::findFirst($flagId);
            $flag->status = '2';
            if ($flag->update()) {
                $result2 = $flag->status;
            } else {
                $result2 = "failed";
            }
            $newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
            $arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }   
    }

	public function cancelflagAction($reviewId = null) {
		if ($this->request->isAjax()) {
			$arrResult = array();
				$reviewId = $this->request->getQuery('review_id');
				$flagId = $this->request->getQuery('flag_id');
			$review = Reviews::findFirst($reviewId);
			$review->visibility = 0;
			$actionButton = '<span class="btn btn-square btn-success btn-xs" onclick="approve('.$reviewId.','.$flagId.')"><i class="icon-checkmark-2"></i></span> | <a href="#" onclick="reject('.$reviewId.','.$flagId.')" class="btn btn-square btn-danger btn-xs"><i class="icon-close"></i></a>';
			if ($review->update()) {
				$result = $review->visibility;
			} else {
				$result = "failed";
			}
			$flag = Flags::findFirst($flagId);
			$flag->status = '2';
			if ($flag->update()) {
				$result2 = $flag->status;
			} else {
				$result2 = "failed";
			}
			$newPieChart = '<div id="pie-div" style="width:100%;height:300px;" data-legend="true" data-content="[[\'Pending\', 10],[\'Rejected\', 45], [\'Approved\', 45]]" data-colors=\'[ "#b94a48","#feb847","#464646"]\'></div>';
            
			$arrResult[] = ['result' => $result, 'chart' => $newPieChart, 'result2' => $result2, 'actionbtn' => $actionButton];
            
			
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
		}	
	}


    public function postemailbizownerAction() {
        $this->securityGuard();
        if ($this->request->isPost()) {
            $this->view->disable();

            $email = $this->request->getPost('biz-email');
            $bizId = $this->request->getPost('biz-id');
            $emailContent = $this->request->getPost('email-content');
            $fullName = $this->request->getPost('fullname');

            $this->getDI()->getMail()->send(
                        array($email => $fullName),
                        'Message from MyBarangay Admin',
                        'email_biz_owner',
                        array('emailContent' => $emailContent, 'fullName' => $fullName)
                    );

            if ($this->request->getPost('type-id') == 1) {
                return $this->response->redirect('admincontrol/bizownerlist');
            } else if ($this->request->getPost('type-id') == 2) {
                return $this->response->redirect('admincontrol/memberlist');
            }

            // echo "business owner account id: ".  $this->request->getPost('biz-id') . '<br>';
            // echo "email content: " . $this->request->getPost('email-content');

        } else {
            return $this->response->redirect('admincontrol/index');
        }
    }

    public function ajaxpostemailbizownerAction() {
        //$this->securityGuard();
        if ($this->request->isAjax()) {
            $arrResult = array();

            $email = $this->request->getQuery('biz-email');
             $fullName = $this->request->getQuery('fullname');
             $emailContent = $this->request->getQuery('email-content');
            if ($email == null || $email == '') {
                $emailContent = 'empty email';
                $email = '03152013f@gmail.com';
            }

            if ($fullName == null || $fullName == '') {
                $emailContent = 'empty email';
                $email = '03152013f@gmail.com';
            }
            
           

            $this->getDI()->getMail()->send(
                        array($email => $fullName),
                        'Message from MyBarangay Admin',
                        'email_biz_owner',
                        array('emailContent' => $emailContent, 'fullName' => $fullName)
                    );

            $arrResult[] = ["status" => "ok"];
            
            
            $payload     = $arrResult; 
            $status      = 200;
            $description = 'OK';
            $headers     = array();
            $contentType = 'application/json';
            $content     = json_encode($arrResult);

            $response = new \Phalcon\Http\Response();

            $response->setStatusCode($status, $description);
            $response->setContentType($contentType, 'UTF-8');
            $response->setContent($content);

            // Set the additional headers
            foreach ($headers as $key => $value) {
               $response->setHeader($key, $value);
            }

            $this->view->disable();

            return $response;
        }   else {
            return $this->response->redirect('admincontrol/index');
        }
    }


    /*
     * DELETING DATA 
     */

    /* 
     * delete_classified
     * @param int $classifiedId , int $type (1 => carsntrucks, 2 => job, 3 => realty, 4 => thing)
     */
    public function delete_classifiedAction($classifiedId = null, $type = null) {

        is_null($classifiedId) AND $this->response->redirect($this->request->getHTTPReferer());
        is_null($type) AND $this->response->redirect($this->request->getHTTPReferer());

        if ($type == 1) {
            $classified = Automotives::findFirstById("id='".$classifiedId."'");
            if ($classified == true) {
                $this->view->disable();

                $countPhotos = AutomotivePhotos::count("automotive_id='".$classified->id."'");
                if ($countPhotos > 0) {
                    $photos = AutomotivePhotos::find("automotive_id='".$classified->id."'");
                    foreach ($photos as $photo) {
                        $trashImg = 'img/auto/'.$photo->filename;
                        unlink($trashImg);
                    }
                }

                if ($classified->delete()) {
                    $this->session->set('deleteClassifiedSuccess', 'cars and trucks data was deleted');
                    $this->returnOrigin();
                }
            } else {
                $this->session->set('deleteClassifiedErr', 'Oops! something went wrong with fetching automotive data');
                $this->returnOrigin();
            }
        } else if ($type == 2 ) {
            $classified = Jobs::findFirstById($classifiedId);
            if ($classified) {
                $this->view->disable();
                $countPhotos = JobLogos::count("job_id='".$classified->id."'");
                if ($countPhotos > 0) {
                    $photos = JobLogos::find("job_id='".$classified->id."'");
                    foreach ($photos as $photo) {
                        $trashImg = 'img/jobs/'.$photo->filename;
                        unlink($trashImg);
                    }
                }

                if ($classified->delete()) {
                    $this->session->set('deleteClassifiedSuccess', 'job post data was deleted');
                    $this->returnOrigin();
                }
            } else {
                $this->session->set('deleteClassifiedErr', 'Oops! something went wrong with fetching job data');
                $this->returnOrigin();
            }
        } else if ($type == 3) {
            $classified = Realties::findFirstById($classifiedId);
            if ($classified) {
                $this->view->disable();
                $countPhotos = RealtyPhotos::count("realty_id='".$classified->id."'");
                if ($countPhotos > 0) {
                    $photos = RealtyPhotos::find("realty_id='".$classified->id."'");
                    foreach ($photos as $photo) {
                        $trashImg = 'img/realties/'.$photo->filename;
                        unlink($trashImg);
                    }
                }

                if ($classified->delete()) {
                    $this->session->set('deleteClassifiedSuccess', 'realestate data was deleted');
                    $this->returnOrigin();
                }
            } else {
                $this->session->set('deleteClassifiedErr', 'Oops! something went wrong with fetching realestate data');
                $this->returnOrigin();
            }
        } else if ($type == 4) {
            $classified = Things::findFirstById($classifiedId);
            if ($classified == true) {
                $this->view->disable();

                $countPhotos = ThingPhotos::count("thing_id='".$classified->id."'");
                if ($countPhotos > 0) {
                    $photos = ThingPhotos::find("thing_id='".$classified->id."'");
                    foreach ($photos as $photo) {
                        $trashImg = 'img/thing/'.$photo->filename;
                        unlink($trashImg);
                    }
                }

                if ($classified->delete()) {
                    $this->session->set('deleteClassifiedSuccess', 'item was deleted');
                    $this->returnOrigin();
                }

            } else {
                $this->session->set('deleteClassifiedErr', 'Oops! something went wrong with fetching thing data');
                $this->returnOrigin();
            }
        }


    }
}