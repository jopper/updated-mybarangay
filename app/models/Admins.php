<?php

class Admins extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

     /**
     *
     * @var integer
     */
    public $reg_id;

     /**
     *
     * @var integer
     */
    public $member_id;

     /**
     *
     * @var integer
     */
    public $grant;

    public function initialize() {
        //$this->belongsTo('reg_id', 'Members', 'id');
        $this->belongsTo('member_id', 'Members', 'id');
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'created' => 'created', 
            'modified' => 'modified',
            'reg_id' => 'reg_id',
            'member_id' => 'member_id',
            'grant' => 'grant'
        );
    }

}
