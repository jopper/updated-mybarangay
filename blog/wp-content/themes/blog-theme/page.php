<?php get_header(); ?>     
        <main role="main" class="main">
          <div class="page-home">
            <div class="container">
              <div class="row">
                <div class="col-sm-8">
                  <div class="page-content">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                       <?php the_content(); ?>
                    <?php endwhile; else : ?>
                      <h1><?php _e( 'Sorry, no posts matched your criteria.' ); ?></h1>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-sm-4">
                  <?php get_sidebar(); ?>
                </div>
              </div>
            </div>
          </div>
        </main>

<?php get_footer(); ?>